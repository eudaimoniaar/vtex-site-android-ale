package com.fizzmod.vtex.config;

import com.fizzmod.vtex.models.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Config extends GeantConfig {

    private static Config config = null;

    public static Config getInstance() {
        if (config == null)
            config = new Config();

        return config;
    }

    private Config() {
        super();
        setContactURL("https://www.geant.com.uy/institucional/contacto");
        setTermsUrl("https://www.geant.com.uy/institucional/terminos-y-condiciones");
        setDealsCollection("?fq=H:499");
        setBestSellingCollection("?fq=H:498");
        setOurBrandsCollection("?fq=H:622");

        setDealsQACollection("?fq=H:499");
        setBestSellingQACollection("?fq=H:498");
        setOurBrandsQACollection("?fq=H:622");
    }

    @Override
    public ArrayList<Category> getFilteredCategories(ArrayList<Category> categories) {
        if (categories == null)
            return null;

        List<Integer> allowedCategories = Arrays.asList(
                14, 342, 34, 320, 1174, 1138, 412, 350, 539, 476, 344, 42, 44, 206, 352, 307
        );

        ArrayList<Category> filteredCategories = new ArrayList<>();
        for (int i = 0; i < allowedCategories.size(); i++) {
            for (Category category : categories) {
                if (category.getId().equals(allowedCategories.get(i))) {
                    filteredCategories.add(category);
                }
            }
        }

        return filteredCategories;
    }

    @Override
    public String getDevFlavorName() {
        return "geantDev";
    }
}
