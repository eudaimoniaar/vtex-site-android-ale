package com.fizzmod.vtex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.UserListAdapterListener;

public class ShoppingListAdapter extends BaseShoppingListAdapter {

    public ShoppingListAdapter(Context context, UserListAdapterListener listener) {
        super(context, listener);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(context)
                .inflate(R.layout.shopping_list_item, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends BaseShoppingListAdapter.ViewHolder {

        private TextView txtListQuantity;

        ViewHolder(final View itemView){
            super(itemView);

            txtListQuantity = (TextView) itemView.findViewById(R.id.shopping_list_item_quantity);
        }

        @Override
        public void loadData(int position) {
            super.loadData(position);

            int shoppingListSize = shoppingList.getSkusList().size();
            txtListQuantity.setText(
                    context.getResources().getQuantityString(R.plurals.text_list_item_quantity, shoppingListSize, shoppingListSize));
        }

    }
}
