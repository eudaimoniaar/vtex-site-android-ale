package com.fizzmod.vtex.adapters.viewholders;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.utils.Utils;

public class DrawerAdapterViewHolder extends BaseDrawerAdapterViewHolder {

    private LinearLayout highlight;

    public DrawerAdapterViewHolder(View convertView) {
        super(convertView);

        highlight = (LinearLayout) convertView.findViewById(R.id.selectedDrawerHighlight);
    }

    @Override
    public void setView(DrawerItem item) {
        super.setView(item);

        highlight.setVisibility(item.enabled ? View.VISIBLE : View.INVISIBLE);
        itemName.setTypeface(null, Typeface.BOLD);
    }

}
