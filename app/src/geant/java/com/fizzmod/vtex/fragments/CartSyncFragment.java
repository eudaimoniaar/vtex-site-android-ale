package com.fizzmod.vtex.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.CartSnapshot;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.service.response.BaseResponse;
import com.fizzmod.vtex.service.response.CartSnapshotResponse;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CartSyncAlertDialog;
import com.fizzmod.vtex.views.CartSyncView;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CartSyncFragment extends BackHandledFragment implements View.OnClickListener {

    private boolean loading;

    public static CartSyncFragment newInstance() {
        return new CartSyncFragment();
    }

    private View cartSyncMessageView;
    private TextView cartSyncMessageTextView;
    private CartSnapshot cartSnapshot;
    private CartSyncView cartSyncSendView;
    private CartSyncView cartSyncReceiveView;
    private CartSyncAlertDialog confirmSendDialog;
    private CartSyncAlertDialog confirmReceiveDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart_sync, container, false);

        cartSyncMessageView = view.findViewById(R.id.fragment_cart_sync_message);
        cartSyncMessageTextView = (TextView) view.findViewById(R.id.fragment_cart_sync_message_text);

        view.findViewById(R.id.fragment_cart_sync_arrow).setOnClickListener(this);

        cartSyncSendView = (CartSyncView) view.findViewById(R.id.fragment_cart_sync_send);
        cartSyncSendView.setOnClickListener(this);

        cartSyncReceiveView = (CartSyncView) view.findViewById(R.id.fragment_cart_sync_receive);
        cartSyncReceiveView.setOnClickListener(this);

        confirmSendDialog = new CartSyncAlertDialog(getActivity());
        confirmSendDialog.setTxtCancel(R.string.cancel);
        confirmSendDialog.setTxtAccept(R.string.accept, this);

        confirmReceiveDialog = new CartSyncAlertDialog(getActivity());

        fetchCartSnapshot();

        return view;
    }

    @Override
    public boolean onBackPressed() {
        return loading;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fragment_cart_sync_arrow:
                getActivity().onBackPressed();
                break;

            case R.id.fragment_cart_sync_send:
                confirmSendDialog.show();
                break;

            case R.id.fragment_cart_sync_receive:
                confirmReceiveDialog.show();
                break;

            case R.id.cart_sync_accept:
                if (confirmSendDialog.isShowing()) {
                    confirmSendDialog.dismiss();
                    showLoader();
                    if (cartSyncReceiveView.getVisibility() == VISIBLE)
                        removeAndSendSnapshot();
                    else
                        sendSnapshot();
                } else if (confirmReceiveDialog.isShowing()) {
                    confirmReceiveDialog.dismiss();
                    showLoader();
                    receiveSnapshot(cartSyncSendView.getVisibility() == VISIBLE);
                }
                break;

            case R.id.cart_sync_cancel:
                if (confirmReceiveDialog.isShowing()) {
                    confirmReceiveDialog.dismiss();
                    showLoader();
                    receiveSnapshot(false);
                }
                break;

            case R.id.fragment_cart_sync_empty_button:
                mListener.closeFragment();
                break;
        }
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    /*******************
     * Private methods *
     *******************/

    private void showLoader() {
        loading = true;
        mListener.onLoadingStart();
    }

    private void hideLoader() {
        loading = false;
        mListener.onLoadingStop();
    }

    private void handleError(final String errorMessage) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    hideLoader();
                }
            });
        }
    }

    private void fetchCartSnapshot() {

        fetchCartSnapshot(new Callback<CartSnapshotResponse>(getActivity()) {
            @Override
            public void onResponse(CartSnapshotResponse response) {
                cartSnapshot = response.getSnapshot();
                setReceiveView();
                onEnd();
                hideLoader();
            }

            @Override
            public void onError(String errorMessage) {
                super.onError(errorMessage);
                onEnd();
            }

            private void onEnd() {
                setSendView();
                if (cartSyncSendView.getVisibility() == VISIBLE || cartSyncReceiveView.getVisibility() == VISIBLE)
                    setDialogViews();
                else
                    setEmptyCartView();
            }

            @Override
            protected void retry() {
                fetchCartSnapshot(this);
            }
        });
    }

    private void fetchCartSnapshot(Callback<CartSnapshotResponse> callback) {
        showLoader();
        JanisService.getInstance(getActivity()).getCartSnapshot(callback);
    }

    private void setEmptyCartView() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getView().findViewById(R.id.fragment_cart_sync_empty_text).setVisibility(VISIBLE);
                    View noCartsButton = getView().findViewById(R.id.fragment_cart_sync_empty_button);
                    noCartsButton.setVisibility(VISIBLE);
                    noCartsButton.setOnClickListener(CartSyncFragment.this);
                }
            });
        }
    }

    private void setDialogViews() {

        int sendMessageResId = -1;
        int sendImageResId = -1;
        int receiveMessageResId = -1;
        int receiveAcceptResId = -1;
        int receiveImageResId = -1;
        boolean sendViewVisible = cartSyncSendView.getVisibility() == VISIBLE;
        boolean receiveViewVisible = cartSyncReceiveView.getVisibility() == VISIBLE;

        if (receiveViewVisible && sendViewVisible) {
            sendMessageResId = R.string.send_cart_existing_confirm_message;
            receiveMessageResId = R.string.receive_cart_existing_confirm_message;
            receiveAcceptResId = R.string.replace;
            sendImageResId = R.drawable.icn_send_cart_on_hold;
            receiveImageResId = R.drawable.icn_receive_full_cart;
            confirmReceiveDialog.setTxtCancel(R.string.unify, this);
        } else if (receiveViewVisible) {
            receiveMessageResId = R.string.receive_cart_confirm_message;
            receiveAcceptResId = R.string.accept;
            receiveImageResId = R.drawable.icn_receive;
            confirmReceiveDialog.setTxtCancel(R.string.cancel);
        } else {
            sendImageResId = R.drawable.icn_send_cart;
            sendMessageResId = R.string.send_cart_confirm_message;
        }

        if (sendMessageResId != -1)
            confirmSendDialog.setTxtMessage(sendMessageResId);

        if (receiveMessageResId != -1)
            confirmReceiveDialog.setTxtMessage(receiveMessageResId);

        if (sendImageResId != -1)
            confirmSendDialog.setImage(sendImageResId);

        if (receiveImageResId != -1)
            confirmReceiveDialog.setImage(receiveImageResId);

        if (receiveAcceptResId != -1)
            confirmReceiveDialog.setTxtAccept(receiveAcceptResId, this);

    }

    private void setReceiveView() {
        int productsQuantity = cartSnapshot.getProductsQuantity();
        if (productsQuantity == 0)
            return;

        cartSyncReceiveView.setProductCount(productsQuantity);
        cartSyncReceiveView.setTitle(R.string.receive_cart_title);
        cartSyncReceiveView.setIcon(R.drawable.icon_arrow_receive);
        cartSyncReceiveView.setVisibility(VISIBLE);
        cartSyncReceiveView.setDate(cartSnapshot.getDateCreated());
    }

    private void setSendView() {
        int totalItems = 0;
        for (Map.Entry<String, Sku> entry : Cart.getInstance().getItems().entrySet())
            totalItems += entry.getValue().getSelectedQuantity();

        if (totalItems == 0)
            return;

        cartSyncSendView.setProductCount(totalItems);
        cartSyncSendView.setTitle(R.string.send_cart_title);
        cartSyncSendView.setDate(new Date());
        cartSyncSendView.setVisibility(VISIBLE);
    }

    private void removeAndSendSnapshot() {
       removeAndSendSnapshot(new Callback<BaseResponse>(getActivity()) {
           @Override
           public void onResponse(BaseResponse response) {
               sendSnapshot();
           }

           @Override
           protected void retry() {
               removeAndSendSnapshot(this);
           }
       });
    }

    private void removeAndSendSnapshot(Callback<BaseResponse> callback) {
        JanisService.getInstance(getActivity()).removeCartSnapshot(callback);
    }

    private void sendSnapshot() {
        sendSnapshot(new Callback<BaseResponse>(getActivity()) {
            @Override
            public void onResponse(BaseResponse response) {
                cartSyncSendView.setTitle(R.string.send_cart_ok_title);
                cartSyncSendView.showSent();
                cartSyncSendView.setIcon(R.drawable.icon_check_circle_off);
                cartSyncSendView.setOnClickListener(null);
                hideLoader();
                if (cartSyncReceiveView.getVisibility() == VISIBLE)
                    cartSyncReceiveView.setVisibility(GONE);
                showMessage(R.string.cart_sync_sent);
            }

            @Override
            protected void retry() {
                sendSnapshot(this);
            }
        });
    }

    private void sendSnapshot(Callback<BaseResponse> callback) {
        JanisService.getInstance(getActivity()).createCartSnapshot(new CartSnapshot(Cart.getInstance()), callback);
    }

    private void receiveSnapshot(final boolean replaceSnapshot) {
        API.getProductsBySku(getActivity(), cartSnapshot.getSkuList(), new ApiCallback<List<Product>>() {
            @Override
            public void onResponse(final List<Product> products) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (replaceSnapshot)
                            Cart.getInstance().emptyCart(getActivity());

                        for (Product product : products) {
                            Sku sku = product.getSku(0);
                            Cart.getInstance().addItemQuantity( sku, getActivity(), cartSnapshot.getSkuQuantity( sku.getId() ) );
                        }

                        setSendView();

                        cartSyncReceiveView.setTitle(R.string.receive_cart_ok_title);
                        cartSyncReceiveView.showSent();
                        cartSyncReceiveView.setIcon(R.drawable.icon_check_circle_off);
                        cartSyncReceiveView.setOnClickListener(null);
                        showMessage(R.string.cart_sync_received);
                        hideLoader();
                        mListener.onReorder();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                handleError(errorMessage);
            }

            @Override
            public void onUnauthorized() {
                // Nothing to do
            }
        });
    }

    // TODO: Extract to Utils?
    private void showMessage(int textResId) {
        cartSyncMessageTextView.setText(textResId);
        cartSyncMessageView.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_IN_LEFT_TO_RIGHT,
                        cartSyncMessageView,
                        300
                )
        );
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (cartSyncMessageView == null)
                    return;
                cartSyncMessageView.startAnimation(
                        Utils.getAnimation(
                                Utils.ANIMATION_OUT_LEFT_TO_RIGHT,
                                cartSyncMessageView,
                                300
                        )
                );
            }
        }, 2500);
    }

    /********************
     * Private callback *
     ********************/

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        public void onError(String errorMessage) {
            handleError(errorMessage);
        }

        @Override
        protected void requestSignIn() {
            mListener.signOut();
            mListener.requestSignIn(Main.FRAGMENT_TAG_CART_SYNC);
        }
    }
}
