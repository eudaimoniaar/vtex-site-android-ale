package com.fizzmod.vtex.fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Sku;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductPage extends BaseProductPage {

    private TextView buyButtonText;
    private TextView productPrice;
    private TextView productListPrice;
    private LinearLayout buyButton;
    private ImageButton buyIcon;

    public ProductPage(){
        super();
    }

    public static ProductPage newInstance(String type, String value) {
        ProductPage fragment = new ProductPage();

        if(type != null && value != null) {
            Bundle args = new Bundle();
            args.putString(type, value);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void setViews(final View view) {
        super.setViews(view);
        productPrice = view.findViewById(R.id.productPrice);
        productListPrice = view.findViewById(R.id.productListPrice);
        productListPrice.setPaintFlags(productListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        buyButton = view.findViewById(R.id.buyButton);
        buyIcon = view.findViewById(R.id.buy);
        view.findViewById(R.id.product_page_footer_shadow).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        buyButtonText = view.findViewById(R.id.buyText);
        return view;
    }

    @Override
    public void expandFooter() {
        super.expandFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_big_white);
        buyButtonText.setVisibility(GONE);
    }

    @Override
    public void collapseFooter() {
        super.collapseFooter();
        buyIcon.setImageResource(R.drawable.icn_cart_small_white);
        buyButtonText.setVisibility(VISIBLE);
    }

    @Override
    public void changeSku(Sku sku, boolean info) {
        super.changeSku(sku, info);
        if (sku != null) {
            if (sku.hasStock()) {
                sku.getBestPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        productPrice.setText(price);
                    }
                });

                if (sku.showListPrice()) {
                    sku.getListPriceFormatted(new TypedCallback<String>() {
                        @Override
                        public void run(String price) {
                            productListPrice.setText(price);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void setClickListeners(final View view) {
        super.setClickListeners(view);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy(v);
            }
        });
    }
}
