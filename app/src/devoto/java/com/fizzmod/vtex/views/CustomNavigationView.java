package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.fragments.CardRequest;
import com.fizzmod.vtex.fragments.ShoppingListsFragment;
import com.fizzmod.vtex.models.DrawerItem;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CARD_REQUEST;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_COUPONS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

// TODO: Unify in geant_commons
public class CustomNavigationView extends BaseCustomNavigationView {

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        signOut = findViewById(R.id.logout);
        signIn = findViewById(R.id.login);
        setClickListeners();
    }

    @Override
    public void selectItem(String fragmentTag) {
        switch (fragmentTag) {
            case FRAGMENT_TAG_LISTS :
                selectItem(fragmentTag, ShoppingListsFragment.class);
                break;
            case FRAGMENT_TAG_CARD_REQUEST :
                selectItem(fragmentTag, CardRequest.class);
                break;
            default:
                super.selectItem(fragmentTag);
        }
    }

    @Override
    protected void addMenuItems() {
        menuItems.add(new DrawerItem(R.drawable.icn_home_on, R.drawable.icn_home_on,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories_on, R.drawable.icn_categories_on, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_fauvorite_on, R.drawable.icn_fauvorite_on, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_list_on, R.drawable.icn_list_on, R.string.drawerMenuItemLists, FRAGMENT_TAG_LISTS, false));
        menuItems.add(new DrawerItem(R.drawable.icn_stores_on, R.drawable.icn_stores_on, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_terms_on, R.drawable.icn_terms_on, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, false));
        menuItems.add(new DrawerItem(R.drawable.icn_contact_on, R.drawable.icn_contact_on, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, false));
        menuItems.add(new DrawerItem(R.drawable.icn_myorders_on, R.drawable.icn_myorders_on, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, false));
        menuItems.add(new DrawerItem(R.drawable.icn_discount_menu, R.drawable.icn_discount_menu, R.string.drawerMenuItemCoupons, FRAGMENT_TAG_COUPONS, false));
        menuItems.add(new DrawerItem(R.drawable.icn_payment_card, R.drawable.icn_payment_card, R.string.drawerMenuItemCardRequest, FRAGMENT_TAG_CARD_REQUEST, false));
    }

    @Override
    protected void rearrangeMenuItems() {
        List<DrawerItem> rearrangedMenuItems = new ArrayList<>();

        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_HOME));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CATEGORIES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_FAVOURITES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_LISTS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_STORES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_TERMS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CONTACT));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_ORDERS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_COUPONS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CARD_REQUEST));

        menuItems = rearrangedMenuItems;
    }

}
