package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;

import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Sku;

public class MinicartHighlightAdapterViewholder extends BaseMinicartAdapterViewHolder {

    MinicartHighlightAdapterViewholder(ViewGroup parent, Callback callback) {
        super(parent, callback);
    }

    @Override
    public void setView(Sku sku) {
        super.setView(sku);

    }
}
