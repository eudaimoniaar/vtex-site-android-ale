package com.fizzmod.vtex.config;

import com.fizzmod.vtex.models.Store;

public class Config extends GeantConfig {

    private static Config config = null;

    public static Config getInstance() {
        if (config == null)
            config = new Config();
        return config;
    }

    private Config() {
        super();
        setHasMultipleSalesChannels(true);

        Store store = new Store();
        store.setId("0");
        store.setName("Montevideo");
        store.setEcommerce(true);
        store.setSalesChannel("3");
        salesChannelStores.add(store);

        Store store2 = new Store();
        store2.setId("1");
        store2.setName("Maldonado");
        store2.setEcommerce(true);
        store2.setSalesChannel("6");
        salesChannelStores.add(store2);
    }

    @Override
    public String getDevFlavorName() {
        return "devotoDev";
    }
}
