package com.fizzmod.vtex.config;

import java.util.Locale;

public class Config extends com.fizzmod.vtex.config.BaseConfig {

    private static Config config = null;

    public static Config getInstance() {
        if (config == null)
            config = new Config();

        return config;
    }

    private Config() {
        super();
        setLocale(new Locale("es", "UY"));
        setLatitude(-34.883156);
        setLongitude(-56.169256);
        setContactURL("http://institucional.tata.com.uy/contacto/");
        setTermsUrl("");
        setDealsQACollection("?fq=H:137");
        setDealsCollection("?fq=H:137");
        setBestSellingCollection("?fq=H:138");
        setBestSellingQACollection("?fq=H:138");
        setLocale(new Locale("es", "UY"));
    }

    @Override
    public String formatPrice(double price){
        return super.formatPrice(price).replace("$", "$U");
    }

    @Override
    public void baseUrlChanged(String newBaseUrl) {
        setTermsUrl(newBaseUrl + "/terminos-y-condiciones-webview");
    }

    @Override
    public String getDevFlavorName() {
        return "tataDev";
    }
}
