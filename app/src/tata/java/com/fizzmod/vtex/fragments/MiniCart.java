package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class MiniCart extends BaseMiniCart {

    private View headerView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        headerView = view.findViewById(R.id.minicart_header);
    }

    @Override
    protected void updateNonEmptyMinicart(AppCompatActivity context, RecyclerView minicartWrapper) {
        super.updateNonEmptyMinicart(context, minicartWrapper);
        Utils.fadeIn(headerView);
    }

    @Override
    protected void updateEmptyMinicart(AppCompatActivity context, RecyclerView minicartWrapper) {
        super.updateEmptyMinicart(context, minicartWrapper);
        Utils.fadeOut(headerView);
    }
}
