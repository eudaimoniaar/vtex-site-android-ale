package com.fizzmod.vtex.activities;

import android.support.v7.app.AppCompatActivity;

/**
 * This activity is disabled on this flavors.
 * */
public class CartSyncSummaryActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 123;

}
