package com.fizzmod.vtex.utils;

import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;

import com.fizzmod.vtex.R;

public class ScrollHandler extends BaseScrollHandler {

    private Integer prevScrollY;

    public ScrollHandler(ScrollListener scrollListener) {
        super(scrollListener);
    }

    @Override
    public void addScrollListener(final View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if(scrollY > oldScrollY) {
                        scrollListener.onScrollUp();
                    }
                }
            });
        } else {
            if (view.getTag(R.id.TAG_SCROLL_LISTENER) == null){
                ViewTreeObserver.OnScrollChangedListener onScrollChangedListener = new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {

                        int scrollY = view.getScrollY();
                        if (prevScrollY != null) {
                            if (prevScrollY < scrollY)
                                scrollListener.onScrollUp();
                        }
                        prevScrollY = scrollY;
                    }
                };

                view.getViewTreeObserver().addOnScrollChangedListener(onScrollChangedListener);
                view.setTag(R.id.TAG_SCROLL_LISTENER, onScrollChangedListener);
            }
        }
    }
}
