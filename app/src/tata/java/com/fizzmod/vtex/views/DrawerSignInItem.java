package com.fizzmod.vtex.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class DrawerSignInItem extends LinearLayout{

    private TextView logoutIcon;
    private TextView textViewLabel;
    private NavigationViewListener navigationViewListener;

    public DrawerSignInItem(Context context) {
        this(context, null);
    }

    public DrawerSignInItem(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawerSignInItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.drawer_sign_in_item, this);
        logoutIcon = (TextView) findViewById(R.id.textViewLogout);
        textViewLabel = (TextView) findViewById(R.id.textViewName);
        textViewLabel.setTextColor(Color.BLACK);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                navigationViewListener.onSignInLayoutClicked();
            }
        });
    }


    private void updateWithCurrentStatus(String userEmail) {
        if (!Utils.isEmpty(userEmail)) {
            textViewLabel.setText(userEmail);
            textViewLabel.setTypeface(null, Typeface.NORMAL);
        } else {
            textViewLabel.setText(R.string.drawerMenuItemSignIn);
            textViewLabel.setTypeface(null, Typeface.BOLD);
        }

        logoutIcon.setVisibility(Utils.isEmpty(userEmail) ? GONE : VISIBLE);
    }

    public void setEmail(String email) {
        updateWithCurrentStatus(email);
    }

    public void setNoUser() {
        updateWithCurrentStatus(null);
    }

    public interface NavigationViewListener{
        void onSignInLayoutClicked();
    }

    public void setNavigationViewListener(NavigationViewListener navigationViewListener) {
        this.navigationViewListener = navigationViewListener;
    }
}
