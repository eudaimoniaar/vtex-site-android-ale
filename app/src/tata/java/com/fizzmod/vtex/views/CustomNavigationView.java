package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.models.User;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_SCANNER;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public class CustomNavigationView extends BaseCustomNavigationView implements DrawerSignInItem.NavigationViewListener{

    private DrawerSignInItem drawerSignInItem;

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        drawerSignInItem = (DrawerSignInItem) findViewById(R.id.drawerSignInItem);
        drawerSignInItem.setNavigationViewListener(this);
        String userEmail = User.getInstance(context).getEmail();
        if (userEmail != null && !userEmail.equals(""))
            drawerSignInItem.setEmail(userEmail);
        else
            drawerSignInItem.setNoUser();
    }

    @Override
    public void selectItem(String fragmentTag) {

        switch (fragmentTag) {
            case FRAGMENT_TAG_STORES:
                selectItem(fragmentTag, com.fizzmod.vtex.fragments.Stores.class);
                break;
            case FRAGMENT_TAG_SCANNER:
                closeNavigationDrawer();
                checkDrawerItem(previousItemPosition);
                navigationViewListener.startScanner();
                break;
            default:
                super.selectItem(fragmentTag);
                return;
        }
    }

    @Override
    protected void addMenuItems() {
        menuItems.add(new DrawerItem(R.drawable.icn_home_on, R.drawable.icn_home_off,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories_on, R.drawable.icn_categories_off, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_favorites_on, R.drawable.icn_favorites_off, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_stores, R.drawable.icn_stores, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, true));
        menuItems.add(new DrawerItem(R.drawable.icn_terms, R.drawable.icn_terms, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_contact, R.drawable.icn_contact, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, true));
        menuItems.add(new DrawerItem(R.drawable.icn_my_orders, R.drawable.icn_my_orders, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_scanner_on, R.drawable.icn_scanner_off, R.string.drawerMenuItemScanner, FRAGMENT_TAG_SCANNER, false));
    }

    @Override
    protected void rearrangeMenuItems() {
        List<DrawerItem> rearrangedMenuItems = new ArrayList<>();

        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_HOME));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CATEGORIES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_SCANNER));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_FAVOURITES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_STORES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_TERMS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CONTACT));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_ORDERS));

        menuItems = rearrangedMenuItems;
    }

    @Override
    public void onSignOut() {
        drawerSignInItem.setNoUser();
    }

    @Override
    public void setUserEmail(String email) {
        drawerSignInItem.setEmail(User.getInstance(getContext()).getEmail());
    }

    @Override
    public void onSignInLayoutClicked() {
        closeNavigationDrawer();
        if(!User.isLogged(getContext()))
            navigationViewListener.onSignInClicked();
        else
            navigationViewListener.onSignOutClicked();
    }
}
