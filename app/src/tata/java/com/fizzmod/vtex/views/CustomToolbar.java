package com.fizzmod.vtex.views;

import android.animation.LayoutTransition;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class CustomToolbar extends BaseCustomToolbar {

    private EditText searchInput;
    private LinearLayout searchBar;
    private boolean showingSearchBar = false;
    private Animation slideUpAnimation, slideDownAnimation;
    private boolean animatingSearchbar = false;

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        scannerIcon.setVisibility(GONE);
        searchIcon.setVisibility(VISIBLE);
        searchInput = (EditText) findViewById(R.id.searchInput);
        searchInput.setCompoundDrawables(null, null, null, null);
        searchBar = (LinearLayout) findViewById(R.id.searchBar);

        // This complements xml attribute animateLayoutChanges="true" and makes the animation for the search bar in TATA work properly
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ((ViewGroup) findViewById(R.id.toolbar)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }

        slideUpAnimation = AnimationUtils.loadAnimation(getContext().getApplicationContext(),
                R.anim.slide_up_animation);

        slideUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                animatingSearchbar = true;
                searchBar.setVisibility(GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animatingSearchbar = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        slideDownAnimation = AnimationUtils.loadAnimation(getContext().getApplicationContext(),
                R.anim.slide_down_animation);

        slideDownAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                animatingSearchbar = true;
                searchBar.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animatingSearchbar = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        searchIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSearchBar();
            }
        });

        searchInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    toolbarListener.performFullTextQuery(v.getText().toString());
                    searchInput.setText("");
                    return true;
                }
                return false;
            }
        });
    }

    private void toggleSearchBar() {

        if (!animatingSearchbar) {
            showingSearchBar = !showingSearchBar;

            if (showingSearchBar) {
                searchBar.startAnimation(slideDownAnimation);
                searchInput.requestFocus();
                toolbarListener.showKeyboard(searchInput);
            } else {
                searchBar.startAnimation(slideUpAnimation);
                toolbarListener.dismissKeyboardFromToolbar();
            }
        }
    }

    @Override
    public void onScrollUp() {
        if (showingSearchBar) {
            toggleSearchBar();
        }
    }

    @Override
    public void loadHomeLayout() {
        // Nothing to do
    }
}
