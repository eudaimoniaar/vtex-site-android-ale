package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.FrameLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;

public class DrawerAdapterViewHolder extends BaseDrawerAdapterViewHolder {

    private FrameLayout indicator;

    public DrawerAdapterViewHolder(View convertView) {
        super(convertView);
        indicator = (FrameLayout) convertView.findViewById(R.id.selectedIndicator);
    }

    @Override
    public void setView(DrawerItem item) {
        super.setView(item);
        if (!item.isSubitem)
            indicator.setVisibility(item.enabled ? View.VISIBLE : View.INVISIBLE);

    }
}
