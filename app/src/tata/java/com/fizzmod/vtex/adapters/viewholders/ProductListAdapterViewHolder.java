package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.ImageView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;

public class ProductListAdapterViewHolder extends BaseProductListAdapterViewHolder {

    private ImageView menuButton;

    public ProductListAdapterViewHolder(View convertView, ProductListCallback clickListener) {
        super(convertView, clickListener);
        itemView.findViewById(R.id.product_item_menu_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentInteractionListener.onProductOptionsClicked((Product) itemView.getTag());
            }
        });
        menuButton = (ImageView) itemView.findViewById(R.id.product_item_menu_button);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        itemView.setBackgroundResource(isRelatedItemsList || isOrderPage ? R.drawable.radius_white : R.drawable.radius_white_grey_border);
        super.setView(position, product, isOrderPage, isRelatedItemsList);
        menuButton.setVisibility(isOrderPage ? View.GONE : View.VISIBLE);
        if(isOrderPage)
            priceHighlight.setVisibility(View.GONE);
    }
}
