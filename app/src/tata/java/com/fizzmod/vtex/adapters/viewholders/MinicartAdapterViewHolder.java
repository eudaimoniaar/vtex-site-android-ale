package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;

import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Sku;

public class MinicartAdapterViewHolder extends BaseMinicartAdapterViewHolder {

    public MinicartAdapterViewHolder(ViewGroup parent, Callback callback) {
        super(parent, callback);
    }

    @Override
    protected boolean showPriceByUnit(Sku sku) {
        return false;
    }

}
