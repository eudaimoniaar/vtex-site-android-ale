package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.views.HighlightTextView;

public class GridAdapterViewHolder extends BaseGridAdapterViewHolder {

    public GridAdapterViewHolder(final Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);
        convertView.findViewById(R.id.product_item_menu_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentInteractionListener.onProductOptionsClicked(product);
            }
        });
    }

}
