package com.fizzmod.vtex.utils;

import android.view.View;

public class ScrollHandler extends BaseScrollHandler {

    public ScrollHandler(ScrollListener scrollListener) {
        super(scrollListener);
        // This class is not used on this flavor
    }

    @Override
    public void addScrollListener(View view) {
        // Nothing to do, This class is not used on this flavor
    }
}
