package com.fizzmod.vtex.adapters.viewholders;

import android.view.ViewGroup;

import com.fizzmod.vtex.interfaces.Callback;

public class MinicartAdapterViewHolder extends MinicartHighlightAdapterViewholder {
    public MinicartAdapterViewHolder(ViewGroup parent, Callback callback) {
        super(parent, callback);
    }
}
