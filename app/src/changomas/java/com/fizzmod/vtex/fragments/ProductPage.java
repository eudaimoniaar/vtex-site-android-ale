package com.fizzmod.vtex.fragments;

import android.os.Bundle;

public class ProductPage extends BaseProductPage {

    public ProductPage(){
        super();
    }

    public static ProductPage newInstance(String type, String value) {
        ProductPage fragment = new ProductPage();

        if(type != null && value != null) {
            Bundle args = new Bundle();
            args.putString(type, value);
            fragment.setArguments(args);
        }

        return fragment;
    }
}
