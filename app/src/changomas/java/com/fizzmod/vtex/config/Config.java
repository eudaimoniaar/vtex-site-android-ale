package com.fizzmod.vtex.config;

import com.fizzmod.vtex.R;

import java.util.HashMap;
import java.util.Map;

public class Config extends com.fizzmod.vtex.config.BaseConfig {

    private static Config config = null;

    public static Config getInstance() {
        if (config == null)
            config = new Config();

        return config;
    }

    private Config() {
        super();
        setContactURL("https://www.changomas.com.ar/contenidos/contacto");
        setTermsUrl("https://www.changomas.com.ar/contenidos/terminosycondiciones");
        setDealsCollection("?fq=H:137");
        setBestSellingCollection("?fq=H:138");
        setOurBrandsCollection("?fq=H:167");
        setOurBrandsQACollection("?fq=H:142");
        setHasMultipleSalesChannels(true);
    }

    @Override
    public void baseUrlChanged(String newBaseUrl) {
        setTermsUrl(newBaseUrl + "/terminos-y-condiciones-webview");
    }

    @Override
    public String getDevFlavorName() {
        return "changomasDev";
    }

    @Override
    public Map<String, Integer> getClientPromosMap() {
        Map<String, Integer> hashImage = new HashMap<>();
        hashImage.put("paga menos", R.drawable.ic_pagas_menos);
        hashImage.put("nuestras marcas", R.drawable.ic_nuestras_marcas);
        hashImage.put("precio bomba", R.drawable.ic_precio_bomba);
        return hashImage;
    }

    @Override
    public boolean doesCheckForUpdates() {
        return true;
    }
}
