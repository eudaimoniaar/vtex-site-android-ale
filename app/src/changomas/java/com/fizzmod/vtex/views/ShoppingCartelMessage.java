package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;

import javax.annotation.Nullable;

/**
 * This class is disabled on this flavor.
 * */
public class ShoppingCartelMessage extends BaseCartelMessage {

    public ShoppingCartelMessage(Context context) {
        super(context);
    }

    public ShoppingCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShoppingCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setPlural(boolean pluralMode) {
        // Nothing to do
    }

    @Override
    public int getBackgroundResource() {
        return 0;
    }

    @Override
    public int getIcon() {
        return 0;
    }
}
