package com.fizzmod.vtex.fragments;

import android.view.View;
import android.widget.ArrayAdapter;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Store;

import java.util.ArrayList;
import java.util.List;

public class SalesChannelsSelector extends BaseSalesChannelsSelector {

    public SalesChannelsSelector() {
        // Required empty public constructor
    }

    @Override
    public void setSpinners() {
        retry.setVisibility(View.GONE);
        selectWrapper.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);

        stores = Config.getInstance().salesChannelStores;

        if (progress == null || selectWrapper == null)
            return;
        progress.setVisibility(View.GONE);
        selectWrapper.setVisibility(View.VISIBLE);
        updateStoresSpinner();
        if (stores.size() == 1)
            selectStore(stores.get(0));

        switcher.showNext();
    }

    public void updateStoresSpinner() {
        List<String> storesList = new ArrayList<String>();
        for (Store store : stores)
            storesList.add(store.name);
        storesSpinner.setPrompt(getResources().getString(R.string.select));
        ArrayAdapter<String> storesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, storesList);
        storesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storesSpinner.setAdapter(storesAdapter);
        if (storesList.size() == 1)
            storesSpinner.setSelection(0);
    }

    @Override
    public void selectStore(){
        try {
            String storeName = storesSpinner.getSelectedItem().toString();
            for (Store store : stores){
                if (store.name.equals(storeName)) {
                    setActiveStore(store);
                    try {
                        if (getActivity().getLocalClassName().equals(Main.class.getName())) {
                            updateStoresSpinner();
                        }
                    } catch (Exception e) {}
                    return;
                }
            }
        } catch(Exception e) {}
    }

}
