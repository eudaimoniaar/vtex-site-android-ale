package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;

public class GeantGridAdapterViewHolder extends FabGridAdapterViewHolder {

    private View productQuantityModifiersWrapper;
    private View subtractQuantity;
    private TextView productQuantity;

    public GeantGridAdapterViewHolder(Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);

        productQuantityModifiersWrapper = convertView.findViewById(R.id.productQuantityModifiersWrapper);
        productQuantity = convertView.findViewById(R.id.productQuantity);
        subtractQuantity = convertView.findViewById(R.id.subtractQuantity);

        subtractQuantity.setOnClickListener(this);
        convertView.findViewById(R.id.addQuantity).setOnClickListener(this);
    }

    @Override
    int getMenuButtonId() {
        return R.id.product_item_fab_menu_button;
    }

    @Override
    int getOptionsButtonId() {
        return R.id.product_options_button;
    }

    @Override
    public void setView(int position, Product product) {
        super.setView(position, product);
        setButtonsUI();
    }

    private void setButtonsUI() {
        final Sku skuInCart = Cart.getInstance().getById(sku.getId());
        if (Config.getInstance().showProductWeight())
            Utils.fadeOut(
                    skuInCart != null && skuInCart.getSelectedQuantity() > 0 ?
                            itemBuyButton : productQuantityModifiersWrapper,
                    new Callback() {
                        @Override
                        public void run(Object data) {
                            Utils.fadeIn(skuInCart != null && skuInCart.getSelectedQuantity() > 0 ?
                                    productQuantityModifiersWrapper : itemBuyButton);
                        }

                        @Override
                        public void run(Object data, Object data2) {
                            // Nothing to do.
                        }
                    });
    }

    @Override
    protected void setQuantity(int quantity) {
        super.setQuantity(quantity);
        String quantityString = Config.getInstance().showProductWeight() && sku.hasWeight() ?
                sku.getWeightFormatted(quantity) :
                String.valueOf(quantity);
        if (productQuantity != null)
            productQuantity.setText(quantityString);
        subtractQuantity.setEnabled(quantity > 1);
        subtractQuantity.setAlpha(quantity > 1 ? 1f : 0.5f);
    }

    private int getQuantity() {
        Sku skuInCart = Cart.getInstance().getById(sku.getId());
        String quantityString = productQuantity.getText().toString();
        return Config.getInstance().showProductWeight() && skuInCart.hasWeight() ?
                skuInCart.getQuantityFromWeight(quantityString) :
                Integer.parseInt(quantityString);
    }

    private void subtractQuantity() {
        if (getQuantity() > 1)
            setQuantity( clickListener.productSubtracted( (int) itemBuyButton.getTag( R.id.TAG_ADAPTER_ITEM ) ) );
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.itemBuyButton:
                setButtonsUI();
                break;

            case R.id.addQuantity:
                addQuantity();
                if (getQuantity() == 2) {
                    subtractQuantity.setEnabled(true);
                    Utils.alpha(subtractQuantity, 0.5f, 1f, 350);
                }
                break;

            case R.id.subtractQuantity:
                subtractQuantity();
                int quantity = getQuantity();
                v.setEnabled(quantity > 1);
                if (quantity <= 1)
                    Utils.alpha(subtractQuantity, 1f, 0.5f, 350);
                break;
        }
    }

}
