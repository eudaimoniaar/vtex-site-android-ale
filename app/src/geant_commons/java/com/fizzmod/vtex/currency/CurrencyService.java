package com.fizzmod.vtex.currency;

import com.fizzmod.vtex.interfaces.TypedCallback;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class CurrencyService {

    private static final long TIME_FOR_NEXT_UPDATE = 180000;    // 3 minutes in milliseconds

    private static CurrencyService instance;

    static CurrencyService getInstance() {
        if (instance == null) {
            instance = new CurrencyService();
        }
        return instance;
    }

    private Float latestMultiplier;
    private Long lastUpdateTime;
    private final List<TypedCallback<Float>> callbacks = new ArrayList<>();
    private boolean waitingResponse = false;

    private CurrencyService() {}

    void getDollarMultiplier(final TypedCallback<Float> callback) {
        if (waitingResponse) {
            synchronized (callbacks) {
                callbacks.add(callback);
            }
        } else if (shouldUpdateMultiplier()) {
            waitingResponse = true;
            CurrencyConverterClient.getCurrencyApiService().getDollarInfo().enqueue(new Callback<List<DollarInfo>>() {
                @Override
                public void onResponse(Call<List<DollarInfo>> call, Response<List<DollarInfo>> response) {
                    if (response.isSuccessful() && !response.body().isEmpty()) {
                        // Currency multipliers are sorted by date, so latest multiplier is the first object.
                        DollarInfo dollarInfo = response.body().get(0);
                        latestMultiplier = dollarInfo.getCurrencyMultiplier();
                        lastUpdateTime = System.currentTimeMillis();
                    } else {
                        latestMultiplier = -1.0f;
                    }
                    onEnd();
                }

                @Override
                public void onFailure(Call<List<DollarInfo>> call, Throwable t) {
                    latestMultiplier = -1.0f;
                    onEnd();
                }

                void onEnd() {
                    waitingResponse = false;
                    callback.run(latestMultiplier);
                    synchronized (callbacks) {
                        for (TypedCallback<Float> c : callbacks)
                            c.run(latestMultiplier);
                        callbacks.clear();
                    }
                }

            });
        } else {
            callback.run(latestMultiplier);
        }
    }

    private boolean shouldUpdateMultiplier() {
        return lastUpdateTime == null || System.currentTimeMillis() - lastUpdateTime >= TIME_FOR_NEXT_UPDATE;
    }

}
