package com.fizzmod.vtex.currency;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

class DollarInfo {

    @SerializedName("fecha")
    private final Date date;

    @SerializedName("cotizacion")
    private final Integer quotation;        // Integer that includes cents.

    public DollarInfo(Date date, Integer quotation) {
        this.date = date;
        this.quotation = quotation;
    }

    public float getCurrencyMultiplier() {
        return 1 / ((float) quotation / 100);
    }

}
