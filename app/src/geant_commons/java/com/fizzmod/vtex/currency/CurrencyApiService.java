package com.fizzmod.vtex.currency;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

interface CurrencyApiService {

    @GET("devotoweb/dataentities/DO/search?_fields=fecha,cotizacion&_where=fecha%3E2001-01-01&_sort=fecha%20DESC")
    Call<List<DollarInfo>> getDollarInfo();

}
