package com.fizzmod.vtex.currency;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.service.AEDateTypeAdapter;
import com.google.gson.GsonBuilder;

import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class CurrencyConverterClient {

    private static CurrencyApiService currencyApiService = null;

    static CurrencyApiService getCurrencyApiService() {
        if (currencyApiService == null)
            currencyApiService = new Retrofit.Builder()
                    .addConverterFactory(
                            GsonConverterFactory.create(
                                    new GsonBuilder()
                                            .registerTypeAdapter(Date.class, new AEDateTypeAdapter())
                                            .create()))
                    .baseUrl(BuildConfig.CURRENCY_MULTIPLIER_API)
                    .client(new OkHttpClient.Builder().addNetworkInterceptor(
                            new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                            .build())
                    .build()
                    .create(CurrencyApiService.class);

        return currencyApiService;
    }

}
