package com.fizzmod.vtex.config;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.currency.DollarConverter;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Store;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.fizzmod.vtex.utils.API.JANIS_STORENAME_HEADER;

abstract class GeantConfig extends BaseConfig {

    public ArrayList<Store> salesChannelStores = new ArrayList<>();

    GeantConfig() {
        super();
        setContactURL("https://www.devoto.com.uy/contacto");
        setTermsUrl("https://www.devoto.com.uy/terminos-y-condiciones");
        setCardRequestUrl("https://www.santander.com.uy/solicitud-tarjetas?prod=master&tar=hiperm%C3%A1s&op=Internacional");
        setDealsCollection("?fq=H:1101");
        setBestSellingCollection("?fq=H:1102");
        setOurBrandsCollection("?fq=H:1103");

        setDealsQACollection("?fq=H:1101");
        setBestSellingQACollection("?fq=H:1102");
        setOurBrandsQACollection("?fq=H:1103");
        setCurrencyConverter(new DollarConverter());
        setLocale(new Locale("es", "UY"));

        setGeantConfig(true);

        setShowProductWeight(true);
    }

    @Override
    public String formatPrice(double price){
        return super.formatPrice(price).replace("$", "$U");
    }

    @Override
    public HashMap<String, String> getExtraHeaders() {
        HashMap<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(JANIS_STORENAME_HEADER, BuildConfig.JANIS_STORENAME);
        return extraHeaders;
    }

    @Override
    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        // The field "Precio Dolar" comes as an array with values "No" OR "Si".
        // Treating it as a String and stripping it from its beginning and end
        // characters will end up returning "No" OR "Si".
        // Example: "Precio Dolar": "["No"]" ==> Will return the String "No".
        return productJsonObject.optString("Precio Dolar", "No")
                .replace("[\"", "")
                .replace("\"]", "")
                .equalsIgnoreCase("Si");
    }

    @Override
    public ArrayList<Category> getFilteredCategories(ArrayList<Category> categories) {
        if (categories == null)
            return null;

        List<Integer> allowedCategories = Arrays.asList(
                412, 350, 539, 476, 14, 342, 34, 320, 1174, 1138, 344, 42, 44, 206, 352, 307
        );

        ArrayList<Category> filteredCategories = new ArrayList<>();
        for (int i = 0; i < allowedCategories.size(); i++) {
            for (Category category : categories) {
                if (category.getId().equals(allowedCategories.get(i))) {
                    filteredCategories.add(category);
                }
            }
        }

        return filteredCategories;
    }
}
