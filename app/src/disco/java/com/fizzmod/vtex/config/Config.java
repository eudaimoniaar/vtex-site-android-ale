package com.fizzmod.vtex.config;

import com.fizzmod.vtex.models.Store;

public class Config extends GeantConfig {

    private static Config config = null;

    public static Config getInstance() {
        if (config == null)
            config = new Config();

        return config;
    }

    private Config() {
        super();
        setContactURL("https://institucional.disco.com.uy/contacto");
        setTermsUrl("https://institucional.disco.com.uy/terminos-y-condiciones");
        setDealsCollection("?fq=H:1181");
        setBestSellingCollection("?fq=H:1182");
        setOurBrandsCollection("?fq=H:1183");

        setDealsQACollection("?fq=H:1181");
        setBestSellingQACollection("?fq=H:1182");
        setOurBrandsQACollection("?fq=H:1183");

        setHasMultipleSalesChannels(true);

        Store store = new Store();
        store.setId("0");
        store.setName("Montevideo");
        store.setEcommerce(true);
        store.setSalesChannel("4");
        salesChannelStores.add(store);

        Store store2 = new Store();
        store2.setId("1");
        store2.setName("Punta del Este");
        store2.setEcommerce(true);
        store2.setSalesChannel("5");
        salesChannelStores.add(store2);
    }

    @Override
    public String getDevFlavorName() {
        return "discoDev";
    }
}
