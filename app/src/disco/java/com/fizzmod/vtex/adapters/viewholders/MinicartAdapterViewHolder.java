package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.BaseMinicartAdapterViewHolder;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Sku;

public class MinicartAdapterViewHolder extends MinicartHighlightAdapterViewholder {

    private TextView labelPromotion;

    public MinicartAdapterViewHolder(ViewGroup parent, Callback callback) {
        super(parent, callback);
        labelPromotion = (TextView) itemView.findViewById(R.id.minicart_item_label_promotion);
    }

    @Override
    public void setView(Sku sku) {
        super.setView(sku);

        if (sku.hasLabelPromotion()) {
            labelPromotion.setText(sku.getLabelName().trim());
            labelPromotion.setVisibility(View.VISIBLE);
        } else
            labelPromotion.setVisibility(View.GONE);
    }
}
