package com.fizzmod.vtex.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CartSyncPagerAdapter;
import com.fizzmod.vtex.fragments.CartSyncSummaryFragment;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CartSyncSummaryActivity extends AppCompatActivity implements
        View.OnClickListener,
        ViewPager.OnPageChangeListener {

    public static final int REQUEST_CODE = 123;

    private ViewPager viewPager;
    private View skipButton;
    private View leftDotView;
    private View middleDotView;
    private View rightDotView;
    private View nextButton;
    private View finishButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_sync_summary_activity);

        CartSyncPagerAdapter adapter = new CartSyncPagerAdapter(getSupportFragmentManager());
        adapter.addFragment( CartSyncSummaryFragment.newInstance(
                R.drawable.onboarding_step_1,
                R.string.cart_sync_summary_first_slide_title,
                R.string.cart_sync_summary_first_slide_description ) );
        adapter.addFragment( CartSyncSummaryFragment.newInstance(
                R.drawable.onboarding_step_2,
                R.string.cart_sync_summary_second_slide_title,
                R.string.cart_sync_summary_second_slide_description ) );
        adapter.addFragment( CartSyncSummaryFragment.newInstance(
                R.drawable.onboarding_step_3,
                R.string.cart_sync_summary_third_slide_title,
                R.string.cart_sync_summary_third_slide_description ) );

        viewPager = (ViewPager) findViewById(R.id.cart_sync_summary_activity_view_pager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        skipButton = findViewById(R.id.cart_sync_summary_activity_skip_button);
        skipButton.setOnClickListener(this);

        leftDotView = findViewById(R.id.cart_sync_summary_activity_dot_left);

        middleDotView = findViewById(R.id.cart_sync_summary_activity_dot_middle);
        middleDotView.setEnabled(false);

        rightDotView = findViewById(R.id.cart_sync_summary_activity_dot_right);
        rightDotView.setEnabled(false);

        nextButton = findViewById(R.id.cart_sync_summary_activity_next_button);
        nextButton.setOnClickListener(this);

        finishButton = findViewById(R.id.cart_sync_summary_activity_finish_button);
        finishButton.setOnClickListener(this);
    }

    private void finishSummary() {
        setResult(RESULT_OK);
        finish();
    }

    private void nextSlide() {
        int nextPosition = viewPager.getCurrentItem() + 1;
        viewPager.setCurrentItem(nextPosition);
        if (nextPosition == 2) {
            skipButton.setVisibility(GONE);
            nextButton.setVisibility(GONE);
            finishButton.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cart_sync_summary_activity_finish_button:
            case R.id.cart_sync_summary_activity_skip_button:
                finishSummary();
                break;
            case R.id.cart_sync_summary_activity_next_button:
                nextSlide();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // Nothing to do.
    }

    @Override
    public void onPageSelected(int position) {
        int dotId = R.id.cart_sync_summary_activity_dot_left;
        if (position == 1)
            dotId = R.id.cart_sync_summary_activity_dot_middle;
        else if (position == 2)
            dotId = R.id.cart_sync_summary_activity_dot_right;
        leftDotView.setEnabled(dotId == leftDotView.getId());
        middleDotView.setEnabled(dotId == middleDotView.getId());
        rightDotView.setEnabled(dotId == rightDotView.getId());
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // Nothing to do.
    }
}
