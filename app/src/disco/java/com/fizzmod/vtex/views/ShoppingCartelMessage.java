package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class ShoppingCartelMessage extends BaseCartelMessage {

    public ShoppingCartelMessage(Context context) {
        this(context, null);
    }

    public ShoppingCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShoppingCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        txtMessage.setVisibility(GONE);
        multipleTextsLayout.setVisibility(VISIBLE);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.cartel_background_shopping;
    }

    @Override
    public int getIcon() {
        return R.drawable.icn_lists_white;
    }

    public void setPlural(boolean pluralMode) {
        upperTxtMessage.setText(pluralMode ?
                R.string.product_added_to_shopping_lists :
                R.string.product_added_to_shopping_list);
    }
}
