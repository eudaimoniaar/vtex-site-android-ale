package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class FavoriteCartelMessage extends BaseCartelMessage {

    public FavoriteCartelMessage(Context context) {
        this(context, null);
    }

    public FavoriteCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoriteCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(boolean favoriteAdded) {
        setText(favoriteAdded ? R.string.favouriteAdded : R.string.favorite_removed);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.fav_alert;
    }

    @Override
    public int getIcon() {
        return R.drawable.icn_heart_white_on;
    }

}
