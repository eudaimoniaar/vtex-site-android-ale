!function (e, t) {
    var a;
    ({
        email: "{{email}}", initialize: function () {
            (a = this).loadCSS(), ~location.href.indexOf("email") && a.fillEmail(), a.addEvent(e, "hashchange", a.hashChange), a.addXMLRequestCallback(a.handleXMLRequestCallback);
            try {
                document.body.classList.add("android-app")
            } catch (e) {
            }
        }, addEvent: function (e, t, a) {
            null !== e && void 0 !== e && (e.addEventListener ? e.addEventListener(t, a, !1) : e.attachEvent ? e.attachEvent("on" + t, a) : e["on" + t] = a)
        }, addXMLRequestCallback: function (e) {
            var t, a;
            XMLHttpRequest.callbacks ? XMLHttpRequest.callbacks.push(e) : (XMLHttpRequest.callbacks = [e], t = XMLHttpRequest.prototype.send, XMLHttpRequest.prototype.send = function () {
                t.apply(this, arguments);
                var e = arguments;
                this.onload = function () {
                    for (a = 0; a < XMLHttpRequest.callbacks.length; a++) XMLHttpRequest.callbacks[a](this, e)
                }
            })
        }, handleXMLRequestCallback: function (e, t) {
            if (void 0 != e && void 0 !== e.responseURL && e.responseURL.match("/items/update") && 200 == e.status && void 0 !== e.responseText) try {
                for (var a = JSON.parse(t[0]).orderItems, n = 0; n < a.length; n++) "object" == typeof BHAndroid && "function" == typeof BHAndroid.itemUpdated && BHAndroid.itemUpdated(a[n].id, a[n].quantity)
            } catch (e) {
            }
        }, hashChange: function (t) {
            ~e.location.hash.toLowerCase().indexOf("email") && a.fillEmail()
        }, fillEmail: function () {
            a.email && ($("#client-pre-email").val(a.email).change().blur(), $("#btn-client-pre-email").click())
        }, loadCSS: function () {
            var e = "";
            a.email && (e += ".link-logout-container { display: none!important; }", e += "fieldset.pre-email { display: none!important; }", e += ".box-client-info-pf .client-email { display: none!important; }", e += ".box-client-info-pf .client-email { display: none!important; }"), e += '#vtexIdContainer ul.vtexIdUI-providers-list li[ng-repeat="provider in auth.oAuthProviders"]  { display: none!important; }';
            var t = document.head || document.getElementByTagName("head")[0], n = document.createElement("style");
            n.type = "text/css"
          ,n.styleSheet?n.styleSheet.cssText=e:n.appendChild(document.createTextNode(e)),t.appendChild(n)
        }
    }).initialize()
}(window);
