/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

public class SelectorItem {

    public final String name;
    public final String value;
    public boolean enabled = false;

    // Constructor.
    public SelectorItem(String name, String value) {
        this.name = name;
        this.value = value;
    }

}