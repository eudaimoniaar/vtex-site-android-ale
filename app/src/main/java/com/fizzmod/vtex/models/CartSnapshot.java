package com.fizzmod.vtex.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CartSnapshot {

    private List<SkuItem> skus = new ArrayList<>();
    private String dateCreated;

    public CartSnapshot(Cart cart) {
        for ( Map.Entry<String, Sku> entry : cart.getItems().entrySet() )
            if (entry.getValue().getSelectedQuantity() > 0)
                skus.add( new SkuItem( Integer.valueOf( entry.getKey() ), entry.getValue().getSelectedQuantity() ) );
    }

    public int getProductsQuantity() {
        int productsQuantity = 0;
        for (SkuItem skuItem : skus)
            productsQuantity += skuItem.getQuantity();
        return productsQuantity;
    }

    public List<String> getSkuList() {
        List<String> skus = new ArrayList<>();
        for (SkuItem skuItem : this.skus)
            skus.add(skuItem.getSku().toString());
        return skus;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public int getSkuQuantity(String sku) {
        for (SkuItem skuItem : skus)
            if (skuItem.getSku().toString().equals(sku))
                return skuItem.getQuantity();
        return 0;
    }
}
