/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.AnalyticsUtils;
import com.fizzmod.vtex.utils.DeviceIdFactory;
import com.fizzmod.vtex.utils.Request;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import okhttp3.Call;
import okhttp3.Response;

import static com.fizzmod.vtex.utils.API.CONTENT_TYPE_HEADER;
import static com.fizzmod.vtex.utils.API.JANIS_CLIENT_HEADER;
import static com.fizzmod.vtex.utils.API.X_APP_PACKAGE;

/**
 * Created by marcos on 23/01/16.
 */
public class Cart {

    private static final String PREFS_NAME = "cart";
    private static final String TOTALIZERS_URL = API.JANIS_API + "2/cart/simulate";
    private static final String MINVALUE_URL = API.JANIS_API + "2/cart/minvalue";

    private static final Cart instance = new Cart();

    private int totalItems;
    private LinkedHashMap<String, Sku> items;
    private double total;
    private String salesChannel = BuildConfig.SALES_CHANNEL;

    public Cart(){
        this.total = 0;
        this.totalItems = 0;
        this.items = new LinkedHashMap<>();
    }

    public static Cart getInstance(){
        return instance;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public LinkedHashMap<String, Sku> getItems() {
        return items;
    }

    private int addItem(Sku sku, boolean increaseQuantity, boolean sumQuantity) {

        if (!sku.hasStock())
            return 0;

        String id = sku.getId();

        if (!items.containsKey(id)) {

            if (sku.getSelectedQuantity() == 0)
                sku.setSelectedQuantity(1);

            items.put(id, new Sku(sku));

            return sku.getSelectedQuantity();
        }

        Sku inCartSku = items.get(id);

        if (increaseQuantity)
            inCartSku.increaseSelectedQuantity();
        else if (sumQuantity)
            inCartSku.addSelectedQuantity(sku.getSelectedQuantity());
        else if (inCartSku.getSelectedQuantity() < sku.getSelectedQuantity())
            // If SKU is already in cart do not override quantity. Leave the highest one.
            inCartSku.setSelectedQuantity(sku.getSelectedQuantity());

        return inCartSku.getSelectedQuantity();
    }

    public int addItemQuantity(Sku sku, Context context, int quantity) {

        if (sku == null)
            return 0;

        Sku skuWithQuantity = new Sku(sku);
        skuWithQuantity.setSelectedQuantity(quantity);
        int totalQuantity = addItem(skuWithQuantity, false, true);
        save(context);

        return totalQuantity;
    }

    public int addItem(Sku sku, Context context) {
        return addItem(sku, context, true);
    }

    public int addItem(Sku sku, Context context, boolean saveCart) {
        int quantity = addItem(sku, false, false);
        if (saveCart)
            save(context);
        AnalyticsUtils.logAddToCartEvent(context, sku.getId(), sku.getName());

        return quantity;
    }

    public int increaseItemQuantity(Sku sku, Context context) {
        int quantity = addItem(sku, true, false);
        save(context);
        AnalyticsUtils.logAddToCartEvent(context, sku.getId(), sku.getName());

        return quantity;
    }

    public int decreaseItemQuantity(Sku sku, Context context) {
        return addItemQuantity(sku, context, -1);
    }

    public void emptyCart(Context context) {
        this.items.clear();
        this.total = 0;
        this.totalItems = 0;
        this.save(context);
    }

    public Sku getById(String id) {
        return items.get(id);
    }

    public void removeItem(String sku) {
        Sku item = this.items.remove(sku);

        if(item != null)
            item.setSelectedQuantity(0);
    }

    public void removeItem(String sku, Context context) {
        this.removeItem(sku);
        this.save(context);
    }

    public void changeItemQuantity(String sku, int quantity) {
        if(this.items.containsKey(sku))
            this.items.get(sku).setSelectedQuantity(quantity);
    }

    public Sku getItemByPosition(int position) {
        int count = 0;
        Set<Map.Entry<String, Sku>> entrySet = this.items.entrySet();
        for (Map.Entry<String, Sku> entry : entrySet) {
            if(count == position)
                return entry.getValue();
            count++;
        }

        return null;
    }

    public double getTotal() {
        return this.total;
    }

    public String getTotalFormatted() {
        return Config.getInstance().formatPrice(this.total);
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public boolean updateItemsQuantity(HashMap<String, Integer> quantityBySku) {

        boolean changed = false;

        Set<Map.Entry<String, Sku>> entrySet = items.entrySet();
        for (Map.Entry<String, Sku> entry : entrySet) {
            Sku sku = entry.getValue();

            int newQuantity = quantityBySku.containsKey(entry.getKey()) ? quantityBySku.get(entry.getKey()) : 0;
            int oldQuantity = sku.getSelectedQuantity();

            if(sku != null && newQuantity != oldQuantity) {
                if(newQuantity == 0)
                    sku.setStock(0);

                sku.setSelectedQuantity(newQuantity);
                changed = true;
            }
        }

        return changed;

    }


    public String getUrl(Context context) {

        Uri.Builder builder = new Uri.Builder()
                .scheme("https")
                .authority(API.HOST.replace("https://", "").replace("http://", ""))
                .path("/checkout/cart/add");


        builder.appendQueryParameter("utm_source", "app");

        try {
            builder.appendQueryParameter("utm_campaign", new DeviceIdFactory(context).getDeviceId());
        }
        catch (NullPointerException e) {}
        catch (RuntimeException e) {}

        if(!this.items.isEmpty()) {
            Set<Map.Entry<String, Sku>> entrySet = this.items.entrySet();
            for (Map.Entry<String, Sku> entry : entrySet) {
                Sku sku = entry.getValue();

                int quantity = sku.getSelectedQuantity();

                if(quantity > 0) { //To avoid VTEX Error
                    builder.appendQueryParameter("sku", sku.getId());
                    builder.appendQueryParameter("qty", String.valueOf(quantity));
                    builder.appendQueryParameter("seller", "1");
                    builder.appendQueryParameter("sc", getSalesChannel());
                }
            }

        } else {
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            try {
                JSONArray json = new JSONArray(settings.getString(PREFS_NAME, ""));

                if (json == null || json.length() == 0)
                    return null;

                for (int i = 0; i < json.length(); i++) {
                    JSONObject data = json.getJSONObject(i);

                    int quantity = data.getInt("quantity");

                    if(quantity > 0) { //To avoid VTEX Error
                        builder.appendQueryParameter("sku", data.getString("sku"));
                        builder.appendQueryParameter("qty", String.valueOf(quantity));
                        builder.appendQueryParameter("seller", "1");
                        builder.appendQueryParameter("sc", getSalesChannel());
                    }
                }
            } catch (JSONException e) {}
        }

        builder.appendQueryParameter("redirect", "true");

        return builder.build().toString();
    }

    public void setSalesChannel(String salesChannel) {
        this.salesChannel = salesChannel;
    }


    public String getSalesChannel() {
        return salesChannel;
    }

    /**
     * Save cart data to SharedPreferences
     * @param context
     */
    public void save(Context context) {
        if(context == null)
            return;

        try {
            String value = "";
            if(items.size() > 0) {
                JSONArray json = new JSONArray();

                Set<Map.Entry<String, Sku>> entrySet = this.items.entrySet();
                for (Map.Entry<String, Sku> entry : entrySet) {
                    JSONObject data = new JSONObject();
                    Sku sku = entry.getValue();

                    data.put("sku", entry.getKey());
                    data.put("quantity", sku.getSelectedQuantity());
                    json.put(data);
                }
                value = json.toString();
            }

            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(PREFS_NAME, value);

            // Commit the edits!
            editor.commit();
        } catch (JSONException e) {}
    }

    /**
     * Restore cart data from SharedPreferences
     * @param context
     * @param callback
     */
    public void restore(Context context, boolean force, final Callback callback) {

        if (!items.isEmpty() && !force) {
            callback.run(false);
            return;
        }

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);

        try {
            JSONArray json = new JSONArray(settings.getString(PREFS_NAME, ""));

            if (json.length() == 0) {
                callback.run(false);
                return;
            }

            int totalRequests = (int) Math.ceil((float) json.length() / (float) API.SOFT_LIMIT);

            final CountDownLatch responseCountDownLatch = new CountDownLatch(totalRequests);
            final HashMap<String, Integer> skus = new HashMap<>();
            HashMap<String, String> params = new HashMap<>();

            Utils.log("Total Requests: " + totalRequests + " " + responseCountDownLatch.getCount());
            final List<String> errorMessages = new ArrayList<>();

            okhttp3.Callback requestCallback = new okhttp3.Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    // Handle error in a better way!
                    Log.e("Cart", "An error occurred when searching products.", e);
                    errorMessages.add(e.getMessage());
                    responseCountDownLatch.countDown();
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    if (response.isSuccessful()) {
                        ArrayList<Product> productList = API.setProductList(response.body().string());

                        Utils.log("Product Found:" + productList.size());
                        for (Product product : productList) {
                            Set<Map.Entry<String, Integer>> entrySet = skus.entrySet();
                            for (Map.Entry<String, Integer> entry : entrySet) {
                                Sku sku = product.getSkuById(entry.getKey());
                                if (sku != null) {
                                    sku.setSelectedQuantity(entry.getValue());
                                    addItem(sku, false, false);
                                }
                            }
                        }
                    } else
                        errorMessages.add(response.message());

                    responseCountDownLatch.countDown();
                }
            };

            Utils.log("Total items: " + json.length());

            // Loop just in case, skus are needed inside of both requests. (?)
            for (int i = 0; i < json.length(); i++) {
                JSONObject data = json.getJSONObject(i);
                skus.put(data.getString("sku"), data.getInt("quantity"));
            }

            for (int i = 0; i < json.length(); i++){
                JSONObject data = json.getJSONObject(i);
                params.put("fq{{" + i + "}}", "skuId:" + Integer.toString(data.getInt("sku")));
                if ((i != 0 && i % (API.SOFT_LIMIT - 1) == 0) || i == (json.length() - 1)) {
                    Utils.log("Performing request: " + i + " " + params.size());
                    API.search(context, null, params, 1, API.SOFT_LIMIT, requestCallback);
                    params.clear();
                }
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean result = false;
                    try {
                        responseCountDownLatch.await();
                        result = true;
                        for (String error : errorMessages)
                            Log.e("Cart", "An error occurred when retrieving products: " + error);
                    } catch (InterruptedException e) {
                        Log.e("Cart", "An exception was thrown in method 'restore'.", e);
                    }
                    callback.run(result);
                }
            }).start();

        } catch (JSONException e) {
            callback.run(true);
        }
    }

    public Call getMinimumCartValue(Context context, okhttp3.Callback callback) {

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();

        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put("cache-control", "no-cache");

        HashMap<String, String> body = new HashMap<>();

        return Request.get(MINVALUE_URL, body, headers, callback);
    }

    public Call getTotalizers(Context context, okhttp3.Callback callback) {

        Set<Map.Entry<String, Sku>> entrySet = this.items.entrySet();

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();

        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        try {
            JSONObject body = new JSONObject();
            JSONArray JSONItems = new JSONArray();

            for (Map.Entry<String, Sku> entry : entrySet) {

                Sku sku = entry.getValue();
                int quantity = sku.getSelectedQuantity();

                if(quantity > 0) { // Otherwise VTEX API will fail!

                    JSONObject item = new JSONObject();
                    item.put("seller", "1");
                    item.put("id", sku.getId());
                    item.put("quantity", sku.getSelectedQuantity());
                    JSONItems.put(item);
                }
            }

            body.put("items", JSONItems);
            body.put("sc", getSalesChannel());

            Store store = Store.restore(context);

            if(store != null)
                body.put("zip", store.getZipCode());
            else
                body.put("zip", "11600");

            return Request.post(TOTALIZERS_URL, body.toString(), headers, callback);

        } catch (JSONException e) {}


        return null;

    }

    public void refresh(Context context, final Callback callback) {

        int totalRequests = (int) Math.ceil((float) items.size() / (float) API.SOFT_LIMIT);
        final CountDownLatch responseCountDownLatch = new CountDownLatch(totalRequests);

        Utils.log("Total Requests: " + totalRequests + " " + responseCountDownLatch.getCount());

        final Set<String> ids =  ((LinkedHashMap) items.clone()).keySet();
        final List<String> errorMessages = new ArrayList<>();

        okhttp3.Callback requestCallback = new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Handle error in a better way!
                Log.e("Cart", "An error occurred when searching products.", e);
                errorMessages.add(e.getMessage());
                responseCountDownLatch.countDown();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ArrayList<Product> productList = API.setProductList(response.body().string());

                    for (int i = 0; i < productList.size(); i++) {
                        Product product = productList.get(i);
                        ArrayList<Sku> productSkus = product.getSkus();

                        for (int j = 0; j < productSkus.size(); j++) {
                            Sku updatedSku = productSkus.get(j);
                            Sku cartSku = items.get(updatedSku.getId());

                            if (updatedSku != null && cartSku != null) {
                                cartSku.setBestPrice(updatedSku.getBestPrice());
                                cartSku.setListPrice(updatedSku.getListPricePrice());
                                cartSku.setStock(updatedSku.getStock());

                                ids.remove(cartSku.getId());
                            }
                        }
                    }
                } else
                    errorMessages.add(response.message());
                responseCountDownLatch.countDown();
            }
        };

        // Loop just in case, skus are needed inside of both requests. (?)

        Set<Map.Entry<String, Sku>> entrySet = this.items.entrySet();
        HashMap<String, String> params = new HashMap<>();

        int i = 0;
        for (Map.Entry<String, Sku> entry : entrySet) {
            params.put("fq{{" + i + "}}", "skuId:" + entry.getKey());
            if ((i != 0 && i % (API.SOFT_LIMIT - 1) == 0) || i == (items.size() - 1)) {
                Utils.log("Performing request: " + i + " " + params.size());
                API.search(context, null, params, 1, API.SOFT_LIMIT, requestCallback);
                params.clear();
            }
            i++;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean result = false;
                try {
                    responseCountDownLatch.await();
                    result = true;

                    // Set no stock

                    for (String skuId : ids) {
                        Sku cartSku = items.get(skuId);
                        if(cartSku != null){
                            cartSku.setStock(0);
                        }
                    }

                    Utils.log("Ids Size: " + ids.size());
                    Utils.log("Items Size: " + items.size());

                    for (String error : errorMessages)
                        Log.e("Cart", "An error occurred when retrieving products: " + error);
                } catch (InterruptedException e) {
                    Log.e("Cart", "An exception was thrown in method 'restore'.", e);
                }
                callback.run(result);
            }
        }).start();

    }
}
