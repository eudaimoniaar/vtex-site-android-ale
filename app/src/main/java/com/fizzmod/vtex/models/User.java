package com.fizzmod.vtex.models;

/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Request;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.fizzmod.vtex.utils.API.JANIS_CLIENT_HEADER;
import static com.fizzmod.vtex.utils.API.X_APP_PACKAGE;

public class User {

    public final static String TOKEN_API = API.JANIS_API + "2/user/token/";

    public final static String VTEX_LOGIN = "vtex";
    public final static String GOOGLE_LOGIN = "google";
    public final static String JWT = "jwt";

    private static final String PREFS_NAME = "user";
    private static User instance = new User();

    private String email = null;
    private String name = null;
    private String lastName = null;
    private String token = null;
    private String loginType = null;
    private String signInToken = null;
    private String signInLoginType = null;

    public User(){
        this(null, null, null);
    }

    public User(String email, String name, String lastName) {
        this.email = email;
        this.name = name;
        this.lastName = lastName;
        instance = this;
    }

    public static User getInstance(Context context){
        if (
            (instance == null || (instance.email == null && instance.token == null && instance.loginType == null) ) &&
            context != null
        )
            instance = restore(context);
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public boolean hasEmail(){
        return email != null && !email.isEmpty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getToken() {
        return token != null ? token : signInToken;
    }

    public User setToken(String token) {
        this.token = token;
        return this;
    }

    public String getLoginType() {
        return loginType == null ? GOOGLE_LOGIN : loginType;
    }

    public User setLoginType(String loginType) {
        this.loginType = loginType;
        return this;
    }

    /**
     * Restore current store from Shared Preferences
     * @param context
     * @return
     */
    public static User restore(Context context) {
        if (context == null)
            return new User();
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        try {
            String userData = settings.getString(PREFS_NAME, null);
            if (!Utils.isEmpty(userData)) {
                JSONObject json = new JSONObject(userData);
                User user = new User(
                        json.getString("email"),
                        json.optString("name", null),
                        json.optString("lastName", null)
                );
                user.setToken(json.optString("token", null));
                user.setLoginType(json.optString("type", null));
                user.setSignInLoginType(json.optString("signInLoginType", null));
                user.setSignInToken(json.optString("signInToken", null));
                return user;
            }
        } catch (JSONException e) {}
        return new User();
    }

    public void save(Context context) {
        save(this, context, null);
    }

    public void save(Context context, com.fizzmod.vtex.interfaces.Callback callback) {
        save(this, context, callback);
    }

    /**
     * Save current user to share preferences
     * @param user
     * @param context
     */
    public static void save(final User user, final Context context, final com.fizzmod.vtex.interfaces.Callback callback) {
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        String url = User.TOKEN_API  + user.getSignInLoginType() + "/" + user.getSignInToken();
        Utils.log("Requesting: " + url);
        Request.get(url, null, headers, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // If no internet or failure saving the user, will retry JWT token later
                save(user, context);
                if (callback != null)
                    callback.run(false);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                boolean isCallSuccessful = false;
                try {
                    JSONObject JSON = new JSONObject(response.body().string());
                    user.setToken(JSON.getString("jwt"));
                    user.setLoginType(JWT); //This line MUST be below getString("jwt") in case an exception occurs there the login type won't be changed!
                    isCallSuccessful = true;
                } catch (JSONException e) {}
                save(user, context);
                if (callback != null)
                    callback.run(isCallSuccessful);
            }
        });
    }

    public static void save(User user, Context context) {
        if (context == null)
            return;
        JSONObject item = new JSONObject();
        try {
            item.put("email", user.getEmail());
            item.put("name", user.getName());
            item.put("lastName", user.getLastName());
            item.put("token", user.getToken());
            item.put("type", user.getLoginType());
            item.put("signInToken", user.getSignInToken());
            item.put("signInLoginType", user.getSignInLoginType());
        } catch (JSONException e) {}
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_NAME, item.toString());
        editor.commit();
    }

    public static void logout(Context context) {
        instance = null;
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    public static boolean isLogged(Context context) {
        User user = getInstance(context);
        return user != null && !Utils.isEmpty(user.getEmail());
    }

    public User setSignInToken(String signInToken) {
        this.signInToken = signInToken;
        return this;
    }

    public User setSignInLoginType(String signInLoginType) {
        this.signInLoginType = signInLoginType;
        return this;
    }

    public String getSignInToken() {
        return signInToken;
    }

    public String getSignInLoginType() {
        return signInLoginType;
    }
}
