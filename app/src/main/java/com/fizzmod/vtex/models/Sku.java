/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.utils.API;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by marcos on 06/01/16.
 */
public class Sku {

    public static final String NA = "N/A";

    private String id;
    private String productId;
    private int stock;
    private int selectedQuantity;
    private int installments;
    private double bestPrice;
    private double listPrice;
    private Double sellingPrice;
    private double installmentPrice;
    private double unitMultiplier;
    private String name;
    private String productName;
    private String brand;
    private Integer brandId;
    private String refId;
    private String EAN;
    private String measurementUnit;
    private String[] variations;
    private String[] images;
    private HashMap<String, String> variationsValue;
    private Long promotionsOutdatedTime;
    private List<Promotion> promotionList;
    private LegalPrice legalPrice;

    private boolean available = false;
    private boolean usesOtherCurrency = false;

    public Sku(){
        selectedQuantity = 0;
        id = null;
        stock = 0;
        bestPrice = 0;
        listPrice = 0;
        sellingPrice = null;
        unitMultiplier = 1;
        name = null;
        productName = null;
        legalPrice = null;
        variationsValue = new HashMap<>();
        promotionList = new ArrayList<>();
    }

    public Sku(Sku other) {
        id = other.id;
        productId = other.productId;
        stock = other.stock;
        selectedQuantity = other.selectedQuantity;
        installments = other.installments;
        bestPrice = other.bestPrice;
        listPrice = other.listPrice;
        sellingPrice = other.sellingPrice;
        installmentPrice = other.installmentPrice;
        unitMultiplier = other.unitMultiplier;
        name = other.name;
        productName = other.productName;
        brand = other.brand;
        brandId = other.brandId;
        refId = other.refId;
        EAN = other.EAN;
        measurementUnit = other.measurementUnit;
        variations = other.variations;
        images = other.images;
        variationsValue = other.variationsValue;
        legalPrice = other.legalPrice;
        promotionList = other.promotionList;
        available = other.available;
        promotionsOutdatedTime = other.promotionsOutdatedTime;
        usesOtherCurrency = other.usesOtherCurrency;
    }

    public List<Promotion> getPromotionList() {
        return hasOutdatedPromotionsInfo() ? new ArrayList<Promotion>() : promotionList;
    }

    public void clearPromotions() {
        promotionList.clear();
    }

    public void addPromotion(Promotion promotion, Long promotionsOutdatedTime) {
        this.promotionsOutdatedTime = promotionsOutdatedTime;
        promotionList.add(promotion);

        if(promotionList.size() > 1)
            Collections.sort(promotionList);
    }

    public boolean hasOutdatedPromotionsInfo() {
        return promotionsOutdatedTime != null && new Date().getTime() >= promotionsOutdatedTime;
    }

    public boolean hasCurrentPromotions() {
        if (hasOutdatedPromotionsInfo() || getPromotionList().isEmpty())
            return false;

        for (Promotion promotion : getPromotionList()) {
            if (promotion.isCurrent())
                return true;
        }

        return false;
    }

    public boolean hasLabelPromotion() {
        if (hasOutdatedPromotionsInfo())
            return false;

        for (Promotion promotion : promotionList)
            if (promotion.hasLabel() && promotion.isCurrent())
                return true;

        return false;
    }

    //return the label name with the highest priority. 1>2.
    public String getLabelName() {
        if (promotionList.isEmpty())
            return "";

        Promotion promotionAux = promotionList.get(0);
        for (Promotion promotion : promotionList) {
            if (promotion.getLabelPriority() < promotionAux.getLabelPriority() && promotion.isCurrent())
                promotionAux = promotion;
        }

        if(promotionAux.isCurrent())
            return promotionAux.getLabelName();
        else
            return "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStock(int stock) {
        this.stock = stock;
        available = stock > 0;
    }

    public boolean hasStock(){
        return this.stock != 0 || available;
    }

    public int getStock() {
        return this.stock;
    }

    public void setImages(String[] images){
        this.images = images;
    }

    public void setVariations(String[] variations){
        this.variations = variations;
    }

    public void addVariationsValue(String variation, String value){
        this.variationsValue.put(variation, value);
    }

    public void setBestPrice(double bestPrice) {
        this.bestPrice = bestPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public boolean hasSellingPrice() {
        return sellingPrice != null;
    }

    public String getId(){
        return this.id;
    }

    public String getProductId() {
        return productId;
    }

    public String getName(){
        return this.name;
    }

    public double getBestPrice(){
        return this.bestPrice * this.unitMultiplier;
    }

    public double getListPricePrice(){
        return this.listPrice * this.unitMultiplier;
    }

    public void getBestPriceFormatted(TypedCallback<String> callback){
        formatPrice(bestPrice * unitMultiplier, callback);
    }

    public void getListPriceFormatted(TypedCallback<String> callback){
        formatPrice(listPrice * unitMultiplier, callback);
    }

    public void getSellingPriceFormatted(TypedCallback<String> callback){
        formatPrice(sellingPrice != null ? sellingPrice : 0f, callback);
    }

    public void getPriceByUnitFormatted(final TypedCallback<String> callback) {
        if (showUnitMultiplier())
            formatPrice(bestPrice, new TypedCallback<String>() {
                @Override
                public void run(String price) {
                    callback.run(price + " x " + measurementUnit);
                }
            });
        else
            getLegalPriceFormatted(callback);
    }

    public String getPriceDiffPercentageFormatted(Context context) {
        return context.getString(
                BuildConfig.PROMOTIONS_API_ENABLED ? R.string.priceHighlight : R.string.simplePriceHighlight,
                getPriceDiffPercentage());
    }

    public int getPriceDiffPercentage() {

        if (bestPrice == 0)
            return 0;

        BigDecimal bestPriceBD = new BigDecimal(bestPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal listPriceBD = new BigDecimal(listPrice).setScale(2, BigDecimal.ROUND_HALF_UP);

        return (listPriceBD.subtract(bestPriceBD)).divide(listPriceBD, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue();
    }

    public String getMainImage(){
        return  API.resizeImage(this.images[0], 500, 500);
    }

    public String getMainImage(int width, int height){
        return API.resizeImage(this.images[0], width, height);
    }

    public String[] getImages(){
        return this.images;
    }

    public String[] getVariations(){
        return this.variations;
    }

    public String getVariationValue(String variation){
        String value = this.variationsValue.get(variation);

        return value instanceof String && value.equalsIgnoreCase(Sku.NA) && this.variationsValue.size() == 1 ? this.getName() : value;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public boolean showListPrice() {
        return getPriceDiffPercentage() >= Config.getInstance().getListPriceThreshold() && bestPrice < listPrice && sellingPrice == null;
    }

    public boolean showPriceDiffPercentage() {
        return getPriceDiffPercentage() >= Config.getInstance().getHighlightPriceThreshold()
                && (!BuildConfig.PROMOTIONS_API_ENABLED || !hasLabelPromotion());
    }

    public boolean showPriceByUnit() {
        return this.legalPrice != null || this.showUnitMultiplier();
    }

    public boolean showUnitMultiplier() {
        return (this.measurementUnit != null && !this.measurementUnit.equalsIgnoreCase("un") && !this.hasSellingPrice());
    }

    public int getSelectedQuantity() {
        return selectedQuantity;
    }

    public void setSelectedQuantity(int selectedQuantity) {
        this.selectedQuantity = selectedQuantity;
    }

    public void addSelectedQuantity(int quantity) {
        selectedQuantity += quantity;
    }

    public void increaseSelectedQuantity() {

        if(this.selectedQuantity < 1000)
            this.selectedQuantity++;
    }


    public static boolean isValidVariationValue(String value){
        return value instanceof String && !value.equalsIgnoreCase(Sku.NA);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public double getInstallmentPrice() {
        return installmentPrice;
    }

    public void getInstallmentPriceFormatted(TypedCallback<String> callback) {
        formatPrice(installmentPrice == 0 ? bestPrice : installmentPrice, callback);
    }

    public boolean hasInstallments(){
        return installments > 1;
    }

    public void setInstallmentPrice(double installmentPrice) {
        this.installmentPrice = installmentPrice;
    }

    public String getRefId() {
        return refId;
    }

    public boolean hasPrefix(List<String> prefixes) {
        if (prefixes == null)
            return false;
        for (String prefix : prefixes)
            if (refId.startsWith(prefix))
                return true;
        return false;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    public double getUnitMultiplier() {
        return unitMultiplier;
    }

    public void setUnitMultiplier(double unitMultiplier) {
        this.unitMultiplier = unitMultiplier;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public LegalPrice getLegalPrice() {
        return legalPrice;
    }

    public void getLegalPriceFormatted(final TypedCallback<String> callback) {
        if (legalPrice == null)
            callback.run(null);
        else {
            formatPrice(
                    bestPrice / legalPrice.multiplier,
                    new TypedCallback<String>() {
                        @Override
                        public void run(String price) {
                            callback.run(price + " x " + legalPrice.unit);
                        }
                    });
        }
    }

    public void setLegalPrice(LegalPrice legalPrice) {
        this.legalPrice = legalPrice;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setUsesOtherCurrency(boolean usesOtherCurrency) {
        this.usesOtherCurrency = usesOtherCurrency;
    }

    private void formatPrice(double price, TypedCallback<String> callback) {
        Config.getInstance().formatPrice(price, usesOtherCurrency, callback);
    }

    public boolean hasWeight() {
        return measurementUnit.equalsIgnoreCase("kg");
    }

    public int getQuantityFromWeight(String weightString) {
        String[] split = weightString.split(" ");
        double weight = Double.parseDouble(split[0]);
        return (int) (split[1].equalsIgnoreCase("g") ?
                weight / 1000 / unitMultiplier :
                weight / unitMultiplier);
    }

    public String getWeightFormatted(int quantity) {
        double weight = quantity * unitMultiplier;
        return weight >= 1 ?
                new DecimalFormat("#.###").format(weight) + " Kg" :
                new DecimalFormat("#").format(weight * 1000) + " g" ;
    }
}
