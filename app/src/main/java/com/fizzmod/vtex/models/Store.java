/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by marcos on 09/02/16.
 */
public class Store {

    private static final String PREFS_STORE = "store"; //selected store
    private static final String PREFS_STORES_LIST = "store_list";
    private static final String PREFS_STORES_DATE = "stores_date"; //Date when the list was saved
    private static final int ONE_DAY_SECONDS = 60 * 60 * 24; // 24 hs


    public double latitude;
    public double longitude;
    public String name;
    public String phone;
    public String schedule;
    public String address;
    public String state;
    public String city;
    public String id;
    public String zipCode;
    public boolean pickup;
    public boolean ecommerce;
    public String salesChannel;
    public ArrayList<String> zips;

    private static Store selectedStore = null;
    private static ArrayList<Store> storesCache = new ArrayList<>();

    public Store() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {

        return Utils.isEmpty(phone) ? "-" : phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSchedule() {

        return Utils.isEmpty(schedule) ? "-" : schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean hasPickup() {
        return pickup;
    }

    public void setPickup(boolean pickup) {
        this.pickup = pickup;
    }

    public boolean hasEcommerce() {
        return ecommerce;
    }

    public void setEcommerce(boolean ecommerce) {
        this.ecommerce = ecommerce;
    }

    public String getSalesChannel() {
        return salesChannel;
    }

    public void setSalesChannel(String salesChannel) {
        this.salesChannel = salesChannel;
    }

    public boolean hasSalesChannel() {
        return !Utils.isEmpty(salesChannel);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public ArrayList<String> getZips() {
        return zips;
    }

    public void setZips(ArrayList<String> zips) {
        this.zips = zips;
    }

    /**
     * Static Methods
     * @param postalCode
     * @param context
     * @return
     */

    /*public static Store getStoreByPostalCode(int postalCode, Context context){

        try {
            JSONArray ranges = new JSONArray(Utils.loadAssetTextAsString(context, "sc.json"));

            for(int i = 0; i < ranges.length(); i++){
                JSONObject range = ranges.getJSONObject(i);

                if(range.getInt("start") <= postalCode && range.getInt("end") >= postalCode){
                    Store store = new Store();
                    store.setName(range.getString("name"));
                    store.setSalesChannel(String.valueOf(range.getInt("sc")));

                    return store;
                }
            }

        } catch (JSONException e) {}

        return null;
    }*/

    /**
     * get Store by postal code
     * @param context
     * @param postalCode
     * @param callback
     * @return
     */

    public static void getStoreByPostalCode(Context context, final int postalCode, final ExtendedCallback callback) {

        Store.getStores(context, new ExtendedCallback() {
            @Override
            public void run(Object data) {

                if(data == null) {
                    //Error
                    callback.run(true, true);
                    return;
                }

                ArrayList<Store> stores = (ArrayList<Store>) data;

                for(int i = 0; i < stores.size(); i++){
                    Store store = stores.get(i);

                    if(store.hasEcommerce() && store.getZips().contains(String.valueOf(postalCode))) {
                        callback.run(store);
                        return;
                    }
                }

                callback.run(null);
            }

            @Override
            public void run(Object data, Object data2) {}

            @Override
            public void error(Object data) {
                callback.error(null);
            }
        });

    }


    /**
     * Restore current store from Shared Preferences
     * @param context
     * @return
     */

    public static Store restore(Context context){

        if(selectedStore != null)
            return selectedStore;

        SharedPreferences settings = context.getSharedPreferences(PREFS_STORE, 0);

        try {
            JSONObject json = new JSONObject(settings.getString(PREFS_STORE, ""));

            selectedStore = new Store();
            selectedStore.setName(json.getString("name"));
            selectedStore.setSalesChannel(json.getString("sc"));
            selectedStore.setId(json.optString("id", null));
            selectedStore.setZipCode(json.optString("zip", null));

            return selectedStore;

        } catch (JSONException e) {
            Utils.log("Restore: " + e.getMessage());
        }

        return null;

    }

    /**
     * Save current store to share preferences
     * @param store
     * @param context
     */

    public static void save(Store store, Context context){

        JSONObject item = new JSONObject();

        try {
            item.put("name", store.getName());
            item.put("id", store.getId());
            item.put("sc", store.getSalesChannel());
            item.put("zip", store.getZipCode());

            selectedStore = store;

        } catch (JSONException e) {}

        SharedPreferences settings = context.getSharedPreferences(PREFS_STORE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_STORE, item.toString());
        editor.commit();

    }

    /**
     * Save Stores JSON
     * @param context
     * @param stores
     */

    public static void saveList(Context context, String stores){

        if(context == null)
            return;

        SharedPreferences settings = context.getSharedPreferences(PREFS_STORES_LIST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_STORES_LIST, stores);
        editor.putLong(PREFS_STORES_DATE, new DateTime().getMillis() / 1000);

        // Commit the edits!
        editor.commit();

    }

    public static ArrayList<Store> restoreList(Context context) {

        SharedPreferences settings = context.getSharedPreferences(PREFS_STORES_LIST, Context.MODE_PRIVATE);

        return isStoresDataValid(context) ? setStores(settings.getString(PREFS_STORES_LIST, "")) : null;

    }

    private static boolean isStoresDataValid(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_STORES_LIST, Context.MODE_PRIVATE);

        long expiryDate = settings.getLong(PREFS_STORES_DATE, 0);
        long unix = new DateTime().getMillis() / 1000;

        return (unix - expiryDate < ONE_DAY_SECONDS);
    }


    /**
     * Get Stores
     * @param context
     * @param callback
     */

    public static void getStores(final Context context, final ExtendedCallback callback){

        if(BuildConfig.PROMOTIONS_API_ENABLED)
            PromotionsService.getInstance(context).getPromotions(new ApiCallback<List<Promotion>>() {
                @Override
                public void onResponse(List<Promotion> promotionList) {
                    getStoreEndPoint(context, callback);
                }

                @Override
                public void onError(String errorMessage) {
                    Log.e(PromotionsService.LOG_ERROR, errorMessage);
                    callback.error(null);
                }

                @Override
                public void onUnauthorized() {
                    onError(context.getString(R.string.errorOccurred));
                }

            });
        else
            getStoreEndPoint(context, callback);
    }

    private static void getStoreEndPoint(final Context context, final ExtendedCallback callback) {
        if(storesCache != null && !storesCache.isEmpty() && isStoresDataValid(context)) {
            callback.run(storesCache);
            return;
        }

        ArrayList<Store> storeList  = Store.restoreList(context);

        if(storeList != null && !storeList.isEmpty()) {
            callback.run(storeList);
            return;
        }

        getStoresFromNetwork(context, callback);
    }

    public static void getStoresFromNetwork(final Context context, final ExtendedCallback callback) {

        API.getStores(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.error(null);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();

                Store.saveList(context, body);
                callback.run(Store.setStores(body));

                response.body().close();
            }
        });
    }


    public static ArrayList<Store> setStores(String body) {

        ArrayList<Store> storeList = new ArrayList<>();

        try {
            JSONObject JSON = new JSONObject(body);


            JSONArray stores = JSON.getJSONArray("stores");

            for (int j = 0; j < stores.length(); j++) {
                JSONObject item = stores.getJSONObject(j);
                Store store = new Store();
                store.setId(item.optString("id"));
                store.setLatitude(item.optDouble("latitude"));
                store.setLongitude(item.optDouble("longitude"));
                store.setState(item.optString("state", ""));
                store.setCity(item.optString("city", ""));
                store.setName(item.optString("name"));
                store.setPhone(item.optString("phone"));
                store.setSchedule(item.optString("timetable"));
                store.setAddress(item.optString("address"));
                store.setPickup(item.optBoolean("pickup", true));
                store.setEcommerce(item.optBoolean("ecommerce", false));
                store.setSalesChannel(item.optString("SC", null));
                store.setZipCode(item.optString("ZIP", null));
                store.setZips(Utils.JSONArrayToArrayList(item.optJSONArray("ZIPList")));

                storeList.add(store);
            }

            storesCache = storeList;

        } catch (JSONException e) {
            //TODO Handle error
            Utils.log(e.getMessage());
        }

        return storeList;
    }
}
