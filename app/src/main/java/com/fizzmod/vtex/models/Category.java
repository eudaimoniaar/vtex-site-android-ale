/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by marcos on 19/01/16.
 */
public class Category {

    public static final String PREFS_NAME_LIST = "categories";
    public static final String PREFS_NAME_VTEX = "categoriesVtex";
    public static final String PREFS_NAME_DATE = "categories_date";

    private String name;
    private Integer id;
    private String URI;
    private Integer group; //group category is a category that does not belong to VTEX, is a client grouping department
    private ArrayList<Category> children;
    private ArrayList<Integer> parentsIds;

    public Category() {
        children = new ArrayList<>();
        parentsIds = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        if(group != null && group == -1)
            group = null;

        this.group = group;
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URL) {
        try {
            String path = new URL(URL).getPath();
            this.URI = path.replaceFirst("/", "");
        } catch (MalformedURLException e) {}
    }

    public Category getChildren(int index) {
        return children.get(index);
    }

    public ArrayList<Category> getChildren() {
        return children;
    }

    public void addChildren(Category children) {
        this.children.add(children);
    }

    public int getCildrenSize(){
        return this.children.size();
    }

    public void addParentId(Integer id){
        if(id != null)
            parentsIds.add(id);
    }

    public String getIdsPath(){
        String path = "";
        int length = parentsIds.size();
        for(int i= 0; i < length; i++){
            path += parentsIds.get(i) + "/";
        }
        return path + id;
    }

    public static Category getCategoryById(ArrayList<Category> categories, int id){
        Category category = null;
        if(categories != null){
            int length = categories.size();
            for(int i= 0; i < length; i++){
                category = categories.get(i);
                Integer categoryId = category.getId();
                if(categoryId != null && categoryId == id)
                    return category;

                Category child = getCategoryById(category.getChildren(), id);
                if(child != null)
                    return child;

            }
        }
        return null;
    }

    public static Category getCategoryByGroup(ArrayList<Category> categories, int group){
        Category category = null;
        if(categories != null){
            int length = categories.size();
            for(int i= 0; i < length; i++){
                category = categories.get(i);
                if(category.getGroup() == group)
                    return category;

            }
        }
        return null;
    }

    public static Category getCategoryByName(ArrayList<Category> categories, ArrayList<String> names, Integer index){
        Category category = null;
        index = index == null ? 0 : index;
        if(categories != null){
            int length = categories.size();
            for(int i= 0; i < length; i++){
                category = categories.get(i);

                if(category.getName().equals(names.get(index).trim()) && index == (names.size() - 1))
                    return category;

                if(category.getName().equals(names.get(index).trim())) {
                    Category child = getCategoryByName(category.getChildren(), names, index + 1);
                    if (child != null)
                        return child;
                }

            }
        }
        return null;
    }

    public static ArrayList<String> buildBreadCrumb(ArrayList<Category> categories, int id){
        ArrayList<String> breadcrumb = new ArrayList<>();

        if(categories != null){
            int length = categories.size();
            for(int i = 0; i < length; i++){
                Category category = categories.get(i);
                Integer categoryId = category.getId();

                if(categoryId != null && categoryId == id)
                    breadcrumb.add(category.getName());

                Category child = getCategoryById(category.getChildren(), id);

                if(child != null){
                    breadcrumb.add(category.getName());
                    breadcrumb.add(child.getName());

                    return breadcrumb;
                }
            }
        }
        return breadcrumb;
    }

    public static void save(Context context, String categories) {
        Category.save(context, categories, PREFS_NAME_LIST);
    }

    public static void save(Context context, String categories, String prefsName){

        if(context == null)
            return;

        SharedPreferences settings = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(prefsName, categories);
        editor.putLong(PREFS_NAME_DATE, new DateTime().getMillis() / 1000);

        // Commit the edits!
        editor.commit();

    }

    public static boolean isSaved(Context context){
        return isSaved(context, PREFS_NAME_LIST);
    }

    public static boolean isSaved(Context context, String prefsName){
        SharedPreferences settings = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);

        long expiracyDate = settings.getLong(PREFS_NAME_DATE, 0);
        long unix = new DateTime().getMillis() / 1000;

        return (unix - expiracyDate) < 86400;
    }

    public static ArrayList<Category> restore(Context context){
        return restore(context, PREFS_NAME_LIST, true);
    }

    public static ArrayList<Category> restore(Context context, String prefsName, boolean filterCategories) {

        SharedPreferences settings = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);

        long expiryDay = settings.getLong(PREFS_NAME_DATE, 0);
        long unix = new DateTime().getMillis() / 1000;

        if (unix - expiryDay < 86400) { //1 day
            if (filterCategories) {
                return Config.getInstance().getFilteredCategories(setCategories(settings.getString(prefsName, ""), prefsName.equals(PREFS_NAME_VTEX)));
            } else
                return setCategories(settings.getString(prefsName, ""), prefsName.equals(PREFS_NAME_VTEX));
        }

        return null;
    }

    public static ArrayList<Category> setCategories(String JSON, boolean fromVtex){
        ArrayList<Category> categoryList = new ArrayList<>();

        try {
            JSONArray categories = new JSONArray(JSON);

            for (int i = 0; i < categories.length(); i++) {
                JSONObject item = categories.getJSONObject(i);

                if(Config.getInstance().getExcludedCategories().contains(item.optInt("id", -1)) || item.getString("name").startsWith("_"))
                    continue;

                JSONArray children = item.getJSONArray("children");

                Category category = parseCategory(item);

                // Second child... really (?)
                for (int j = 0; j < children.length(); j++) {
                    JSONObject secondItem = children.getJSONObject(j);

                    if(Config.getInstance().getExcludedCategories().contains(secondItem.getInt("id")) || secondItem.getString("name").startsWith("_"))
                        continue;

                    JSONArray secondChildren = secondItem.getJSONArray("children");

                    Category categoryFirstChildren = parseCategory(secondItem);
                    categoryFirstChildren.addParentId(category.getId());

                    // Third Child... FOR FUCK'S SAKE MAKE THIS FUCKER RECURSIVE.... It's 3 AM I'm just to tired :P
                    for (int k = 0; k < secondChildren.length(); k++) {
                        JSONObject thirdItem = secondChildren.getJSONObject(k);

                        if(Config.getInstance().getExcludedCategories().contains(thirdItem.getInt("id")) || thirdItem.getString("name").startsWith("_"))
                            continue;

                        Category categorySecondChildren = parseCategory(thirdItem);
                        categorySecondChildren.addParentId(category.getId());
                        categorySecondChildren.addParentId(categoryFirstChildren.getId());

                        JSONArray thirdChildren = thirdItem.getJSONArray("children");


                        // Fourth children - BECAUSE FUCK YOU, THAT'S WHY
                        // http://lmgtfy.com/?q=recursion+101
                        for (int h = 0; h < thirdChildren.length(); h++) {
                            JSONObject fourthItem = thirdChildren.getJSONObject(h);

                            if (Config.getInstance().getExcludedCategories().contains(fourthItem.getInt("id")) || fourthItem.getString("name").startsWith("_"))
                                continue;


                            Category categoryThirdChildren = parseCategory(fourthItem);
                            categoryThirdChildren.addParentId(category.getId());
                            categoryThirdChildren.addParentId(categoryFirstChildren.getId());
                            categoryThirdChildren.addParentId(categorySecondChildren.getId());

                            categorySecondChildren.addChildren(categoryThirdChildren);
                        }

                        //Add second children
                        categoryFirstChildren.addChildren(categorySecondChildren);
                    }

                    //Add first chidren
                    category.addChildren(categoryFirstChildren);
                }
                //Add item to list
                categoryList.add(category);
            }

            DataHolder.getInstance().setCategories(categoryList, fromVtex);

        } catch (JSONException e) {
            Utils.log(e.getMessage());
        }

        return categoryList;
    }

    public static Category parseCategory(JSONObject categoryJson) throws JSONException {

        Category category = new Category();

        Integer group = categoryJson.has("group") ? categoryJson.optInt("group", -1) : null;

        category.setId(group != null && group != -1 ? null : categoryJson.optInt("id"));
        category.setName(categoryJson.getString("name"));
        category.setURI(categoryJson.optString("url", null));
        category.setGroup(group);

        return category;
    }


    /**
     * Get Categories from category API, it may be grouped categories from middleware or vtex categories, depending on the client
     * @param context
     * @param callback
     */

    public static void getCategories(final Context context, final Callback callback) {

        ArrayList<Category> categoryList = DataHolder.getInstance().getCategories(false);

        if(categoryList == null) //restore from file
            categoryList = Category.restore(context);

        if(categoryList != null) {
            callback.run(categoryList);
            return;
        }


        API.getCategories(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {}

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String JSON = response.body().string();

                Category.save(context, JSON);
                callback.run(Category.setCategories(JSON, false));

            }
        });
    }

    /**
     * Get Categories directly from VTEX
     * @param context
     * @param callback
     */

    public static void getVtexCategories(final Context context, final Callback callback){
        ArrayList<Category> categoryList = DataHolder.getInstance().getCategories(true, false);

        if(categoryList == null) //restore from file
            categoryList = Category.restore(context, PREFS_NAME_VTEX, false);

        if(categoryList != null && categoryList.size() != 0) {
            callback.run(categoryList);
            return;
        }

        API.getVtexCategories(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String JSON = response.body().string();
                Category.save(context, JSON, PREFS_NAME_VTEX);
                callback.run(Category.setCategories(JSON, true));

            }
        });
    }
}
