/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by marcos on 04/07/16.
 */
public class Order {
    public final static int STATUS_CANCELLED = 0;
    public final static int STATUS_IN_PROGRESS = 1;
    public final static int STATUS_BILLED = 2;

    private static final Map<String, String> statusList;

    static {
        statusList = new HashMap<>();
        statusList.put("payment-pending", "Pago pendiente");
        statusList.put("payment-approved", "Pago aprobado");
        statusList.put("canceled", "Cancelado");
        statusList.put("cancel", "Cancelación solicitada");
        statusList.put("cancellation-requested", "Cancelación solicitada");
        statusList.put("invoiced", "Facturado");
        statusList.put("shipped", "Enviado");
        statusList.put("window-to-cancel", "Cancelación disponible");
        statusList.put("ready-for-handling", "Iniciando el proceso de entrega");
        statusList.put("start-handling", "Preparando entrega");
        statusList.put("handling", "Preparando entrega");
        statusList.put("shipping-notification", "Entregado");
    }

    public String id;
    public String sequence;
    public String date;
    public String status;
    public String paymentName;
    public String note;

    //User data
    public String userEmail;
    public String userName;
    public String userPhone;

    // Shipping Data
    public String street;
    public String CP;
    public String city;
    public String state;
    public String number;
    public String complement;
    public String neighourhood;
    public String receiverName;
    public String courier;
    public String shippingDateStart;
    public String shippingDateEnd;
    public boolean deliveryWindow;

    public double total;
    public double discounts;
    public double shipping;
    public double subtotal;
    public double tax;
    public double change;
    public ArrayList<Sku> items;

    public Order(){

        this.items = new ArrayList<>();
        this.total = 0;
        this.discounts = 0;
        this.shipping = 0;
        this.subtotal = 0;
        this.tax = 0;
        this.change = 0;

        this.city = "";
        this.street = "";
        this.userEmail = "";
        this.note = null;

        this.street = "";
        this.CP = "";
        this.city = "";
        this.state = "";
        this.number = "";
        this.complement = "";
        this.neighourhood = "";
        this.receiverName = "";
        this.courier = "";
        this.shippingDateStart = "";
        this.shippingDateEnd = "";
    }


    public String getStatus(){
        return statusList.containsKey(status) ? statusList.get(status) : "-";
    }

    public int getStatusColor(){

        if(status.equals("cancel") || status.equals("canceled") || status.equals("cancellation-requested"))
           return STATUS_CANCELLED;

        if(status.equals("invoiced") || status.equals("shipped"))
            return STATUS_BILLED;

        return STATUS_IN_PROGRESS;
    }

    public String getCity(){
        return Utils.flattenToAscii(city).toUpperCase().equals("CIUDAD AUTONOMA BUENOS AIRES") ? "CABA" : city;
    }

    private String fixTimezone(String date){
        if(Utils.isEmpty(date))
            return "-";

        return date.replace("+00:00", Config.getInstance().getTimezoneOffset());
    }

    public String getDeliveryWindow(){

        //DateTimeZone zone = DateTimeZone.forID( "America/Argentina/Buenos_Aires" );
        try{

            DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(fixTimezone(shippingDateStart));
            DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");

            String day = Utils.dayToSpanish(dateTime.getDayOfWeek());
            String month  = Utils.monthToSpanish(dateTime.getMonthOfYear());
            String extra = "";

            if(deliveryWindow) {
                DateTime endDateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(fixTimezone(shippingDateEnd));
                extra = " / " + dateTime.toString(fmt) + " - " + endDateTime.toString(fmt);
            }

            return day + " " + dateTime.getDayOfMonth() + " " + month + extra;

        }catch(IllegalArgumentException e){
            return "-";
        }

    }

}
