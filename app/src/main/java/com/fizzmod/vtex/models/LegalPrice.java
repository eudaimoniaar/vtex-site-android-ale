package com.fizzmod.vtex.models;

import com.fizzmod.vtex.utils.Utils;

/**
 * Created by marcos on 09/08/17.
 */

public class LegalPrice {

    final public static int KEY_REF_ID = 0;
    final public static int KEY_SKU_ID = 1;

    public String unit;
    public double multiplier;

    public LegalPrice(String unit, double multiplier){
        this.multiplier = multiplier;
        this.unit = Utils.isEmpty(unit) ? "UN" : unit;
    }
}
