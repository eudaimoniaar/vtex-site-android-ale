/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import java.util.ArrayList;

/**
 * Created by marcos on 18/06/16.
 */
public class Filter {

    public String name;
    public String rel;
    public ArrayList<String> values;
    public ArrayList<String> texts;

    public Filter(String name, String rel, ArrayList<String> values, ArrayList<String> texts) {
        this.name = name;
        this.rel = rel;
        this.values = values;
        this.texts = texts;
    }
}
