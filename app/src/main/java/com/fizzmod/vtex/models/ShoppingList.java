package com.fizzmod.vtex.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShoppingList {

    private final Integer id;
    private String name;
    @SerializedName("skus")
    private List<SkuItem> skuItems;
    private HashMap<String, Integer> skusMap;

    public ShoppingList(String name) {
        id = null;
        this.name = name;
    }

    public ShoppingList(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public ShoppingList(ShoppingList other) {
        id = other.id;
        name = other.name;
        getSkuItems().addAll(other.getSkuItems());
        getSkusMap().putAll(other.getSkusMap());
    }

    public Integer getId() {
        return id;
    }

    public void setSkus(ShoppingList other) {
        if (other == null)
            return;
        for (SkuItem si : other.getSkuItems())
            getSkusMap().put(si.getSku(), si.getQuantity());
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void deleteProducts(List<String> skusToRemove) {
        final List<SkuItem> toRemove = new ArrayList<>();
        for (SkuItem si : getSkuItems())
            if (skusToRemove.contains(si.getSku()))
                toRemove.add(si);
        getSkuItems().removeAll(toRemove);
    }

    public int getProductQuantity(String sku) {
        if (!getSkusMap().containsKey(sku))
            return 0;

        return getSkusMap().get(sku);
    }

    public List<String> getSkusList() {
        return new ArrayList<>(getSkusMap().keySet());
    }

    public int getTotalProductsQuantity() {
        int count = 0;

        for (Integer skuQuantity : getSkusMap().values())
            count += skuQuantity;

        return count;
    }

    public boolean isEmpty() {
        return getSkusMap().isEmpty();
    }

    private List<SkuItem> getSkuItems() {
        if (skuItems == null)
            skuItems = new ArrayList<>();
        return skuItems;
    }

    private HashMap<String, Integer> getSkusMap() {
        if (skusMap == null)
            skusMap = new HashMap<>();
        return skusMap;
    }

    private class SkuItem {

        /*
         * All fields
         *
         * "sku_list": 1,
         * "sku": 4,
         * "quantity": 4,
         * "user_created": 0,
         * "date_created": 0,
         * "user_modified": null,
         * "data_modified": null
         */

        private final String sku;
        private final Integer quantity;

        private SkuItem(String sku, Integer quantity) {
            this.sku = sku;
            this.quantity = quantity;
        }

        public String getSku() {
            return sku;
        }

        public Integer getQuantity() {
            return quantity;
        }

    }
}
