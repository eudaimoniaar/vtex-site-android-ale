package com.fizzmod.vtex.models;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShoppingListsModifications {

    private final String sku;
    @SuppressLint("UseSparseArrays")
    private final HashMap<Integer, ListData> listsIdsAndQuantities = new HashMap<>();

    public ShoppingListsModifications(String sku, List<ShoppingList> shoppingLists) {
        this.sku = sku;
        for (ShoppingList pl : shoppingLists)
            listsIdsAndQuantities.put(
                    pl.getId(),
                    new ListData( pl.getName(), pl.getProductQuantity( sku ) ) );
    }

    public String getSku() {
        return sku;
    }

    public void incrementQuantity(Integer id) {
        if(listsIdsAndQuantities.containsKey(id))
            listsIdsAndQuantities.get(id).add(1);
    }

    public boolean decrementQuantity(Integer id) {
        return listsIdsAndQuantities.containsKey(id) && listsIdsAndQuantities.get(id).add(-1);
    }

    public List<Integer> getModifiedIds() {
        List<Integer> modifiedIds = new ArrayList<>();
        for (Integer id : listsIdsAndQuantities.keySet())
            if (listsIdsAndQuantities.get(id).wasModified())
                modifiedIds.add(id);
        return modifiedIds;
    }

    public List<String> getNames(List<Integer> ids) {
        final List<String> names = new ArrayList<>();
        for (Integer id : ids)
            if (listsIdsAndQuantities.containsKey(id))
                names.add(listsIdsAndQuantities.get(id).getName());
        return names;
    }

    public Integer getQuantity(Integer id) {
        return listsIdsAndQuantities.containsKey(id) ?
                listsIdsAndQuantities.get(id).getModifiedQuantity() :
                0;
    }

    public boolean listWasModified(Integer id) {
        return listsIdsAndQuantities.containsKey(id) && listsIdsAndQuantities.get(id).wasModified();
    }

    private class ListData {

        private final String name;
        private final Integer originalQuantity;
        private Integer modifiedQuantity;

        private ListData(String name, Integer quantity) {
            this.name = name;
            originalQuantity = quantity;
            modifiedQuantity = quantity;
        }

        String getName() {
            return name;
        }

        Integer getModifiedQuantity() {
            return modifiedQuantity;
        }

        /**
         * @return True if success; False otherwise.
         */
        boolean add(Integer quantity) {
            modifiedQuantity += quantity;
            if (modifiedQuantity < 0) {
                modifiedQuantity = 0;
                return false;
            }
            return true;
        }

        boolean wasModified() {
            return !originalQuantity.equals(modifiedQuantity);
        }
    }
}
