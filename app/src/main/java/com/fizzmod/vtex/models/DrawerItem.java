/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

public class DrawerItem {

    public int icon;
    public int iconOff;
    public int textId;
    public String fragmentTag;
    public boolean enabled;
    public boolean isSubitem;

    // Constructor.
    public DrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem) {

        this.icon = icon;
        this.iconOff = iconOff;
        this.textId = textId;
        this.fragmentTag = fragmentTag;
        this.enabled = false;
        this.isSubitem = isSubitem;
    }

    public DrawerItem(int icon, int iconOff, int textId, String fragmentTag, boolean isSubitem, boolean enabled) {
        this(icon, iconOff, textId, fragmentTag, isSubitem);
        this.enabled = enabled;
    }



}