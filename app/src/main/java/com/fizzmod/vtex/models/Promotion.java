package com.fizzmod.vtex.models;

import android.support.annotation.NonNull;

import com.fizzmod.vtex.config.Config;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Promotion implements Comparable<Promotion>{

    public static final String LOG_ERROR = "PROMOTION ERROR";
    private static final char LABEL_SEPARATOR = '-';
    private static final String PROMOTION_SEPARATOR = ":";
    private static final Map<String, Integer> hashImage = Config.getInstance().getClientPromosMap();

    private String
            name,
            labelName,
            type,
            discountType,
            priority,
            code;
    private Integer
            labelPriority;
    private Float value;
    private Date
            start,
            end;
    private Boolean
            cumulative;
    private List<String>
            products,
            skus,
            categories,
            brands;
    @SerializedName("exclude_skus")
    private List<String> excludedSkus;
    @SerializedName("sales_channels")
    private List<String> salesChannels;
    @SerializedName("skus_group_2")
    private List<String> skusGroup2;
    @SerializedName("exclude_categories")
    private List<String> excludedCategories;
    @SerializedName("sku_prefixes")
    private List<String> skuPrefixes;
    @SerializedName("exclude_sku_prefixes")
    private List<String> excludedSkuPrefixes;

    public Promotion() {
        labelPriority = 99;
    }

    //GETS
    public List<String> getProducts() {
        return products;
    }

    public List<String> getSkus() {
        return skus;
    }

    public List<String> getBrands() {
        return brands;
    }

    public List<String> getExcludedSkus() {
        return excludedSkus;
    }

    public List<String> getSalesChannels() {
        return salesChannels;
    }

    public List<String> getSkusGroup2() {
        return skusGroup2;
    }

    public List<String> getExcludedCategories() {
        return excludedCategories;
    }

    public List<String> getSkuPrefixes() {
        return skuPrefixes;
    }

    public List<String> getExcludedSkuPrefixes() {
        return excludedSkuPrefixes;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDiscountType() {
        return discountType;
    }

    public Float getValue() {
        return value;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public Integer getPriority() {
        return Integer.valueOf(priority);
    }

    public Boolean isCumulative() {
        return cumulative;
    }

    public String getCode() {
        return code;
    }

    //CONTAINS
    public boolean filtersBySku() {
        return skus != null && !skus.isEmpty();
    }

    public boolean hasSku(String sku) {
        return skus != null && (skus.contains(sku));
    }

    public boolean filtersByCategory() {
        return categories != null && !categories.isEmpty();
    }

    //the product categoriesId comes with this format: /343/113/342
    public boolean hasCategory(String categoriesId) {
        if (categories == null || categoriesId.isEmpty())
            return false;

        List<String> ids = Arrays.asList(categoriesId.split("/"));

        for (String category : categories)
            if (ids.contains(category))
                return true;

        return false;
    }

    public boolean filtersBySalesChannel() {
        return salesChannels != null && !salesChannels.isEmpty();
    }

    public boolean hasSalesChannel(String salesChannel) {
        return salesChannels == null || salesChannels.isEmpty() || salesChannels.contains(salesChannel);
    }

    public boolean hasBrand(Integer brandId) {
        String brand = "";
        if(brandId != null){
            brand = brandId.toString();
        }
        return brands != null && brands.contains(brand);
    }

    public boolean hasBrand(String brand) {
        return brands != null && brands.contains(brand);
    }

    public boolean hasSkuGroup2(String skuId) {
        return skusGroup2 != null && skusGroup2.contains(skuId);
    }

    public boolean hasProductRefId(String refId) {
        return products != null && products.contains(refId);
    }

    public boolean hasExcludedSku(String skuId) {
        return excludedSkus != null && excludedSkus.contains(skuId);
    }

    //the product categoriesId comes with this format: /343/113/342
    public boolean hasExcludedCategory(String categoriesId) {
        if (excludedCategories == null || categoriesId.isEmpty())
            return false;

        List<String> ids = Arrays.asList(categoriesId.split("/"));

        for (String category : excludedCategories)
            if (ids.contains(category))
                return true;

        return false;
    }

    /**
     * The name can be the promotion, or a string that contains "{some_text}:{promotion_name}"
     * */
    public int getPromotionImageResource() throws NullPointerException{
        String key = name.toLowerCase().trim();

        if (hashImage.containsKey(key))
            return hashImage.get(key);

        if (key.contains(PROMOTION_SEPARATOR)) {
            String[] parts = key.split(PROMOTION_SEPARATOR);
            if (parts.length == 2 && hashImage.containsKey(parts[1]))
                return hashImage.get(parts[1]);
        }

        throw new NullPointerException("hash key doesn't exist");
    }

    public boolean hasLabel() {
        return name.contains("label");
    }

    public void parseLabel() {
        if (labelName != null)
            return;
        labelName = name.substring(name.lastIndexOf(LABEL_SEPARATOR) + 1);
        String lblPriority = searchPriorityLabel();
        if (!lblPriority.equals(""))
            labelPriority = Integer.parseInt(lblPriority);
        else
            labelPriority = 99; //gets the last priority.
    }

    public String getLabelName() {
        return labelName;
    }

    public Integer getLabelPriority() {
        return labelPriority;
    }

    // Returns the 2 characters before the label separator because the priority have a format like this: 01. 1 > 2
    private String searchPriorityLabel() {
        String curedName = name.replace(" ", "");
        int hyphenQuantity = StringUtils.countMatches(curedName, Character.toString(LABEL_SEPARATOR));

        if (hyphenQuantity < 2) {
            int posA = curedName.indexOf(LABEL_SEPARATOR);
            return posA == -1 ? "" : curedName.substring(posA - 2, posA);
        }
        return "";
    }

    @Override
    public int compareTo(@NonNull Promotion promotion) {
        return this.getPriority() - promotion.getPriority();    //ascending order
    }

    public boolean isCurrent() {
        Date currentDate = new Date();
        return currentDate.compareTo(getStart()) >= 0 && currentDate.compareTo(getEnd()) < 0;
    }

}
