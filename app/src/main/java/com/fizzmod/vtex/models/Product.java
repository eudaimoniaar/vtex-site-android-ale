/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.utils.API;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Marcos Casagrande on 06/01/16.
 */
public class Product implements Serializable {

    private static final String PREFS_NAME = "product";

    public String name;
    public String id;
    public String description;
    public String image;
    public String brand;
    public int brandId;
    public String url;
    public String refId;
    public String categoriesId;
    public ArrayList<Sku> skus;
    private TreeMap<String, String> specifications;
    public float bestPrice;
    public float listPrice;
    public int quantity;
    public ArrayList<String> categories;

    /**This constructor was created for testing*/
    public Product(String name, String id, String description, String image, String brand, int quantity, int brandId){
        this.name = name;
        this.id = id;
        this.description = description;
        this.image = image;
        this.brand = brand;
        this.quantity = quantity;
        this.brandId = brandId;
    }

    public Product(){
        this.name = null;
        this.id = null;
        this.description = null;
        this.image = null;
        this.quantity = 0;
        this.categories = new ArrayList<>();
        this.categoriesId = null;
        this.skus = new ArrayList<>();
        this.specifications = new TreeMap<>();
    }

    public boolean hasOutdatedPromotion() {
        for (Sku sku : skus)
            if (sku.hasOutdatedPromotionsInfo())
                return true;
        return false;
    }

    public interface OnProductItemClickListener {
        void onItemClick(Product item);
    }

    /**
     * Setters
     */

    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addSku(Sku sku){
        this.skus.add(sku);
    }

    public void setCategoriesId(String categoryId) {
        this.categoriesId = categoryId;
    }
    /**
     * Getters
     */

    public String getName(){
        return this.name;
    }
    public String getDescription(){
        return this.description;
    }
    public String getId(){
        return this.id;
    }
    public String getRefId() { return refId; }
    public String getImage(){
        return getImage(500,500);
    }
    public String getImage(int width, int height){
        return API.resizeImage(this.image, width, height);
    }

    public String getCategoriesId() {
        return categoriesId;
    }

    public float getBestPrice(){
        return this.bestPrice;
    }
    public float getListPrice(){
        return this.listPrice;
    }

    public Sku getSku(int pos) {
        if(this.skus != null && this.skus.size() > 0)
            return this.skus.get(pos);
        return null;
    }

    /**
     * TODO: Walmart uses only one Sku per Product.
     * TODO: This method should be refactored when implementing a client that
     * TODO: uses more than one Sku per Product.
     */
    public Sku getMainSku() {
        return getSku(0);
    }

    public Sku getSkuById(String id){
        int length = this.skus.size();
        if(this.skus != null && length > 0){
            for(int i = 0; i < length; i++) {
                Sku sku = this.skus.get(i);
                if(sku.getId().equals(id))
                    return sku;
            }
        }
        return null;
    }

    public int getSkuLength(){
        return this.skus.size();
    }

    public ArrayList<Sku> getSkus() {
        return skus;
    }

    public void setSkus(ArrayList<Sku> skus) {
        this.skus = skus;
    }

    public TreeMap<String, String> getSpecifications() {
        // Original code:
        // return specifications;

        // Temp code to filter specs for Devoto
        if (!BuildConfig.TITLE.equals("Devoto") && !BuildConfig.TITLE.equals("Disco"))
            return specifications;
        else {
            specifications.remove("Precio Dolar");
            specifications.remove("aceptaCuotas");
            specifications.remove("cucarda1");
            specifications.remove("cucarda1-disco");
            specifications.remove("cucarda2");
            specifications.remove("cucarda2-disco");
            specifications.remove("cucarda3");
            specifications.remove("cucarda3-disco");
            specifications.remove("multiplicador tipo fiambre");

            return specifications;
        }
    }

    public void addSpecifications(String key, String value) {
        if(!key.startsWith("_")){
            this.specifications.put(key, value);
        }
    }

    public void setBestPrice(float bestPrice) {
        this.bestPrice = bestPrice;
    }

    public void setListPrice(float listPrice) {
        this.listPrice = listPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        //sometimes a category has a "/" in it's name, so replace it with ||| to avoid incorrect split
        categories = categories.replaceAll(" +/ +", "|||");
        this.categories.addAll(Arrays.asList(categories.substring(1).split("\\s*/\\s*")));
        int length = this.categories.size();
        for(int i= 0; i < length; i++){
            String value = this.categories.get(i).replace("|||", " / ");
            this.categories.set(i, value);
        }
    }

    public boolean hasDescription(){
        return this.description != null && !this.description.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        return  (o instanceof Product) && ((Product) o).getName().equals(this.getName());
    }

    /**
     * Save cart data to SharedPreferences
     * @param context
     */
    public void save(Context context, String skuId, int quantity){

        try {
            String value = "";

            JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("sku", skuId);
            json.put("quantity", quantity);

            value = json.toString();

            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(PREFS_NAME, value);

            // Commit the edits!
            editor.commit();
        } catch (JSONException e) {}
    }

    /**
     * Restore cart data from SharedPreferences
     * @param context
     * @param callback
     */
    public static void restore(Context context, final Callback callback){

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        try {

            JSONObject json = new JSONObject(settings.getString(PREFS_NAME, ""));

            if(json == null || json.length() == 0){
                callback.run(null);
                return;
            }

            HashMap<String, String> params = new HashMap<>();
            final HashMap<String, Integer> skus = new HashMap<>();

            params.put("fq", "skuId:" + json.getString("sku"));
            skus.put(json.getString("sku"), json.getInt("quantity"));

            API.search(context, null, params, 1, API.SOFT_LIMIT, new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    callback.run(null);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    ArrayList<Product> productList = API.setProductList(response.body().string());

                    Product product = productList.get(0);

                    Set<Map.Entry<String, Integer>> entrySet = skus.entrySet();
                    for (Map.Entry<String, Integer> entry : entrySet) {
                        Sku sku = product.getSkuById(entry.getKey());
                        if (sku != null) {
                            sku.setSelectedQuantity(entry.getValue());
                        }
                    }

                    callback.run(product);
                }
            });

        } catch (JSONException e) {
            callback.run(null);
        }

    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
}
