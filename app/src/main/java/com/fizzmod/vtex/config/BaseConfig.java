package com.fizzmod.vtex.config;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.currency.CurrencyConverter;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.LegalPrice;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public abstract class BaseConfig {

    private Locale locale = new Locale("es", "AR");
    private double latitude = -37.3268222;
    private double longitude = -61.2157049;
    private String contactURL = "";
    private String termsUrl = "";
    private String cardRequestUrl = "";
    private String dealsCollection = "";
    private String dealsQACollection = "";
    private String bestSellingCollection = "";
    private String bestSellingQACollection = "";
    private String ourBrandsCollection = "";
    private String ourBrandsQACollection = "";

    private String timezoneOffset = "-03:00";
    private double listPriceThreshold = 10;
    private int highlightPriceThreshold = 10;

    private int sliderWidth = 999;
    private int sliderHeight = 600;

    /**
     * enable/disable adding product from product list
     **/
    private boolean fastAdd = true;

    /**
     * enable/disable code scanner
     **/
    private boolean hasScanner = true;

    /**
     * Disable/enable tax
     **/
    private boolean hasTax = false;

    /**
     * Whether to show refId instead of brand in productList
     **/
    private boolean showRefIDInProductList = false;

    /**
     * Enable/Disable store selection
     **/
    private boolean hasMultipleSalesChannels = false;

    /**
     * If category grouping is true, the category tree will not be retrieved directly from VTEX
     **/
    private boolean categoryGrouping = false;

    /**
     * Prefetch SKU images in product page. Setting to false will reduce memory footprint
     **/
    private boolean preFetchSkuImages = false;

    /**
     * To apply specific configuration like orderId only for geant flavors
     **/
    private boolean isGeantConfig = false;

    /**
     * Show total weight instead of total count of a product
     **/
    private boolean showProductWeight = false;

    private String homeTopBanner = null;
    private String homeBottomBanner = null;

    private List<String> excludedSpecifications = Arrays.asList(
            "Descuento Asociados", "Precio Legal", "Producto pesable", "Multiplicador", "Minicart Suggestions",
            "catTotem", "subcatTotem"
    );

    private List<Integer> excludedCategories = new ArrayList<>();

    private CurrencyConverter currencyConverter;

    protected BaseConfig() { }

    public void formatPrice(final double price, boolean convertCurrency, final TypedCallback<String> callback) {
        if (currencyConverter == null || !convertCurrency) {
            callback.run(formatPrice(price));
        } else {
            currencyConverter.convertCurrency(price, new TypedCallback<String>() {
                @Override
                public void run(String convertedCurrency) {
                    callback.run(convertedCurrency);
                }
            });
        }
    }

    public String formatPrice(double price) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        return currencyFormatter.format(price);
    }

    /**
     * Legal Price key
     **/

    private int legalPriceKey = LegalPrice.KEY_REF_ID;

    public void baseUrlChanged(String newBaseUrl) {
        // Nothing to do.
    }

    public Map<String, Integer> getClientPromosMap() {
        return new HashMap<>();
    }

    public HashMap<String, String> getExtraHeaders() {
        return new HashMap<>();
    }

    public abstract String getDevFlavorName();

    public String getOurBrandsCollection() {
        return BuildConfig.FLAVOR.equals(getDevFlavorName()) ? ourBrandsQACollection : ourBrandsCollection;
    }

    public String getDealsCollection() {
        return BuildConfig.FLAVOR.equals(getDevFlavorName()) ? dealsQACollection : dealsCollection;
    }

    public String getBestSellingCollection() {
        return BuildConfig.FLAVOR.equals(getDevFlavorName()) ? bestSellingQACollection : bestSellingCollection;
    }

    public ArrayList<Category> getFilteredCategories(ArrayList<Category> categories) {
        return categories;
    }

    public boolean isFastAdd() {
        return fastAdd;
    }

    protected void setFastAdd(boolean fastAdd) {
        this.fastAdd = fastAdd;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    protected void setTimezoneOffset(String timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public double getListPriceThreshold() {
        return listPriceThreshold;
    }

    protected void setListPriceThreshold(double listPriceThreshold) {
        this.listPriceThreshold = listPriceThreshold;
    }

    public int getHighlightPriceThreshold() {
        return highlightPriceThreshold;
    }

    protected void setHighlightPriceThreshold(int highlightPriceThreshold) {
        this.highlightPriceThreshold = highlightPriceThreshold;
    }

    public int getSliderWidth() {
        return sliderWidth;
    }

    protected void setSliderWidth(int sliderWidth) {
        this.sliderWidth = sliderWidth;
    }

    public int getSliderHeight() {
        return sliderHeight;
    }

    protected void setSliderHeight(int sliderHeight) {
        this.sliderHeight = sliderHeight;
    }

    public boolean isHasScanner() {
        return hasScanner;
    }

    protected void setHasScanner(boolean hasScanner) {
        this.hasScanner = hasScanner;
    }

    public boolean hasTax() {
        return hasTax;
    }

    protected void setHasTax(boolean hasTax) {
        this.hasTax = hasTax;
    }

    public boolean isShowRefIDInProductList() {
        return showRefIDInProductList;
    }

    protected void setShowRefIDInProductList(boolean showRefIDInProductList) {
        this.showRefIDInProductList = showRefIDInProductList;
    }

    public boolean hasMultipleSalesChannels() {
        return hasMultipleSalesChannels;
    }

    protected void setHasMultipleSalesChannels(boolean hasMultipleSalesChannels) {
        this.hasMultipleSalesChannels = hasMultipleSalesChannels;
    }

    public boolean isCategoryGrouping() {
        return categoryGrouping;
    }

    protected void setCategoryGrouping(boolean categoryGrouping) {
        this.categoryGrouping = categoryGrouping;
    }

    public boolean isPreFetchSkuImages() {
        return preFetchSkuImages;
    }

    protected void setPreFetchSkuImages(boolean preFetchSkuImages) {
        this.preFetchSkuImages = preFetchSkuImages;
    }

    public String getHomeTopBanner() {
        return homeTopBanner;
    }

    protected void setHomeTopBanner(String homeTopBanner) {
        this.homeTopBanner = homeTopBanner;
    }

    public String getHomeBottomBanner() {
        return homeBottomBanner;
    }

    protected void setHomeBottomBanner(String homeBottomBanner) {
        this.homeBottomBanner = homeBottomBanner;
    }

    public List<String> getExcludedSpecifications() {
        return excludedSpecifications;
    }

    protected void setExcludedSpecifications(List<String> excludedSpecifications) {
        this.excludedSpecifications = excludedSpecifications;
    }

    public List<Integer> getExcludedCategories() {
        return excludedCategories;
    }

    protected void setExcludedCategories(List<Integer> excludedCategories) {
        this.excludedCategories = excludedCategories;
    }

    public int getLegalPriceKey() {
        return legalPriceKey;
    }

    protected void setLegalPriceKey(int legalPriceKey) {
        this.legalPriceKey = legalPriceKey;
    }

    public Locale getLocale() {
        return locale;
    }

    protected void setLocale(Locale locale) {
        this.locale = locale;
    }

    public double getLatitude() {
        return latitude;
    }

    protected void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    protected void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getContactURL() {
        return contactURL;
    }

    protected void setContactURL(String contactURL) {
        this.contactURL = contactURL;
    }

    public String getTermsUrl() {
        return termsUrl;
    }

    protected void setTermsUrl(String termsUrl) {
        this.termsUrl = termsUrl;
    }

    public String getCardRequestUrl() {
        return cardRequestUrl;
    }

    protected void setCardRequestUrl(String cardRequestUrl) {
        this.cardRequestUrl = cardRequestUrl;
    }

    protected void setDealsCollection(String dealsCollection) {
        this.dealsCollection = dealsCollection;
    }

    protected void setBestSellingCollection(String bestSellingCollection) {
        this.bestSellingCollection = bestSellingCollection;
    }

    protected void setOurBrandsCollection(String ourBrandsCollection) {
        this.ourBrandsCollection = ourBrandsCollection;
    }

    protected void setOurBrandsQACollection(String ourBrandsQACollection) {
        this.ourBrandsQACollection = ourBrandsQACollection;
    }

    protected void setDealsQACollection(String dealsQACollection) {
        this.dealsQACollection = dealsQACollection;
    }

    protected void setBestSellingQACollection(String bestSellingQACollection) {
        this.bestSellingQACollection = bestSellingQACollection;
    }

    protected void setCurrencyConverter(CurrencyConverter currencyConverter) {
        this.currencyConverter = currencyConverter;
    }

    public CurrencyConverter getCurrencyConverter() {
        return currencyConverter;
    }

    public boolean usesOtherCurrency(JSONObject productJsonObject) {
        return false;
    }

    public boolean doesCheckForUpdates() {
        return false;
    }

    public boolean isGeantConfig() {
        return isGeantConfig;
    }

    protected void setGeantConfig(boolean geantConfig) {
        isGeantConfig = geantConfig;
    }

    protected void setShowProductWeight(boolean showProductWeight) {
        this.showProductWeight = showProductWeight;
    }

    public boolean showProductWeight() {
        return showProductWeight;
    }
}
