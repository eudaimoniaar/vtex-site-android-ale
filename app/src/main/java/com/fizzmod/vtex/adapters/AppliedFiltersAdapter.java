package com.fizzmod.vtex.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.AppliedFiltersAdapterViewHolder;

import java.util.ArrayList;
import java.util.List;

public class AppliedFiltersAdapter extends RecyclerView.Adapter<AppliedFiltersAdapterViewHolder> {

    private final List<Item> items = new ArrayList<>();
    private AppliedFiltersAdapterViewHolder.FilterDeselectionListener listener;

    public AppliedFiltersAdapter(AppliedFiltersAdapterViewHolder.FilterDeselectionListener listener) {
        this.listener = listener;
    }

    @Override
    public AppliedFiltersAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AppliedFiltersAdapterViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(AppliedFiltersAdapterViewHolder holder, int position) {
        Item item = items.get(position);
        holder.bindView(item.text, item.value, item.position, item.filterPosition);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addItem(String text, String value, Integer position, Integer filterPosition, boolean notifyDataChanged) {
        items.add(new Item(text, value, position, filterPosition));
        if (notifyDataChanged)
            notifyItemInserted(items.size() - 1);
    }

    public void removeItem(String value) {
        int i = items.indexOf(new Item(value));
        if (i == -1)
            return;
        items.remove(i);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        items.clear();
        notifyDataSetChanged();
    }

    private class Item {
        private String text;
        private String value;
        private Integer position;
        private Integer filterPosition;

        private Item(String text, String value, Integer position, Integer filterPosition) {
            this.text = text;
            this.value = value;
            this.position = position;
            this.filterPosition = filterPosition;
        }

        private Item(String value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Item)
                return value.equals(((Item) o).value);
            return super.equals(o);
        }
    }

}
