package com.fizzmod.vtex.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.PagerAdapter;

public class BasePagerAdapterViewHolder extends RecyclerView.ViewHolder {

    protected TextView textView;

    protected BasePagerAdapterViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_page, parent, false));
        textView = (TextView) itemView.findViewById(R.id.pager_text);
    }

    public void bindView(final int pageNumber,
                         boolean isCurrentPage,
                         boolean isLastPage,
                         final PagerAdapter.OnPageClickListener listener) {
        textView.setText(String.valueOf(pageNumber));
        bindView(isCurrentPage);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPageClick(pageNumber);
            }
        });
    }

    public void bindView(boolean isCurrentPage) {
        textView.setBackgroundResource( isCurrentPage ?
                R.drawable.button_page_active :
                R.drawable.button_page);
        textView.setTextColor( itemView.getContext().getResources().getColor(
                isCurrentPage ? R.color.pageButtonActiveText : R.color.pageTextColor ) );
    }
}
