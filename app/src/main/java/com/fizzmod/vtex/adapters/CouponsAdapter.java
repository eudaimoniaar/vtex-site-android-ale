package com.fizzmod.vtex.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.CouponHeaderViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CouponStatusHeaderViewHolder;
import com.fizzmod.vtex.adapters.viewholders.CouponViewHolder;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CouponsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements CouponStatusHeaderViewHolder.Listener {

    public final static int HEADERS_COUNT = 2;

    private final static int FLAG_COUPON_ACTIVATED = 0;
    private final static int FLAG_COUPONS_FILTERED = 1;

    private final List<Coupon> filteredCoupons = new ArrayList<>();
    private List<Coupon> coupons;
    private CouponsSelectionListener listener;

    private boolean physicalPrintEnabled = false;
    private final int HEADER_VIEW_TYPE = 0;
    private final int STATUS_HEADER_VIEW_TYPE = 1;
    private final int COUPON_VIEW_TYPE = 2;

    public CouponsAdapter(List<Coupon> coupons, CouponsSelectionListener listener) {
        this.coupons = coupons;
        filteredCoupons.addAll(coupons);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return HEADER_VIEW_TYPE;
            case 1:
                return STATUS_HEADER_VIEW_TYPE;
            default:
                return COUPON_VIEW_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_VIEW_TYPE:
                return new CouponHeaderViewHolder(parent, listener);
            case STATUS_HEADER_VIEW_TYPE:
                return new CouponStatusHeaderViewHolder(parent, listener, this);
            default:
                return new CouponViewHolder(parent, filteredCoupons, listener);
        }
    }

    @Override
    public int getItemCount() {
        // This adapter implements HEADERS_COUNT more views so you have to add it to the total item count
        return filteredCoupons.size() + HEADERS_COUNT;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case HEADER_VIEW_TYPE:
                break;
            case STATUS_HEADER_VIEW_TYPE:
                CouponStatusHeaderViewHolder status = (CouponStatusHeaderViewHolder) viewHolder;
                status.bindView(coupons, physicalPrintEnabled);
                break;
            default:
                CouponViewHolder holder = (CouponViewHolder) viewHolder;
                // The same reason as before, the coupons position got pushed back by HEADERS_COUNT
                holder.bindView(filteredCoupons.get(position - HEADERS_COUNT), position - HEADERS_COUNT);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (holder.getItemViewType() == STATUS_HEADER_VIEW_TYPE && !payloads.isEmpty()) {
            switch ((int) payloads.get(0)) {
                case FLAG_COUPON_ACTIVATED:
                    ((CouponStatusHeaderViewHolder) holder).updateCounts(coupons);
                    break;
                case FLAG_COUPONS_FILTERED:
                    ((CouponStatusHeaderViewHolder) holder).couponsFilterApplied(filteredCoupons.isEmpty());
                    break;
            }
        } else
            super.onBindViewHolder(holder, position, payloads);
    }

    public void setPhysicalPrintEnabled(boolean physicalPrintEnabled) {
        this.physicalPrintEnabled = physicalPrintEnabled;
        notifyDataSetChanged();
    }

    public boolean getPhysicalPrintEnabled() {
        return physicalPrintEnabled;
    }

    public void updateCoupons(List<Coupon> coupons) {
        //Just update everything if there are new coupons
        if (this.coupons.size() != coupons.size()) {
            this.coupons = coupons;
            filteredCoupons.addAll(coupons);
            notifyDataSetChanged();
            return;
        }

        //Checks if coupon is the same on every step, see Coupon.java for equal condition
        for (int i = 0; i < coupons.size(); i++) {
            if (!this.coupons.get(i).equals(coupons.get(i))) {
                this.coupons.set(i, coupons.get(i));
                filteredCoupons.addAll(this.coupons);
                notifyItemChanged(i);
                return;
            }
        }
    }

    public void notifyCouponActivated(int position) {
        notifyItemChanged(position + HEADERS_COUNT);
        notifyItemChanged(1, FLAG_COUPON_ACTIVATED);                // Update status header's data
    }

    /* *************************************** *
     *  CouponStatusHeaderViewHolder.Listener  *
     * *************************************** */

    @Override
    public void performFiltering(String text) {
        int prevCount = filteredCoupons.size();
        filteredCoupons.clear();
        if (Utils.isEmpty(text))
            filteredCoupons.addAll(coupons);
        else
            for (Coupon coupon : coupons)
                if (coupon.hasTitle(text))
                    filteredCoupons.add(coupon);
        // The same reason as before, the coupons position got pushed back by HEADERS_COUNT
        notifyItemRangeRemoved(HEADERS_COUNT, prevCount);
        notifyItemChanged(1, FLAG_COUPONS_FILTERED);
    }
}
