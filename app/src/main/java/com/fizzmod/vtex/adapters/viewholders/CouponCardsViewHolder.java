package com.fizzmod.vtex.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.CouponsCardsListener;
import com.fizzmod.vtex.service.response.CIResponse;

public class CouponCardsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CIResponse ci;
    private TextView cardId;
    private TextView cardBrand;
    private CouponsCardsListener listener;

    public CouponCardsViewHolder(ViewGroup parent, CouponsCardsListener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_card, parent, false));
        this.listener = listener;
        cardId = itemView.findViewById(R.id.coupon_card_id);
        cardBrand = itemView.findViewById(R.id.coupon_card_brand);
        itemView.setOnClickListener(this);
    }

    public void bindView(CIResponse ci) {
        this.ci = ci;
        cardId.setText(ci.getCardNumber());
        cardBrand.setText(ci.getProgramNumber());
    }

    /*******************
     * OnClickListener *
     *******************/

    @Override
    public void onClick(View v) {
        listener.onCardClicked(ci);
    }
}
