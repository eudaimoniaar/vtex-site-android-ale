package com.fizzmod.vtex.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.BaseCouponsListener;

public class CouponHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private LinearLayout couponsChangeAccount;
    private BaseCouponsListener couponsListener;

    public CouponHeaderViewHolder(ViewGroup parent, BaseCouponsListener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_coupons_header, parent, false));
        couponsListener = listener;
        couponsChangeAccount = itemView.findViewById(R.id.coupons_change_account_wrapper);
        couponsChangeAccount.setVisibility(View.VISIBLE);
        TextView couponsChangeAccountButton = itemView.findViewById(R.id.coupons_change_account_button);
        couponsChangeAccountButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        couponsChangeAccount.setVisibility(View.GONE);
        couponsListener.onChangeAccount();
    }
}
