package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ProductItemPricesLayout;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class BaseGridAdapterViewHolder implements View.OnClickListener {

    protected ProductListCallback clickListener;
    protected OnFragmentInteractionListener fragmentInteractionListener;

    private ProductItemPricesLayout productItemPricesLayout;

    private TextView productTitle;
    private TextView productBrand;
    private TextView priceHighlight;
    private TextView inCartFlagText;

    private LinearLayout inCartFlag;
    private View progress;
    private LinearLayout linearPromotion;

    protected View itemBuyButton;

    private ImageView image;

    private int imageSize;

    private Context context;

    protected Sku sku;
    protected Product product;

    BaseGridAdapterViewHolder(Context context, View convertView, final ProductListCallback clickListener) {
        this.context = context;
        this.clickListener = clickListener;
        productTitle = convertView.findViewById(R.id.productTitle);
        productBrand = convertView.findViewById(R.id.productBrand);
        inCartFlag = convertView.findViewById(R.id.productInCartFlag);
        inCartFlagText = convertView.findViewById(R.id.productInCartFlagText);
        progress = convertView.findViewById(R.id.imageLoading);
        linearPromotion = convertView.findViewById(R.id.product_item_promotion_layout);
        image = convertView.findViewById(R.id.productImage);
        priceHighlight = convertView.findViewById(R.id.productPriceHighlight);
        itemBuyButton = convertView.findViewById(R.id.itemBuyButton);
        imageSize = (int) context.getResources().getDimension(R.dimen.listImageSize);
        productItemPricesLayout = convertView.findViewById(R.id.productItemPricesAndPromos);

        if (Config.getInstance().isFastAdd())
            itemBuyButton.setOnClickListener(this);
    }

    public void setView(int position, Product product) {

        itemBuyButton.setTag(R.id.TAG_ADAPTER_ITEM, position);
        this.product = product;

        sku = product.getMainSku();

        Sku skuInCart = Cart.getInstance().getById(sku.getId());

        productTitle.setText(product.getName());
        productBrand.setText(Config.getInstance().isShowRefIDInProductList() ? sku.getRefId() : product.getBrand());

        if (sku.hasStock()) {
            sku.getBestPriceFormatted(new TypedCallback<String>() {
                @Override
                public void run(String price) {
                    productItemPricesLayout.setBestPriceText(price);
                }
            });
            if (sku.showListPrice()) {
                sku.getListPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        productItemPricesLayout.showListPrice(price);
                    }
                });
            } else
                productItemPricesLayout.hideListPrice();
            if (sku.showPriceDiffPercentage()) {
                priceHighlight.setVisibility(View.VISIBLE);
                priceHighlight.setText(sku.getPriceDiffPercentageFormatted(context));
            } else
                priceHighlight.setVisibility(View.GONE);

            if (sku.showPriceByUnit()) {
                sku.getPriceByUnitFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        productItemPricesLayout.showPriceByUnit( context.getString( R.string.product_price_by_unit, price ) );
                    }
                });
            } else {
                productItemPricesLayout.hidePriceByUnit();
            }
        } else {
            productItemPricesLayout.onNoStock();
            priceHighlight.setVisibility(View.GONE);
        }

        if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasCurrentPromotions()) {
            if (linearPromotion.getChildCount() > 0)
                linearPromotion.removeAllViews();
            linearPromotion.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 1);
            //find and set the image with the same name.
            for (Promotion promotion : sku.getPromotionList()) {
                try {
                    ImageView imageView = new ImageView(context);
                    imageView.setImageResource(promotion.getPromotionImageResource());
                    imageView.setLayoutParams(lp);
                    linearPromotion.addView(imageView);
                } catch (NullPointerException exception) {
                    Log.e(Promotion.LOG_ERROR, exception.toString());
                }
            }
        } else {
            linearPromotion.setVisibility(View.GONE);
        }

        if (skuInCart != null) {
            setQuantity(skuInCart.getSelectedQuantity());
            inCartFlag.setVisibility(View.VISIBLE);
        } else
            inCartFlag.setVisibility(View.INVISIBLE);

        progress.setVisibility(View.VISIBLE);
        image.setVisibility(View.GONE);

        Picasso.with(context)
                .load(product.getImage())
                .resize(imageSize, imageSize)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) //Skip disk cache for big image.
                .into(image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        if (progress != null)
                            progress.setVisibility(View.GONE);
                        image.setVisibility(View.VISIBLE);
                        onImageLoaded();
                    }

                    @Override
                    public void onError() {}
                });
    }

    public boolean isMenuPressedInView() {
        return false;
    }

    public void setMenuButtonInteractionListener(MenuButtonInteractionListener listener) {
        // Nothing to do
    }

    public void hideMenu() {
        // Nothing to do
    }

    public void setParentFragment(String parentFragment) {
        // Nothing to do
    }

    public void setListener(OnFragmentInteractionListener fragmentInteractionListener) {
        this.fragmentInteractionListener = fragmentInteractionListener;
    }

    protected void onImageLoaded() {
        // Nothing to do
    }

    protected void setQuantity(int quantity) {
        inCartFlagText.setText( context.getString(
                R.string.flagInCart,
                Config.getInstance().showProductWeight() && sku.hasWeight() ?
                        sku.getWeightFormatted(quantity) :
                        String.valueOf(quantity) ) );
    }

    protected void addQuantity() {
        setQuantity( clickListener.productAdded( (int) itemBuyButton.getTag( R.id.TAG_ADAPTER_ITEM ) ) );
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.itemBuyButton) {
            if (clickListener == null || inCartFlagText == null)
                return;
            addQuantity();
            Utils.fadeIn(inCartFlag);
        }
    }
}
