/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.adapters;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.MinicartAdapterViewHolder;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Sku;

import java.util.LinkedHashMap;

/**
 * Created by marcos on 06/01/16.
 */
public class MinicartAdapter extends RecyclerView.Adapter<MinicartAdapterViewHolder> {

    private LinkedHashMap<String, Sku> items;
    private String[] mKeys;
    private AppCompatActivity context;
    private Callback callback;

    public MinicartAdapter(AppCompatActivity context, LinkedHashMap<String, Sku> items, Callback callback){
        this.context = context;
        this.callback = callback;
        this.items = items;

        //Position - Sku ID
        this.mKeys = items.keySet().toArray(new String[items.size()]);
    }

    @Override public MinicartAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MinicartAdapterViewHolder(parent, callback);
    }

    public Object getItem(int position) {
        return items.get(mKeys[position]);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public int getItemCount() {
        return mKeys.length;
    }

    @Override public void onBindViewHolder(final MinicartAdapterViewHolder viewHolder, final int position) {
        viewHolder.setView((Sku) getItem(position));
    }

    public void remove(int position) {
        if (position < 0 || mKeys.length <= position)
            return;

        Cart.getInstance().removeItem(mKeys[position], context);
        mKeys = items.keySet().toArray(new String[items.size()]);
        notifyItemRemoved(position);
    }

    public void updateProducts(LinkedHashMap<String, Sku> items){
        this.items = items;
        this.mKeys = items.keySet().toArray(new String[items.size()]);
    }

}