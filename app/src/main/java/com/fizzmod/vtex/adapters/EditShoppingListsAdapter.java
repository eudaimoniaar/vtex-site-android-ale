package com.fizzmod.vtex.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ShoppingListQuantityChangeListener;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.ShoppingListsModifications;

import java.util.List;

public class EditShoppingListsAdapter extends RecyclerView.Adapter {

    private final Context context;
    private final List<ShoppingList> shoppingLists;
    private final ShoppingListQuantityChangeListener listener;
    private final ShoppingListsModifications modifications;

    public EditShoppingListsAdapter(Context context,
                                    ShoppingListQuantityChangeListener listener,
                                    List<ShoppingList> shoppingLists,
                                    String selectedSku) {
        this.context = context;
        this.listener = listener;
        this.shoppingLists = shoppingLists;
        modifications = new ShoppingListsModifications(selectedSku, shoppingLists);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(context)
                .inflate(R.layout.edit_shopping_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.setInfo(shoppingLists.get(position));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public ShoppingListsModifications getModifications() {
        return modifications;
    }

    /** ViewHolder */

    private class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nameTextView, counterTextView;
        private ImageButton minusButton, plusButton;
        private Integer listId;
        private FrameLayout counterWrapper;

        private ViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.edit_shopping_list_item_name);
            counterTextView = (TextView) itemView.findViewById(R.id.edit_shopping_list_item_counter);
            minusButton = (ImageButton) itemView.findViewById(R.id.edit_shopping_list_item_minus_button);
            plusButton = (ImageButton) itemView.findViewById(R.id.edit_shopping_list_item_plus_button);
            counterWrapper = (FrameLayout) itemView.findViewById(R.id.edit_shopping_list_item_counter_wrapper);

            nameTextView.setActivated(false);
            counterTextView.setActivated(false);
            minusButton.setActivated(false);
            counterWrapper.setActivated(false);
            plusButton.setActivated(true);
            counterTextView.setText("0");

            if(modifications.getQuantity(listId) > 0){
                minusButton.setActivated(true);
                counterWrapper.setActivated(true);
            }

            minusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    decrementQuantity();
                }
            });

            plusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    incrementQuantity();
                }
            });
        }

        public void setInfo(ShoppingList shoppingList) {
            listId = shoppingList.getId();
            Integer quantity = modifications.getQuantity(listId);
            minusButton.setActivated(quantity > 0);
            boolean listWasModified = modifications.listWasModified(listId);
            nameTextView.setActivated(listWasModified);
            counterTextView.setActivated(listWasModified);
            nameTextView.setText(shoppingList.getName());
            counterTextView.setText(String.valueOf(quantity));
        }

        private void incrementQuantity() {
            modifications.incrementQuantity(listId);
            if(!minusButton.isActivated()){
                minusButton.setActivated(true);
            }
            onQuantityModified();
        }

        private void decrementQuantity() {
            if (!modifications.decrementQuantity(listId))
                return;
            if (modifications.getQuantity(listId) == 0 && minusButton.isActivated()){
                minusButton.setActivated(false);
            }
            onQuantityModified();
        }

        private void onQuantityModified() {
            counterTextView.setText(String.valueOf(modifications.getQuantity(listId)));
            boolean listWasModified = modifications.listWasModified(listId);
            nameTextView.setActivated(listWasModified);
            counterTextView.setActivated(listWasModified);
            counterWrapper.setActivated(listWasModified);
            listener.onCountChanged();
        }

    }

}
