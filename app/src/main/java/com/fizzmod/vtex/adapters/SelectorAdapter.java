/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.viewholders.SelectorAdapterViewHolder;
import com.fizzmod.vtex.models.SelectorItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcos on 10/01/16.
 */
public class SelectorAdapter extends BaseAdapter {

    private final List<SelectorItem> items = new ArrayList<>();

    public SelectorAdapter(List<SelectorItem> items) {
        this.items.addAll(items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SelectorAdapterViewHolder viewHolder;
        Context context = parent.getContext();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.selector_item, parent, false);
            viewHolder = new SelectorAdapterViewHolder(convertView);
            convertView.setTag(R.id.TAG_VIEW_HOLDER, viewHolder);
        } else
            viewHolder = (SelectorAdapterViewHolder) convertView.getTag(R.id.TAG_VIEW_HOLDER);
        convertView.setTag(R.id.TAG_ADAPTER_ITEM, position);
        viewHolder.setView(items.get(position));
        return convertView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<SelectorItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

}
