package com.fizzmod.vtex.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.fizzmod.vtex.R;

public class FilterValueAdapterViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

    private FilterSelectionListener listener;
    private String value;
    private int position;
    private int filterPosition;

    public FilterValueAdapterViewHolder(ViewGroup parent, FilterSelectionListener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_filter_value, parent, false));
        this.listener = listener;
    }

    public void bindView(String value, String text, int position, int filterPosition, boolean isSelected) {
        this.value = value;
        this.position = position;
        this.filterPosition = filterPosition;
        CheckBox checkBox = (CheckBox) itemView;
        checkBox.setText(text);
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setChecked(isSelected);
        checkBox.setOnCheckedChangeListener(this);
    }

    /***************************
     * OnCheckedChangeListener *
     ***************************/

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String text = buttonView.getText().toString();
        if (isChecked)
            listener.onFilterSelected(text, value, position, filterPosition);
        else
            listener.onFilterDeselected(text, value, position, filterPosition);
    }

    /*************
     * Interface *
     *************/

    public interface FilterSelectionListener extends AppliedFiltersAdapterViewHolder.FilterDeselectionListener {

        void onFilterSelected(String text, String value, Integer position, Integer filterPosition);

    }
}
