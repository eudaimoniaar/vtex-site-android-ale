package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

public class BaseMinicartAdapterViewHolder extends RecyclerView.ViewHolder {

    private TextView minicartItemTitle;
    private TextView minicartItemBrand;
    private TextView minicartItemPrice;
    private TextView minicartItemListPrice;
    private TextView minicartPriceByUnit;
    private TextView minicartItemQuantity;
    private TextView promotion;
    private ImageView minicartItemImage;
    private ImageView deleteMinicartItem;
    private ImageButton addQuantity;
    private ImageButton substractQuantity;
    private int imageSize;
    private String outOfStock;
    private long lastClick = -1;
    private float disabledAlpha;

    BaseMinicartAdapterViewHolder(ViewGroup parent, final Callback callback) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_minicart_item, parent, false));

        minicartItemTitle = (TextView) itemView.findViewById(R.id.minicartItemTitle);
        minicartItemBrand = (TextView) itemView.findViewById(R.id.minicartItemBrand);
        minicartItemPrice = (TextView) itemView.findViewById(R.id.minicartItemPrice);
        minicartItemQuantity = (TextView) itemView.findViewById(R.id.productQuantity);
        minicartItemListPrice = (TextView) itemView.findViewById(R.id.minicartItemListPrice);
        minicartPriceByUnit = (TextView) itemView.findViewById(R.id.productPriceByUnit);
        promotion = (TextView) itemView.findViewById(R.id.minicart_item_label_promotion);
        deleteMinicartItem = (ImageView) itemView.findViewById(R.id.deleteMinicartItem);
        minicartItemImage = (ImageView) itemView.findViewById(R.id.minicartItemImage);
        addQuantity = (ImageButton) itemView.findViewById(R.id.addQuantity);
        substractQuantity = (ImageButton) itemView.findViewById(R.id.substractQuantity);
        imageSize = (int) itemView.getContext().getResources().getDimension(R.dimen.minicartImageSize);
        outOfStock = itemView.getContext().getResources().getString(R.string.outOfStock);

        TypedValue outValue = new TypedValue();
        parent.getContext().getResources().getValue(R.dimen.productQuantityDisabledAlpha, outValue, true);
        disabledAlpha = outValue.getFloat();

        deleteMinicartItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                //Cart.getInstance().removeItem(skuId, context);
                callback.run(getAdapterPosition(), true);
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.run(getAdapterPosition(), false);
            }
        });

        addQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sku sku = (Sku) itemView.getTag();
                int selectedQuantity = getQuantity(sku) + 1;

                if (selectedQuantity == 2) {
                    substractQuantity.setEnabled(true);
                    Utils.alpha(substractQuantity, disabledAlpha, 1f, 350);
                }

                setQuantity(sku, selectedQuantity);
                sku.setSelectedQuantity(selectedQuantity);
                Cart.getInstance().save(itemView.getContext());

                ((Main) itemView.getContext()).getMinicart().updateMiniCart(false, false, true, null);

                lastClick = System.currentTimeMillis();
                final long selfClick = lastClick;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(selfClick == lastClick)
                            ((Main) itemView.getContext()).getMinicart().getMinimumCartValue();
                    }
                }, 300);

                //Tell Minicart an action was performed
                callback.run(true);
            }
        });

        substractQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sku sku = (Sku) itemView.getTag();
                int quantity = getQuantity(sku) - 1;
                int selectedQuantity = Math.max(quantity, 1);

                if (selectedQuantity <= 1)
                    Utils.alpha(substractQuantity, 1f, disabledAlpha, 350);

                substractQuantity.setEnabled(selectedQuantity > 1);

                setQuantity(sku, selectedQuantity);
                sku.setSelectedQuantity(selectedQuantity);
                Cart.getInstance().save(itemView.getContext());
                ((Main) itemView.getContext()).getMinicart().updateMiniCart(false, false, true, null);

                lastClick = System.currentTimeMillis();
                final long selfClick = lastClick;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(selfClick == lastClick)
                            ((Main) itemView.getContext()).getMinicart().getMinimumCartValue();
                    }
                }, 300);

                //Tell Minicart an action was performed
                callback.run(true);
            }
        });
    }

    public void setView(final Sku sku) {

        itemView.setTag(sku);

        Picasso
                .with(itemView.getContext())
                .load(sku.getMainImage(imageSize, imageSize))
                .into(minicartItemImage);

        String skuName = !sku.getName().equals(sku.getProductName()) ? " - " + sku.getName() : "";
        minicartItemTitle.setText(sku.getProductName() + skuName);
        minicartItemBrand.setText(sku.getBrand());
        setQuantity(sku, sku.getSelectedQuantity());

        addQuantity.setEnabled(sku.hasStock());
        addQuantity.setAlpha(sku.hasStock() ? 1f : disabledAlpha);

        substractQuantity.setEnabled(sku.getSelectedQuantity() > 1 && sku.hasStock());
        substractQuantity.setAlpha(sku.getSelectedQuantity() > 1 && sku.hasStock() ? 1f : disabledAlpha);

        if (sku.hasStock()) {
            sku.getBestPriceFormatted(new TypedCallback<String>() {
                @Override
                public void run(String price) {
                    minicartItemPrice.setText(price);
                }
            });
            minicartItemListPrice.setPaintFlags(minicartItemListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (sku.showListPrice()) {
                sku.getListPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        minicartItemListPrice.setText(price);
                    }
                });
            } else {
                minicartItemListPrice.setText("");
            }
            minicartItemListPrice.setVisibility(sku.showListPrice() ? View.VISIBLE : View.GONE);

            if (showPriceByUnit(sku)) {
                sku.getPriceByUnitFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        minicartPriceByUnit.setText(
                                itemView.getContext().getString(R.string.product_price_by_unit, price));
                    }
                });
                minicartPriceByUnit.setVisibility(View.VISIBLE);
            } else
                minicartPriceByUnit.setVisibility(View.GONE);
        } else {
            minicartItemPrice.setText(outOfStock);
            minicartItemListPrice.setVisibility(View.GONE);
            minicartPriceByUnit.setVisibility(View.GONE);
        }

        if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasCurrentPromotions() && sku.hasLabelPromotion()) {
            promotion.setText(sku.getLabelName().trim());
            promotion.setVisibility(View.VISIBLE);
        } else {
            if(promotion != null)
                promotion.setVisibility(View.GONE);
        }

        deleteMinicartItem.setTag(R.id.LIST_ITEM_SKU, sku.getId());
    }

    protected boolean showPriceByUnit(Sku sku) {
        return sku.showPriceByUnit();
    }

    private int getQuantity(Sku sku) {
        String quantityString = minicartItemQuantity.getText().toString();
        return Config.getInstance().showProductWeight() && sku.hasWeight() ?
                sku.getQuantityFromWeight(quantityString) :
                Integer.parseInt(quantityString);
    }

    private void setQuantity(Sku sku, int quantity) {
        minicartItemQuantity.setText(
                Config.getInstance().showProductWeight() && sku.hasWeight() ?
                        sku.getWeightFormatted(quantity) :
                        String.valueOf(quantity) );
    }

}
