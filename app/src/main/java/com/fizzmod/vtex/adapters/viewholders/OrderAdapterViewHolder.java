package com.fizzmod.vtex.adapters.viewholders;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.models.Order;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class OrderAdapterViewHolder extends RecyclerView.ViewHolder {

    private TextView orderIdText;
    private TextView orderDateText;
    private TextView orderStatusText;
    private TextView orderTotalText;
    private Order order;

    public OrderAdapterViewHolder(View itemView) {
        super(itemView);
        orderIdText = (TextView) itemView.findViewById(R.id.orderId);
        orderStatusText = (TextView) itemView.findViewById(R.id.orderStatus);
        orderDateText = (TextView) itemView.findViewById(R.id.orderDate);
        orderTotalText = (TextView) itemView.findViewById(R.id.orderTotal);
    }

    public void bindView(Order order, boolean isGridAdapter) {
        this.order = order;

        DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(order.date);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");

        if (Config.getInstance().isGeantConfig()) {
            orderIdText.setText(order.id);
        } else {
            orderIdText.setText(order.sequence);
        }
        orderDateText.setText(dateTime.toString(fmt));
        if (isGridAdapter && BuildConfig.TITLE == "Changomas") {
            orderStatusText.setText(order.getStatus().replace("proceso", "\nproceso"));
        } else {
            orderStatusText.setText(order.getStatus());
        }
        orderTotalText.setText(Config.getInstance().formatPrice(order.total));

        View greenBullet =  itemView.findViewById(R.id.order_status_billed);
        View redBullet =  itemView.findViewById(R.id.order_status_cancelled);
        View yellowBullet =  itemView.findViewById(R.id.order_status_in_progress);

        int statusColor = order.getStatusColor();
        greenBullet.setSelected(statusColor == Order.STATUS_BILLED);
        redBullet.setSelected(statusColor == Order.STATUS_CANCELLED);
        yellowBullet.setSelected(statusColor == Order.STATUS_IN_PROGRESS);

        int textColorResId = R.color.inProgressBullet;
        if (statusColor == Order.STATUS_BILLED)
            textColorResId = R.color.billedBullet;
        else if(statusColor == Order.STATUS_CANCELLED)
            textColorResId = R.color.cancelledBullet;
        orderStatusText.setTextColor( ContextCompat.getColor(itemView.getContext(), textColorResId) );

        itemView.setTag(order);
    }
}
