package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.SelectorItem;

public class SelectorAdapterViewHolder {

    private TextView textView;
    private ImageView imageView;

    public SelectorAdapterViewHolder(View view) {
        textView = (TextView) view.findViewById(R.id.selectorName);
        imageView = (ImageView) view.findViewById(R.id.selectorImage);
    }

    public void setView(SelectorItem selectorItem) {
        textView.setText(selectorItem.name);
        textView.setSelected(selectorItem.enabled);
        imageView.setSelected(selectorItem.enabled);
    }

}
