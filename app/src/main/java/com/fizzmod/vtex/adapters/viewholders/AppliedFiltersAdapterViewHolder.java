package com.fizzmod.vtex.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class AppliedFiltersAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private FilterDeselectionListener listener;

    public AppliedFiltersAdapterViewHolder(ViewGroup parent, FilterDeselectionListener listener) {
        super( LayoutInflater.from( parent.getContext() ).inflate(
                R.layout.sublayout_applied_filter,
                parent,
                false ) );
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    public void bindView(String text, String value, Integer position, Integer filterPosition) {
        ((TextView)itemView).setText(text);
        itemView.setTag(R.id.TAG_FILTER_VALUE_POSITION, position);
        itemView.setTag(R.id.TAG_FILTER_POSITION, filterPosition);
        itemView.setTag(R.id.TAG_FILTER_VALUE, value);
    }

    /*******************
     * OnClickListener *
     *******************/

    @Override
    public void onClick(View v) {
        listener.onFilterDeselected(
                ((TextView)itemView).getText().toString(),
                (String) itemView.getTag(R.id.TAG_FILTER_VALUE),
                (Integer) itemView.getTag(R.id.TAG_FILTER_POSITION),
                (Integer) itemView.getTag(R.id.TAG_FILTER_VALUE_POSITION) );
    }

    /*************
     * Interface *
     *************/

    public interface FilterDeselectionListener {

        void onFilterDeselected(String text, String value, Integer position, Integer filterPosition);

    }

}
