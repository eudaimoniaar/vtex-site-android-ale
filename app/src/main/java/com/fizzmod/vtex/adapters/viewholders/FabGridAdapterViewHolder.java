package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.view.View;

import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.views.FloatingMenuButton;

@SuppressWarnings("unused")
public abstract class FabGridAdapterViewHolder extends BaseGridAdapterViewHolder {

    private View optionsButtonView;
    private FloatingMenuButton menuButton;

    FabGridAdapterViewHolder(Context context, View convertView, ProductListCallback clickListener) {
        super(context, convertView, clickListener);
        menuButton = (FloatingMenuButton) convertView.findViewById(getMenuButtonId());
        menuButton.hideOptionsButton();
        optionsButtonView = convertView.findViewById(getOptionsButtonId());
        optionsButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuButton.isPressed())
                    menuButton.hideMenu();
                else
                    menuButton.showMenu();
            }
        });
    }

    @Override
    public void setView(int position, Product product) {
        menuButton.resetMenu();
        optionsButtonView.setVisibility(View.INVISIBLE);
        menuButton.setProduct(product);
        super.setView(position, product);
    }

    @Override
    protected void onImageLoaded() {
        menuButton.setVisibility(View.VISIBLE);
        optionsButtonView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean isMenuPressedInView() {
        return menuButton.isPressed();
    }

    @Override
    public void setMenuButtonInteractionListener(MenuButtonInteractionListener listener) {
        menuButton.setMenuButtonInteractionListener(listener);
    }

    @Override
    public void hideMenu() {
        menuButton.hideMenu();
    }

    @Override
    public void setParentFragment(String parentFragment) {
        menuButton.setParentFragment(parentFragment);
    }

    @Override
    public void setListener(OnFragmentInteractionListener fragmentInteractionListener) {
        menuButton.setListener(fragmentInteractionListener);
    }

    abstract int getMenuButtonId();

    abstract int getOptionsButtonId();

}
