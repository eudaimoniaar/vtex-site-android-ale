package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;

import java.util.List;

public class CouponStatusHeaderViewHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener,
        TextWatcher {

    private View searchInputWrapper;
    private View clearTextImage;
    private View progressView;
    private LinearLayout noCouponResult;
    private TextView noCouponResultText;
    private TextView couponAvailable;
    private TextView couponActivated;
    private EditText searchInputText;
    private Switch couponsPhysicalPrint;
    private CouponsSelectionListener couponsSelectionListener;
    private Listener listener;
    private Context context;

    private CountDownTimer countDownTimer = new CountDownTimer(1500, 1500) {
        @Override
        public void onTick(long millisUntilFinished) {
            // Nothing to do.
        }

        @Override
        public void onFinish() {
            progressView.setVisibility(View.GONE);
            String text = searchInputText.getText().toString();
            clearTextImage.setVisibility(text.isEmpty() ? View.GONE : View.VISIBLE);
            listener.performFiltering(text);
        }
    };

    public CouponStatusHeaderViewHolder(ViewGroup parent, CouponsSelectionListener couponsSelectionListener, Listener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_coupons_status, parent, false));
        this.context = parent.getContext();
        this.listener = listener;
        this.couponsSelectionListener = couponsSelectionListener;
        noCouponResult = itemView.findViewById(R.id.coupons_no_coupons_results);
        noCouponResultText = itemView.findViewById(R.id.coupons_no_coupons_results_text);
        couponAvailable = itemView.findViewById(R.id.coupons_available);
        couponActivated = itemView.findViewById(R.id.coupons_activated);
        progressView = itemView.findViewById(R.id.coupons_progress_bar);
        clearTextImage = itemView.findViewById(R.id.coupons_search_text_clear);
        clearTextImage.setOnClickListener(this);
        searchInputWrapper = itemView.findViewById(R.id.coupons_search_text_wrapper);
        searchInputText = itemView.findViewById(R.id.coupons_search_text);
        searchInputText.addTextChangedListener(this);
        itemView.findViewById(R.id.coupons_instructions_text).setOnClickListener(this);
        couponsPhysicalPrint = itemView.findViewById(R.id.coupons_physical_print);
        couponsPhysicalPrint.setClickable(false);
        itemView.findViewById(R.id.coupons_physical_print_wrapper).setOnClickListener(this);
    }

    public void bindView(List<Coupon> coupons, boolean physicalPrint) {
        couponAvailable.setText( String.valueOf( coupons.size() ) );
        couponActivated.setText( String.valueOf( getActivatedCouponsCount( coupons ) ) );
        noCouponResult.setVisibility(coupons.isEmpty() ? View.VISIBLE : View.GONE);
        searchInputWrapper.setVisibility(coupons.isEmpty() ? View.GONE : View.VISIBLE);
        couponsPhysicalPrint.setChecked(physicalPrint);
    }

    public void updateCounts(List<Coupon> coupons) {
        couponActivated.setText( String.valueOf( getActivatedCouponsCount( coupons ) ) );
    }

    public void couponsFilterApplied(boolean listIsEmpty) {
        noCouponResult.setVisibility(listIsEmpty ? View.VISIBLE : View.GONE);
        if (listIsEmpty)
            noCouponResultText.setText(R.string.coupons_empty_filtered_coupons);
    }

    private int getActivatedCouponsCount(List<Coupon> coupons) {
        int activatedCount = 0;
        for (int i = 0; i < coupons.size(); i++) {
            if (coupons.get(i).getEnabled())
                activatedCount++;
        }
        return activatedCount;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.coupons_instructions_text:
                couponsSelectionListener.onInstructionsClicked();
                break;
            case R.id.coupons_physical_print_wrapper:
                couponsSelectionListener.onPhysicalPrintClicked();
                break;
            case R.id.coupons_search_text_clear:
                clearTextImage.setVisibility(View.GONE);
                searchInputText.removeTextChangedListener(this);
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && view.getWindowToken() != null)
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                searchInputText.setText("");
                listener.performFiltering("");
                searchInputText.addTextChangedListener(this);
                break;
        }
    }

    /* ************* *
     *  TextWatcher  *
     * ************* */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Nothing to do.
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Nothing to do.
    }

    @Override
    public void afterTextChanged(Editable s) {
        clearTextImage.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);
        countDownTimer.cancel();
        countDownTimer.start();
    }

    /* ****************** *
     *  Public interface  *
     * ****************** */

    public interface Listener {

        void performFiltering(String text);

    }
}