package com.fizzmod.vtex.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.UserListAdapterListener;
import com.fizzmod.vtex.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.VISIBLE;

public class BaseShoppingListAdapter extends RecyclerView.Adapter<BaseShoppingListAdapter.ViewHolder> {

    private List<ShoppingList> shoppingLists = new ArrayList<>();
    private List<ShoppingList> selectedShoppingLists = new ArrayList<>();
    public Context context;
    private UserListAdapterListener listener;
    private boolean isEditing;

    public BaseShoppingListAdapter(Context context, UserListAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        isEditing = false;
    }

    public void setShoppingLists(List<ShoppingList> lists) {
        shoppingLists.clear();
        shoppingLists.addAll(lists);
        notifyDataSetChanged();
    }

    public void addShoppingList(ShoppingList shoppingList) {
        shoppingLists.add(shoppingList);
        notifyDataSetChanged();
    }

    public boolean isEditing() {
        return isEditing;
    }

    public void exitEditMode() {
        isEditing = false;
        selectedShoppingLists.clear();
        notifyDataSetChanged();
    }

    public void toggleSelectAll() {
        int selectedCount = selectedShoppingLists.size();
        selectedShoppingLists.clear();
        if (selectedCount != shoppingLists.size())
            selectedShoppingLists.addAll(shoppingLists);
        notifyDataSetChanged();
    }

    public void deleteSelected() {

        if (selectedShoppingLists.size() == 0)
            return;

        shoppingLists.removeAll(selectedShoppingLists);
        selectedShoppingLists.clear();
        notifyDataSetChanged();
    }

    public List<ShoppingList> getSelectedShoppingLists() {
        return selectedShoppingLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(context)
                .inflate(R.layout.shopping_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.loadData(position);
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ShoppingList shoppingList;

        private TextView txtListTitle;
        private ImageView iconList;
        private ImageView selectorIcon;
        private ImageView addToCartButton;

        ViewHolder(final View itemView) {
            super(itemView);

            txtListTitle = (TextView) itemView.findViewById(R.id.shopping_list_item_title);
            iconList = (ImageView) itemView.findViewById(R.id.shopping_list_item_icon);
            selectorIcon = (ImageView) itemView.findViewById(R.id.shopping_list_selected_icon);
            addToCartButton = (ImageView) itemView.findViewById(R.id.shopping_list_cart_icon);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(!isEditing) {
                        changeView();
                        addList();
                    }
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isEditing)
                        listener.onListSelected(shoppingList);
                    else if (selectedShoppingLists.contains(shoppingList))
                        removeList();
                    else
                        addList();
                }
            });

            iconList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeView();
                }
            });

            addToCartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isEditing)
                        listener.onAddToCart(shoppingList);
                    else
                        itemView.performClick();
                }
            });

        }

        public void loadData(int position) {
            shoppingList = shoppingLists.get(position);
            txtListTitle.setText(shoppingList.getName());
            selectorIcon.setActivated(isEditing && selectedShoppingLists.contains(shoppingList));
            addToCartButton.setActivated(!isEditing);
            iconList.setVisibility(isEditing ? View.GONE : VISIBLE);
            selectorIcon.setVisibility(isEditing ? VISIBLE : View.GONE);
        }

        private void changeView() {
            listener.onLongClick();
            isEditing = true;
            notifyDataSetChanged();
        }

        private void addList() {
            selectorIcon.setActivated(true);
            selectedShoppingLists.add(shoppingList);
        }

        private void removeList() {
            selectorIcon.setActivated(false);
            selectedShoppingLists.remove(shoppingList);
        }
    }
}
