package com.fizzmod.vtex.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.FilterValueAdapterViewHolder;
import com.fizzmod.vtex.adapters.viewholders.FiltersAdapterViewHolder;
import com.fizzmod.vtex.models.Filter;

import java.util.ArrayList;
import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapterViewHolder> {

    private final List<Filter> filters = new ArrayList<>();
    private FilterValueAdapterViewHolder.FilterSelectionListener listener;
    private List<String> selectedFilters;

    public FiltersAdapter(FilterValueAdapterViewHolder.FilterSelectionListener listener) {
        this.listener = listener;
    }

    @Override
    public FiltersAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FiltersAdapterViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(FiltersAdapterViewHolder holder, int position) {
        holder.bindView(filters.get(position), position, selectedFilters);
    }

    @Override
    public void onBindViewHolder(FiltersAdapterViewHolder holder, int position, List<Object> payloads) {
        for (Object o : payloads)
            holder.bindView((Integer) o);
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    public void setFilters(List<Filter> filters, List<String> selectedFilters) {
        this.selectedFilters = selectedFilters;
        this.filters.clear();
        this.filters.addAll(filters);
        notifyDataSetChanged();
    }

    public void deselectFilter(Integer position, Integer filterValuePosition) {
        notifyItemChanged(position, filterValuePosition);
    }
}
