package com.fizzmod.vtex.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ShoppingListAdapterInterface;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BaseShoppingListProductsAdapter extends RecyclerView.Adapter {

    private List<Product> productList = new ArrayList<>();
    private List<Product> selectedProductList = new ArrayList<>();
    private Context context;
    private ShoppingListAdapterInterface listener;
    private boolean isEditing;

    public BaseShoppingListProductsAdapter(Context context) {
        this.context = context;
        isEditing = false;
    }

    public List<Product> getProductList(){
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    public void setListener(ShoppingListAdapterInterface listener) {
        this.listener = listener;
    }

    public void toggleSelectAllProducts() {
        int selectedCount = selectedProductList.size();
        selectedProductList.clear();
        if (selectedCount != productList.size())
            selectedProductList.addAll(productList);
        notifyDataSetChanged();
    }

    public void removeSelectedProduct() {
        if (selectedProductList.isEmpty())
            return;

        productList.removeAll(selectedProductList);
        selectedProductList.clear();
        notifyDataSetChanged();
    }

    public void exitEditMode() {
        isEditing = false;
        selectedProductList.clear();
        notifyDataSetChanged();
    }

    public List<Product> getSelectedProducts() {
        return selectedProductList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.shopping_list_product_item,
                parent,
                false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.loadData(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public boolean isEditing() {
        return isEditing;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle;
        private TextView txtPrice;
        private TextView txtListPrice;
        private TextView txtPriceByUnit;
        private TextView txtProductBrand;
        private TextView txtProductCount;
        private ImageView productImage;
        private ImageView checkProduct;
        private Product product;

        private ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.shopping_list_product_item_title);
            txtPrice = (TextView) itemView.findViewById(R.id.shopping_list_product_item_product_price);
            txtListPrice = (TextView) itemView.findViewById(R.id.shopping_list_product_item_list_price);
            txtPriceByUnit = (TextView) itemView.findViewById(R.id.shopping_list_product_item_price_by_unit);
            txtProductBrand = (TextView) itemView.findViewById(R.id.shopping_list_product_item_brand);
            txtProductCount = (TextView) itemView.findViewById(R.id.shopping_list_product_item_count);
            productImage = (ImageView) itemView.findViewById(R.id.shopping_list_product_item_image);
            checkProduct = (ImageView) itemView.findViewById(R.id.shopping_list_product_item_selection_view);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (!isEditing) {
                        listener.onLongClick();
                        isEditing = true;
                        notifyDataSetChanged();
                        addToList();
                    }
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isEditing)
                        listener.onClick(product);
                    else if (selectedProductList.contains(product))
                        removeFromList();
                    else
                        addToList();
                }
            });
        }

        private void loadData(Product product) {
            this.product = product;
            Sku sku = product.getSku(0);
            txtTitle.setText(product.getName());
            sku.getBestPriceFormatted(new TypedCallback<String>() {
                @Override
                public void run(String price) {
                    txtPrice.setText(price);
                }
            });

            if (sku.showListPrice()) {
                txtListPrice.setPaintFlags(txtListPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                sku.getListPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        txtListPrice.setText(price);
                    }
                });
            }
            txtListPrice.setVisibility(sku.showListPrice() ? View.VISIBLE : View.GONE);

            if (sku.showPriceByUnit()) {
                sku.getPriceByUnitFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        txtPriceByUnit.setText( context.getString( R.string.product_price_by_unit, price) );
                    }
                });
            }
            txtPriceByUnit.setVisibility(sku.showPriceByUnit() ? View.VISIBLE : View.GONE);

            txtProductBrand.setText(product.getBrand());
            txtProductCount.setText( context.getString( R.string.product_counter, sku.getSelectedQuantity() ) );
            Picasso
                    .with(context)
                    .load(product.getImage())
                    .into(productImage);

            if (!isEditing) {
                checkProduct.setVisibility(View.GONE);
                return;
            }

            checkProduct.setVisibility(View.VISIBLE);
            checkProduct.setActivated(selectedProductList.contains(product));
        }

        private void addToList() {
            selectedProductList.add(product);
            checkProduct.setActivated(true);
        }

        private void removeFromList() {
            selectedProductList.remove(product);
            checkProduct.setActivated(false);
        }

    }

}
