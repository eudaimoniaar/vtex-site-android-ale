package com.fizzmod.vtex.adapters.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CouponViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView couponDate;
    private ImageView couponImage;
    private TextView couponDiscount;
    private TextView couponDescription;
    private TextView couponPLU;
    private Button couponButton;
    private TextView couponTerms;
    private Context context;

    private List<Coupon> coupons;
    private int position;
    private CouponsSelectionListener listener;

    public CouponViewHolder(ViewGroup parent, List<Coupon> coupons, CouponsSelectionListener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_item, parent, false));
        couponDate = itemView.findViewById(R.id.coupons_expire_date);
        couponImage = itemView.findViewById(R.id.coupons_image);
        couponDiscount = itemView.findViewById(R.id.coupons_discount);
        couponDescription = itemView.findViewById(R.id.coupons_description);
        couponPLU = itemView.findViewById(R.id.coupons_plu);
        couponButton = itemView.findViewById(R.id.coupons_activate);
        couponTerms = itemView.findViewById(R.id.coupons_terms);

        context = parent.getContext();
        this.coupons = coupons;
        this.listener = listener;
    }

    public void bindView(final Coupon coupon, final int position) {
        this.position = position;
        couponDate.setText(context.getString(R.string.coupon_item_expire_date, coupon.getCreationDate(), coupon.getExpirationDate()));

        int imageSize = (int) context.getResources().getDimension(R.dimen.coupon_image_size);
        Picasso.with(context)
                .load(coupon.getPictureThumb())
                .resize(imageSize, imageSize)
                .into(couponImage);

        couponDiscount.setText(coupon.getTitles().get(0));
        couponDescription.setText(getDescription(coupon.getTitles()));
        couponPLU.setText(context.getString(R.string.coupon_plu, coupon.getCouponCode()));

        couponButton.setText(getButtonText(coupon.getEnabled()));
        couponButton.setActivated(coupon.getEnabled());
        couponButton.setTextColor(context.getResources().getColor(coupon.getEnabled() ? R.color.coupons_button_primary : R.color.coupons_button_secondary));
        couponButton.setEnabled(true);
        couponButton.setOnClickListener(this);
        couponTerms.setOnClickListener(this);
    }

    private String getDescription(List<String> titles) {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < titles.size(); i++) {
            builder.append(titles.get(i));
            if (i < titles.size() - 1)
                builder.append(" ");
        }
        return builder.toString();
    }

    private int getButtonText(boolean enabled) {
        return enabled ? R.string.coupons_deactivate : R.string.coupons_activate;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.coupons_activate:
                couponButton.setEnabled(false);
                couponButton.setTextColor(context.getResources().getColor(R.color.coupons_button_secondary));
                String couponCode = String.valueOf(coupons.get(position).getCouponCode());
                listener.onCouponButtonClicked(couponCode, position, coupons);
                break;
            case R.id.coupons_terms:
                listener.onTermsDialogClicked();
        }
    }
}
