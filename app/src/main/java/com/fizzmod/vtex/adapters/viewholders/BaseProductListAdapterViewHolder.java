package com.fizzmod.vtex.adapters.viewholders;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class BaseProductListAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView productTitle;
    private TextView productBrand;
    private TextView bestPrice;
    private TextView listPrice;
    protected TextView priceHighlight;
    private TextView priceByUnit;
    private TextView purchasedQuantity;
    private TextView inCartFlagText;
    private LinearLayout inCartFlag;
    private View progress;
    private LinearLayout linearPromotion;
    protected View itemBuyButton;
    private ImageView image;
    private ImageView cartIcon;
    private int imageSize;
    private String outOfStock;
    protected ProductListCallback clickListener;

    @SuppressWarnings("WeakerAccess")
    protected OnFragmentInteractionListener fragmentInteractionListener;

    BaseProductListAdapterViewHolder(final View itemView, final ProductListCallback clickListener) {
        super(itemView);
        this.clickListener = clickListener;
        productTitle = (TextView) itemView.findViewById(R.id.productTitle);
        productBrand = (TextView) itemView.findViewById(R.id.productBrand);
        purchasedQuantity = (TextView) itemView.findViewById(R.id.purchasedQuantity);
        inCartFlagText = (TextView) itemView.findViewById(R.id.productInCartFlagText);
        inCartFlag = (LinearLayout) itemView.findViewById(R.id.productInCartFlag);
        progress = itemView.findViewById(R.id.imageLoading);
        linearPromotion = (LinearLayout) itemView.findViewById(R.id.product_item_promotion_layout);
        image = (ImageView) itemView.findViewById(R.id.productImage);
        cartIcon = (ImageView) itemView.findViewById(R.id.cartIcon);
        bestPrice = (TextView) itemView.findViewById(R.id.productBestPrice);
        listPrice = (TextView) itemView.findViewById(R.id.productListPrice);
        priceHighlight = (TextView) itemView.findViewById(R.id.productPriceHighlight);
        priceByUnit = (TextView) itemView.findViewById(R.id.productPriceByUnit);
        itemBuyButton = itemView.findViewById(R.id.itemBuyButton);
        imageSize = (int) itemView.getResources().getDimension(R.dimen.listImageSize);
        outOfStock = itemView.getResources().getString(R.string.outOfStock);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (!isMenuPressedInView())
                    clickListener.goToProduct((Product) v.getTag());
            }
        });

        if (Config.getInstance().isFastAdd())
            itemBuyButton.setOnClickListener(this);
    }

    public void setView(int position, Product product, boolean isOrderPage, @SuppressWarnings("unused") boolean isRelatedItemsList) {

        itemView.setTag(product);
        itemView.setTag(R.id.TAG_ADAPTER_ITEM, position);

        Sku sku = product.getMainSku();

        Sku skuInCart = Cart.getInstance().getById(sku.getId());

        productTitle.setText(product.getName());
        productBrand.setText(Config.getInstance().isShowRefIDInProductList() ? sku.getRefId() : product.getBrand());

        if (sku.hasCurrentPromotions()) {
            if (linearPromotion.getChildCount() > 0)
                linearPromotion.removeAllViews();

            linearPromotion.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 1);
            for (Promotion promotion : sku.getPromotionList()) {
                //find and set the image with the same name.
                try {
                    ImageView imageView = new ImageView(itemView.getContext());
                    imageView.setImageResource(promotion.getPromotionImageResource());
                    imageView.setLayoutParams(lp);
                    linearPromotion.addView(imageView);
                } catch (NullPointerException exception) {
                    Log.e(Promotion.LOG_ERROR, exception.toString());
                }
            }
        } else
            linearPromotion.setVisibility(View.GONE);

        if(sku.hasStock()) {

            TypedCallback<String> callback = new TypedCallback<String>() {
                @Override
                public void run(String convertedCurrency) {
                    bestPrice.setText(convertedCurrency);
                }
            };
            if (sku.hasSellingPrice())
                sku.getSellingPriceFormatted(callback);
            else
                sku.getBestPriceFormatted(callback);

            if (sku.showListPrice()) {
                listPrice.setPaintFlags(listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                sku.getListPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        listPrice.setText(price);
                    }
                });
                listPrice.setVisibility(View.VISIBLE);
            } else
                listPrice.setVisibility(View.GONE);

            if (sku.showPriceDiffPercentage()) {
                priceHighlight.setText(sku.getPriceDiffPercentageFormatted(itemView.getContext()));
                priceHighlight.setVisibility(View.VISIBLE);
            } else
                priceHighlight.setVisibility(View.GONE);

            if (sku.showPriceByUnit()) {
                sku.getPriceByUnitFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        priceByUnit.setText( itemView.getContext().getString( R.string.product_price_by_unit, price ) );
                    }
                });
                priceByUnit.setVisibility(View.VISIBLE);
                //listPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, listPriceSizeSmall);
            } else {
                priceByUnit.setVisibility(View.GONE);
                //listPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, listPriceSizeBig);
            }

        } else {
            bestPrice.setText(outOfStock);
            listPrice.setVisibility(View.GONE);
            priceHighlight.setVisibility(View.GONE);
            priceByUnit.setVisibility(View.GONE);
        }

        progress.setVisibility(View.VISIBLE);
        image.setVisibility(View.GONE);

        if (skuInCart != null) {
            setQuantity(skuInCart.getSelectedQuantity());
            inCartFlag.setVisibility(View.VISIBLE);
        } else
            inCartFlag.setVisibility(View.INVISIBLE);

        int selectedQuantity = sku.getSelectedQuantity();

        showOrderPage(isOrderPage, selectedQuantity);

        Picasso.with(image.getContext())
                .load(product.getImage())
                .resize(imageSize, imageSize)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) //Skip disk cache for big image.
                .into(image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        if (progress != null)
                            progress.setVisibility(View.GONE);
                        image.setVisibility(View.VISIBLE);
                        onImageLoaded();
                    }

                    @Override
                    public void onError() {}
                });
    }

    public boolean isMenuPressedInView() {
        return false;
    }

    public void setMenuButtonInteractionListener(MenuButtonInteractionListener listener) {
        // Nothing to do
    }

    public void hideMenu() {
        // Nothing to do
    }

    public void setParentFragment(String parentFragment) {
        // Nothing to do
    }

    public void setListener(OnFragmentInteractionListener fragmentInteractionListener) {
        this.fragmentInteractionListener = fragmentInteractionListener;
    }

    protected void onImageLoaded() {
        //Nothing to do
    }

    public void showOrderPage(boolean isOrderPage, int selectedQuantity) {
        cartIcon.setVisibility(selectedQuantity > 0 && isOrderPage ? View.GONE: View.VISIBLE);
        purchasedQuantity.setVisibility(selectedQuantity > 0 && isOrderPage ? View.VISIBLE : View.GONE);

        if(selectedQuantity > 0 && isOrderPage)
            purchasedQuantity.setText(itemView.getContext().getString(R.string.product_counter, selectedQuantity));
    }

    protected void setQuantity(int quantity) {
        setQuantity(
                Cart.getInstance().getById( ( ( Product ) itemView.getTag() ).getMainSku().getId() ),
                quantity);
    }

    protected void setQuantity(Sku sku, int quantity) {
        inCartFlagText.setText( itemView.getContext().getString(
                R.string.flagInCart,
                Config.getInstance().showProductWeight() && sku.hasWeight() ?
                        sku.getWeightFormatted(quantity) :
                        String.valueOf(quantity)) );
    }

    protected void addQuantity() {
        setQuantity( clickListener.productAdded( (int) itemView.getTag( R.id.TAG_ADAPTER_ITEM ) ) );
    }

    /* ********************** *
     *  View.OnClickListener  *
     * ********************** */

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.itemBuyButton) {
            if (clickListener == null || inCartFlagText == null)
                return;
            addQuantity();
            Utils.fadeIn(inCartFlag);
        }
    }
}
