package com.fizzmod.vtex.adapters.viewholders;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.FilterValuesAdapter;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.utils.Utils;

import java.util.List;

public class FiltersAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView textView;
    private RecyclerView valuesRecyclerView;
    private FilterValuesAdapter valuesAdapter;

    public FiltersAdapterViewHolder(ViewGroup parent, FilterValueAdapterViewHolder.FilterSelectionListener listener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_filter, parent, false));
        textView = (TextView) itemView.findViewById(R.id.filter_name);
        valuesRecyclerView = (RecyclerView) itemView.findViewById(R.id.filter_values_recycler_view);
        valuesRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        valuesAdapter = new FilterValuesAdapter(listener);
        valuesRecyclerView.setAdapter(valuesAdapter);
        itemView.setOnClickListener(this);
    }

    public void bindView(Filter filter, Integer filterPosition, List<String> selectedFilters) {
        textView.setText(filter.name);
        valuesAdapter.setFilter(filter, filterPosition, selectedFilters);
    }

    public void bindView(Integer filterValuePosition) {
        valuesAdapter.notifyItemChanged(filterValuePosition);
    }

    /*******************
     * OnClickListener *
     *******************/

    @Override
    public void onClick(View v) {
        if (valuesRecyclerView.getVisibility() == View.VISIBLE) {
            textView.setSelected(false);
            Utils.collapse(valuesRecyclerView, 400);
        } else {
            textView.setSelected(true);
            Utils.expand(valuesRecyclerView, 400);
        }
    }
}
