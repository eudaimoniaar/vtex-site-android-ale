package com.fizzmod.vtex.adapters.viewholders;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.utils.Utils;

public class BaseDrawerAdapterViewHolder {

    protected TextView itemName;
    protected View convertView;
    protected ImageView icon;

    BaseDrawerAdapterViewHolder(View convertView) {
        this.convertView = convertView;

        icon = (ImageView) convertView.findViewById(R.id.imageViewIcon);
        itemName = (TextView) convertView.findViewById(R.id.textViewName);
    }

    public void setView(DrawerItem item) {

        Resources resources = convertView.getResources();

        if(item.isSubitem) {
            convertView.setBackgroundColor(item.enabled ? resources.getColor(R.color.drawerSubitemBackgroundOn) : resources.getColor(R.color.drawerSubitemBackgroundOff));
            itemName.setTextColor(item.enabled ? resources.getColor(R.color.drawerSubitemTextColorOn) : resources.getColor(R.color.drawerSubitemTextColorOff));
        } else {
            Utils.setBackground(convertView, item.enabled ? resources.getDrawable(R.drawable.drawer_item_on) : resources.getDrawable(R.drawable.drawer_item_off));
            itemName.setTextColor(item.enabled ? resources.getColor(R.color.drawerItemTextOn) : resources.getColor(R.color.drawerItemTextOff));
        }

        icon.setImageResource(item.enabled ? item.icon : item.iconOff);
        itemName.setText(item.textId);
        itemName.setAllCaps(!item.isSubitem);
        itemName.setTypeface(null, item.isSubitem ? Typeface.NORMAL : Typeface.BOLD);

        ViewGroup.LayoutParams params =  convertView.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params.height = item.isSubitem ? (int) resources.getDimension(R.dimen.drawerSubItem) : (int) resources.getDimension(R.dimen.drawerItem);
        convertView.setLayoutParams(params);
    }
}
