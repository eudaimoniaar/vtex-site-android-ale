package com.fizzmod.vtex.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.fizzmod.vtex.adapters.viewholders.FilterValueAdapterViewHolder;
import com.fizzmod.vtex.models.Filter;

import java.util.List;

public class FilterValuesAdapter extends RecyclerView.Adapter<FilterValueAdapterViewHolder> {

    private Filter filter;
    private Integer filterPosition;
    private List<String> selectedFilterValues;
    private FilterValueAdapterViewHolder.FilterSelectionListener listener;

    public FilterValuesAdapter(FilterValueAdapterViewHolder.FilterSelectionListener listener) {
        this.listener = listener;
    }

    @Override
    public FilterValueAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FilterValueAdapterViewHolder(parent, listener);
    }

    @Override
    public void onBindViewHolder(FilterValueAdapterViewHolder holder, int position) {
        String filterValue = filter.rel + filter.values.get(position);
        holder.bindView(
                filterValue,
                filter.texts.get(position),
                position,
                filterPosition,
                selectedFilterValues.contains(filterValue));
    }

    @Override
    public int getItemCount() {
        return filter.values.size();
    }

    public void setFilter(Filter filter, Integer filterPosition, List<String> selectedFilterValues) {
        this.filter = filter;
        this.filterPosition = filterPosition;
        this.selectedFilterValues = selectedFilterValues;
        notifyDataSetChanged();
    }

}
