package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;

import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.views.FloatingMenuButton;

@SuppressWarnings("unused")
public abstract class FabProductListAdapterViewHolder extends BaseProductListAdapterViewHolder {

    private View optionsButtonView;
    private FloatingMenuButton menuButton;

    FabProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        menuButton = (FloatingMenuButton) itemView.findViewById(getMenuButtonId());
        menuButton.hideOptionsButton();
        optionsButtonView = itemView.findViewById(getOptionsButtonId());
        optionsButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuButton != null) {
                    if (menuButton.isPressed())
                        menuButton.hideMenu();
                    else
                        menuButton.showMenu();
                }
            }
        });
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedItemsList) {
        menuButton.resetMenu();
        optionsButtonView.setVisibility(View.INVISIBLE);
        menuButton.setProduct(product);
        super.setView(position, product, isOrderPage, isRelatedItemsList);
    }

    @Override
    public boolean isMenuPressedInView() {
        return menuButton.isPressed();
    }

    @Override
    public void setMenuButtonInteractionListener(MenuButtonInteractionListener listener) {
        menuButton.setMenuButtonInteractionListener(listener);
    }

    @Override
    public void hideMenu() {
        menuButton.hideMenu();
    }

    @Override
    public void setParentFragment(String parentFragment) {
        menuButton.setParentFragment(parentFragment);
    }

    @Override
    public void setListener(OnFragmentInteractionListener fragmentInteractionListener) {
        menuButton.setListener(fragmentInteractionListener);
    }

    @Override
    protected void onImageLoaded() {
        menuButton.setVisibility(View.VISIBLE);
        optionsButtonView.setVisibility(View.VISIBLE);
    }

    abstract int getMenuButtonId();

    abstract int getOptionsButtonId();

}
