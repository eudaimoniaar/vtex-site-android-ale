package com.fizzmod.vtex.currency;

import com.fizzmod.vtex.interfaces.TypedCallback;

public interface CurrencyConverter {

    void convertCurrency(double currency, TypedCallback<String> callback);

}
