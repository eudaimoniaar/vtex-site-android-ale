package com.fizzmod.vtex.service.response;

import com.fizzmod.vtex.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

public class ShoppingListsResponse extends BaseResponse {

    private final Integer id;
    private final List<ShoppingList> lists = new ArrayList<>();

    ShoppingListsResponse(int code, String message, Integer id) {
        super(code, message);
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public List<ShoppingList> getLists() {
        return lists;
    }
}
