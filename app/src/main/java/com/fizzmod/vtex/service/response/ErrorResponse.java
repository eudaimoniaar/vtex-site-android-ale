package com.fizzmod.vtex.service.response;

public class ErrorResponse extends BaseResponse {

    private final String error;
    private final String errorId;

    public ErrorResponse(int code, String message, String error, String errorId) {
        super(code, message);
        this.error = error;
        this.errorId = errorId;
    }

    public String getError() {
        return error;
    }

    public String getErrorId() {
        return errorId;
    }

}
