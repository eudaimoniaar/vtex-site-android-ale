package com.fizzmod.vtex.service.params;

public class CouponEnableBody {

    private boolean enabled;

    public CouponEnableBody(boolean enabled) {
        this.enabled = enabled;
    }
}
