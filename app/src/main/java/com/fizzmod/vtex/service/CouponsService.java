package com.fizzmod.vtex.service;

import android.content.Context;
import android.util.Log;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.service.params.CouponEnableBody;
import com.fizzmod.vtex.service.params.CouponImpressionBody;
import com.fizzmod.vtex.service.response.CIResponse;
import com.fizzmod.vtex.service.response.CouponToggleResponse;
import com.fizzmod.vtex.service.response.ErrorResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponsService {

    private static CouponsService instance;
    private static final String TAG = "CouponsService";

    public static CouponsService getInstance(Context context) {
        if (instance == null)
            instance = new CouponsService(context.getApplicationContext());

        return instance;
    }

    private final Context context;
    private final CouponsApiService couponsApiService;

    private CouponsService(Context context) {
        this.context = context;
        couponsApiService = ApiClient.getCouponsApiService();
    }

    /**************************
     * COUPONS methods *
     **************************/

    public void getCards(final String ci, final ApiCallback<List<CIResponse>> callback) {
        handleResponse(couponsApiService.getCards(ci, BuildConfig.JANIS_STORENAME), callback);
    }

    public void getCoupons(final String clientNumber, final ApiCallback<List<Coupon>> callback) {
        handleResponse(couponsApiService.getCoupons(clientNumber), callback);
    }

    public void toggleCoupon(final String couponNumber, final boolean enable, final ApiCallback<CouponToggleResponse> callback) {
        handleResponse(couponsApiService.toggleCoupon(couponNumber, new CouponEnableBody(enable)), callback);
    }

    public void togglePhysicalPrint(final String clientNumber, final boolean enable, final ApiCallback<CouponToggleResponse> callback) {
        handleResponse(couponsApiService.togglePhysicalPrint(clientNumber, new CouponImpressionBody(enable)), callback);
    }

    /**************************
     * Private methods *
     **************************/

    private ErrorResponse parseErrorResponse(Response response) {
        try {
            return new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
        } catch (IOException e) {
            return new ErrorResponse(500, "Unexpected error", "Unexpected error", "00000");
        }
    }

    private String getErrorMessage(Response response) {
        String errorMessage;
        ErrorResponse errorResponse = parseErrorResponse(response);
        errorMessage = errorResponse.getError();
        if (errorMessage == null || errorMessage.isEmpty())
            errorMessage = errorResponse.getMessage();
        return errorMessage;
    }

    private <T> void handleResponse(Call<T> call, final ApiCallback<T> callback) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful())
                    callback.onResponse(response.body());
                else {
                    Log.d(TAG, "An error occurred: [" + getErrorMessage(response) + "]");
                    if (response.code() == 401)
                        callback.onUnauthorized();
                    else
                        callback.onError(context.getString(R.string.errorOccurred));
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                Log.e(TAG, "An error occurred.", t);
                callback.onError(context.getString(R.string.errorOccurred));
            }
        });
    }
}
