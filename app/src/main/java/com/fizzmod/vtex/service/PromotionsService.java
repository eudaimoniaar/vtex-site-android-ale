package com.fizzmod.vtex.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.service.response.ErrorResponse;
import com.fizzmod.vtex.utils.PromotionsHandler;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ApplySharedPref")
public class PromotionsService {

    public final static String LOG_ERROR = "LOG_ERROR_PROMOTION";

    private final static String TAG = "PromotionsService";
    private final static String PROMOTIONS_SHARED_PREFERENCES = "PROMOTIONS_SHARED_PREFERENCES";
    private final static String PROMOTIONS_KEY = "PROMOTIONS_KEY";
    private final static String PROMOTIONS_LAST_REFRESH_KEY = "PROMOTIONS_LAST_REFRESH_KEY";
    private final static long PROMOTIONS_REFRESH_INTERVAL = 300000;     // 5 minutes

    // Ignoring warning because "context" is the application context until the app is killed
    @SuppressLint("StaticFieldLeak")
    private static PromotionsService instance;

    public static PromotionsService getInstance(Context context) {
        if (instance == null)
            instance = new PromotionsService(context.getApplicationContext());
        return instance;
    }

    private final Context context;
    private final PromotionsApiService promotionsApiService;
    private final SharedPreferences sharedPreferences;

    private PromotionsService(Context context) {
        this.context = context;
        promotionsApiService = ApiClient.getPromotionsApiService();
        sharedPreferences = context.getSharedPreferences(PROMOTIONS_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void initializePromotions(final Runnable callback) {
        if (!promotionsAreOutdated()) {
            PromotionsHandler.getInstance().setPromotions(getPromotions(), getOutdatedTime());
            callback.run();
        } else {
            sharedPreferences.edit().remove(PROMOTIONS_KEY).commit();
            PromotionsHandler.getInstance().clearPromotions();
            refreshPromotions(new ApiCallback<List<Promotion>>() {
                @Override
                public void onResponse(List<Promotion> object) {
                    callback.run();
                }

                @Override
                public void onError(String errorMessage) {
                    callback.run();
                }

                @Override
                public void onUnauthorized() {
                    callback.run();
                }
            });
        }
    }

    public void getPromotions(final ApiCallback<List<Promotion>> callback) {
        if (promotionsAreOutdated()) {
            sharedPreferences.edit().remove(PROMOTIONS_KEY).commit();
            PromotionsHandler.getInstance().clearPromotions();
        }
        List<Promotion> promotions = getPromotions();
        if (promotions != null)
            callback.onResponse(promotions);
        else
            refreshPromotions(callback);
    }

    /* *************** *
     * Private methods *
     * *************** */

    private ErrorResponse parseErrorResponse(Response response) {
        try {
            return new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
        } catch (IOException e) {
            return new ErrorResponse(500, "Unexpected error", "Unexpected error", "00000");
        }
    }

    private String getErrorMessage(Response response) {
        String errorMessage;
        ErrorResponse errorResponse = parseErrorResponse(response);
        errorMessage = errorResponse.getError();
        if (errorMessage == null || errorMessage.isEmpty())
            errorMessage = errorResponse.getMessage();
        return errorMessage;
    }

    private <T> void handleResponse(Call<T> call, final ApiCallback<T> callback) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful())
                    callback.onResponse(response.body());
                else {
                    Log.d(TAG, "An error occurred: [" + getErrorMessage(response) + "]");
                    callback.onError(context.getString(R.string.unexpected_error));
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                Log.e(TAG, "An error occurred.", t);
                callback.onError(context.getString(R.string.unexpected_error));
            }
        });
    }

    private List<Promotion> getPromotions() {
        return sharedPreferences.contains(PROMOTIONS_KEY) ?
                Arrays.asList(
                        new Gson().fromJson(
                                sharedPreferences.getString(PROMOTIONS_KEY, ""),
                                Promotion[].class
                        )
                ) :
                null;
    }

    private void savePromotions(List<Promotion> promotions) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (promotions == null || promotions.isEmpty())
            editor.remove(PROMOTIONS_KEY);
        else
            editor.putString(PROMOTIONS_KEY, new Gson().toJson(promotions));
        editor.putString(PROMOTIONS_LAST_REFRESH_KEY, String.valueOf(new Date().getTime()));
        editor.commit();
        PromotionsHandler.getInstance().setPromotions(promotions, getOutdatedTime());
    }

    public boolean promotionsAreOutdated() {
        String lastRefreshString = sharedPreferences.getString(PROMOTIONS_LAST_REFRESH_KEY, "");
        if (lastRefreshString.isEmpty())
            return true;
        Long lastRefreshTime = Long.valueOf(lastRefreshString);
        return new Date().getTime() - lastRefreshTime > PROMOTIONS_REFRESH_INTERVAL;
    }

    private long getOutdatedTime() {
        return Long.parseLong(sharedPreferences.getString(PROMOTIONS_LAST_REFRESH_KEY, "")) + PROMOTIONS_REFRESH_INTERVAL;
    }

    private void refreshPromotions(final ApiCallback<List<Promotion>> callback) {
        handleResponse(
                promotionsApiService.getPromotions(),
                new ApiCallback<List<Promotion>>() {
                    @Override
                    public void onResponse(List<Promotion> promotions) {
                        savePromotions(promotions);
                        callback.onResponse(promotions);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        callback.onError(errorMessage);
                    }

                    @Override
                    public void onUnauthorized() {
                        onError(context.getString(R.string.errorOccurred));
                    }
                });
    }

}
