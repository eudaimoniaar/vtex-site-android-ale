package com.fizzmod.vtex.service;

import android.content.Context;

import com.fizzmod.vtex.BuildConfig;
import com.google.gson.GsonBuilder;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiClient {

    private static PromotionsApiService promotionsApiService = null;
    private static JanisApiService janisApiService = null;
    private static CouponsApiService couponsApiService = null;

    static JanisApiService getJanisApiService(Context context) {
        if (janisApiService == null)
            janisApiService = buildApiService(new JanisRequestInterceptor(context), BuildConfig.API)
                    .create(JanisApiService.class);

        return janisApiService;
    }

    static PromotionsApiService getPromotionsApiService() {
        if (promotionsApiService == null)
            promotionsApiService = buildApiService(
                    new PromotionRequestInterceptor(),
                    BuildConfig.PROMOTIONS_API_HOST)
                    .create(PromotionsApiService.class);

        return promotionsApiService;
    }

    static CouponsApiService getCouponsApiService() {
        if (couponsApiService == null)
            couponsApiService = buildApiService(new CouponRequestInterceptor(), BuildConfig.COUPONS_API)
                    .create(CouponsApiService.class);

        return couponsApiService;
    }

    private static Retrofit buildApiService(Interceptor interceptor, String apiHost) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .baseUrl(apiHost)
                .client(
                        new OkHttpClient.Builder()
                                .addInterceptor(interceptor)
                                .addNetworkInterceptor(
                                        new HttpLoggingInterceptor()
                                                .setLevel(HttpLoggingInterceptor.Level.BODY)
                                )
                                .build()
                )
                .build();
    }
}
