package com.fizzmod.vtex.service;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class AEDateTypeAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {

    private DateFormat dateFormat;

    public AEDateTypeAdapter() {
        dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss",        // This is the format I need
                Locale.getDefault());
        // This is the key line which converts the date to UTC which cannot be accessed with the default serializer
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override public synchronized JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return new JsonPrimitive(dateFormat.format(date));
    }

    @Override public synchronized Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        DateTime temp = new DateTime(jsonElement.getAsString());
        return temp.toDate();
    }

}