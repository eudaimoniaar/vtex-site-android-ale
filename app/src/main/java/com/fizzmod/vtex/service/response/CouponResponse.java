package com.fizzmod.vtex.service.response;

import java.util.ArrayList;
import java.util.List;

public class CouponResponse extends BaseResponse {

    private final Integer id;
    private final List<String> lists = new ArrayList<>();

    CouponResponse(int code, String message, Integer id) {
        super(code, message);
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public List<String> getLists() {
        return lists;
    }
}
