package com.fizzmod.vtex.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.airbnb.deeplinkdispatch.DeepLink;
import com.airbnb.deeplinkdispatch.DeepLinkModule;
import com.fizzmod.vtex.Main;

/**
 * Created by marcos on 12/03/17.
 */

@DeepLinkModule
public class DeepLinkRouter {

    public static final String ACTION_DEEP_LINK_PRODUCT = "deep_link_product";
    public static final String ACTION_DEEP_LINK_SEARCH = "deep_link_search";

    @DeepLink("https://{host}/{productLink}/p")
    public static Intent intentForDeepLinkProductPage(Context context) {
        return new Intent(context, Main.class).setAction(ACTION_DEEP_LINK_PRODUCT);
    }

    @DeepLink({ "https://{host}/busca/", "https://{host}/busca"})
    public static Intent intentForDeepLinkSearchPage(Context context, Bundle bundle) {
        return new Intent(context, Main.class).setAction(ACTION_DEEP_LINK_SEARCH);
    }
}
