/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.LegalPrice;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.PromotionsService;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Marcos Casagrande on 06/01/16.
 */
public class API {

    /**
     * API Search version that the APP uses. This is a header we send, in case VTEX does a non backwards compatibility update.
     * VTEX Does not know this header, but it will provide a way for them to implement the compatibility
     */

    private static final String APISearchVersion = "1.0.3";

    public static final int SEARCH_BY_SKU_LIMIT = 10;
    public static final int SOFT_LIMIT = 24;
    public static final int HARD_LIMIT = 49;
    public static final String SEARCH_API_FILTER_SEPARATOR = "_::_";

    public static String HOST = Utils.isEmpty(BuildConfig.VTEX_HOST) ? "http://" + BuildConfig.STORENAME + ".vtexcommercestable.com.br" : BuildConfig.VTEX_HOST;
    public static final String JANIS_API = BuildConfig.JANIS_HOST + "api/";
    public static final String FRONT_URL = BuildConfig.FRONT_HOST;

    private static final String VTEX_ID_URL = "https://vtexid.vtex.com.br/api/vtexid/pub/authenticated/user";
    private static final String API_SEARCH = "/api/catalog_system/pub/products/search/";
    private static final String VTEX_API_CATEGORIES = "/api/catalog_system/pub/category/tree/5?json";
    private static final String STORES_URL = "/files/stores.json";

    private static final String SPECIFICATIONS_URL = JANIS_API + "1/specifications";
    private static final String ORDERS_URL = JANIS_API + "1/order/vtex";
    private static final String ORDER_URL = JANIS_API + "1/order/vtex/";
    private static final String BANNERS_URL = (BuildConfig.API.isEmpty() ? BuildConfig.BANNER_API : BuildConfig.API) + "1/banner";
    private static final String CATEGORIES_URL = (BuildConfig.API.isEmpty() ? JANIS_API : BuildConfig.API) + "category/ecommerce";
    private static final String CUSTOMER_URL = JANIS_API + "2/customer/device/";
    private static final String SEARCH_API_URL = "/docs/search.json";

    public static final String CONTENT_TYPE_HEADER = "Content-type";
    public static final String JANIS_CLIENT_HEADER = "Janis-Client";
    public static final String JANIS_STORENAME_HEADER = "Janis-Storename";
    public static final String X_AUTH_HEADER = "X-Auth";
    public static final String X_TOKEN_HEADER = "X-Token";
    public static final String X_APP_PACKAGE = "X-APP-Package";

    public static final String PRODUCT_SEARCH_CATEGORY_KEY = "fq-category";

    /**
     * Get API default headers
     *
     * @return
     */

    public static HashMap<String, String> getDefaultAPIHeaders() {

        HashMap<String, String> headers = new HashMap<>();

        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put("App-OS", "Android");
        headers.put("App-OS-Version", Build.VERSION.RELEASE);
        headers.put("App-Device", Build.MODEL);
        headers.put("App-Version", BuildConfig.VERSION_NAME);

        try {
            String unix = String.valueOf(new DateTime().getMillis() / 1000);
            byte[] data = unix.getBytes("UTF-8");
            headers.put("Request-Id", Utils.randomString(5) + Base64.encodeToString(data, Base64.NO_WRAP) + Utils.randomString(16));
            headers.put("Request-Time", unix);
        } catch (UnsupportedEncodingException e) {
        } catch (IllegalArgumentException e) {
        }

        return headers;
    }

    /**
     * Resize VTEX Image
     *
     * @param src
     * @param width
     * @param height
     * @return
     */
    public static String resizeImage(String src, int width, int height) {
        src = Normalizer.normalize(src, Normalizer.Form.NFKD);
        src = src.replaceAll("[^\\p{ASCII}]", "");
        Pattern p = Pattern.compile("(/ids/[0-9]+)(-[0-9]+-[0-9]+)?");
        Matcher m = p.matcher(src);
        if (m.find()) {
            return m.replaceAll(m.group(1) + "-" + width + "-" + height);
        }

        return src;
    }


    public static String getUrl(boolean secure) {
        return "http" + (secure ? "s" : "") + "://" + FRONT_URL;
    }

    /**
     * Get API search total items
     *
     * @param resourcesHeader
     * @param responseJson
     * @return
     */
    public static int getTotalResources(String resourcesHeader, String responseJson) {

        if (resourcesHeader != null) {
            Pattern p = Pattern.compile("[0-9]+\\-[0-9]+/([0-9]+)");
            Matcher m = p.matcher(resourcesHeader);
            return m.find() ? Integer.valueOf(m.group(1)) : 0;
        }

        if (Utils.isSearchApiEnabled() && responseJson != null) {
            try {
                JSONObject searchAPIResponse = new JSONObject(responseJson);
                return searchAPIResponse.getJSONObject("data").getInt("count");
            } catch (Exception e) {
                Log.d("Search", "Error when parsing Search API response for total resources.", e);
            }
        }

        return 0;
    }

    /**
     * Perform a VTEX API Search query
     *
     * @param context
     * @param query
     * @param page
     * @param limit
     * @param callback
     */
    public static void search(Context context,
                              String query,
                              HashMap<String, String> parameters,
                              Integer page,
                              int limit,
                              Callback callback) {
        search(context, query, parameters, page, limit, false, callback);
    }

    public static void search(Context context,
                              String query,
                              HashMap<String, String> parametersMap,
                              Integer page,
                              int limit,
                              boolean isFullTextSearch,
                              final Callback callback) {
        final HashMap<String, String> parameters;
        if (parametersMap != null)
            parameters = new HashMap<>(parametersMap);
        else
            parameters = new HashMap<>();
        if (isFullTextSearch && Utils.isSearchApiEnabled()) {
            searchWithSearchAPI(query, parameters, page, limit, callback);
            return;
        }
        parameters.put("sc", Cart.getInstance().getSalesChannel());

        if (page != null) {
            int to = page * (limit) - 1;
            int from = (page - 1) * limit;
            parameters.put("_from", "" + from);
            parameters.put("_to", "" + to);
        }
        final Uri.Builder builder = Uri.parse(
                API.getUrl(true) + API.API_SEARCH + (query == null ? "" : query.trim())
        ).buildUpon();
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            builder.appendQueryParameter(
                    entry.getKey().replaceAll("\\{\\{[0-9]+\\}\\}", ""),
                    entry.getValue()
            );
        }

        if (parameters.containsKey(PRODUCT_SEARCH_CATEGORY_KEY))
            builder.appendQueryParameter("fq", parameters.get(PRODUCT_SEARCH_CATEGORY_KEY));

        //API Search cache issue.
        builder.appendQueryParameter("no-cache", String.valueOf(System.nanoTime()));

        final HashMap<String, String> headers = new HashMap<>();
        headers.put("API-Search-Version", APISearchVersion);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);

        getPromotions(context, new Runnable() {
            @Override
            public void run() {
                Request.get(builder.build().toString(), null, headers, callback);
            }
        });
    }

    /**
     * Get a Product by it's SKU
     *
     * @param context
     * @param skus
     * @param callback
     */
    public static void getProductsBySku(Context context, List<String> skus, final ApiCallback<List<Product>> callback) {

        HashMap<String, String> params = new HashMap<>();
        final CountDownLatch responseCountDownLatch = new CountDownLatch(
                (int) Math.ceil((float) skus.size() / (float) API.SEARCH_BY_SKU_LIMIT));
        final List<String> errorMessages = new ArrayList<>();
        final List<Product> allProducts = new ArrayList<>();

        for (int i = 0; i < skus.size(); i++) {
            params.put("fq{{" + i + "}}", "skuId:" + skus.get(i));
            if ((i + 1) % SEARCH_BY_SKU_LIMIT == 0 || i == skus.size() - 1) {
                search(context, null, params, null, SEARCH_BY_SKU_LIMIT, new Callback() {
                    private void onEnd(ArrayList<Product> products, String errorMessage) {
                        if (errorMessage != null)
                            errorMessages.add(errorMessage);
                        synchronized (allProducts) {
                            if (products != null)
                                allProducts.addAll(products);
                        }
                        responseCountDownLatch.countDown();
                    }

                    @Override
                    public void onFailure(Call call, IOException e) {
                        onEnd(null, e.getMessage());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        onEnd(setProductList(response.body().string()), null);
                    }
                });
                params.clear();
            }
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    responseCountDownLatch.await();
                    for (String error : errorMessages)
                        Log.e("Cart", "An error occurred when retrieving products: " + error);
                    if (errorMessages.isEmpty())
                        callback.onResponse(allProducts);
                    else
                        callback.onError(errorMessages.get(0));
                } catch (InterruptedException e) {
                    Log.e("Cart", "An exception was thrown in method 'restore'.", e);
                }
            }
        }).start();
    }

    /**
     * Get a Product by its ID
     *
     * @param context
     * @param productId
     * @param callback
     */
    public static void getProduct(Context context, String productId, final Callback callback) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("fq", "productId:" + productId);
        params.put("sc", Cart.getInstance().getSalesChannel());
        getPromotions(context, new Runnable() {
            @Override
            public void run() {
                Request.get(API.getUrl(true) + API.API_SEARCH, params, null, callback);
            }
        });
    }

    /**
     * Get a Product by its link
     *
     * @param context
     * @param link
     * @param callback
     */
    public static void getProductByLink(Context context, String link, final Callback callback) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("sc", Cart.getInstance().getSalesChannel());
        final StringBuilder linkBuilder = new StringBuilder(link);
        if (link.startsWith("/"))
            linkBuilder.deleteCharAt(0);
        if (!link.endsWith("/p"))
            linkBuilder.append("/p");
        getPromotions(context, new Runnable() {
            @Override
            public void run() {
                Request.get(API.getUrl(true) + API.API_SEARCH + linkBuilder.toString(), params, null, callback);
            }
        });
    }

    /**
     * Get a Product by it's EAN
     *
     * @param context
     * @param EAN
     * @param callback
     */
    public static void getProductByEan(Context context, String EAN, final Callback callback) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("fq", "alternateIds_Ean:" + EAN);
        params.put("sc", Cart.getInstance().getSalesChannel());
        getPromotions(context, new Runnable() {
            @Override
            public void run() {
                Request.get(API.getUrl(true) + API.API_SEARCH, params, null, callback);
            }
        });
    }

    /**
     * Get VTEX Category tree.
     *
     * @param callback
     * @return
     */

    public static Call getCategories(Callback callback) {

        String URL = Config.getInstance().isCategoryGrouping() ? CATEGORIES_URL : API.HOST + API.VTEX_API_CATEGORIES;

        HashMap<String, String> headers;

        if (Config.getInstance().isCategoryGrouping())
            headers = getDefaultAPIHeaders();
        else
            headers = new HashMap<>();

        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        HashMap<String, String> clientHeaders = Config.getInstance().getExtraHeaders();
        for (HashMap.Entry<String, String> entry : clientHeaders.entrySet()) {
            headers.put(entry.getKey(), entry.getValue());
        }

        return Request.get(URL, null, headers, callback);
    }

    /**
     * Get VTEX Category tree.
     *
     * @param callback
     * @return
     */

    public static Call getVtexCategories(Callback callback) {

        String URL = API.HOST + API.VTEX_API_CATEGORIES;

        HashMap<String, String> headers = new HashMap<>();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        return Request.get(URL, null, headers, callback);
    }


    /**
     * Get users orders
     *
     * @param callback
     * @return
     */

    public static Call getOrders(User user, int page, Callback callback) {

        HashMap<String, String> params = new HashMap<>();
        params.put("page", String.valueOf(page));

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
//        HashMap<String, String> headers = getDefaultAPIHeaders(true);
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_AUTH_HEADER, user.getLoginType());
        headers.put(X_TOKEN_HEADER, user.getToken());
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        Utils.log("Getting order from " + API.ORDERS_URL + " using: " + params.get("auth"));
        Utils.log("Token: " + params.get("token"));

        return Request.get(API.ORDERS_URL, params, headers, callback);
    }

    /**
     * Get Order by ID
     *
     * @param callback
     * @return
     */

    public static Call getOrder(User user, String id, Callback callback) {

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_AUTH_HEADER, user.getLoginType());
        headers.put(X_TOKEN_HEADER, user.getToken());
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        return Request.get(API.ORDER_URL + id, null, headers, callback);
    }


    /**
     * Get Stores
     *
     * @param callback
     * @return
     */

    public static Call getStores(Callback callback) {

        String URL = HOST + STORES_URL;
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        return Request.get(URL, null, headers, callback);

    }

    /**
     * Get Order by ID
     *
     * @param isMainSlider
     * @param callback
     * @return
     */

    public static Call getBanners(boolean isMainSlider, Callback callback) {

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();;
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        HashMap<String, String> params = new HashMap<>();
        params.put("onlyCurrentActive", "true");
        params.put("platform", "1");
        params.put("section", isMainSlider ? "1" : "2");

        return Request.get(API.BANNERS_URL, params, headers, callback);
    }


    /**
     * Get Specifications by category
     *
     * @param callback
     * @return
     */

    public static Call getSpecifications(String category, Callback callback) {

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        HashMap<String, String> params = new HashMap<>();
        params.put("category", category);
        params.put("sc", Cart.getInstance().getSalesChannel());
        return Request.get(SPECIFICATIONS_URL, params, headers, callback);

    }


    /**
     * Get User info by token
     *
     * @return
     */

    public static Call vtexId(String token, Callback callback) {

        HashMap<String, String> params = new HashMap<>();
        params.put("authToken", token.replaceAll(" ", "+"));

        return Request.get(VTEX_ID_URL, params, null, callback);

    }

    /**
     * Get User info by token
     *
     * @return
     */

    public static Call registerUser(Context context, Callback callback) {

        String url = CUSTOMER_URL;
        User user = User.getInstance(context);

        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());

        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();;
        headers.put(CONTENT_TYPE_HEADER, "application/json");
        headers.put(JANIS_CLIENT_HEADER, BuildConfig.JANIS_CLIENT);
        headers.put(X_AUTH_HEADER, user.getSignInLoginType());
        headers.put(X_TOKEN_HEADER, user.getSignInToken());
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);
        try {
            url += "/" + new DeviceIdFactory(context).getDeviceId();
        } catch (Exception e) {
        }

        return Request.put(url, params, headers, callback);

    }

    /**
     * Setup Product List
     *
     * @param JSON
     * @return
     */

    public static ArrayList<Product> setProductList(String JSON) {
        return Utils.isSearchApiEnabled() && JSON.startsWith("{") ?
                setSearchAPIProductList(JSON) :
                setProductList(JSON, null);
    }

    public static ArrayList<Product> setProductList(String JSON, String excludeProduct) {
        try {
            return setProductList(JSON, excludeProduct, false);
        } catch (JSONException e) {
            return new ArrayList<>();
        }
    }

    public static ArrayList<Product> setProductList(String JSON, String excludeProduct, boolean throwException) throws JSONException {
        ArrayList<Product> productList = new ArrayList<Product>();

        try {
            JSONArray items = new JSONArray(JSON);

            ArrayList<String> checker = new ArrayList<>();

            if (excludeProduct != null)
                checker.add(excludeProduct);

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                String productId = item.getString("productId");
                boolean usesOtherCurrency = Config.getInstance().usesOtherCurrency(item);

                if (!checker.contains(productId)) {

                    checker.add(productId);
                    Product product = new Product();
                    product.setId(productId);
                    product.setName(item.getString("productName"));
                    product.setDescription(item.getString("description"));
                    product.setBrand(item.getString("brand"));
                    product.setUrl(item.getString("link"));
                    product.setRefId(item.getString("productReference"));
                    String productData = item.optString("_ProductData");
                    if (productData != null) {
                        product.setBrandId(Utils.extractBrandId(productData));
                    }

                    HashMap<String, LegalPrice> legalPrices = parseLegalPrice(item);

                    try {
                        product.setCategories(item.getJSONArray("categories").getString(0));
                        product.setCategoriesId(item.getJSONArray("categoriesIds").getString(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONArray allSpecifications = item.optJSONArray("allSpecifications");

                    if (allSpecifications != null) {
                        for (int s = 0; s < allSpecifications.length(); s++) {
                            String spec = allSpecifications.getString(s);
                            String value = Utils.implodeJSONArray(item.getJSONArray(spec));
                            product.addSpecifications(spec, value);
                        }
                    }

                    /**
                     *
                     *  SKU DATA
                     *
                     **/

                    JSONArray skus = item.getJSONArray("items");
                    for (int j = 0; j < skus.length(); j++) {
                        JSONObject skuData = skus.getJSONObject(j);
                        JSONArray imagesArray = skuData.getJSONArray("images");
                        JSONObject priceData = ((JSONObject) skuData.getJSONArray("sellers").get(0)).getJSONObject("commertialOffer");
                        JSONArray installments = priceData.optJSONArray("Installments");

                        JSONArray variationsTemp = skuData.optJSONArray("variations");
                        ArrayList<String> variations = new ArrayList<>();
                        if (variationsTemp != null) {
                            variationsTemp = skuData.getJSONArray("variations");
                            for (int k = 0; k < variationsTemp.length(); k++) {
                                String variation = variationsTemp.getString(k);

                                String value = skuData.getJSONArray(variation).getString(0);
                                if (Sku.isValidVariationValue(value))
                                    variations.add(variation);

                            }
                        }

                        if (j == 0) {
                            product.setImage(((JSONObject) imagesArray.get(0)).getString("imageUrl"));
                        }

                        Sku sku = new Sku();
                        sku.setId(skuData.getString("itemId"));
                        sku.setName(skuData.getString("name"));
                        sku.setListPrice(priceData.getDouble("ListPrice"));
                        sku.setStock(priceData.getInt("AvailableQuantity"));
                        sku.setBestPrice(priceData.getDouble("Price"));
                        sku.setUnitMultiplier(skuData.getDouble("unitMultiplier"));
                        sku.setMeasurementUnit(skuData.getString("measurementUnit"));
                        sku.setUsesOtherCurrency(usesOtherCurrency);

                        try {
                            sku.setRefId(skuData.getJSONArray("referenceId").getJSONObject(0).getString("Value"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        /**
                         * Set Legal Price
                         */

                        String legalPriceKey = null;

                        if (Config.getInstance().getLegalPriceKey() == LegalPrice.KEY_REF_ID)
                            legalPriceKey = sku.getRefId();
                        else if (Config.getInstance().getLegalPriceKey() == LegalPrice.KEY_SKU_ID)
                            legalPriceKey = sku.getId();

                        LegalPrice legalPrice = legalPrices.get(legalPriceKey);
                        sku.setLegalPrice(legalPrice);

                        // Product fields
                        sku.setProductId(productId);
                        sku.setProductName(item.getString("productName"));
                        sku.setBrand(item.getString("brand"));
                        String skuProductData = item.optString("_ProductData");
                        if (skuProductData != null) {
                            sku.setBrandId(Utils.extractBrandId(skuProductData));
                        }
                        // End SKU product fields

                        if (variations.size() > 0) {
                            sku.setVariations(variations.toArray(new String[variations.size()]));

                            for (int k = 0; k < variations.size(); k++) {
                                String variation = variations.get(k);
                                String value = skuData.getJSONArray(variation).getString(0);
                                sku.addVariationsValue(variation, value);
                            }
                        }

                        String[] images = new String[imagesArray.length()];
                        for (int k = 0; k < imagesArray.length(); k++) {
                            images[k] = ((JSONObject) imagesArray.get(0)).getString("imageUrl");
                        }
                        sku.setImages(images);

                        double bestInstallmentValue = 0;
                        int bestInstallment = 1;
                        if (installments != null) {
                            for (int k = 0; k < installments.length(); k++) {
                                JSONObject installment = installments.getJSONObject(k);
                                int numberOfInstallments = installment.getInt("NumberOfInstallments");
                                double interest = installment.getDouble("InterestRate");
                                if (numberOfInstallments > bestInstallment && interest == 0.0f) {
                                    bestInstallment = numberOfInstallments;
                                    bestInstallmentValue = installment.getDouble("Value");
                                }
                            }
                        }

                        sku.setInstallments(bestInstallment);
                        sku.setInstallmentPrice(bestInstallmentValue);

                        /** Add sku to Product **/
                        product.addSku(sku);
                    }

                    PromotionsHandler.getInstance().setPromotionToProduct(product);

                    productList.add(product);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            if (throwException)
                throw e;
            Utils.log(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            if (throwException)
                throw e;

            Utils.log(e.getMessage());
        }

        return productList;
    }

    public static ArrayList<Filter> setSpecificationList(String JSON) {
        Utils.longInfo("Setting specification list: " + JSON);
        try {
            JSONObject responseJson = new JSONObject(JSON);
            if (responseJson.has("specifications"))
                return setVtexSpecificationList(responseJson);
            else if (Utils.isSearchApiEnabled())
                return setSearchSpecificationList(responseJson);
        } catch (JSONException e) {
            Log.d("API - Filters", "Error when parsing specifications (filters) list.", e);
        }

        return new ArrayList<>();
    }

    /* *************** *
     * Private methods *
     * *************** */

    private static ArrayList<Product> setSearchAPIProductList(String JSON) {
        ArrayList<Product> productsList = new ArrayList<>();
        try {
            JSONArray productsJsonArray = new JSONObject(JSON).getJSONObject("data").getJSONArray("views");
            for (int i = 0; i < productsJsonArray.length(); i++) {

                JSONObject productJson = productsJsonArray.getJSONObject(i);
                JSONObject salesChannelJson = getSearchAPISalesChannelJson(productJson.getJSONArray("sales_channel_data"));
                if (salesChannelJson == null)
                    continue;

                Product product = new Product();
                product.setId(productJson.getString("product_id"));
                product.setName(productJson.getString("title"));
                product.setDescription(product.getName());
                product.setBrand(productJson.getString("brand"));
                product.setUrl(FRONT_URL + "/" + productJson.getString("permalink"));
                product.setRefId(productJson.getString("upc"));
                product.setImage(productJson.getString("image"));

                JSONObject categoryJson = productJson.getJSONObject("taxon");
                product.setCategories(categoryJson.getString("name"));
                product.setCategoriesId(categoryJson.getString("id"));

                // SKU DATA
                Sku sku = new Sku();
                sku.setId(productJson.getString("sku_id"));
                sku.setName(product.getName());
                sku.setAvailable(productJson.getBoolean("available"));
                sku.setImages(new String[]{product.getImage()});

                JSONObject skuData = productJson.getJSONObject("product_data");
                sku.setUnitMultiplier(skuData.getDouble("unit_multiplier"));
                sku.setMeasurementUnit(skuData.getString("measurement_unit"));

                // Set Legal Price
                JSONObject legalPriceJson = productJson.optJSONObject("legal_price");
                if (legalPriceJson != null) {
                    sku.setLegalPrice(new LegalPrice(
                            legalPriceJson.optString("unit", "UN"),
                            legalPriceJson.optDouble("unit_multiplier", 1)
                    ));
                }

                double bestPrice = salesChannelJson.getDouble("best_price") / 100;
                sku.setBestPrice(bestPrice);
                double listPrice = salesChannelJson.getDouble("list_price") / 100;
                sku.setListPrice(listPrice == 0 ? bestPrice : listPrice);
                sku.setRefId(product.getRefId());

                // Product fields
                sku.setProductId(product.getId());
                sku.setProductName(product.getName());
                sku.setBrand(product.getBrand());
                sku.setBrandId(product.getBrandId());
                // End SKU product fields

                // Add sku to Product
                product.addSku(sku);

                PromotionsHandler.getInstance().setPromotionToProduct(product);

                productsList.add(product);
            }
        } catch (JSONException e) {
            Log.d("Search", "Error when parsing products list from Search API", e);
        }

        return productsList;
    }

    private static JSONObject getSearchAPISalesChannelJson(JSONArray salesChannelsJsonArray) {
        String salesChannel = Cart.getInstance().getSalesChannel();
        try {
            for (int i = 0; i < salesChannelsJsonArray.length(); i++) {
                JSONObject salesChannelJson = (JSONObject) salesChannelsJsonArray.get(i);
                if (salesChannel.equals(salesChannelJson.getString("sales_channel_id")))
                    return salesChannelJson;
            }
        } catch (Exception e) {
            Log.d("Search", "Error when parsing sales channel from Search response", e);
        }
        return null;
    }

    private static void searchWithSearchAPI(String text, HashMap<String, String> parameters, Integer page, int limit, Callback callback) {
        Uri.Builder urlBuilder = Uri.parse(BuildConfig.SEARCH_API + SEARCH_API_URL).buildUpon()
                .appendQueryParameter("bucket", BuildConfig.SEARCH_API_BUCKET)
                .appendQueryParameter("family", BuildConfig.SEARCH_API_FAMILY)
                .appendQueryParameter("view", BuildConfig.SEARCH_API_VIEW)
                .appendQueryParameter("window", String.valueOf(limit))
                .appendQueryParameter("attributes[sales_channel][]", Cart.getInstance().getSalesChannel())
                .appendQueryParameter("text", text);
        setSearchSort(urlBuilder, parameters.get("O"));
        setSearchFilters(urlBuilder, parameters);
        if (page != null)
            urlBuilder.appendQueryParameter("page", page.toString());
        if (parameters.containsKey(PRODUCT_SEARCH_CATEGORY_KEY))
            urlBuilder.appendQueryParameter("taxonomies[]", parameters.get(PRODUCT_SEARCH_CATEGORY_KEY));
        Request.get(urlBuilder.toString(), null, null, callback);
    }

    private static void setSearchSort(Uri.Builder urlBuilder, String sortType) {
        if (Utils.isEmpty(sortType))
            return;
        String searchSortType = null;
        switch (sortType) {
            case Search.PRODUCT_SORT_NAME_ASC:
            case Search.PRODUCT_SORT_NAME_DESC:
            case Search.PRODUCT_SORT_PRICE_ASC:
                urlBuilder.appendQueryParameter("direction", "1");
            case Search.PRODUCT_SORT_PRICE_DESC:
                searchSortType = "price_sv";
                break;
            case Search.PRODUCT_SORT_SALE_DESC:
                searchSortType = "buys_sv";
                break;
            case Search.PRODUCT_SORT_RATE_DESC:
                searchSortType = "rating_sv";
                break;
            case Search.PRODUCT_SORT_DISCOUNT_DESC:
                searchSortType = "discount_sv";
                break;
            case Search.PRODUCT_SORT_RELEASE_DATE_DESC:
                searchSortType = "release_date_sv";
                break;
        }
        if (searchSortType != null)
            urlBuilder.appendQueryParameter("sort", searchSortType);
    }

    private static void setSearchFilters(Uri.Builder urlBuilder, HashMap<String, String> parameters) {
        for (Map.Entry<String, String> paramEntry : parameters.entrySet())
            if (paramEntry.getKey().startsWith("fq{{") && paramEntry.getValue().contains(SEARCH_API_FILTER_SEPARATOR)) {
                String[] valueSplit = paramEntry.getValue().split(SEARCH_API_FILTER_SEPARATOR);
                urlBuilder.appendQueryParameter("attributes[" + valueSplit[0] + "][]", valueSplit[1]);
            }
    }

    private static ArrayList<Filter> setVtexSpecificationList(JSONObject responseJson) throws JSONException {
        ArrayList<Filter> specificationList = new ArrayList<>();
        JSONArray items = responseJson.getJSONArray("specifications");
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);
            specificationList.add(new Filter(
                    item.getString("name"),
                    item.getString("rel"),
                    Utils.JSONArrayToArrayList(item.getJSONArray("values")),
                    Utils.JSONArrayToArrayList(item.getJSONArray("text"))
            ));
        }
        return specificationList;
    }

    private static ArrayList<Filter> setSearchSpecificationList(JSONObject responseJson) throws JSONException {
        ArrayList<Filter> filters = new ArrayList<>();
        JSONArray attributesJsonArray = responseJson.getJSONObject("data").getJSONArray("attributes");
        for (int i = 0; i < attributesJsonArray.length(); i++) {
            JSONObject itemJson = attributesJsonArray.getJSONObject(i);
            String itemId = itemJson.optString("_id");
            String itemName = itemJson.getString("name_presentation");
            if (itemName.isEmpty() ||
                    itemId.isEmpty() ||
                    itemId.equalsIgnoreCase("_Vendor") ||
                    itemId.equalsIgnoreCase("sales_channel"))
                continue;
            ArrayList<String> values = new ArrayList<>();
            ArrayList<String> names = new ArrayList<>();
            JSONArray valuesJsonArray = itemJson.getJSONArray("values");
            for (int j = 0; j < valuesJsonArray.length(); j++) {
                JSONObject valueJson = valuesJsonArray.getJSONObject(j);
                String value = valueJson.optString("value");
                String name = valueJson.optString("value_presentation");
                if (value.isEmpty() || name.isEmpty())
                    continue;
                values.add(value);
                names.add(name);
            }
            filters.add(new Filter(
                    itemName,
                    itemId + SEARCH_API_FILTER_SEPARATOR,
                    values,
                    names
            ));
        }
        return filters;
    }

    /**
     * Make sure promotions are up-to-date before getting products.
     */
    private static void getPromotions(Context context, final Runnable callback) {
        if (!BuildConfig.PROMOTIONS_API_ENABLED) {
            callback.run();
            return;
        }
        PromotionsService.getInstance(context).getPromotions(new ApiCallback<List<Promotion>>() {
            @Override
            public void onResponse(List<Promotion> object) {
                callback.run();
            }

            @Override
            public void onError(String errorMessage) {
                callback.run();
            }

            @Override
            public void onUnauthorized() {
                callback.run();
            }
        });
    }

    /**
     * Get Legal price
     *
     * @param item
     * @return
     */

    public static HashMap<String, LegalPrice> parseLegalPrice(JSONObject item) {

        HashMap<String, LegalPrice> legalPrices = new HashMap<>();

        try {
            String legalPriceJson = item.getJSONArray("Precio Legal").getString(0);

            JSONObject legalPrice = new JSONObject(legalPriceJson);

            Iterator<?> keys = legalPrice.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                JSONObject itemInfo = legalPrice.getJSONObject(key);
                if (itemInfo instanceof JSONObject) {

                    LegalPrice lp = new LegalPrice(
                            itemInfo.optString("unit", "UN"),
                            itemInfo.optDouble("multiplier", 1)
                    );

                    legalPrices.put(key, lp);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return legalPrices;

    }
}
