package com.fizzmod.vtex.utils;

import android.content.Context;

import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.User;

public abstract class RetryableApiCallback<T> implements ApiCallback<T> {

    private int retries = 0;
    private final Context context;
    private int maxRetries = 1;

    public RetryableApiCallback(Context context) {
        this.context = context;
    }

    public RetryableApiCallback(Context context, int maxRetries) {
        this.context = context;
        this.maxRetries = maxRetries;
    }

    @Override
    public void onUnauthorized() {
        final User user = User.getInstance(context);
        if (Utils.isEmpty(user.getSignInToken())) {
            requestSignIn();
            return;
        }
        User.save(user, context, new Callback() {
            @Override
            public void run(Object isCallSuccessful) {
                if ((Boolean) isCallSuccessful && retries < maxRetries) {
                    retries++;
                    retry();
                } else
                    requestSignIn();
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    protected abstract void retry();

    protected abstract void requestSignIn();

}
