package com.fizzmod.vtex.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreferences {
    private SharedPreferences prefs;
    private Context context;

    public static final String FIREBASE_CLOUD_MESSAGING = "bhfcm";
    public static final String SET_NOTIFY = "set_notify";
    public static final String FB_TOKEN = "token";
    public static final String SET_RETRY = "set_retry";

    public MySharedPreferences(Context context){
        this.context = context;
        prefs = context.getSharedPreferences(FIREBASE_CLOUD_MESSAGING, Context.MODE_PRIVATE);
    }

    public void saveNotificationSubscription(boolean value){
        SharedPreferences.Editor edits = prefs.edit();
        edits.putBoolean(SET_NOTIFY, value);
        edits.apply();
    }

    public boolean hasUserSubscribeToNotification(){
        return prefs.getBoolean(SET_NOTIFY, false);
    }

    public void clearAllSubscriptions(){
        prefs.edit().clear().apply();
    }

    public void saveToken(String token){
        SharedPreferences.Editor edits = prefs.edit();
        edits.putString(FB_TOKEN, token);
        edits.apply();
    }

    public String getToken(){
        return prefs.getString(FB_TOKEN, null);
    }

    public void setRetry(boolean retry){
        SharedPreferences.Editor edits = prefs.edit();
        edits.putBoolean(SET_RETRY, retry);
        edits.apply();
    }

    public boolean shouldRetry(){
        return prefs.getBoolean(SET_RETRY, false);
    }
}