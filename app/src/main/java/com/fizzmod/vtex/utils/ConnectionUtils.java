package com.fizzmod.vtex.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


public class ConnectionUtils {


    final public static int NO_CONNECTION = 0;
    final public static int WIFI_CONNECTION = 1;
    final public static int MOBILE_CONNECTION = 2;

    private static ConnectionUtils instance = new ConnectionUtils();

    static Context context;
    static ConnectivityManager connectivityManager;
    static TelephonyManager telephonyManager;


    public static ConnectionUtils getInstance(Context context) {
        ConnectionUtils.context = context.getApplicationContext();

        if(ConnectionUtils.connectivityManager == null)
            ConnectionUtils.connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(ConnectionUtils.telephonyManager == null)
            ConnectionUtils.telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        return instance;
    }

    /**
     * Check if there is a network available
     * @return boolean
     */

    public boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ConnectionUtils.connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Check whether the device is connected to WIFI or MOBILE network
     * @return
     */
    public int getConnectionType() {

        NetworkInfo[] netInfo = ConnectionUtils.connectivityManager.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    return WIFI_CONNECTION;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    return MOBILE_CONNECTION;
        }
        return NO_CONNECTION;
    }


    /**
     * Check if there is an internet connection (Do not confuse with network available)
     * WARNING: MUST BE CALLED OUTSIDE THE MAIN THREAD (ASYNC TASK, THREAD, OR HANDLER)
     * @return
     */

    public boolean hasInternetAccess() {
        if (this.isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://clients3.google.com/generate_204").openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();

                return (urlc.getResponseCode() == 204 && urlc.getContentLength() == 0);

            } catch (IOException e) {


            }
        }
        return false;
    }

    /**
     * Check if there is fast connectivity
     * @return
     */
    public boolean isConnectedFast(){
        NetworkInfo info = ConnectionUtils.connectivityManager.getActiveNetworkInfo();
        return (this.isNetworkAvailable() && this.isConnectionFast(info.getType(), ConnectionUtils.telephonyManager.getNetworkType()));
    }

    /**
     * Check if the connection is fast
     * @param type
     * @param subType
     * @return
     */

    public boolean isConnectionFast(int type, int subType){

        if(type == ConnectivityManager.TYPE_WIFI){
            return true;
        }else if(type == ConnectivityManager.TYPE_MOBILE){

            switch(subType){
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
				/*
				 * Above API level 7, make sure to set android:targetSdkVersion
				 * to appropriate level to use these
				 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        }
        return false;
    }

}
