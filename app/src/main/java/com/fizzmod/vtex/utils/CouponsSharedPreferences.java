package com.fizzmod.vtex.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class CouponsSharedPreferences {

    private final static String COUPONS_SHARED_PREFERENCES = "Coupons";
    private final static String COUPONS_CI = "COUPONS_CI";
    private final static String COUPONS_CARD = "COUPONS_CARD";
    private final static String COUPONS_SAVED_USER_EMAIL = "COUPONS_SAVED_USER_EMAIL";

    private static CouponsSharedPreferences instance;

    private final SharedPreferences sharedPreferences;

    private CouponsSharedPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(COUPONS_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static CouponsSharedPreferences getInstance(Context context) {
        if (instance == null)
            instance = new CouponsSharedPreferences(context.getApplicationContext());
        return instance;
    }

    public String getEmail() {
        return sharedPreferences.getString(COUPONS_SAVED_USER_EMAIL, "");
    }

    public void saveEmail(String email) {
        sharedPreferences.edit().putString(COUPONS_SAVED_USER_EMAIL, email).apply();
    }

    public boolean hasCI() {
        return sharedPreferences.contains(COUPONS_CI);
    }

    public void saveCI(String ci) {
        sharedPreferences.edit().putString(COUPONS_CI, ci).apply();
    }

    public String getCI() {
        return sharedPreferences.getString(COUPONS_CI, "");
    }

    public void deleteCI() {
        sharedPreferences.edit().remove(COUPONS_CI).apply();
    }

    public boolean hasCard() {
        return sharedPreferences.contains(COUPONS_CARD);
    }

    public void saveCard(String card) {
        sharedPreferences.edit().putString(COUPONS_CARD, card).apply();
    }

    public String getCard() {
        return sharedPreferences.getString(COUPONS_CARD, "");
    }

    public void deleteCard() {
        sharedPreferences.edit().remove(COUPONS_CARD).apply();
    }


}
