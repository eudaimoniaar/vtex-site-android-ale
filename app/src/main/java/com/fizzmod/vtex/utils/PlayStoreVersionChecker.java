package com.fizzmod.vtex.utils;

import android.os.AsyncTask;

import com.fizzmod.vtex.BuildConfig;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PlayStoreVersionChecker extends AsyncTask<String, String, String> {

    public String playStoreVersion = "0.0.0";

    private OkHttpClient client = new OkHttpClient();

    private String execute(String url) throws IOException {
        okhttp3.Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    public String doInBackground(String... params) {
        try {
            String html = execute("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "&hl=en");
            Pattern blockPattern = Pattern.compile("Current Version.*([0-9]+\\.[0-9]+\\.[0-9]+)</span>");
            Matcher blockMatch = blockPattern.matcher(html);
            if(blockMatch.find()) {
                Pattern versionPattern = Pattern.compile("[0-9]+\\.[0-9]+\\.[0-9]+");
                Matcher versionMatch = versionPattern.matcher(blockMatch.group(0));
                if(versionMatch.find()) {
                    playStoreVersion = versionMatch.group(0);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return playStoreVersion;
    }


}