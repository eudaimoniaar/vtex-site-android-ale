package com.fizzmod.vtex.utils;

import android.content.Context;
import android.os.Bundle;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.fizzmod.vtex.BuildConfig;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;

public class AnalyticsUtils {

    /*****************
     * Custom events *
     *****************/
    private static final String METHOD = "method";
    private static final String SCAN_OPEN = "SCAN_OPEN";
    private static final String SCAN_SEARCH_OK = "SCAN_SEARCH_OK";
    private static final String SCAN_SEARCH_NONE = "SCAN_SEARCH_NONE";

    /*****************
     * Custom params *
     *****************/
    private static final String EAN = "EAN";
    private static final String VTEX = "vtex";
    private static final String GOOGLE = "google";

    private static FirebaseAnalytics mFirebaseAnalyticsInstance;
    private static AppEventsLogger mAppEventsLogger;

    private static FirebaseAnalytics getFirebaseAnalyticsInstance(Context context) {
        if (mFirebaseAnalyticsInstance == null)
            mFirebaseAnalyticsInstance = FirebaseAnalytics.getInstance(context);
        return mFirebaseAnalyticsInstance;
    }

    private static AppEventsLogger getFacebookEventsLogger(Context context) {
        if (mAppEventsLogger == null)
            mAppEventsLogger = AppEventsLogger.newLogger(context.getApplicationContext());
        return mAppEventsLogger;
    }

    public static void logSearchEvent(Context context, String searchQuery) {
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(Param.SEARCH_TERM, searchQuery);
            getFirebaseAnalyticsInstance(context).logEvent(Event.SEARCH, bundle);
        }
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, searchQuery);
            getFacebookEventsLogger(context).logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, bundle);
        }
    }

    public static void logViewItemEvent(Context context, String itemId, String itemName) {
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(Param.ITEM_ID, itemId);
            bundle.putString(Param.ITEM_NAME, itemName);
            getFirebaseAnalyticsInstance(context).logEvent(Event.VIEW_ITEM, bundle);
        }
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, itemId);
            bundle.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, itemName);
            getFacebookEventsLogger(context).logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, bundle);
        }
    }

    public static void logAddToCartEvent(Context context, String itemId, String itemName) {
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(Param.ITEM_ID, itemId);
            bundle.putString(Param.ITEM_NAME, itemName);
            getFirebaseAnalyticsInstance(context).logEvent(Event.ADD_TO_CART, bundle);
        }
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, itemId);
            bundle.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, itemName);
            getFacebookEventsLogger(context).logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, bundle);
        }
    }

    /**
     * This method does not log a purchase. This is used to notify that the user clicked the "Buy"
     * button and was redirected to the corresponding web.
     * */
    public static void logEcommercePurchaseEvent(Context context) {
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED)
            getFirebaseAnalyticsInstance(context).logEvent(Event.ECOMMERCE_PURCHASE, null);
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED)
            getFacebookEventsLogger(context).logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT);
    }

    /*
    * FirebaseAnalytics.Param.SIGN_UP_METHOD was deprecated.
    * FirebaseAnalytics.Param.METHOD not found in version 9.8.0
    * */
    public static void logLoginEvent(Context context, boolean fromVtex) {
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED) {
            Bundle bundle = new Bundle();
            bundle.putString(METHOD, fromVtex ? VTEX : GOOGLE);
            getFirebaseAnalyticsInstance(context).logEvent(Event.LOGIN, bundle);
        }
    }

    public static void logScanOpenEvent(Context context) {
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED)
            getFirebaseAnalyticsInstance(context).logEvent(SCAN_OPEN, null);
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED)
            getFacebookEventsLogger(context).logEvent(SCAN_OPEN, null);
    }

    public static void logScanSearchOkEvent(Context context, String ean) {
        Bundle bundle = new Bundle();
        bundle.putString(EAN, ean);
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED)
            getFirebaseAnalyticsInstance(context).logEvent(SCAN_SEARCH_OK, bundle);
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED)
            getFacebookEventsLogger(context).logEvent(SCAN_SEARCH_OK, bundle);
    }

    public static void logScanSearchNoneEvent(Context context, String ean) {
        Bundle bundle = new Bundle();
        bundle.putString(EAN, ean);
        if (BuildConfig.FIREBASE_ANALYTICS_ENABLED)
            getFirebaseAnalyticsInstance(context).logEvent(SCAN_SEARCH_NONE, bundle);
        if (BuildConfig.FACEBOOK_ANALYTICS_ENABLED)
            getFacebookEventsLogger(context).logEvent(SCAN_SEARCH_NONE, bundle);
    }

}
