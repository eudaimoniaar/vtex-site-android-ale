package com.fizzmod.vtex.utils;

import android.view.View;
import android.view.animation.TranslateAnimation;

/**
 * Created by marcos on 05/06/17.
 */

public class Animations {

    public static void slideToBottom(View view, int duration){
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    public static void slideToTop(View view, int duration){
        TranslateAnimation animate = new TranslateAnimation(0, 0, view.getHeight(), 0);
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }
}
