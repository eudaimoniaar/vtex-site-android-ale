/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.utils;


import android.net.Uri;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class Request {


    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final int CONNECT_TIMEOUT = 15;
    public static final int READ_TIMEOUT = 30;


    public static OkHttpClient client = null;

    /**
     * Get the current OkHttpClient or creates a new one if it doesn't exists
     * @return OkHttpClient
     */

    public static OkHttpClient getClient(){

        if(client == null){

            client = new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS) // connect timeout
                    .writeTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS) // socket timeout
                    .build();
        }

        return client;
    }

    /**
     * HTTP POST Request - Raw Data
     * @param url String
     * @param parameters parameters (Key Value)
     * @param headers headers
     * @param callback
     * @return
     */

    public static Call post(String url, HashMap<String, String> parameters, HashMap<String, String> headers, Callback callback) {

        String data = null;
        if(parameters != null){
            JSONObject JSON = new JSONObject(parameters);
            data = JSON.toString();
        }

        return Request.post(url, data, headers, callback);

    }

    /**
     * HTTP POST Request - Raw Data
     * @param url
     * @param data (JSON)
     * @param headers
     * @param callback
     * @return
     */

    public static Call post(String url, String data, HashMap<String, String> headers, Callback callback) {

        /** Setup Headers **/
        Headers.Builder headersBuilder = new Headers.Builder();

        if(headers != null) {
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                headersBuilder.add(entry.getKey(), entry.getValue());
            }
        }
        okhttp3.RequestBody body = RequestBody.create(JSON, data);

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .post(body)
                .headers(headersBuilder.build())
                .build();

        Call call = getClient().newCall(request);
        call.enqueue(callback);
        return call;

    }

    /**
     * HTTP PUT Request - Raw Data
     * @param url
     * @param parameters parameters (Key Value)
     * @param headers
     * @param callback
     * @return
     */

    public static Call put(String url, HashMap<String, String> parameters, HashMap<String, String> headers, Callback callback) {

        String data = "";
        if(parameters != null){
            JSONObject JSON = new JSONObject(parameters);
            data = JSON.toString();
        }

        /** Setup Headers **/
        Headers.Builder headersBuilder = new Headers.Builder();

        if(headers != null) {
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                headersBuilder.add(entry.getKey(), entry.getValue());
            }
        }
        okhttp3.RequestBody body = RequestBody.create(JSON, data);

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .put(body)
                .headers(headersBuilder.build())
                .build();

        Call call = getClient().newCall(request);
        call.enqueue(callback);
        return call;

    }

    /**
     * HTTP GET Request
     * @param url
     * @param callback
     * @return
     */

    public static Call get(String url, Callback callback) {

        return Request.get(url, null, null, callback);

    }

    /**
     * HTTP GET Request
     * @param url
     * @param headers
     * @param callback
     * @return
     */

    public static Call get(String url, HashMap<String, String> params, HashMap<String, String> headers, Callback callback) {

        /** Setup Headers **/
        Headers.Builder headersBuilder = new Headers.Builder();
        Uri.Builder URLBuilder = Uri.parse(url).buildUpon();

        if(headers instanceof  HashMap){
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                headersBuilder.add(entry.getKey(), entry.getValue());
            }
        }

        if(params instanceof HashMap){
            Set<Map.Entry<String, String>> entrySet = params.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                URLBuilder.appendQueryParameter(
                        entry.getKey().replaceAll("\\{\\{[0-9]+\\}\\}", ""),
                        entry.getValue()
                );
            }
        }

        url = URLBuilder.build().toString();
        Utils.log("Requesting: " + url);
        Utils.log("Headers: " + (headers != null ? headers.toString() : "{}"));

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .get()
                .headers(headersBuilder.build())
                .build();

        Call call = getClient().newCall(request);
        call.enqueue(callback);
        return call;

    }

}
