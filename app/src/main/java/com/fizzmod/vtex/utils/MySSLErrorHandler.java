package com.fizzmod.vtex.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.support.v7.app.AlertDialog;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by marcos on 23/06/17.
 */

public class MySSLErrorHandler {

    public final static int SSL_PENDING_USER_ACTION = 0;
    public final static int SSL_HANDLER_PROCEED = 1;
    public final static int SSL_HANDLER_CANCEL = 2;
    public final static int SSL_UPDATE_WEBVIEW = 3;

    public Context context;

    private HashMap<String, Integer> sslErrorActionByHost;

    private ArrayList<Callback> SSLCallbacks;


    public MySSLErrorHandler(Context context) {
        SSLCallbacks = new ArrayList<>();
        sslErrorActionByHost = new HashMap<>();

        this.context = context;

    }

    public void handle(WebView view, final SslErrorHandler handler, SslError error, final Callback callback) {
        String host = "default";

        try {
            URL url = new URL(view.getUrl());
            host = url.getHost();
        } catch (MalformedURLException e) {}

        final String finalHost = host;

        Integer sslErrorAction = sslErrorActionByHost.get(finalHost);

        if(sslErrorAction == null ||sslErrorAction == SSL_PENDING_USER_ACTION)
            SSLCallbacks.add(getSSLCallback(handler));


        /**
         * Show error for first time by host
         */

        if(sslErrorAction == null) {

            sslErrorActionByHost.put(finalHost, SSL_PENDING_USER_ACTION);

            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            String title = context.getResources().getString(R.string.sslError);
            String description = "SSL Certificate error.";

            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    description = context.getResources().getString(R.string.sslUntrusted);
                    break;
                case SslError.SSL_EXPIRED:
                    description = context.getResources().getString(R.string.sslExpired);
                    break;
                case SslError.SSL_IDMISMATCH:
                    description = context.getResources().getString(R.string.sslIdMismatch);
                    break;
                case SslError.SSL_NOTYETVALID:
                    description = context.getResources().getString(R.string.sslNotYetValid);
                    break;

                case SslError.SSL_INVALID:
                    title = context.getResources().getString(R.string.webViewOutdated);
                    description = context.getResources().getString(R.string.webViewOutdatedDescription);
                    break;
            }

            builder.setTitle(title);
            builder.setMessage(description);
            builder.setCancelable(false);

            /**
             * Continue button
             */

            builder.setPositiveButton(context.getResources().getString(R.string.webViewOutdatedContinue), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sslErrorActionByHost.put(finalHost, SSL_HANDLER_PROCEED);

                    for(int i = 0; i < SSLCallbacks.size(); i++)
                        SSLCallbacks.get(i).run(SSL_HANDLER_PROCEED);

                    SSLCallbacks.clear(); //Free up memory
                    dialog.cancel();
                }
            });

            /**
             * Update button
             */

            builder.setNeutralButton(context.getResources().getString(R.string.webViewOutdatedUpdate), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sslErrorActionByHost.put(finalHost, SSL_UPDATE_WEBVIEW);

                    callback.run(SSL_UPDATE_WEBVIEW);

                    dialog.cancel();

                }
            });

            /**
             * Exit button
             */

            builder.setNegativeButton(context.getResources().getString(R.string.webViewOutdatedExit), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sslErrorActionByHost.put(finalHost, SSL_HANDLER_CANCEL);

                    callback.run(SSL_HANDLER_CANCEL);

                    dialog.cancel();
                }
            });

            // create alert dialog
            AlertDialog alertDialog = builder.create();

            // show it
            alertDialog.show();

        } else {

            /**
             * This will be called if an SSL error occurs after click listener
             */

            switch (sslErrorAction) {

                case SSL_HANDLER_PROCEED:
                    if (handler != null)
                        handler.proceed();
                    break;

                case SSL_HANDLER_CANCEL:
                case SSL_UPDATE_WEBVIEW:
                    if (handler != null)
                        handler.cancel();
                    break;
            }
        }
    }

    private Callback getSSLCallback(final SslErrorHandler handler) {

        return new Callback() {
            @Override
            public void run(Object action) {
                if(handler instanceof SslErrorHandler){

                    try {
                        if((int) action == SSL_HANDLER_PROCEED) {
                            handler.proceed(); //Otherwise page won't load if WebView is outdated
                            return;
                        }

                        handler.cancel();

                    } catch (Exception e) {}
                }
            }

            @Override
            public void run(Object handler, Object proceed) {}
        };
    }

    public void destroy() {
        context = null;
        sslErrorActionByHost = null;
        SSLCallbacks = null;
    }
}
