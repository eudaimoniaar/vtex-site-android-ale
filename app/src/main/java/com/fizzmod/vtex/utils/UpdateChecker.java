package com.fizzmod.vtex.utils;

import android.util.Log;

import com.fizzmod.vtex.BuildConfig;

public class UpdateChecker {

    public boolean needToUpdateApp() {
        String currentVersion = BuildConfig.VERSION_NAME;
        String playStoreVersion;

        PlayStoreVersionChecker versionChecker = new PlayStoreVersionChecker();
        try {
            playStoreVersion = versionChecker.execute().get();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        Log.d("UpdateChecker", "PlayStore app version: " + playStoreVersion + ", current app version: " + currentVersion);
        return compareVersion(currentVersion, playStoreVersion);
    }

    private boolean compareVersion(String currentVersion, String playStoreVersion) {
        String[] currentVals = currentVersion.split("\\.");
        String[] playStoreVals = playStoreVersion.split("\\.");
        int i = 0;

        //find first different number
        while (i < currentVals.length && i < playStoreVals.length && currentVals[i].equals(playStoreVals[i])) {
            i++;
        }
        //check by version number, and else by number of version numbers
        if (i < currentVals.length && i < playStoreVals.length) {
            int diff = Integer.valueOf(playStoreVals[i]) - Integer.valueOf(currentVals[i]);
            return diff > 0;
        } else {
            int diff = playStoreVals.length - currentVals.length;
            return diff > 0;
        }
    }
}
