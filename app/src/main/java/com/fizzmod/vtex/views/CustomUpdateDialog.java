package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class CustomUpdateDialog extends AlertDialog {

    private Context context;
    private TextView
            txtMessage,
            txtCancel,
            txtAccept,
            txtTitle;

    public CustomUpdateDialog(Context context) {
        super(context);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.custom_update_dialog, null);

        txtTitle = (TextView) view.findViewById(R.id.alert_dialog_title);
        txtMessage = (TextView) view.findViewById(R.id.alert_dialog_description);
        txtCancel = (TextView) view.findViewById(R.id.alert_dialog_cancel);
        txtAccept = (TextView) view.findViewById(R.id.alert_dialog_accept);

        txtTitle.setText(context.getString(R.string.update_title));
        txtMessage.setText(context.getString(R.string.update_description));
        txtCancel.setText(context.getString(R.string.update_close));

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Window window = getWindow();
        if (window != null)
            window.setBackgroundDrawable(view.getResources().getDrawable(R.drawable.transparent));

        setView(view);
    }

    public void setTxtTitle(int titleId) {
        txtTitle.setText(context.getString(titleId));
    }

    public void setTxtMessage(int messageId) {
        txtMessage.setText(context.getString(messageId));
    }

    public void setTxtAccept(int messageId, View.OnClickListener listener) {
        txtAccept.setText(context.getString(messageId));
        txtAccept.setOnClickListener(listener);
    }

    public void setTxtCancel(int messageId) {
        txtCancel.setText(context.getString(messageId));
    }

    public void setCancelListener(View.OnClickListener listener) {
        txtCancel.setOnClickListener(listener);
    }

}
