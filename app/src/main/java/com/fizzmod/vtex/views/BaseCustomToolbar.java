package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ToolbarListener;
import com.fizzmod.vtex.utils.BaseScrollHandler;
import com.fizzmod.vtex.utils.Utils;

public abstract class BaseCustomToolbar extends Toolbar implements BaseScrollHandler.ScrollListener {

    protected ToolbarListener toolbarListener;
    protected LinearLayout searchIcon;
    protected LinearLayout scannerIcon;
    private RelativeLayout minicartIcon;

    protected BaseCustomToolbar(Context context) {
        this(context, null);
    }

    protected BaseCustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.toolbarStyle);
    }

    protected BaseCustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.sublayout_toolbar, this);

        minicartIcon = (RelativeLayout) findViewById(R.id.minicartIcon);
        searchIcon = (LinearLayout) findViewById(R.id.searchIcon);
        scannerIcon = (LinearLayout) findViewById(R.id.scannerIcon);


        /** Scanner Icon**/
        scannerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarListener.onScannerClicked();
            }
        });

        /** Minicart Icon**/
        minicartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarListener.onMinicartClicked();
            }
        });

        /** Logo**/
        findViewById(R.id.toolbarLogo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarListener.onLogoClicked();
            }
        });

        /** Search Icon**/
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarListener.onSearchClicked();
            }
        });
    }

    public void startMinicartIconAnimation() {
        ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
        scale.setDuration(500);
        scale.setInterpolator(new OvershootInterpolator());
        minicartIcon.startAnimation(scale);
    }

    public void setToolbarListener(ToolbarListener toolbarListener) {
        this.toolbarListener = toolbarListener;
    }

    public void fadeOutSearchIcon() {
        Utils.fadeOut(searchIcon);
    }

    public void showSearchIcon() {
        searchIcon.setVisibility(View.VISIBLE);
    }

    public void loadHomeLayout() {
        fadeOutSearchIcon();
    };
}
