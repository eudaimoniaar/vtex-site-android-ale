package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class CartSyncAlertDialog extends AlertDialog{

    private ImageView imageView;
    private TextView txtMessage;
    private TextView txtCancel;
    private TextView txtAccept;

    public CartSyncAlertDialog(Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.cart_sync_alert_dialog, null);

        imageView = (ImageView) view.findViewById(R.id.cart_sync_image);
        txtMessage = (TextView) view.findViewById(R.id.cart_sync_description);
        txtCancel = (TextView) view.findViewById(R.id.cart_sync_cancel);
        txtAccept = (TextView) view.findViewById(R.id.cart_sync_accept);

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        setView(view);
    }

    public void setImage(int imageId) {
        imageView.setImageResource(imageId);
    }

    public void setTxtMessage(int messageId) {
        txtMessage.setText(messageId);
    }

    public void setTxtAccept(int messageId, View.OnClickListener listener) {
        txtAccept.setText(messageId);
        txtAccept.setOnClickListener(listener);
    }

    public void setTxtCancel(int messageId) {
        txtCancel.setText(messageId);
    }

    public void setTxtCancel(int messageId, View.OnClickListener listener) {
        txtCancel.setText(messageId);
        txtCancel.setOnClickListener(listener);
    }

}