package com.fizzmod.vtex.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.Utils;

public class SocialIconsLayout extends LinearLayout implements View.OnClickListener {

    private ImageView facebook;
    private ImageView twitter;
    private ImageView youtube;
    private ImageView linkedin;
    private ImageView email;
    private ImageView instagram;

    public SocialIconsLayout(Context context) {
        this(context, null);
    }

    public SocialIconsLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SocialIconsLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(context, R.layout.social_icons, this);

        setLayouts();
        setVisibilities();
        setlisteners();
    }

    private void setLayouts() {
        facebook = (ImageView) findViewById(R.id.facebook);
        twitter = (ImageView) findViewById(R.id.twitter);
        youtube = (ImageView) findViewById(R.id.youtube);
        linkedin = (ImageView) findViewById(R.id.linkedin);
        email = (ImageView) findViewById(R.id.email);
        instagram = (ImageView) findViewById(R.id.instagram);
    }

    private void setlisteners() {
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        youtube.setOnClickListener(this);
        linkedin.setOnClickListener(this);
        email.setOnClickListener(this);
        instagram.setOnClickListener(this);
    }

    private void setVisibilities() {
        facebook.setVisibility(Utils.isEmpty(BuildConfig.FACEBOOK) ? View.GONE : View.VISIBLE);
        twitter.setVisibility(Utils.isEmpty(BuildConfig.TWITTER) ? View.GONE : View.VISIBLE);
        youtube.setVisibility(Utils.isEmpty(BuildConfig.YOUTUBE) ? View.GONE : View.VISIBLE);
        linkedin.setVisibility(Utils.isEmpty(BuildConfig.LINKEDIN) ? View.GONE : View.VISIBLE);
        email.setVisibility(Utils.isEmpty(BuildConfig.EMAIL) ? View.GONE : View.VISIBLE);
        instagram.setVisibility(Utils.isEmpty(BuildConfig.INSTAGRAM) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.facebook:
                getContext().startActivity(
                        Utils.newFacebookIntent(getContext().getPackageManager(), BuildConfig.FACEBOOK)
                );
                break;
            case R.id.twitter:
                getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.TWITTER)));
                break;
            case R.id.linkedin:
                Intent intentLinkedin = new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.LINKEDIN));
                getContext().startActivity(intentLinkedin);
                break;
            case R.id.youtube:
                Intent intentYoutube = new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.YOUTUBE));
                getContext().startActivity(intentYoutube);
                break;
            case R.id.email:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.EMAIL));
                getContext().startActivity(browserIntent);
            case R.id.instagram:
                getContext().startActivity(
                        Utils.newInstagramIntent(getContext().getPackageManager(), BuildConfig.INSTAGRAM)
                );
            default:
                break;
        }
    }
}
