package com.fizzmod.vtex.views;

import android.content.Context;
import android.os.Handler;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.DrawerAdapter;
import com.fizzmod.vtex.interfaces.NavigationViewListener;
import com.fizzmod.vtex.models.DrawerItem;
import com.fizzmod.vtex.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_COUPONS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public abstract class BaseCustomNavigationView extends FrameLayout{

    protected NavigationViewListener navigationViewListener;
    protected LinearLayout scannerIcon;
    protected LinearLayout signOut;
    protected LinearLayout signIn;
    private DrawerAdapter drawerAdapter;
    protected List<DrawerItem> menuItems = new ArrayList<>();
    protected int previousItemPosition;
    private int selectedItemPos;
    private String selectedItemId;
    private NavigationView navigationView;

    protected BaseCustomNavigationView(Context context) {
        this(context, null);
    }

    protected BaseCustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    protected BaseCustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.navigation_view, this);
        navigationViewListener = (NavigationViewListener) context;

        ListView drawerList = (ListView) findViewById(R.id.drawer_list);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        NavigationMenuView navigationMenuView = findNavigationMenuView();

        if (navigationMenuView != null) {
            navigationMenuView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        }

        addMenuItems();
        rearrangeMenuItems();

        drawerAdapter = new DrawerAdapter(menuItems);
        drawerList.setAdapter(drawerAdapter);
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                previousItemPosition = selectedItemPos;

                selectedItemPos = (Integer) view.getTag(R.id.TAG_ADAPTER_POSITION);

                navigationViewListener.closeMinicart();

                if (previousItemPosition == selectedItemPos) {
                    closeNavigationDrawer();
                    return;
                }

                if (previousItemPosition != -1)
                    menuItems.get(previousItemPosition).enabled = false; //Deactivate previous item

                menuItems.get(selectedItemPos).enabled = true; //Activate previous item

                selectedItemId = menuItems.get(selectedItemPos).fragmentTag;

                closeNavigationDrawer();
                drawerAdapter.updateData(menuItems);
                drawerAdapter.notifyDataSetChanged();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectItem(selectedItemId);
                    }
                }, 300);
            }
        });
    }

    private NavigationMenuView findNavigationMenuView() {
        for (int i = 0; i < navigationView.getChildCount(); i++) {
            if (navigationView.getChildAt(i) instanceof NavigationMenuView) {
                return (NavigationMenuView) navigationView.getChildAt(i);
            }
        }
        return null;
    }

    protected void selectItem(String fragmentTag, Class fragmentClass) {
        closeNavigationDrawer();
        checkDrawerItem(fragmentTag);
        if (fragmentClass != null) {
            navigationViewListener.onMenuItemSelected(fragmentTag, fragmentClass);
        }
    }


    public void selectItem(String fragmentTag){

        Class fragmentClass = null;

        switch (fragmentTag) {
            case FRAGMENT_TAG_CATEGORIES:
                fragmentClass = com.fizzmod.vtex.fragments.Categories.class;
                break;
            case FRAGMENT_TAG_STORES:
                fragmentClass = com.fizzmod.vtex.fragments.Stores.class;
                break;
            case FRAGMENT_TAG_HOME:
                fragmentClass = com.fizzmod.vtex.fragments.Home.class;
                break;
            case FRAGMENT_TAG_FAVOURITES:
                fragmentClass = com.fizzmod.vtex.fragments.Favourites.class;
                break;
            case FRAGMENT_TAG_ORDERS:
                fragmentClass = com.fizzmod.vtex.fragments.Orders.class;
                break;
            case FRAGMENT_TAG_CONTACT:
                fragmentClass = com.fizzmod.vtex.fragments.Contact.class;
                break;
            case FRAGMENT_TAG_TERMS:
                fragmentClass = com.fizzmod.vtex.fragments.Terms.class;
                break;
            case FRAGMENT_TAG_COUPONS:
                fragmentClass = com.fizzmod.vtex.fragments.Coupons.class;
                break;
            default:
                break;
        }

        selectItem(fragmentTag, fragmentClass);

    }

    protected DrawerItem findItem(String tag) {
        for (DrawerItem item : menuItems) {
            if (item.fragmentTag.equals(tag)){
               return item;
            }
        }
        return null;
    }


    public void setStoreName(String text) {
        ((TextView) findViewById(R.id.storeName)).setText(text);
    }


    /**
     * Uncheck navigation drawer selected item
     */
    public void uncheckDrawerItem() {
        if(selectedItemPos != -1){
            menuItems.get(selectedItemPos).enabled = false;
            drawerAdapter.updateData(menuItems);
            drawerAdapter.notifyDataSetChanged();
            selectedItemPos = -1;
        }
    }

    /**
     * Check given navigation drawer item
     */
    public void checkDrawerItem(String fragmentTag) {
        checkDrawerItem(getDrawerItemPosition(fragmentTag));
    }

    protected void checkDrawerItem(Integer pos) {

        if(pos == null){
            // Uncheck selected one
            if(selectedItemPos != -1)
                menuItems.get(selectedItemPos).enabled = false;

            drawerAdapter.updateData(menuItems);
            drawerAdapter.notifyDataSetChanged();

            selectedItemPos = -1;
            return;
        }

        if(selectedItemPos != -1)
            menuItems.get(selectedItemPos).enabled = false;

        if (pos != -1)
            menuItems.get(pos).enabled = true;

        drawerAdapter.updateData(menuItems);
        drawerAdapter.notifyDataSetChanged();

        selectedItemPos = pos;
    }

    public void closeNavigationDrawer(){
            navigationViewListener.closeDrawers();
    }

    protected void addMenuItems() {

        menuItems.add(new DrawerItem(R.drawable.icn_home_on, R.drawable.icn_home_off,  R.string.drawerMenuItemHome, FRAGMENT_TAG_HOME, false, true));
        menuItems.add(new DrawerItem(R.drawable.icn_categories_on, R.drawable.icn_categories_off, R.string.drawerMenuItemCategories, FRAGMENT_TAG_CATEGORIES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_favorites_on, R.drawable.icn_favorites_off, R.string.drawerMenuItemFavorites, FRAGMENT_TAG_FAVOURITES, false));
        menuItems.add(new DrawerItem(R.drawable.icn_stores_on, R.drawable.icn_stores_off, R.string.drawerMenuItemStores, FRAGMENT_TAG_STORES, true));
        menuItems.add(new DrawerItem(R.drawable.icn_terms_on, R.drawable.icn_terms_off, R.string.drawerMenuItemTerms, FRAGMENT_TAG_TERMS, true));
        menuItems.add(new DrawerItem(R.drawable.icn_contact_on, R.drawable.icn_contact, R.string.drawerMenuItemContact, FRAGMENT_TAG_CONTACT, true));
        menuItems.add(new DrawerItem(R.drawable.icn_my_orders_on, R.drawable.icn_my_orders_off, R.string.drawerMenuItemMyOrders, FRAGMENT_TAG_ORDERS, true));

    }

    protected void rearrangeMenuItems() {
        //By default, the order of the items will be the one in addMenuItems()
    }

    protected void setClickListeners() {
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeNavigationDrawer();
                navigationViewListener.onSignOutClicked();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeNavigationDrawer();
                navigationViewListener.onSignInClicked();
            }
        });
    }

    public void setUserEmail(String email) {
        ((TextView) findViewById(R.id.navigationEmail)).setText(email);
        findViewById(R.id.login).setVisibility(View.GONE);
        Utils.fadeIn(findViewById(R.id.logout));
        findViewById(R.id.navigationProfile).setVisibility(View.VISIBLE);
    }

    public void onSignOut() {
        findViewById(R.id.logout).setVisibility(View.GONE);
        Utils.fadeIn(findViewById(R.id.login));
        findViewById(R.id.navigationProfile).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.navigationEmail)).setText("-");
    }

    private Integer getDrawerItemPosition(String fragmentTag) {

        for(int i = 0; i < menuItems.size(); i++){
            if(menuItems.get(i).fragmentTag.equals(fragmentTag)) {
                return i;
            }
        }
        return null;
    }
}
