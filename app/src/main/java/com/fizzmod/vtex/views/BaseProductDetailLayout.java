package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fizzmod.vtex.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public abstract class BaseProductDetailLayout extends FrameLayout {

    protected View loadingImage;
    protected ImageView productImage;
    protected LinearLayout linearPromotion;

    protected BaseProductDetailLayout(@NonNull Context context) {
        this(context, null);
    }

    protected BaseProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    protected BaseProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(context, R.layout.product_detail, this);
        loadingImage = findViewById(R.id.imageLoading);
        productImage = (ImageView) findViewById(R.id.productImage);
        linearPromotion = (LinearLayout) findViewById(R.id.product_page_linear_promotion);
    }

    public void showLoadingImage() {
        loadingImage.setVisibility(View.VISIBLE);
    }


    public void hideProductImage() {
        productImage.setVisibility(View.GONE);
    }

    public void loadProductImage(String mainImage) {
        Picasso.with(getContext())
                .load(mainImage)
                .resize(450, 450)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) //Skip disk cache for big image.
                .into(productImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        onProductImageLoaded();
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    public void showLinearPromotion() {
        linearPromotion.setVisibility(View.VISIBLE);
    }

    public void addViewToLinearPromotion(ImageView imageView) {
        linearPromotion.addView(imageView);
    }

    public void setHiglightTextViewText(String priceDiffPercentageFormatted) {
        //Nothing to do, not used by default
    }

    protected void onProductImageLoaded (){
        if (loadingImage != null) {
            loadingImage.setVisibility(View.GONE);
            productImage.setVisibility(View.VISIBLE);
        }
    }


}
