package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.vtex.R;

public class CouponsCartelMessage extends BaseCartelMessage {
    public CouponsCartelMessage(Context context) {
        this(context, null);
    }

    public CouponsCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CouponsCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getBackgroundResource() {
        return R.drawable.cartel_background_cart;
    }

    @Override
    public int getIcon() {
        return R.drawable.icn_check_white;
    }

}
