package com.fizzmod.vtex.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public abstract class ParallelogramTextView extends AppCompatTextView {

    Paint mBorderPaint;
    Paint mInnerPaint;

    public ParallelogramTextView(Context context) {
        this(context, null);
    }

    public ParallelogramTextView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public ParallelogramTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mBorderPaint = new Paint();
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setColor(getBorderColor());
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(getBorderWidth());
        mBorderPaint.setStrokeJoin(Paint.Join.ROUND);

        mInnerPaint = new Paint();
        mInnerPaint.setAntiAlias(true);
        mInnerPaint.setColor(Color.TRANSPARENT);
        mInnerPaint.setStyle(Paint.Style.FILL);
        mInnerPaint.setStrokeJoin(Paint.Join.ROUND);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Path path = new Path();
        int offset = getBorderWidth();
        path.moveTo(getWidth() - offset, offset);
        path.lineTo(getWidth()*0.1f + offset, offset);
        path.lineTo(offset, getHeight() - offset);
        path.lineTo(getWidth()*0.9f - offset, getHeight() - offset);
        path.lineTo(getWidth() - offset, offset);
        path.lineTo(getWidth() - offset*2, offset);
        canvas.drawPath(path, mInnerPaint);
        canvas.drawPath(path, mBorderPaint);
    }

    protected abstract int getBorderColor();

    protected abstract int getBorderWidth();

}