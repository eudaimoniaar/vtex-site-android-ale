package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.fragments.Favourites;
import com.fizzmod.vtex.interfaces.MenuButtonInteractionListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.utils.Utils;

public class FloatingMenuButton extends FrameLayout {

    private FloatingActionButton favoriteButton, shareButton, addToListButton;
    private boolean isPressed;
    private Product product;
    private OnFragmentInteractionListener viewListener;
    private MenuButtonInteractionListener menuButtonInteractionListener;
    private String parentFragment;

    public FloatingMenuButton(Context context) {
        this(context, null);
    }

    public FloatingMenuButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FloatingMenuButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.layout_floating_menu_button, this);

        isPressed = false;
        favoriteButton = (FloatingActionButton) findViewById(R.id.menu_button_add_to_favorites);
        shareButton = (FloatingActionButton) findViewById(R.id.menu_button_share);
        addToListButton = (FloatingActionButton) findViewById(R.id.menu_button_add_to_list);
        findViewById(R.id.menu_button_options).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPressed)
                    hideMenu();
                else
                    showMenu();
            }
        });

        favoriteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (product == null) {
                    Toast.makeText(getContext(), R.string.unexpected_error, Toast.LENGTH_SHORT).show();
                    return;
                }

                boolean isFavorite = Favourites.exists(getContext(), product.getId());
                if (isFavorite)
                    Favourites.remove(getContext(), product.getId());
                else
                    Favourites.save(getContext(), product.getId());
                viewListener.onFavoriteProduct(!isFavorite);

                hideMenu();
            }
        });

        shareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product != null) {
                    Utils.share(getContext(), product);
                    hideMenu();
                } else
                    Toast.makeText(getContext(), R.string.unexpected_error, Toast.LENGTH_SHORT).show();
            }
        });

        addToListButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product != null) {
                    viewListener.onAddProductToList(getSelectedSku(), parentFragment);
                    hideMenu();
                } else
                    Toast.makeText(getContext(), R.string.unexpected_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getSelectedSku() {
        return product.getMainSku().getId();
    }

    private void showButton(FloatingActionButton button, int toXResId, int toYResId) {

        int toX = -getResources().getDimensionPixelSize(toXResId);
        int toY = -getResources().getDimensionPixelSize(toYResId);
        int fromVisibility = button.isShown() ? 1 : 0;
        Utils.translateViewAnimation(button, toX, toY, fromVisibility, 1);
    }

    private void hideButton(FloatingActionButton button) {
        int to = getResources().getDimensionPixelSize(R.dimen.origin_translation);
        int fromVisibility = button.isShown() ? 1 : 0;
        Utils.translateViewAnimation(button, to, to, fromVisibility, 0);
    }

    public void showMenu() {
        isPressed = true;

        showButton(addToListButton, R.dimen.fab_long_translation, R.dimen.fab_short_translation);
        showButton(favoriteButton, R.dimen.fab_medium_translation, R.dimen.fab_medium_translation);
        showButton(shareButton, R.dimen.fab_short_translation, R.dimen.fab_long_translation);

        menuButtonInteractionListener.onShow();
    }

    public void hideMenu() {
        isPressed = false;

        hideButton(addToListButton);
        hideButton(favoriteButton);
        hideButton(shareButton);
    }

    public void resetMenu() {
        setVisibility(GONE);
        hideMenu();
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setListener(OnFragmentInteractionListener listener) {
        viewListener = listener;
    }

    public void setMenuButtonInteractionListener(MenuButtonInteractionListener listener) {
        menuButtonInteractionListener = listener;
    }

    public void setParentFragment(String parentFragment) {
        this.parentFragment = parentFragment;
    }

    public void hideOptionsButton() {
        findViewById(R.id.menu_button_options).setVisibility(GONE);
    }

}
