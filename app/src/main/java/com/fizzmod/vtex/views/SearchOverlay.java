package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.SearchBarListener;
import com.fizzmod.vtex.utils.Utils;

public class SearchOverlay extends RelativeLayout {

    private SearchBarListener searchBarListener;
    protected EditText searchInput;

    public SearchOverlay(Context context) {
        this(context, null);
    }

    public SearchOverlay(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.search_overlay, this);
        searchInput = (EditText) findViewById(R.id.searchbarSearchInput);
        searchInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    searchBarListener.onSearchFromSearchOverlay(v.getText().toString());
                    searchInput.setText("");
                    return true;
                }
                return false;
            }
        });
        findViewById(R.id.searchOverlay).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                closeOverlay();
            }
        });
    }

    /**
     * Open Search Input
     * @param
     */

    public void openSearch() {

        Utils.fadeIn(findViewById(R.id.search_overlay), new Callback() {
            @Override
            public void run(Object data) {

                searchInput.requestFocus();

                try{
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }catch (NullPointerException e){}

            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    /**
     * Close Store Selector overlay
     */

    public void closeOverlay() {
        searchInput.getText().clear();
        Utils.fadeOut(this);
        searchBarListener.dismissKeyboard();
    }

    public void setSearchBarListener(SearchBarListener searchBarListener) {
        this.searchBarListener = searchBarListener;
    }
}
