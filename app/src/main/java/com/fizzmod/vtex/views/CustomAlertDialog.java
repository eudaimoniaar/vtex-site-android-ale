package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.fizzmod.vtex.R;

/**
 * Created by vidueirof on 12/19/17.
 */

public class CustomAlertDialog extends AlertDialog{

    private Context context;
    private TextView
            txtMessage,
            txtCancel,
            txtAccept;

    public CustomAlertDialog(Context context) {
        super(context);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.custom_alert_dialog, null);

        txtMessage = (TextView) view.findViewById(R.id.alert_dialog_description);
        txtCancel = (TextView) view.findViewById(R.id.alert_dialog_cancel);
        txtAccept = (TextView) view.findViewById(R.id.alert_dialog_accept);

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Window window = getWindow();
        if (window != null)
            window.setBackgroundDrawable(view.getResources().getDrawable(R.drawable.transparent));

        setView(view);
    }

    public void setTxtMessage(int messageId) {
        txtMessage.setText(context.getString(messageId));
    }

    public void setTxtAccept(int messageId, View.OnClickListener listener){
        txtAccept.setText(context.getString(messageId));
        txtAccept.setOnClickListener(listener);
    }

    public void setTxtCancel(int messageId){
        txtCancel.setText(context.getString(messageId));
    }

    public void setTxtCancel(int messageId, View.OnClickListener listener){
        txtCancel.setText(context.getString(messageId));
        txtCancel.setOnClickListener(listener);
    }
}
