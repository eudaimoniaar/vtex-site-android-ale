package com.fizzmod.vtex.views;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;

public class BaseProductItemPricesLayout extends LinearLayout {

    private TextView listPrice;
    private TextView bestPrice;
    private TextView priceByUnit;

    protected BaseProductItemPricesLayout(Context context) {
        this(context, null);
    }

    protected BaseProductItemPricesLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseProductItemPricesLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.product_item_prices, this);

        listPrice = (TextView) findViewById(R.id.productListPrice);
        bestPrice = (TextView) findViewById(R.id.productBestPrice);
        priceByUnit = (TextView) findViewById(R.id.productPriceByUnit);
    }

    public void showListPrice(String listPriceFormatted) {
        listPrice.setPaintFlags(listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        listPrice.setText(listPriceFormatted);
        listPrice.setVisibility(VISIBLE);
    }

    public void hideListPrice() {
        listPrice.setVisibility(View.GONE);

    }

    public void setBestPriceText(String bestPriceFormatted) {
        bestPrice.setText(bestPriceFormatted);
    }


    public void onNoStock() {
        listPrice.setText(getResources().getString(R.string.outOfStock));
        bestPrice.setVisibility(GONE);
        hidePriceByUnit();
    }

    public void showPriceByUnit(String priceByUnitFormatted) {
        priceByUnit.setText(priceByUnitFormatted);
        priceByUnit.setVisibility(View.VISIBLE);
    }


    public void hidePriceByUnit() {
        priceByUnit.setVisibility(View.GONE);
    }
}
