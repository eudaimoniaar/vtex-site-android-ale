package com.fizzmod.vtex.views;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.utils.Utils;

public abstract class BaseCartelMessage extends LinearLayout {

    private static final int ANIMATION_DURATION = 300, WAIT_TIME = 1300;

    public static int getAnimationDuration() {
        return ANIMATION_DURATION * 2 + WAIT_TIME;
    }

    protected LinearLayout multipleTextsLayout;
    protected TextView upperTxtMessage;
    protected TextView lowerTxtMessage;
    protected TextView txtMessage;

    public BaseCartelMessage(Context context) {
        this(context, null);
    }

    public BaseCartelMessage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseCartelMessage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.base_cartel_message_layout, this);

        ImageView imageView = (ImageView)findViewById(R.id.base_cartel_icon);
        txtMessage = (TextView)findViewById(R.id.base_cartel_title);
        upperTxtMessage = (TextView)findViewById(R.id.base_cartel_upper_text);
        lowerTxtMessage = (TextView)findViewById(R.id.base_cartel_lower_text);
        multipleTextsLayout = (LinearLayout) findViewById(R.id.base_cartel_multiple_texts);

        imageView.setImageResource(getIcon());
        setBackgroundResource(getBackgroundResource());

    }

    public void display() {

        if (isShown())
            return;

        setVisibility(VISIBLE);

        startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_IN_LEFT_TO_RIGHT,
                        this,
                        ANIMATION_DURATION,
                        new Callback() {
                            @Override
                            public void run(Object data) {
                                hide();
                            }

                            @Override
                            public void run(Object data, Object data2) {
                                hide();
                            }
                        }));

    }

    private void hide() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startAnimation(
                        Utils.getAnimation(
                                Utils.ANIMATION_OUT_LEFT_TO_RIGHT,
                                BaseCartelMessage.this,
                                ANIMATION_DURATION,
                                new Callback() {
                                    @Override
                                    public void run(Object data) {
                                        setVisibility(GONE);
                                    }

                                    @Override
                                    public void run(Object data, Object data2) {
                                        setVisibility(GONE);
                                    }
                                }));
            }
        }, WAIT_TIME);
    }

    public void setText(int textResId) {
        setText( getContext().getString( textResId ) );
    }

    public void setText(String text) {
        if (txtMessage.getVisibility() == VISIBLE)
            txtMessage.setText(text);
        else
            lowerTxtMessage.setText(text);
    }

    public abstract int getBackgroundResource();
    public abstract int getIcon();
}
