/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.CustomApplication;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.MySSLErrorHandler;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.net.MalformedURLException;
import java.net.URL;

public class Checkout extends AppCompatActivity {

    public static final String RESULT_FRAGMENT_KEY = "fragment";
    public static final String RESULT_CLEAR_KEY = "clear";
    public static final String RESULT_PRODUCT_LINK_KEY = "productLink";

    private String email;
    private WebView webView;
    private boolean orderFinished = false;
    private MySSLErrorHandler sslErrorHandler;
    private URL checkoutUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_progressbar_layout);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.loadingStatusBar));
        }

        // [START shared_tracker]
        // Obtain the shared Tracker instance.
        CustomApplication application = (CustomApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        // [END shared_tracker]


        setWebView();

        if (mTracker != null) {
            mTracker.setScreenName("Checkout");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (sslErrorHandler != null) {
            sslErrorHandler.destroy();
            sslErrorHandler = null;
        }

    }


    @Override
    public void onBackPressed() {

        if (orderFinished) {
            finishWithResult(Main.FRAGMENT_TAG_HOME, null, true);
            return;
        }

        super.onBackPressed();
    }

    public void finishWithResult(String navigateTo, String extra, boolean clearCart) {

        Bundle data = new Bundle();
        data.putBoolean(RESULT_CLEAR_KEY, orderFinished || clearCart);
        data.putString(RESULT_FRAGMENT_KEY, navigateTo);

        if (navigateTo.equals(Main.FRAGMENT_TAG_PRODUCT)) {
            data.putString(RESULT_PRODUCT_LINK_KEY, extra);
        }

        Intent intent = new Intent();
        intent.putExtras(data);
        setResult(Activity.RESULT_OK, intent);

        /*
         *   Reset
         */

        orderFinished = false; //Just in case...

        finish();
    }

    public void setWebView() {

        sslErrorHandler = new MySSLErrorHandler(this);
        webView = (WebView) findViewById(R.id.webView);
        email = User.getInstance(this).getEmail();

        String defaultUA = webView.getSettings().getUserAgentString();
        webView.getSettings().setUserAgentString(defaultUA.replace("; wv", "")); // Set your custom user-agent

        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.webViewWrapper);
        final RelativeLayout loading = (RelativeLayout) findViewById(R.id.loading);
        final ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        final TextView progressText = (TextView) findViewById(R.id.progressText);

        loading.setVisibility(View.VISIBLE);
        progress.setProgress(0);
        progressText.setText("0%");

        swipeLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorSecondary);

        if (Build.VERSION.SDK_INT >= 19) {
            // chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
                progressText.setText(newProgress + "%");
            }

            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                Log.d("Checkout webview", message + " -- From line "
                        + lineNumber + " of "
                        + sourceID);
            }

        });


        /*try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.google.android.webview", 0);
            Utils.log("Webview version: " + packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            //Handle exception
        }*/

        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String urlString) {

                swipeLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);

                URL url = null;
                try {
                    url = new URL(urlString);

                    String path = url.getPath();
                    String host = url.getHost();

                    // Handle home
                    if (host.equals(checkoutUrl.getHost())) {

                        //Inject javascript
                        String script = Utils.loadAssetTextAsString(Checkout.this, "checkout.js");
                        if (script != null)
                            view.loadUrl("javascript:" + script.replace("{{email}}", Utils.isEmpty(email) ? "" : email));

                        if (url.getPath().contains("/orderPlaced")) {
                            orderFinished = true;
                            Cart.getInstance().emptyCart(Checkout.this);
                        }

                    }

                } catch (MalformedURLException e) {
                    Log.e(getLocalClassName(), e.getLocalizedMessage());
                }

            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                if (sslErrorHandler == null)
                    return;
                sslErrorHandler.handle(view, handler, error, new Callback() {
                    @Override
                    public void run(Object data) {
                        if (data == null)
                            return;
                        if ((Integer) data == MySSLErrorHandler.SSL_UPDATE_WEBVIEW) {
                            Utils.openGooglePlay(Checkout.this, "com.google.android.webview");
                            finishWithResult(Main.FRAGMENT_TAG_HOME, null, false);
                        }
                        if ((Integer) data == MySSLErrorHandler.SSL_HANDLER_CANCEL)
                            finishWithResult(Main.FRAGMENT_TAG_HOME, null, false);
                    }

                    @Override
                    public void run(Object data, Object data2) {

                    }
                });
            }

            public boolean shouldOverrideUrlLoading(WebView view, String urlString) {

                URL url;
                try {
                    url = new URL(urlString);

                    String path = url.getPath();
                    String host = url.getHost();

                    if (orderFinished) {
                        finishWithResult(
                                path.endsWith("/orders") ?
                                        Main.FRAGMENT_TAG_ORDERS :
                                        Main.FRAGMENT_TAG_HOME,
                                null,
                                true);
                        return true;
                    }

                    // Handle home
                    if (path.equals("/") && host.equals(checkoutUrl.getHost())) {
                        finishWithResult(Main.FRAGMENT_TAG_HOME, null, false);
                        return true;
                    }

                    // Handle product page
                    if (path.endsWith("/p")) {
                        finishWithResult(Main.FRAGMENT_TAG_PRODUCT, path, false);
                        return true;
                    }

                } catch (MalformedURLException e) {
                    Log.e(getLocalClassName(), e.getLocalizedMessage());
                }

                return false;
            }

        });

        /**
         * Handle back pressed
         */

        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack() && !orderFinished) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });


        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });

        /**
         * Add javascript interface
         */

        webView.addJavascriptInterface(new JsObject(), "BHAndroid");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        loadWebView();

    }


    public void loadWebView() {
        String URL = Cart.getInstance().getUrl(this);
        try {
            checkoutUrl = new URL(URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Utils.log(e.getMessage());
        }
        //TODO handle NULL url
        Utils.log(URL);
        clearCookies(this);
        webView.loadUrl(URL);
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(context);
            cookieSyncManager.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
    }

    /**
     * Javascript Injected Methods
     */

    class JsObject {
        @JavascriptInterface
        public void itemUpdated(String sku, int quantity) {
            if (quantity == 0)
                Cart.getInstance().removeItem(sku, getApplicationContext());
            else Cart.getInstance().changeItemQuantity(sku, quantity);
        }
    }

}
