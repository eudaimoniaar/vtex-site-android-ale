package com.fizzmod.vtex.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.fragments.CreateShoppingListFragment;
import com.fizzmod.vtex.fragments.EditShoppingListsFragment;
import com.fizzmod.vtex.interfaces.CreateShoppingListListener;
import com.fizzmod.vtex.interfaces.EditShoppingListsListener;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.ShoppingListsModifications;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class BaseShoppingListsActivity extends AppCompatActivity implements
        CreateShoppingListListener,
        EditShoppingListsListener {

    public static final String USER_LISTS_SKU_KEY = "SELECTED_SKU_KEY";
    public static final String USER_LISTS_NEW_LIST = "NEW_LIST";
    public static final String USER_LISTS_MODIFIED_NAMES = "MODIFIED_NAMES";
    public static final String USER_LISTS_MODIFIED_MANY = "MODIFIED_NAMES_MANY";
    public static final String REQUEST_SIGN_IN = "REQUEST_SIGN_IN";

    private EditShoppingListsFragment editShoppingListsFragment;
    private CreateShoppingListFragment createShoppingListFragment;

    public static void start(Activity activity) {
        start(
                activity,
                new Intent(activity, ShoppingListsActivity.class),
                Main.CREATE_SHOPPING_LISTS_ACTIVITY);
    }

    public static void start(Activity activity, String sku) {
        Intent intent = new Intent(activity, ShoppingListsActivity.class);
        intent.putExtra(USER_LISTS_SKU_KEY, sku);
        start(activity, intent, Main.EDIT_SHOPPING_LISTS_ACTIVITY);
    }

    private static void start(Activity activity, Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(R.anim.enter_from_bottom, R.anim.exit_to_top);
    }

    private final List<ShoppingList> shoppingLists = new ArrayList<>();

    private RelativeLayout progressLayout;
    private String selectedSku;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_lists);

        progressLayout = (RelativeLayout) findViewById(R.id.progress);
        selectedSku = getIntent().getStringExtra(USER_LISTS_SKU_KEY);

        if (selectedSku == null) {
            changeFragment(getCreateShoppingListFragment());
            hideProgress();
        } else
            fetchShoppingLists();
    }

    @Override
    public void onBackPressed() {
        if (progressLayout.getVisibility() == View.VISIBLE)
            return;

        if (editShoppingListsFragment != null && !editShoppingListsFragment.isVisible()) {
            changeFragment(editShoppingListsFragment, true);
            return;
        }

        super.onBackPressed();
    }

    /*******************
     * Private methods *
     *******************/

    private void createList(String name, Callback<ShoppingList> callback) {
        JanisService.getInstance(this).createShoppingList(name, callback);
    }

    private void saveLists(ShoppingListsModifications modifications, Callback<Object> callback) {
        JanisService.getInstance(this).updateShoppingListsSkus(modifications, callback);
    }

    private void fetchShoppingLists() {
        fetchShoppingLists(new Callback<List<ShoppingList>>(this) {
            @Override
            public void onResponse(final List<ShoppingList> lists) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        shoppingLists.addAll(lists);
                        if (shoppingLists.isEmpty())
                            changeFragment(getCreateShoppingListFragment());
                        else
                            changeFragment(getEditShoppingListsFragment());
                        hideProgress();
                    }
                });
            }

            @Override
            public void onError(final String errorMessage) {
                showErrorToast(errorMessage);
                finish();
            }

            @Override
            protected void retry() {
                fetchShoppingLists(this);
            }
        });
    }

    private void fetchShoppingLists(Callback<List<ShoppingList>> callback) {
        JanisService.getInstance(this).getShoppingLists(callback);
    }

    private void changeFragment(Fragment fragment){
        changeFragment(fragment, false);
    }

    private void changeFragment(Fragment fragment, boolean goingBack){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        goingBack ? R.anim.enter_from_left : R.anim.enter_from_right,
                        goingBack ? R.anim.exit_to_right : R.anim.exit_to_left)
                .replace(R.id.list_manager_fragment_layout, fragment)
                .commit();
    }

    private EditShoppingListsFragment getEditShoppingListsFragment() {
        if (editShoppingListsFragment == null) {
            editShoppingListsFragment = new EditShoppingListsFragment();
            editShoppingListsFragment.setListener(this);
            editShoppingListsFragment.setShoppingLists(shoppingLists);
            editShoppingListsFragment.setSelectedSku(selectedSku);
        }
        return editShoppingListsFragment;
    }

    private CreateShoppingListFragment getCreateShoppingListFragment() {
        if (createShoppingListFragment == null) {
            createShoppingListFragment = new CreateShoppingListFragment();
            createShoppingListFragment.setListener(this);
        }
        return createShoppingListFragment;
    }

    private void hideProgress() {
        progressLayout.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    private void showErrorToast(final String errorMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgress();
                Toast.makeText(BaseShoppingListsActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /******************************
     * CreateShoppingListListener *
     ******************************/

    @Override
    public void onCloseCreateList() {
        finish();
    }

    @Override
    public void onCreateList(final String name) {
        showProgress();
        createList(name, new Callback<ShoppingList>(this) {
            @Override
            public void onResponse(ShoppingList shoppingList) {
                shoppingLists.add(shoppingList);

                if (selectedSku != null) {
                    changeFragment(getEditShoppingListsFragment());
                    hideProgress();
                } else {
                    getIntent().putExtra(USER_LISTS_NEW_LIST, new Gson().toJson(shoppingList));
                    setResult(RESULT_OK, getIntent());
                    finish();
                }
            }

            @Override
            protected void retry() {
                createList(name, this);
            }
        });
    }

    /*****************************
     * EditShoppingListsListener *
     *****************************/

    @Override
    public void onNewList() {
        changeFragment(getCreateShoppingListFragment());
    }

    @Override
    public void onCloseEditShoppingLists() {
        finish();
    }

    @Override
    public void onSaveLists(final ShoppingListsModifications modifications) {
        final List<Integer> modifiedIds = modifications.getModifiedIds();
        if (modifiedIds.isEmpty())
            return;

        showProgress();
        saveLists(modifications, new Callback<Object>(this) {
            @Override
            public void onResponse(Object object) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String names = TextUtils.join(", ", modifications.getNames(modifiedIds));
                        getIntent().putExtra(USER_LISTS_MODIFIED_NAMES, names);
                        getIntent().putExtra(USER_LISTS_MODIFIED_MANY, modifiedIds.size() > 1);
                        setResult(RESULT_OK, getIntent());
                        finish();
                    }
                });
            }

            @Override
            protected void retry() {
                saveLists(modifications, this);
            }
        });
    }

    /********************
     * Private callback *
     ********************/

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        public void onError(final String errorMessage) {
            showErrorToast(errorMessage);
        }

        @Override
        protected void requestSignIn() {
            Intent intent = new Intent();
            intent.putExtra(REQUEST_SIGN_IN, true);
            setResult(RESULT_CANCELED, intent);
            finish();
        }

    }

}
