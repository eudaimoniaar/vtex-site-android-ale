package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

import com.fizzmod.vtex.config.Config;

/**
 * Created by marcos on 30/04/17.
 */

public class Contact extends BaseWebviewFragment {

    public Contact() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Favourites.
     */

    public static Contact newInstance() {
        return new Contact();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setURL(Config.getInstance().getContactURL());
        setUI(view);
    }
}
