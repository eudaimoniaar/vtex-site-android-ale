/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.SelectorAdapter;
import com.fizzmod.vtex.animations.ResizeAnimation;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.ProductCallback;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.interfaces.ScrollViewListener;
import com.fizzmod.vtex.interfaces.TypedCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.AnalyticsUtils;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ObservableScrollView;
import com.fizzmod.vtex.views.ProductDetailLayout;
import com.fizzmod.vtex.views.ProductPricesAndPromosLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import okhttp3.Call;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductPage#newInstance} factory method to
 * create an instance of this fragment.
 * TODO http://stackoverflow.com/questions/8023453/do-i-need-to-restore-all-variables-onresume
 */
public abstract class BaseProductPage extends BackHandledFragment {

    public static final String ARG_PRODUCT = "pi";
    public static final String ARG_EAN = "pe";
    public static final String ARG_PRODUCT_LINK = "pl";

    private String productId;
    private String productLink;
    private String EAN;
    private int selectedQuantity = 1;
    private Sku currentSku = null;
    private Product product;

    private TextView productRefId;
    private TextView productQuantity;
    private TextView productTitle;
    private TextView installments;
    private LinearLayout productAdded;
    private ProductPricesAndPromosLayout productPricesAndPromosLayout;
    private ProductDetailLayout productDetailLayout;
    private LinearLayout productFooter;
    private ImageButton buyButton;
    private ImageView substractQuantity;
    private TextView productBrand;
    private int previousSelectPosition = -1;
    private LinearLayout selectorOverlay;
    private SelectorAdapter selectorAdapter;
    private ListView selectorList;
    private TextView selectorTitle;
    private ObservableScrollView productScroll;
    private AnimationAdapter relatedAdapter;


    private HashMap<String, String[]> variationsValue;
    private HashMap<String, String> selectedVariations;

    private boolean selectorOpen = false;
    private boolean closingOverlay = false;
    private boolean productMessageVisible = false;
    private boolean expandingHeader = false;
    private boolean collapsingHeader = true;
    private boolean socialVisible = true;

    private float substractQuantityButtonDisabledAlpha;

    public BaseProductPage() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPressed() {
        if(selectorOpen){
            closeSelector();
            return true;
        }

        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            productId = getArguments().getString(ARG_PRODUCT, null);
            productLink = getArguments().getString(ARG_PRODUCT_LINK, null);
            EAN = getArguments().getString(ARG_EAN, null);

            Utils.log("Pl: "+ productLink);
            Utils.log("Pe: "+ EAN);
            Utils.log("Pi: "+ productId);
        }

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_product_page, container, false);

        /*  Apparently, dimens does not support a float as a value as in
            "<dimen name="someName">5f</dimen>" so I had to work around it,
            found the solution at: https://stackoverflow.com/questions/3282390/add-floating-point-value-to-android-resources-values*/
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.substractQuantityButtonDisabledAlpha, outValue, true);
        substractQuantityButtonDisabledAlpha = outValue.getFloat();

        mListener.onFragmentStart(Main.FRAGMENT_TAG_PRODUCT);

        view.findViewById(R.id.retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                getProduct(view);
            }
        });

        view.findViewById(R.id.productNotFound).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener != null)
                    mListener.requestSearch();
            }
        });

        getProduct(view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        productPricesAndPromosLayout = null;
        productBrand = null;
        productRefId = null;
        productDetailLayout = null;
        productQuantity = null;
        productTitle = null;
        installments = null;
        productAdded = null;

        relatedAdapter = null;

        productFooter = null;
        buyButton = null;
        substractQuantity = null;


        selectorOverlay = null;
        selectorAdapter = null;
        selectorList = null;
        selectorTitle = null;
        productScroll = null;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(product != null){
            product.save(getActivity(), currentSku == null ? null : currentSku.getId(), selectedQuantity);
        }
    }

    public void getProduct(final View view){

        getProduct(new ProductCallback() {

            @Override
            public void loaded(final Product prod) {

                final Activity activity = getActivity();

                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(mListener == null)
                            return;

                        mListener.onLoadingStop();
                        product = prod;
                        AnalyticsUtils.logViewItemEvent(activity, product.getId(), product.getName());
                        setUI(view);
                    }
                });
            }

            @Override
            public void notFound() {

                Activity activity = getActivity();

                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(mListener == null)
                            return;

                        mListener.onLoadingStop();
                        Utils.fadeIn(view.findViewById(R.id.productNotFound));
                    }
                });
            }

            @Override
            public void scanError() {

                Activity activity = getActivity();

                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(mListener == null)
                            return;

                        mListener.onLoadingStop();

                        String errorMessage = getResources().getString(Utils.isValidEan(EAN) ? R.string.productNotFound : R.string.invalidCode);
                        ((TextView) view.findViewById(R.id.productErrorMessage)).setText(errorMessage);
                        Utils.fadeIn(view.findViewById(R.id.errorWrapper));
                    }
                });

            }

            @Override
            public void retry() {

                Activity activity = getActivity();

                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(mListener == null)
                            return;

                        mListener.onLoadingStop();
                        view.findViewById(R.id.retry).setVisibility(View.VISIBLE);
                    }
                });

            }

        });
    }

    public void getProduct(final ProductCallback callback){

        if (EAN != null) {
            //TODO if(Utils.isValidEan(code))
            mListener.onLoadingStart();
            API.getProductByEan(getActivity(), EAN, new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    if(callback != null)
                        callback.retry();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    ArrayList<Product> productList = API.setProductList(response.body().string());

                    if (!productList.isEmpty()) {
                        Product product = productList.get(0);
                        DataHolder.getInstance().setProduct(product);
                        AnalyticsUtils.logScanSearchOkEvent(getActivity(), EAN);
                        if(callback != null)
                            callback.loaded(product);
                    } else {
                        // Product by EAN not found!
                        AnalyticsUtils.logScanSearchNoneEvent(getActivity(), EAN);
                        if(callback != null)
                            callback.scanError();
                    }
                    //Handle error
                }
            });

            return;
        }

        if (productLink != null) {
            mListener.onLoadingStart();
            API.getProductByLink(getActivity(), productLink, new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if(callback != null)
                        callback.retry();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    ArrayList<Product> productList = API.setProductList(response.body().string());
                    if (!productList.isEmpty()) {
                        Product product = productList.get(0);
                        DataHolder.getInstance().setProduct(product);
                        callback.loaded(product);
                    } else
                        callback.notFound();
                    //Handle error
                }
            });
            return;
        }

        /**
         * Use product from data Holder
         */

        if(productId == null || !DataHolder.getInstance().isForceGetProduct()) {
            Product product = DataHolder.getInstance().getProduct();

            DataHolder.getInstance().setForceGetProduct(false);

            if(product != null) {
                callback.loaded(product);
                return;
            }

            Product.restore(getActivity(), new Callback() {
                @Override
                public void run(Object data) {
                    callback.loaded((Product) data);
                }

                @Override
                public void run(Object data, Object data2) {}
            });

            return;

        }

        mListener.onLoadingStart();
        API.getProduct(getActivity(), productId, new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //TODO handle error

                if(callback != null)
                    callback.retry();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ArrayList<Product> productList = API.setProductList(response.body().string());

                if (!productList.isEmpty()) {
                    Product product = productList.get(0);
                    DataHolder.getInstance().setProduct(product);
                    callback.loaded(product);
                } else {
                    callback.notFound();
                }

                //Handle error
            }
        });
    }

    /**
     * Set the product UI
     */

    public void setUI(final View view){

        if(product == null) {
            // Do something...
            return;
        }

        Sku sku = product.getSku(0);

        Utils.log("Product: " + product.getId());

        setViews(view);

        setQuantity(1);
        substractQuantity.setAlpha(substractQuantityButtonDisabledAlpha);

        if (selectedQuantity <= 1) {
            substractQuantity.setEnabled(false);
        }

        if (BuildConfig.PROMOTIONS_API_ENABLED && sku.hasCurrentPromotions()) {
            productDetailLayout.showLinearPromotion();
            productDetailLayout.setHiglightTextViewText(sku.getPriceDiffPercentageFormatted(getActivity()));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 1);
            //find and set the image with the same name.
            for (Promotion promotion : sku.getPromotionList()) {
                try {
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setImageResource(promotion.getPromotionImageResource());
                    imageView.setLayoutParams(lp);
                    productDetailLayout.addViewToLinearPromotion(imageView);
                } catch (NullPointerException exception) {
                    Log.e(Promotion.LOG_ERROR, exception.toString());
                }
            }

            if(sku.hasLabelPromotion()) {
                productPricesAndPromosLayout.showLabelPromotion(sku.getLabelName());
            }
        }

        productTitle.setText(product.getName());
        productPricesAndPromosLayout.setProductTitleFooterText(product.getName());
        productBrand.setText(product.getBrand());

        ((TextView) view.findViewById(R.id.productDescription)).setText(
                Html.fromHtml(product.getDescription()), TextView.BufferType.SPANNABLE);

        if(!product.hasDescription())
            view.findViewById(R.id.descriptionWrapper).setVisibility(View.GONE);

        view.findViewById(R.id.arrowDownDescription).setEnabled(false);
        view.findViewById(R.id.arrowDownSpec).setEnabled(false);

        setSpecifications(view, product.getSpecifications());
        setFavourite(view);
        setProductSliders(view);
        setScrollListener(view);
        setClickListeners(view);

        //Must be called LAST, so all other listeners are set
        setSkuSelectors(view, sku);


        //Show view when everything is set up
        Utils.fadeIn(view.findViewById(R.id.productWrapper), new Callback() {
            @Override
            public void run(Object data) {
                //Hide description wrapper, after height has been calculated correctly
                view.findViewById(R.id.productDescriptionWrapper).setVisibility(View.GONE);
                //Utils.smoothScroll(productScroll, 0, 300);
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    public void setViews(final View view){
        productRefId = (TextView) view.findViewById(R.id.productRefId);
        productQuantity = (TextView) view.findViewById(R.id.productQuantity);
        productBrand = (TextView) view.findViewById(R.id.productBrand);
        productAdded = (LinearLayout) view.findViewById(R.id.productAdded);
        productDetailLayout = (ProductDetailLayout) view.findViewById(R.id.productDetailLayout);
        productPricesAndPromosLayout = (ProductPricesAndPromosLayout) view.findViewById(R.id.productPricesAndPromosLayout);

        productTitle = (TextView) view.findViewById(R.id.productTitle);
        installments = (TextView) view.findViewById(R.id.productInstallments);

        productFooter = (LinearLayout) view.findViewById(R.id.productFooter);
        buyButton = (ImageButton) view.findViewById(R.id.buy);
        substractQuantity = (ImageView) view.findViewById(R.id.substractQuantity);

        selectorOverlay = (LinearLayout) view.findViewById(R.id.itemSelectorOverlay);
        selectorList = (ListView) view.findViewById(R.id.itemSelectorList);
        selectorTitle = (TextView) view.findViewById(R.id.itemSelectorText);

        productScroll = (ObservableScrollView) view.findViewById(R.id.productScroll);
    }


    public void showErrors(View view){

        if(EAN != null) {
            String errorMessage = getResources().getString(Utils.isValidEan(EAN) ? R.string.productNotFound : R.string.invalidCode);
            ((TextView) view.findViewById(R.id.productErrorMessage)).setText(errorMessage);
            Utils.fadeIn(view.findViewById(R.id.errorWrapper));
        }

    }

    /**
     * Setup product variations
     * @param view
     * @param sku
     */

    public void setSkuSelectors(View view, Sku sku){

        final String[] variations = sku.getVariations();
        variationsValue = new HashMap<>();
        selectedVariations = new HashMap<>();

        buyButton.setEnabled(false);
        buyButton.setAlpha(0.5f);

        if(variations == null){
            changeSku(sku);
        }else {

            buildInstallments(sku);
            /** Set Image & Price from first SKU **/
            changeSku(sku, true);

            LinearLayout skuSelectorWrapper = (LinearLayout) view.findViewById(R.id.skuSelectorWrapper);
            for(int i = 0; i < variations.length; i++ ){

                /** Set SKU Selector **/

                String[] skusVariations = new String[product.getSkuLength()];

                int variationCount = 0;
                int length =  product.getSkuLength();
                for(int j = 0; j < length; j++){
                    Sku skuAux = product.getSku(j);
                    String value = skuAux.getVariationValue(variations[i]);
                    if(value != null) {
                        skusVariations[variationCount] = value;
                        variationCount++;
                    }

                    /** Pre-fetch all sku main images **/

                    if(Config.getInstance().isPreFetchSkuImages())
                        Picasso.with(getActivity()).load(skuAux.getMainImage(450, 450)).fetch();
                }

                String aux[] = Utils.arrayUnique(skusVariations);

                variationsValue.put(variations[i], aux);
                selectedVariations.put(variations[i], aux.length == 1 ? aux[0] : null);

                if((variations.length > 1 && aux.length > 1) || variations.length == 1) {
                    LinearLayout skuSelector = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.product_sku_selector, skuSelectorWrapper, false);
                    ((TextView) skuSelector.findViewById(R.id.skuSelectorText)).setText(variations[i]);

                    skuSelector.setTag(variations[i]);

                    if(aux.length == 1){
                        ((TextView) skuSelector.findViewById(R.id.skuSelectorValue)).setText(aux[0]);
                    }

                    skuSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String variation = v.getTag().toString();
                            setSkuVariations(variationsValue.get(variation), variation, (TextView) v.findViewById(R.id.skuSelectorValue));
                        }
                    });

                    skuSelectorWrapper.addView(skuSelector);
                }
            }

            setSkuBySelectedVariations();
        }

    }

    /**
     * Set the Sku variations of a Sku Selector: IE: Set all available colors of Color selector
     * @param values
     * @param variation
     * @param text
     */
    public void setSkuVariations(String[] values, final String variation, final TextView text){
        final ArrayList<SelectorItem> items = new ArrayList<>();

        for(int i = 0; i < values.length; i++){
            SelectorItem item = new SelectorItem(values[i], "");

            if(selectedVariations.get(variation) == values[i])
                item.enabled = true;

            items.add(item);
        }

        selectorTitle.setText(variation);

        selectorAdapter = new SelectorAdapter(items);

        selectorList.setAdapter(selectorAdapter);
        selectorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position != previousSelectPosition) {

                    SelectorItem current = items.get(position);

                    selectedVariations.put(variation, current.name);
                    setSkuBySelectedVariations();

                    text.setText(current.name);

                }

                closeSelector();
            }
        });

        openSelector();

    }

    /**
     * Set global click listeners
     * @param view
     */
    public void setClickListeners(final View view) {

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy(v);
            }
        });

        view.findViewById(R.id.addQuantity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addQuantity(v);
            }
        });

        view.findViewById(R.id.substractQuantity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                substractQuantity(v);
            }
        });

        view.findViewById(R.id.closeOverlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeSelector();
            }
        });

        selectorOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeSelector();
            }
        });

        view.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.scale(v);
                Utils.share(getActivity(), product);
            }
        });

        if (BuildConfig.SHOPPING_LISTS_ENABLED) {
            View listsIcon = view.findViewById(R.id.toList);
            listsIcon.setVisibility(View.VISIBLE);
            listsIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.scale(view);
                    mListener.onAddProductToList(product.getSku(0).getId(), Main.FRAGMENT_TAG_ORDERS);
                    }
            });
        }

        view.findViewById(R.id.relatedWrapper).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final RelativeLayout sliderWrapper = (RelativeLayout) view.findViewById(R.id.relatedSliderWrapper);
                final ImageView arrow = (ImageView) view.findViewById(R.id.arrowDownRelated);
                int visibility = sliderWrapper.getVisibility();


                if(visibility == View.GONE){
                    arrow.setEnabled(true);
                    Utils.expand(sliderWrapper, 300, new Callback() {
                        @Override
                        public void run(Object data) {
                            RecyclerView.Adapter productAdapter = ((RecyclerView) view.findViewById(R.id.related)).getAdapter();
                            if(productAdapter != null)
                                productAdapter.notifyDataSetChanged();
                            Utils.smoothScroll(productScroll, sliderWrapper.getTop(), 300);
                        }

                        @Override
                        public void run(Object data, Object data2) {}
                    });
                }else {
                    arrow.setEnabled(false);
                    Utils.collapse(sliderWrapper, 300);
                }
            }
        });
    }

    /**
     * Manage scroll
     * @param view
     */

    public void setScrollListener(View view){
        final LinearLayout topContentWrapper = (LinearLayout) view.findViewById(R.id.topWrapper);
        final LinearLayout socialWrapper = (LinearLayout) view.findViewById(R.id.socialWrapper);
        final float socialHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48 * 3, getResources().getDisplayMetrics());;

        productScroll.setScrollViewListener(new ScrollViewListener() {
            @Override
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {

                if (y > oldy) {
                    backHandlerInterface.onViewScrollUp();
                }

                if(topContentWrapper == null || productTitle == null || socialWrapper == null)
                    return;

                Rect scrollBounds = new Rect();
                scrollView.getDrawingRect(scrollBounds);

                float contentWrapperBottom = topContentWrapper.getY() + topContentWrapper.getHeight() - socialHeight;
                float productTitleBottom = productTitle.getY() + productTitle.getHeight() - 15; //15 is hardcoded offset

                if (productTitleBottom > y) {
                    //Visible
                    expandingHeader = false;
                    collapseFooter();
                } else {
                    collapsingHeader = false;
                    expandFooter();
                    //Title gone
                }

                if (contentWrapperBottom > y)
                    showSocial(socialWrapper);
                else
                    hideSocial(socialWrapper);

            }
        });
    }

    /**
     * Setup favourite button and manage its actions
     * @param view
     */
    public void setFavourite(View view){

        ImageView favouriteIcon = (ImageView) view.findViewById(R.id.favourite);

        if (Favourites.exists(getActivity(), product.getId())) {
            favouriteIcon.setImageResource(R.drawable.favourite_active);
            Utils.scale(favouriteIcon);
        }

        favouriteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFavorite = Favourites.exists(getActivity(), product.getId());
                int iconResId = R.drawable.favourite_inactive;
                if (isFavorite)
                    Favourites.remove(getActivity(), product.getId());
                else {
                    Favourites.save(getActivity(), product.getId());
                    iconResId = R.drawable.favourite_active;
                }
                ((ImageView) v).setImageResource(iconResId);
                mListener.onFavoriteProduct(!isFavorite);
                Utils.scale(v);
            }
        });
    }

    /**
     * Open SKU selector
     */
    public void openSelector(){
        selectorOpen = true;
        Utils.fadeIn(selectorOverlay);
    }

    /**
     * Close SKU selector
     */
    public void closeSelector(){
        selectorOpen = false;
        if(!closingOverlay) {
            closingOverlay = true;
            Utils.fadeOut(selectorOverlay, new Callback() {
                @Override
                public void run(Object data) {
                    closingOverlay = false;
                }

                @Override
                public void run(Object data, Object data2) {}
            });
        }
    }

    /**
     * Set product specifications
     * @param view
     * @param specifications
     */

    public void setSpecifications(View view, TreeMap<String, String> specifications){
        int count = 0;
        if(specifications instanceof  TreeMap){
            LinearLayout specificationsWrapper = (LinearLayout) view.findViewById(R.id.specifications);

            Set keys = specifications.keySet();
            for (Iterator i = keys.iterator(); i.hasNext();) {
                String key = (String) i.next();
                String value = specifications.get(key);

                if(!Config.getInstance().getExcludedSpecifications().contains(key)) {

                    LinearLayout specification = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.sublayout_specification, specificationsWrapper, false);

                    ((TextView) specification.findViewById(R.id.key)).setText(key);
                    ((TextView) specification.findViewById(R.id.value)).setText(value);

                    if (!i.hasNext()) {
                        specification.setBackgroundResource(0);
                    }

                    specificationsWrapper.addView(specification);
                    count++;
                }
            }
        }

        view.findViewById(R.id.productSpecificationsWrapper).setVisibility(count == 0 ? View.GONE : View.VISIBLE);

    }

    /**
     * Set current SKU by a given selected variation.
     */
    public void setSkuBySelectedVariations(){

        Sku sku = null;
        int length = product.getSkuLength();
        for(int i = 0; i < length; i++){
            sku = product.getSku(i);

            boolean flag = true;
            Set<Map.Entry<String, String>> entrySet = selectedVariations.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                if(sku.getVariationValue(entry.getKey()) != entry.getValue())
                    flag = false;
            }

            if(flag) {
                changeSku(sku);
                break;
            }
        }

    }

    /**
     * Set given SKU
     * @param skuId
     */

    public void changeSku(String skuId){

        Sku sku = null;
        int length = product.getSkuLength();
        for(int i = 0; i < length ; i++){
            sku = product.getSku(i);
            if(sku.getId().equals(skuId))
                break;
            sku = null;
        }


        changeSku(sku);
    }

    public void changeSku(Sku sku){
        changeSku(sku, false);
    }

    public void changeSku(Sku sku, boolean info){
        if (sku != null){
            if (!info)
                currentSku = sku;

            if (sku.hasStock()) {
                if (!info) {
                    buyButton.setEnabled(true);
                    buyButton.setAlpha(1f);
                }

                sku.getBestPriceFormatted(new TypedCallback<String>() {
                    @Override
                    public void run(String price) {
                        productPricesAndPromosLayout.setProductBestPriceText(price);
                    }
                });

                if (sku.showListPrice()) {
                    sku.getListPriceFormatted(new TypedCallback<String>() {
                        @Override
                        public void run(String price) {
                            productPricesAndPromosLayout.showListPrice(price);
                        }
                    });
                } else
                    productPricesAndPromosLayout.hideListPrice();

                if (sku.showPriceDiffPercentage()){
                    productPricesAndPromosLayout.showProductPriceHighlight(sku.getPriceDiffPercentageFormatted(getActivity()));
                    productDetailLayout.setHiglightTextViewText(sku.getPriceDiffPercentageFormatted(getActivity()));
                } else
                    productPricesAndPromosLayout.hideProductPriceHighlight();

                if (sku.showPriceByUnit()){
                    sku.getPriceByUnitFormatted(new TypedCallback<String>() {
                        @Override
                        public void run(String price) {
                            productPricesAndPromosLayout.showPriceByUnit(
                                    getString(R.string.product_price_by_unit, price));
                        }
                    });
                } else {
                    productPricesAndPromosLayout.hidePriceByUnit();
                }

                buildInstallments(sku);

            } else {
                if (!info) {
                    buyButton.setEnabled(false);
                    buyButton.setAlpha(0.5f);
                }

                productPricesAndPromosLayout.setProductBestPriceText(getResources().getString(R.string.outOfStock));
                productPricesAndPromosLayout.hideListPrice();
                productPricesAndPromosLayout.hideProductPriceHighlight();
            }

            if(productRefId != null)
                productRefId.setText(sku.getRefId());
            productDetailLayout.showLoadingImage();
            productDetailLayout.hideProductImage();
            productDetailLayout.loadProductImage(sku.getMainImage());
        }
    }

    /**
     * Buy button listener
     * @param view
     */

    public void buy(View view){
        if(currentSku == null)
            return;

        currentSku.setSelectedQuantity(selectedQuantity);

        if(currentSku instanceof Sku)
            Cart.getInstance().addItem(currentSku, getActivity());

        mListener.onProductAdded();

        if(!productMessageVisible){

            productMessageVisible = true;
            productAdded.startAnimation(
                    Utils.getAnimation(
                            Utils.ANIMATION_IN_LEFT_TO_RIGHT,
                            productAdded,
                            300
                    )
            );

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(productAdded == null)
                        return;

                    productAdded.startAnimation(
                            Utils.getAnimation(
                                    Utils.ANIMATION_OUT_LEFT_TO_RIGHT,
                                    productAdded,
                                    300
                            )
                    );
                    productMessageVisible = false;
                }
            }, 2500);
        }

    }

    /**
     * Hide social wrapper
     * @param social
     */

    public void hideSocial(View social){
        if(socialVisible) {
            socialVisible = false;
            Utils.fadeOut(social, 150);
        }
    }

    /**
     * Show social wrapper
     * @param social
     */

    public void showSocial(View social){
        if(!socialVisible) {
            Utils.fadeIn(social);
            socialVisible = true;
        }
    }

    /**
     * Expand footer
     */
    public void expandFooter(){
        if(expandingHeader)
            return;

        expandingHeader = true;

        int targetHeight = (int) getResources().getDimension(R.dimen.productFooterHeightExpanded);
        int initialHeight = (int) getResources().getDimension(R.dimen.productFooterHeight);

        ResizeAnimation resizeAnimation = new ResizeAnimation(
                productFooter,
                targetHeight - initialHeight,
                initialHeight

        );
        resizeAnimation.setDuration(300);
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        productPricesAndPromosLayout.animateProductTitleFooter();
        productFooter.startAnimation(resizeAnimation);
    }

    /**
     * Collapse footer
     */
    public void collapseFooter(){
        if(collapsingHeader)
            return;

        collapsingHeader = true;

        int targetHeight = (int) getResources().getDimension(R.dimen.productFooterHeight);
        int initialHeight = (int) getResources().getDimension(R.dimen.productFooterHeightExpanded);

        ResizeAnimation resizeAnimation = new ResizeAnimation(
                productFooter,
                targetHeight - initialHeight,
                initialHeight

        );
        resizeAnimation.setDuration(300);
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        productPricesAndPromosLayout.hideProductTitleFooter();
        productFooter.startAnimation(resizeAnimation);
    }

    /**
     * Increase quantity by 1
     * @param view
     */
    public void addQuantity(View view){
        selectedQuantity = getQuantity() + 1;
        setQuantity(selectedQuantity);
        if (selectedQuantity == 2) {
            substractQuantity.setEnabled(true);
            Utils.alpha(substractQuantity, substractQuantityButtonDisabledAlpha, 1f, 350);
        }
    }

    /**
     * Decrease quantity by 1
     * @param view
     */

    public void substractQuantity(View view){
        selectedQuantity = Math.max(getQuantity() - 1, 1);
        setQuantity(selectedQuantity);
        view.setEnabled(selectedQuantity > 1);
        if (selectedQuantity <= 1)
            Utils.alpha(substractQuantity, 1f, substractQuantityButtonDisabledAlpha, 350);
    }

    /**
     * Build product installments.
     * @param sku
     */
    public void buildInstallments(final Sku sku){
        if (sku == null || !BuildConfig.SHOW_INSTALLMENTS)
            return;

        Resources res = getResources();
        if (!sku.hasInstallments()){
            productPricesAndPromosLayout.setProductBestPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productBestPriceBig));
            productPricesAndPromosLayout.setProductListPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productListPriceBig));
            productPricesAndPromosLayout.setProductPriceHighlightTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceHighlightBig));
            productPricesAndPromosLayout.setProductPriceByUnitTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceByUnitBig));
            productPricesAndPromosLayout.setProductTitleFooterTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productFooterTitleBig));

            return;
        }

        productPricesAndPromosLayout.setProductBestPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productBestPriceSmall));
        productPricesAndPromosLayout.setProductListPriceTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productListPriceSmall));
        productPricesAndPromosLayout.setProductPriceHighlightTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceHighlightSmall));
        productPricesAndPromosLayout.setProductPriceByUnitTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productPriceByUnitSmall));
        productPricesAndPromosLayout.setProductTitleFooterTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.productFooterTitleSmall));

        sku.getInstallmentPriceFormatted(new TypedCallback<String>() {
            @Override
            public void run(String price) {
                installments.setText(
                        Html.fromHtml( getString( R.string.installments, sku.getInstallments(), price ) ),
                        TextView.BufferType.SPANNABLE);
                installments.setVisibility(View.VISIBLE);
            }
        });
    }



    /**
     * Setup product Sliders - Related Products...
     * @param view
     */

    private void setProductSliders(final View view){

        view.findViewById(R.id.relatedProgress).setVisibility(View.VISIBLE);

        Category.getVtexCategories(getActivity(), new Callback() {
            @Override
            public void run(Object categories) {

                final Activity activity = getActivity();

                if (activity == null)
                    return;
                Category category = Category.getCategoryByName(
                        (ArrayList<Category>) categories,
                        product.getCategories(),
                        null
                );

                //Related products1
                if (category == null){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            view.findViewById(R.id.relatedProgress).setVisibility(View.GONE);
                            //TODO Show no results...
                        }
                    });
                    return;
                }

                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("fq", "C:" + category.getIdsPath());
                getItems(parameters, new Callback() {
                    @Override
                    public void run(final Object data) {

                        if (activity == null)
                            return;

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                relatedAdapter = Utils.setItemsSlider(
                                        view,
                                        (ArrayList) data,
                                        R.id.related,
                                        R.id.relatedProgress,
                                        Main.FRAGMENT_TAG_ORDERS,
                                        mListener,
                                        new ProductListCallback() {
                                            @Override
                                            public void goToProduct(Product product) {
                                                productScroll.scrollTo(0,0);
                                                DataHolder.getInstance().setForceGetProduct(true);
                                                if (mListener != null)
                                                    mListener.onProductSelected(product.getId());
                                            }

                                            @Override
                                            public int productAdded(int position) {
                                                return 0;
                                            }

                                            @Override
                                            public int productSubtracted(int position) {
                                                return 0;
                                            }

                                            @Override
                                            public int productAdded(Sku sku) {
                                                int quantity = Cart.getInstance().increaseItemQuantity(sku, getActivity());
                                                if (mListener != null)
                                                    mListener.onProductAdded();
                                                return quantity;
                                            }

                                            @Override
                                            public int productSubtracted(Sku sku) {
                                                int quantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                                                if (mListener != null)
                                                    mListener.onProductAdded();
                                                return quantity;
                                            }
                                        },
                                        true);
                            }
                        });
                    }

                    @Override
                    public void run(Object data, Object data2) {}
                });

            }

            @Override
            public void run(Object data, Object data2) {}
        });

    }

    /**
     * Get Items given api serach parameters
     * @param parameters
     * @param callback
     */
    private void getItems(HashMap<String, String> parameters, final Callback callback){

        API.search(getActivity(), null, parameters, 1, API.SOFT_LIMIT, new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //TODO handle connection errors
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                ArrayList productList = API.setProductList(response.body().string(), product.getId());
                callback.run(productList);
            }
        });

    }


    @Override
    public void refresh(){

        // Search product by ID & only refresh price. other information does not change by sales channel.
        if(product != null) {

            View view = getView();

            API.getProduct(getActivity(), product.getId(), new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    try {
                        final ArrayList<Product> productList = API.setProductList(response.body().string(), null, true);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (!productList.isEmpty()) {
                                    product = productList.get(0);
                                    Sku refreshedSku = product.getSku(0);

                                    changeSku(refreshedSku);
                                } else {
                                    // No stock
                                    Sku sku = product.getSku(0);
                                    sku.setStock(0);

                                    changeSku(sku);
                                }
                            }
                        });


                    } catch (JSONException e) {

                    }
                }
            });

            if(view != null)
                setProductSliders(view);
        }

    }

    @Override
    public void cartUpdated(){

        if(relatedAdapter != null) {
            relatedAdapter.notifyDataSetChanged();
        }
    }

    private int getQuantity() {
        Sku sku = currentSku != null ? currentSku : product.getSku(0);
        String quantityString = productQuantity.getText().toString();
        return Config.getInstance().showProductWeight() && sku.hasWeight() ?
                sku.getQuantityFromWeight(quantityString) :
                Integer.parseInt(quantityString);
    }

    private void setQuantity(int quantity) {
        Sku sku = currentSku != null ? currentSku : product.getSku(0);
        productQuantity.setText(
                Config.getInstance().showProductWeight() && sku.hasWeight() ?
                        sku.getWeightFormatted(quantity) :
                        String.valueOf(quantity) );
    }

}
