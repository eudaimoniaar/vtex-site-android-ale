package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.activities.CouponsInstructionsActivity;
import com.fizzmod.vtex.interfaces.CouponsFragmentInteractionListener;
import com.fizzmod.vtex.utils.CouponsSharedPreferences;
import com.fizzmod.vtex.views.CouponsCartelMessage;

import static android.view.View.VISIBLE;
import static com.fizzmod.vtex.Main.COUPONS_INSTRUCTIONS_ACTIVITY;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_COUPONS;

public class Coupons extends BackHandledFragment implements CouponsFragmentInteractionListener {

    private RelativeLayout progressLayout;
    private CouponsCartelMessage couponsCartel;

    public Coupons() {
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupons, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressLayout = view.findViewById(R.id.progress);

        couponsCartel = view.findViewById(R.id.coupons_cartel);

        updateUI();
    }

    /****** Ui Methods ******/

    private void updateUI() {
        if (CouponsSharedPreferences.getInstance(getActivity()).hasCI()) {
            onLoadingStarted();
            changeView(CouponsSelection.class);

        } else {
            changeView(CouponsAccess.class);
        }
    }

    private void eliminateData() {
        CouponsSharedPreferences.getInstance(getActivity()).deleteCI();
        CouponsSharedPreferences.getInstance(getActivity()).deleteCard();
    }

    private void changeView(Class fragmentClass) {
        try {
            BaseCouponsFragment fragment = (BaseCouponsFragment) fragmentClass.newInstance();
            fragment.setListener(this);

            FragmentManager fragmentManager = getFragmentManager();

            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.animator.fade_in, R.animator.fade_out, R.animator.fade_out, R.animator.fade_in)
                    .replace(R.id.coupons_fragment_contents, fragment, FRAGMENT_TAG_COUPONS)
                    .commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        }
    }

    /****** Listeners ******/

    public void onLoadingFinished() {
        progressLayout.setVisibility(View.GONE);
    }

    public void onLoadingStarted() {
        progressLayout.setVisibility(VISIBLE);
    }

    public void onChangeAccount() {
        eliminateData();
        changeView(CouponsAccess.class);
    }

    public void onCISelected() {
        //Due to changes, now you will not pass through cards and select the first card from the card API
//        changeView(CouponsCards.class);
        onCardSelected();
    }

    public void onCardSelected() {
        changeView(CouponsSelection.class);
    }

    public void onInstructionsClicked() {
        Activity activity = getActivity();
        Intent intent = new Intent(activity, CouponsInstructionsActivity.class);
        activity.startActivityForResult(intent, COUPONS_INSTRUCTIONS_ACTIVITY);
    }

    public void onShowCartel(int id) {
        couponsCartel.setText(id);
        couponsCartel.display();
    }

}