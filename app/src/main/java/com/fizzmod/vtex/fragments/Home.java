/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.PromotionsHandler;
import com.fizzmod.vtex.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.annotation.Nonnull;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import okhttp3.Call;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductPage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home extends BackHandledFragment {

    /**TODO, detect when are two menuButtons activated*/

    private static final int PRODUCT_TYPE_DEALS = 1;
    private static final int PRODUCT_TYPE_BEST_SELLERS = 2;
    private static final int PRODUCT_TYPE_OUR_BRANDS = 3;
    private static final int SLIDER_DURATION_MILLIS = 5000;
    private static final int SLIDER_DELAY_AUTO_CYCLE_MILLIS = 6000;
    public static final int SLIDER_VISIBILITY_DELAY_MILLIS = 2000;
    private static WeakHashMap<Integer, ArrayList<Product>> productsCache = new WeakHashMap<>();
    private static final Map<Integer, String> queries;

    private EditText homeSearch;

    static
    {
        queries = new HashMap<>();
        queries.put(PRODUCT_TYPE_DEALS, Config.getInstance().getDealsCollection());
        queries.put(PRODUCT_TYPE_BEST_SELLERS, Config.getInstance().getBestSellingCollection());
        queries.put(PRODUCT_TYPE_OUR_BRANDS, Config.getInstance().getOurBrandsCollection());
    }

    private SliderLayout mainSlider;
    private SliderLayout footerSlider;
    private HashMap<Integer, AnimationAdapter> adapterMap = new HashMap<>();

    public Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProductPage.
     */

    public static Home newInstance() {
        return new Home();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);

        mListener.reloadToolbarLayout();

        backHandlerInterface.addScrollListener((ScrollView) view.findViewById(R.id.homeScrollView));

        homeSearch = (EditText) view.findViewById(R.id.homeSearch);
        homeSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    mListener.onSearchFromHomeFragment(v.getText().toString());
                    homeSearch.setText("");
                    return true;
                }
                return false;
            }
        });

        mListener.onFragmentStart(Main.FRAGMENT_TAG_HOME);

        adapterMap = new HashMap<>();

        mainSlider = view.findViewById(R.id.mainSlider);
        setSlider(mainSlider);
        if (BuildConfig.FOOTER_BANNER_ENABLED) {
            footerSlider = view.findViewById(R.id.footerSlider);
            setSlider(footerSlider);
        }

        //Deals
        getDeals(view);

        //Best sellers
        getBestSelling(view);

        //Our brands
        getOurBrands(view);

        if(!Utils.isEmpty(Config.getInstance().getHomeTopBanner())){
            Picasso
                    .with(getActivity())
                    .load(Config.getInstance().getHomeTopBanner())
                    .into((ImageView) view.findViewById(R.id.mainBanner));

        }else {
            view.findViewById(R.id.mainBanner).setVisibility(View.GONE);
        }

        if(!Utils.isEmpty(Config.getInstance().getHomeBottomBanner())){
            Picasso
                    .with(getActivity())
                    .load(Config.getInstance().getHomeBottomBanner())
                    .into((ImageView) view.findViewById(R.id.bottomBanner));
        }else {
            view.findViewById(R.id.bottomBanner).setVisibility(View.GONE);
        }

        // LISTENERS

        view.findViewById(R.id.bestSellingHeader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.performQuery(Config.getInstance().getBestSellingCollection(), getResources().getString(R.string.bestSelling), Search.TYPE_TITLE);
            }
        });

        view.findViewById(R.id.dealsHeader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.performQuery(Config.getInstance().getDealsCollection(), getResources().getString(R.string.deals), Search.TYPE_TITLE);
            }
        });

        view.findViewById(R.id.ourBrandsHeader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.performQuery(Config.getInstance().getOurBrandsCollection(), getResources().getString(R.string.ourBrands), Search.TYPE_TITLE);
            }
        });

        view.findViewById(R.id.retryBestSelling).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBestSelling(view);
            }
        });

        view.findViewById(R.id.retryDeals).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeals(view);
            }
        });

        view.findViewById(R.id.retryOurBrands).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOurBrands(view);
            }
        });
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if(mainSlider != null) {
            mainSlider.stopAutoCycle();
            mainSlider = null;
        }
        if(footerSlider != null) {
            footerSlider.stopAutoCycle();
            footerSlider = null;
        }
        adapterMap = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get Deals
     * @param view
     */

    private void getDeals(final View view){

        view.findViewById(R.id.dealsProgress).setVisibility(View.VISIBLE);
        view.findViewById(R.id.retryDeals).setVisibility(View.GONE);
        view.findViewById(R.id.deals).setVisibility(View.GONE);

        getItems(PRODUCT_TYPE_DEALS, view, new Callback() {
            @Override
            public void run(final Object data) {
                Activity activity = getActivity();

                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(adapterMap == null || view == null || data == null)
                            return;

                        AnimationAdapter adapter = Utils.setItemsSlider(
                                view,
                                (ArrayList) data,
                                R.id.deals,
                                R.id.dealsProgress,
                                Main.FRAGMENT_TAG_HOME,
                                false,
                                mListener,
                                getItemsCallback(),
                                false);

                        adapterMap.put(PRODUCT_TYPE_DEALS, adapter);
                    }
                });
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /**
     * Get best selling
     * @param view
     */

    private void getBestSelling(final View view) {

        view.findViewById(R.id.bestSellingProgress).setVisibility(View.VISIBLE);
        view.findViewById(R.id.retryBestSelling).setVisibility(View.GONE);
        view.findViewById(R.id.bestSelling).setVisibility(View.GONE);

        getItems(PRODUCT_TYPE_BEST_SELLERS, view, new Callback() {
            @Override
            public void run(final Object data) {
                Activity activity = getActivity();
                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(adapterMap == null)
                            return;

                        AnimationAdapter adapter = Utils.setItemsSlider(
                                view,
                                (ArrayList) data,
                                R.id.bestSelling,
                                R.id.bestSellingProgress,
                                Main.FRAGMENT_TAG_HOME,
                                false,
                                mListener,
                                getItemsCallback(),
                                false);
                        adapterMap.put(PRODUCT_TYPE_BEST_SELLERS, adapter);
                    }
                });
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /**
     * Get our brands
     * @param view
     */

    private void getOurBrands(final View view) {

        if ( Utils.isEmpty( Config.getInstance().getOurBrandsCollection() ) )
            return;

        view.findViewById(R.id.ourBrandsHeader).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ourBrandsItems).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ourBrandsProgress).setVisibility(View.VISIBLE);
        view.findViewById(R.id.retryOurBrands).setVisibility(View.GONE);
        view.findViewById(R.id.ourBrands).setVisibility(View.GONE);

        getItems(PRODUCT_TYPE_OUR_BRANDS, view, new Callback() {
            @Override
            public void run(final Object data) {
                Activity activity = getActivity();
                if(activity == null)
                    return;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(adapterMap == null)
                            return;

                        AnimationAdapter adapter = Utils.setItemsSlider(
                                view,
                                (ArrayList) data,
                                R.id.ourBrands,
                                R.id.ourBrandsProgress,
                                Main.FRAGMENT_TAG_HOME,
                                false,
                                mListener,
                                getItemsCallback(),
                                false);
                        adapterMap.put(PRODUCT_TYPE_OUR_BRANDS, adapter);
                    }
                });
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /**
     * Get Items click listener callback
     * @return
     */

    private ProductListCallback getItemsCallback() {
        return new ProductListCallback() {
            @Override
            public void goToProduct(Product product) {
                DataHolder.getInstance().setProduct(product);
                mListener.onProductSelected(product.getId());
            }

            @Override
            public int productAdded(int position) {
                return 0;
            }

            @Override
            public int productSubtracted(int position) {
                return 0;
            }

            @Override
            public int productAdded(Sku sku) {
                int quantity = Cart.getInstance().increaseItemQuantity(sku, getActivity());
                mListener.onProductAdded();

                return quantity;
            }

            @Override
            public int productSubtracted(Sku sku) {
                int quantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                mListener.onProductAdded();

                return quantity;
            }
        };
    }


    /**
     * Get Items by type
     * @param type
     * @param view
     * @param callback
     */

    private void getItems(final int type, final View view, final Callback callback){

        if (productsCache.containsKey(type)) {
            ArrayList<Product> products = productsCache.get(type);
            if (BuildConfig.PROMOTIONS_API_ENABLED && productsHaveOutdatedPromotion(products))
                getPromotionsForProductsCache(products, callback);
            else
                callback.run(products);
            return;
        }

        API.search(getActivity(), queries.get(type), null, 1, API.SOFT_LIMIT, new okhttp3.Callback() {
            @Override
            public void onFailure(@Nonnull Call call, @Nonnull IOException e) {
                Activity activity = getActivity();

                if(activity == null || activity.isFinishing())
                    return;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(type == PRODUCT_TYPE_BEST_SELLERS){
                            view.findViewById(R.id.bestSellingProgress).setVisibility(View.GONE);
                            view.findViewById(R.id.retryBestSelling).setVisibility(View.VISIBLE);
                        }

                        if(type == PRODUCT_TYPE_DEALS){
                            view.findViewById(R.id.dealsProgress).setVisibility(View.GONE);
                            view.findViewById(R.id.retryDeals).setVisibility(View.VISIBLE);
                        }

                        if(type == PRODUCT_TYPE_OUR_BRANDS){
                            view.findViewById(R.id.ourBrandsProgress).setVisibility(View.GONE);
                            view.findViewById(R.id.retryOurBrands).setVisibility(View.VISIBLE);
                        }

                    }
                });
            }

            @Override
            public void onResponse(@Nonnull Call call, @Nonnull final Response response) throws IOException {
                ArrayList<Product> productList = API.setProductList(response.body().string());

                Activity activity = getActivity();

                if(activity == null || activity.isFinishing())
                    return;

                if(productList.size() > 0)
                    productsCache.put(type, productList);

                if(callback != null)
                    callback.run(productList);
            }
        });

    }

    /**
     * Set slider
     */
    private void setSlider(final SliderLayout slider){
        final Activity activity = getActivity();
        // To avoid flickering when slider is created and automatically moves to the next slider
        // We stop auto cycle, and enable it some time after images have been retrieved.
        slider.stopAutoCycle();
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        // Set correct size to slider
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float proportion = (float) metrics.widthPixels / (float) Config.getInstance().getSliderWidth();
        float newHeight = Config.getInstance().getSliderHeight() * proportion;
        Utils.log("Screen width: " + metrics.widthPixels);
        Utils.log("Slider height: " + newHeight);
        LinearLayout.LayoutParams sliderLayoutParams = (LinearLayout.LayoutParams) slider.getLayoutParams();
        sliderLayoutParams.height = (int) newHeight;
        API.getBanners(slider.getId() == R.id.mainSlider, new okhttp3.Callback() {
            @Override
            public void onFailure(@Nonnull Call call, @Nonnull IOException e) {
                Log.e("BANNER_ERROR", e.toString());
            }

            @Override
            public void onResponse(@Nonnull Call call, @Nonnull Response response) throws IOException {
                if (!response.isSuccessful() || response.body() == null)
                    return;
                String responseString = response.body().string();
                JSONArray banners = null;
                if (responseString.startsWith("{")) {
                    try {
                        banners = new JSONObject(responseString).getJSONArray("banners");
                    } catch (JSONException e) {
                        Log.e("HomeBanners", "Exception parsing banners JSON", e);
                    }
                } else if (responseString.startsWith("[")) {
                    try {
                        banners = new JSONArray(responseString);
                    } catch (JSONException e) {
                        Log.e("HomeBanners", "Exception parsing banners JSONArray", e);
                    }
                }
                if (banners == null)
                    return;
                final JSONArray finalBanners = banners;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        drawSlider(slider, finalBanners);
                    }
                });
            }
        });
    }

    private void drawSlider(final SliderLayout slider, final JSONArray banners) {
        final boolean isMainSlider = slider.getId() == R.id.mainSlider;
        if (isMainSlider && mainSlider == null || !isMainSlider && footerSlider == null)
            return;
        Activity activity = getActivity();
        for (int i = 0; i < banners.length(); i++) {
            try {
                JSONObject banner = banners.getJSONObject(i);
                DefaultSliderView defaultSliderView = new DefaultSliderView(activity);
                // initialize a SliderLayout
                defaultSliderView.image(banner.getString("src"));
                slider.addSlider(defaultSliderView);
                final String link = banner.optString("link");
                final String trackingId = banner.optString("trackingId");
                defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {
                        mListener.onBannerPress(trackingId);
                        try {
                            URL url = new URL(link);
                            String urlPath = url.getPath();
                            if (urlPath.endsWith("/p"))
                                mListener.onProductLinkSelected(urlPath);
                            else
                                mListener.performQuery("?" + url.getQuery(), null);
                        } catch (MalformedURLException e) {
                            Log.e("HomeFragment", "An exception was caught in slider view's OnSliderClickListener.", e);
                        }
                    }
                });
            } catch (JSONException e) {
                Log.e("HomeFragment", "An exception was caught in 'drawSlider'.", e);
            }
        }
        if (!isMainSlider && banners.length() == 0)
            return;
        slider.setVisibility(View.INVISIBLE);

        // If auto-cycle starts right away, the last slider
        // is shown for a brief time. To avoid that, the slider
        // is "hidden", "shown" some time later, and then
        // the auto-cycle is started
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Checking for null as view may have been destroyed already!
                if (isMainSlider && mainSlider != null || !isMainSlider && footerSlider != null) {
                    slider.setVisibility(View.VISIBLE);
                    slider.startAutoCycle(SLIDER_DELAY_AUTO_CYCLE_MILLIS, SLIDER_DURATION_MILLIS, true);
                }
            }
        }, SLIDER_VISIBILITY_DELAY_MILLIS);
    }

    @Override
    public void refresh(){

        View view = getView();

        if(view != null) {

            productsCache.clear();

            //Deals
            getDeals(view);

            //Best sellers
            getBestSelling(view);

            //Our brands
            getOurBrands(view);
        }
    }

    @Override
    public void cartUpdated(){

        if(adapterMap != null) {

            for (final AnimationAdapter value : adapterMap.values()) {

                if(value != null) {
                    // Disable animation to avoid flickering caused by double animation
                    value.setFirstOnly(true);
                    value.notifyDataSetChanged();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                // Enable animation
                                value.setFirstOnly(false);
                            }catch(Exception ignore) {}
                        }
                    }, 505);
                }
            }
        }
    }

    private boolean productsHaveOutdatedPromotion(ArrayList<Product> products) {
        for (Product product : products)
            if (product.hasOutdatedPromotion())
                return true;
        return false;
    }

    private void getPromotionsForProductsCache(final ArrayList<Product> products, final Callback callback) {
        PromotionsService.getInstance(getActivity()).getPromotions(new ApiCallback<List<Promotion>>() {
            @Override
            public void onResponse(List<Promotion> object) {
                for (Product product : products)
                    PromotionsHandler.getInstance().setPromotionToProduct(product);
                callback.run(products);
            }

            @Override
            public void onError(String errorMessage) {
                callback.run(products);
            }

            @Override
            public void onUnauthorized() {
                callback.run(products);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.PROMOTIONS_API_ENABLED && PromotionsService.getInstance(getActivity()).promotionsAreOutdated())
            refresh();
    }
}
