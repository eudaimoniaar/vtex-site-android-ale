package com.fizzmod.vtex.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CouponsAdapter;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.CouponsSelectionListener;
import com.fizzmod.vtex.models.Coupon;
import com.fizzmod.vtex.service.CouponsService;
import com.fizzmod.vtex.service.response.CIResponse;
import com.fizzmod.vtex.service.response.CouponToggleResponse;
import com.fizzmod.vtex.utils.CouponsSharedPreferences;
import com.fizzmod.vtex.views.CouponsTermsAlertDialog;
import com.fizzmod.vtex.views.CustomAlertDialog;

import java.util.ArrayList;
import java.util.List;

public class CouponsSelection extends BaseCouponsFragment implements CouponsSelectionListener {

    private RecyclerView couponGrid;
    private CustomAlertDialog confirmPhysicalPrintDialog;

    private CouponsAdapter couponsAdapter;

    public CouponsSelection() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupons_recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        couponGrid = view.findViewById(R.id.coupons_grid);

        confirmPhysicalPrintDialog = new CustomAlertDialog(getActivity());
        confirmPhysicalPrintDialog.setTxtAccept(R.string.accept, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLoadingStarted();
                confirmPhysicalPrintDialog.dismiss();
                String clientNumber = CouponsSharedPreferences.getInstance(getActivity()).getCard();
                CouponsService.getInstance(getActivity()).togglePhysicalPrint(clientNumber, !couponsAdapter.getPhysicalPrintEnabled(), new ApiCallback<CouponToggleResponse>() {
                    @Override
                    public void onResponse(CouponToggleResponse object) {
                        couponsAdapter.setPhysicalPrintEnabled(!couponsAdapter.getPhysicalPrintEnabled());
                        listener.onLoadingFinished();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        listener.onLoadingFinished();
                    }

                    @Override
                    public void onUnauthorized() {
                        listener.onLoadingFinished();
                    }
                });
            }
        });
        confirmPhysicalPrintDialog.setTxtCancel(R.string.cancel);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Changed due to card selection skip
        if (CouponsSharedPreferences.getInstance(getActivity()).hasCard()) {
            getCoupons();
        } else {
            getCards(CouponsSharedPreferences.getInstance(getActivity()).getCI());
        }
    }

    private void showNoCouponMessage() {
        setCoupons(new ArrayList<Coupon>());
        listener.onLoadingFinished();
    }

    /****** Api Calls ******/

    // Added due to card selection skip
    private void getCards(String ci) {
        CouponsService.getInstance(getActivity()).getCards(ci, new ApiCallback<List<CIResponse>>() {
            @Override
            public void onUnauthorized() {
                showNoCouponMessage();
            }

            @Override
            public void onResponse(List<CIResponse> cards) {
                if (cards.size() > 0) {
                    CouponsSharedPreferences.getInstance(getActivity()).saveCard(cards.get(0).getClientNumber());
                    getCoupons();
                } else {
                    showNoCouponMessage();
                }
            }

            @Override
            public void onError(String errorMessage) {
                showNoCouponMessage();
            }
        });
    }

    private void getCoupons() {
        CouponsService.getInstance(getActivity()).getCoupons(CouponsSharedPreferences.getInstance(getActivity()).getCard(), new ApiCallback<List<Coupon>>() {
            @Override
            public void onResponse(List<Coupon> coupons) {
                setCoupons(coupons);
                listener.onLoadingFinished();
            }

            @Override
            public void onError(String errorMessage) {
                showNoCouponMessage();
            }

            @Override
            public void onUnauthorized() {
                showNoCouponMessage();
            }
        });
    }

    public void onTermsDialogClicked() {
        new CouponsTermsAlertDialog(getActivity()).show();
    }

    public void onCouponButtonClicked(final String couponCode, final int position, final List<Coupon> coupons) {
        CouponsService.getInstance(getActivity()).toggleCoupon(couponCode, !coupons.get(position).getEnabled(), new ApiCallback<CouponToggleResponse>() {
            @Override
            public void onResponse(CouponToggleResponse object) {
                coupons.get(position).setEnabled(!coupons.get(position).getEnabled());
                listener.onShowCartel(coupons.get(position).getEnabled() ? R.string.coupons_cartel_activated : R.string.coupons_cartel_deactivated);
                couponsAdapter.notifyCouponActivated(position);
            }

            @Override
            public void onError(String errorMessage) {
            }

            @Override
            public void onUnauthorized() {
            }
        });
    }

    public void onInstructionsClicked() {
        listener.onInstructionsClicked();
    }

    public void onPhysicalPrintClicked() {
        int messageId = !couponsAdapter.getPhysicalPrintEnabled() ? R.string.coupons_enable_physical_print : R.string.coupons_disable_physical_print;
        confirmPhysicalPrintDialog.setTxtMessage(messageId);
        confirmPhysicalPrintDialog.show();
    }

    /****** Data Setters ******/

    private void setCoupons(List<Coupon> coupons) {
        if (couponsAdapter != null) {
            couponsAdapter.updateCoupons(coupons);
            return;
        }

        couponsAdapter = new CouponsAdapter(coupons, this);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // By getSpanSize doc, if you return 2, it occupies 2 grid spaces, therefore, full size
                if (position < CouponsAdapter.HEADERS_COUNT) {
                    // And here you want the header(pos 0) and the status header(pos 1) to be fullsize
                    return 2;
                }
                return 1;
            }
        });
        couponGrid.setLayoutManager(layoutManager);

        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration();
        couponGrid.addItemDecoration(decoration);

        couponGrid.setAdapter(couponsAdapter);
    }

    public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            //This function gets each view by position and decorates their margin,
            // so that you can have different margins for top, right, left, bottom of the view
            if (parent.getChildAdapterPosition(view) > 1) { //normal coupons vertical and horizontal spacing
                outRect.left = (int) getActivity().getResources().getDimension(R.dimen.coupon_lateral_offset);
                outRect.right = (int) getActivity().getResources().getDimension(R.dimen.coupon_lateral_offset);
                outRect.bottom = (int) getActivity().getResources().getDimension(R.dimen.coupon_vertical_spacing);
            } else if (parent.getChildAdapterPosition(view) == 1) //0 is header, 1 is for status header
                outRect.bottom = (int) getActivity().getResources().getDimension(R.dimen.coupon_vertical_spacing);
            //so that the divider in the status header is not stuck to the coupons
        }
    }

}