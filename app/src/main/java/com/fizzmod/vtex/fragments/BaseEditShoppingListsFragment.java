package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.EditShoppingListsAdapter;
import com.fizzmod.vtex.interfaces.EditShoppingListsListener;
import com.fizzmod.vtex.interfaces.ShoppingListQuantityChangeListener;
import com.fizzmod.vtex.models.ShoppingList;

import java.util.List;

public class BaseEditShoppingListsFragment extends Fragment implements ShoppingListQuantityChangeListener {

    private List<ShoppingList> shoppingLists;
    private EditShoppingListsListener listener;
    private Button saveButton;
    private String selectedSku;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_shopping_lists, container, false);

        RecyclerView recyclerView =
                (RecyclerView) view.findViewById(R.id.select_list_recycler_view);
        recyclerView.setLayoutManager( new LinearLayoutManager( getContext() ) );
        final EditShoppingListsAdapter adapter = new EditShoppingListsAdapter(
                getContext(),
                this,
                shoppingLists,
                selectedSku);
        recyclerView.setAdapter(adapter);

        saveButton = (Button) view.findViewById(R.id.select_list_save_button);
        saveButton.setEnabled(false);

        view.findViewById(R.id.select_list_close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCloseEditShoppingLists();
            }
        });

        view.findViewById(R.id.select_list_create_new_list_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNewList();
            }
        });

        view.findViewById(R.id.select_list_save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSaveLists(adapter.getModifications());
            }
        });

        return view;
    }

    public void setListener(EditShoppingListsListener listener){
        this.listener = listener;
    }

    public void setShoppingLists(List<ShoppingList> shoppingLists) {
        this.shoppingLists = shoppingLists;
    }

    public void setSelectedSku(String selectedSku) {
        this.selectedSku = selectedSku;
    }

    /** ShoppingListQuantityChangeListener */

    @Override
    public void onCountChanged() {
        if (!saveButton.isEnabled())
            saveButton.setEnabled(true);
    }
}
