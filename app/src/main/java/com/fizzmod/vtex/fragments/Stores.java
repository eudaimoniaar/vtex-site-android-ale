/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ExtendedCallback;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

public class Stores extends BackHandledFragment implements OnMapReadyCallback {

    public final static int MY_PERMISSIONS_LOCATION = 1;
    public final static double LATITUDE = Config.getInstance().getLatitude();
    public final static double LONGITUDE = Config.getInstance().getLongitude();

    private static ArrayList<Store> stores = new ArrayList<>();
    private boolean storesInitialized = false;

    TextView storeAddress;
    TextView storePhone;
    TextView storeName;
    TextView storeSchedule;
    LinearLayout storeInfo;

    private MapView mapView;
    private GoogleMap map;
    private HashMap<Marker, Store> markers = new HashMap<Marker, Store>();

    public Stores() {}

    public static Stores newInstance() {
        return new Stores();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {

        if (container == null)
            return null;

        final View v = inflater.inflate(R.layout.fragment_stores, container, false);

        mListener.onFragmentStart(Main.FRAGMENT_TAG_STORES);
        mListener.onLoadingStart();

        storeInfo = (LinearLayout) v.findViewById(R.id.storeInfo);
        storeAddress = (TextView) v.findViewById(R.id.storeAddress);
        storePhone = (TextView) v.findViewById(R.id.storePhone);
        storeName = (TextView) v.findViewById(R.id.storeName);
        storeSchedule = (TextView) v.findViewById(R.id.storeSchedule);

        markers = new HashMap<>();
        stores = new ArrayList<>();

        getStores();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mapView = (MapView) v.findViewById(R.id.storesMap);
                mapView.onCreate(savedInstanceState);

                mListener.onLoadingStop();
                // Gets to GoogleMap from the MapView and does initialization stuff
                mapView.getMapAsync(Stores.this);
            }
        }, 500);

        return v;
    }

    private void getStores() {
        if (stores != null && stores.size() != 0)
            return;
        Store.getStores(getActivity(), new ExtendedCallback() {

            @Override
            public void error(Object error) {
                // Add retry
            }

            @Override
            public void run(Object storeList) {
                stores = (ArrayList<Store>) storeList;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setStores();
                    }
                });
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /**
     * Set store markers
     */
    private void setStores() {
        if (map == null || storesInitialized || stores == null || stores.size() == 0)
            return;
        storesInitialized = true;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Store store : stores) {
            LatLng position = new LatLng(store.getLatitude(), store.getLongitude());
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
            );
            markers.put(marker, store);
            builder.include(position);
        }
        LatLngBounds latLngBounds = builder.build();
        CameraUpdate cameraUpdate = stores.size() == 1 ?
                CameraUpdateFactory.newLatLngZoom(latLngBounds.getCenter(), 17) :
                CameraUpdateFactory.newLatLngBounds(latLngBounds,30);  // offset from edges of the map in pixels
        map.animateCamera(cameraUpdate);
    }

    @Override
    public void onResume() {
        if (mapView != null)
            mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (map != null) {
            map.clear();
            map = null;
        }

        if (mapView != null) {
            mapView.onDestroy();
            mapView = null;
        }

        // Null views
        storeAddress = null;
        storePhone = null;
        storeName = null;
        storeSchedule = null;
        storeInfo = null;

        markers = null;
        storesInitialized = false;
        stores = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        mapView.onResume();
        //Check permissions
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            //Requests permission
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_LOCATION);

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        // MapsInitializer.initialize(this.getActivity());

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Store store = markers.get(marker);
                try {
                    storeAddress.setText(store.getAddress());
                    storePhone.setText(store.getPhone());
                    storeName.setText(store.getName());
                    storeSchedule.setText(store.getSchedule());
                    if (storeInfo.getVisibility() == View.GONE)
                        Utils.fadeIn(storeInfo);
                } catch (NullPointerException e) {}
                return false;
            }
        });

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(LATITUDE, LONGITUDE), 12);
        map.animateCamera(cameraUpdate);
        setStores();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    map.setMyLocationEnabled(true);
                    map.getUiSettings().setMyLocationButtonEnabled(true);
                }
                return;
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
