package com.fizzmod.vtex.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.CreateShoppingListListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseCreateShoppingListFragment extends Fragment {

    private CreateShoppingListListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_shopping_list, container, false);

        final EditText editText = (EditText) view.findViewById(R.id.create_list_edit_text);
        editText.requestFocus();

        view.findViewById(R.id.create_list_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().isEmpty())
                    editText.setError(getString(R.string.create_list_empty_name));
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null)
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    listener.onCreateList(editText.getText().toString());
                }
            }
        });

        view.findViewById(R.id.create_list_close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCloseCreateList();
            }
        });

        view.findViewById(R.id.create_list_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    public void setListener(CreateShoppingListListener listener){
        this.listener = listener;
    }
}
