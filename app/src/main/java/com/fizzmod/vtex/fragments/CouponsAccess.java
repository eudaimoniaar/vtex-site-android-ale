package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.utils.CouponsSharedPreferences;

public class CouponsAccess extends BaseCouponsFragment {

    private EditText couponEditCI;
    private TextView couponInvalid;
    private Button couponInputButton;

    public CouponsAccess() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupons_access, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        couponInvalid = view.findViewById(R.id.coupons_invalid_ci_text);

        couponInputButton = view.findViewById(R.id.coupons_input_ci_button);
        couponInputButton.setEnabled(false);
        couponInputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissKeyboard();
                CouponsSharedPreferences.getInstance(getActivity()).saveCI(couponEditCI.getText().toString());
                listener.onLoadingStarted();
                listener.onCISelected();
            }
        });

        couponEditCI = view.findViewById(R.id.coupons_edit_ci);
        couponEditCI.requestFocus();
        couponEditCI.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                allowCIButtonClick(isValidCi(s.toString()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        couponEditCI.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String ci = couponEditCI.getText().toString();
                if (actionId == EditorInfo.IME_ACTION_DONE && isValidCi(ci)) {
                    dismissKeyboard();
                    CouponsSharedPreferences.getInstance(getActivity()).saveCI(ci);
                    listener.onLoadingStarted();
                    listener.onCISelected();
                    return false;
                }
                return true;
            }
        });

        listener.onLoadingFinished();
    }

    /****** Ui Methods ******/

    private void allowCIButtonClick(boolean isValid) {
        couponEditCI.setActivated(!isValid);
        couponEditCI.setCompoundDrawablesWithIntrinsicBounds(0, 0, isValid ? R.drawable.icn_check_green : 0, 0);
        couponInputButton.setEnabled(isValid);
        couponInvalid.setVisibility(isValid ? View.GONE : View.VISIBLE);
    }

    /****** Private Methods ******/

    private void dismissKeyboard() {
        Activity act = getActivity();
        if (act.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), 0);
        }
    }

    private boolean isValidCi(String ci) {
        //TODO for now dont match digit or letter and only by length of 7 or 8
        //ci.matches("([0-9]{8})|([A-Z]{1}[0-9]{6})")
        return ci.matches("\\w{7,8}");
    }

}
