package com.fizzmod.vtex.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.CouponCardsAdapter;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.CouponsCardsListener;
import com.fizzmod.vtex.service.CouponsService;
import com.fizzmod.vtex.service.response.CIResponse;
import com.fizzmod.vtex.utils.CouponsSharedPreferences;
import com.fizzmod.vtex.views.CustomAlertDialog;

import java.util.ArrayList;
import java.util.List;

/*
* TODO card select screen deprecated, to remove or re implement in the future
* As of 9/11 of year 2019 It was asked to deprecate the card selection
* From now on, the api should only send 1 card and you have to select it automatically, bypassing this screen
*/

public class CouponsCards extends BaseCouponsFragment implements CouponsCardsListener {

    private RecyclerView recyclerView;
    private CustomAlertDialog confirmCardDialog;

    public CouponsCards() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupons_recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.coupons_grid);

        confirmCardDialog = new CustomAlertDialog(getActivity());
        confirmCardDialog.setTxtCancel(R.string.cancel);
        confirmCardDialog.setTxtMessage(R.string.coupons_select_card_dialog);

        getCards(CouponsSharedPreferences.getInstance(getActivity()).getCI());
    }

    /****** Api Calls ******/

    private void getCards(String ci) {
        CouponsService.getInstance(getActivity()).getCards(ci, new ApiCallback<List<CIResponse>>() {
            @Override
            public void onUnauthorized() {
                listener.onLoadingFinished();
            }

            @Override
            public void onResponse(List<CIResponse> object) {
                setCards(object);
                listener.onLoadingFinished();
            }

            @Override
            public void onError(String errorMessage) {
                setCards(new ArrayList<CIResponse>());
                listener.onLoadingFinished();
            }
        });
    }

    /****** Data Setters ******/

    private void setCards(List<CIResponse> cards) {
        CouponCardsAdapter cardsAdapter = new CouponCardsAdapter(cards, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        VerticalSpaceItemDecoration decoration = new VerticalSpaceItemDecoration();

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(decoration);
        recyclerView.setAdapter(cardsAdapter);
    }

    private class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            if (parent.getChildAdapterPosition(view) > 1)
                outRect.bottom = (int) getActivity().getResources().getDimension(R.dimen.coupon_card_vertical_spacing);
        }
    }

    public void onCardClicked(final CIResponse card) {
        confirmCardDialog.setTxtAccept(R.string.accept, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CouponsSharedPreferences.getInstance(getActivity()).saveCard(card.getClientNumber());
                confirmCardDialog.dismiss();
                listener.onLoadingStarted();
                listener.onCardSelected();
            }
        });
        confirmCardDialog.show();
    }

}
