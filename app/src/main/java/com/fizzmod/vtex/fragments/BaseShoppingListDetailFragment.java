package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.ShoppingListProductsAdapter;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OptionsHeaderListener;
import com.fizzmod.vtex.interfaces.ShoppingListAdapterInterface;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.service.response.BaseResponse;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.fizzmod.vtex.views.CartCartelMessage;
import com.fizzmod.vtex.views.CustomAlertDialog;
import com.fizzmod.vtex.views.OptionsHeaderLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseShoppingListDetailFragment extends BackHandledFragment implements
        ShoppingListAdapterInterface,
        OptionsHeaderListener {

    private final List<Product> products = new ArrayList<>();
    private ShoppingList shoppingList;

    private ShoppingListProductsAdapter adapter;

    private OptionsHeaderLayout optionsLayout;
    private CartCartelMessage cartelMessage;
    private LinearLayout buyLayout;
    private RelativeLayout progressLayout;
    private TextView txtListPrice;
    private TextView txtProductsCount;
    private CustomAlertDialog confirmDeleteDialog;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        if (!User.isLogged(getActivity())) {
            mListener.closeFragment();
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_shopping_list_detail, container, false);

        progressLayout = (RelativeLayout) view.findViewById(R.id.progress);
        cartelMessage = (CartCartelMessage) view.findViewById(R.id.shopping_list_detail_cartel_message);
        buyLayout = (LinearLayout) view.findViewById(R.id.shopping_list_detail_buy_layout);

        optionsLayout = (OptionsHeaderLayout) view.findViewById(R.id.shopping_list_detail_options_layout);
        optionsLayout.setTitle(shoppingList.getName());
        optionsLayout.setListener(this);
        optionsLayout.setBackgroundColor(getResources().getColor(getBackgroundColor()));

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.shopping_list_detail_list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new ShoppingListProductsAdapter(view.getContext());
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);

        txtListPrice = (TextView) view.findViewById(R.id.shopping_list_detail_total_price);
        txtProductsCount = (TextView) view.findViewById(R.id.shopping_list_detail_products_count);
        setTotalCount();

        view.findViewById(R.id.shopping_list_detail_buy_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter.getProductList().isEmpty() || cartelMessage.isShown())
                    return;

                Cart cart = Cart.getInstance();
                Activity activity = getActivity();
                for (String sku : shoppingList.getSkusList())
                    cart.addItemQuantity(getSku(sku), activity, shoppingList.getProductQuantity(sku));
                onProductAdded(false);
            }
        });

        confirmDeleteDialog = new CustomAlertDialog(getActivity());
        confirmDeleteDialog.setTxtAccept(R.string.delete, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteSelectedProducts();
            }
        });
        confirmDeleteDialog.setTxtCancel(R.string.cancel);

        fetchShoppingList();

        return view;
    }

    public int getImageResource() {
        return R.drawable.icon_list_blue;
    }

    public int getBackgroundColor() {
        return R.color.white;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mListener != null)
            mListener = null;
    }

    @Override
    public boolean onBackPressed() {

        boolean isEditing = adapter.isEditing();
        if (isEditing)
            exitEditMode();

        return isEditing || progressLayout.getVisibility() == VISIBLE;
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    public void setProductList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }

    /*******************************
     * ShoppingListAdapterListener *
     *******************************/

    @Override
    public void onLongClick() {
        // TODO: When entering edit mode, "buyLayout" is eating part of the scroller
        optionsLayout.toggleMenu();
        buyLayout.setVisibility(GONE);
    }

    @Override
    public void onClick(Product product) {
        DataHolder.getInstance().setProduct(product);
        mListener.onProductSelected(product.getId());
    }

    /*************************
     * OptionsHeaderListener *
     *************************/

    @Override
    public void onBack() {
        optionsLayout.toggleMenu();
        adapter.exitEditMode();
        buyLayout.setVisibility(VISIBLE);
    }

    @Override
    public void onSelectAll() {
        adapter.toggleSelectAllProducts();
    }

    @Override
    public void onDeleteSelected() {
        int selectedCount = adapter.getSelectedProducts().size();
        if (selectedCount == 0)
            return;

        confirmDeleteDialog.setTxtMessage(selectedCount == 1 ?
                R.string.delete_single_shopping_list_product_alert :
                R.string.delete_many_shopping_list_products_alert);
        confirmDeleteDialog.show();
    }

    @Override
    public void onAddSelectedToCart() {
        List<Product> selectedProducts = adapter.getSelectedProducts();
        if (selectedProducts.isEmpty() || cartelMessage.isShown())
            return;

        HashMap<String, Integer> skusMap = new HashMap<>();

        for (Product p : adapter.getSelectedProducts()) {
            Sku sku = p.getSku(0);
            Integer quantity = sku.getSelectedQuantity();
            if (skusMap.containsKey(sku.getId()))
                quantity += skusMap.get(sku.getId());

            skusMap.put(sku.getId(), quantity);
        }

        Cart cart = Cart.getInstance();
        Activity activity = getActivity();
        for (Map.Entry<String, Integer> entry : skusMap.entrySet())
            cart.addItemQuantity(getSku(entry.getKey()), activity, entry.getValue());

        onProductAdded(true);
    }

    /*******************
     * Private methods *
     *******************/

    private void fetchShoppingList() {
        final Activity activity = getActivity();
        fetchShoppingList(new Callback<ShoppingList>(activity) {
            @Override
            public void onResponse(ShoppingList other) {
                shoppingList.setSkus(other);
                fetchAllProducts();
            }

            @Override
            public void onError(String errorMessage) {
                showErrorToast(errorMessage);
                activity.onBackPressed();
            }

            @Override
            protected void retry() {
                fetchShoppingList(this);
            }
        });
    }

    private void fetchShoppingList(Callback<ShoppingList> callback) {
        JanisService.getInstance(getActivity()).getShoppingListSkus(shoppingList.getId(), callback);
    }

    private void fetchAllProducts() {

        List<String> skus = new ArrayList<>();
        for (String sku : shoppingList.getSkusList())
            if (!skus.contains(sku))
                skus.add(sku);

        if (skus.isEmpty()) {
            hideProgress();
            setTotalPrice();
            return;
        }

        API.getProductsBySku(getActivity(), skus, new ApiCallback<List<Product>>() {
            @Override
            public void onResponse(List<Product> retrievedProducts) {
                for (Product p : retrievedProducts) {
                    Sku sku = p.getSku(0);
                    sku.setSelectedQuantity(shoppingList.getProductQuantity(sku.getId()));
                }
                products.clear();
                products.addAll(retrievedProducts);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setProductList(products);
                        setTotalPrice();
                        setTotalCount();
                        hideProgress();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                showErrorToast(getString(R.string.errorOccurred));
            }

            @Override
            public void onUnauthorized() {
                onError(getString(R.string.errorOccurred));
            }
        });
    }

    private void setTotalPrice() {

        float totalPrice = 0;

        for (Product p : products) {
            Sku sku = p.getSku(0);
            totalPrice += sku.getBestPrice() * sku.getSelectedQuantity();
        }

        txtListPrice.setText(Config.getInstance().formatPrice(totalPrice));
    }

    private void setTotalCount() {
        txtProductsCount.setText(getString(R.string.shopping_list_total_count, adapter.getItemCount()));
    }

    private void hideProgress() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressLayout.setVisibility(GONE);
            }
        });
    }

    private void showProgress() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressLayout.setVisibility(VISIBLE);
            }
        });
    }

    private void showErrorToast(final String errorMessage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgress();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void exitEditMode() {
        adapter.exitEditMode();
        optionsLayout.toggleMenu();
        buyLayout.setVisibility(VISIBLE);
    }

    private void onProductAdded(boolean addedSelected) {
        mListener.onProductAdded();

        int textResId = R.string.list_added;
        if (addedSelected)
            textResId = R.string.products_added;

        cartelMessage.setText(textResId);
        cartelMessage.display();
    }

    private Sku getSku(String id) {

        for (Product p : products) {
            Sku sku = p.getSku(0);
            if (sku.getId().equals(id))
                return sku;
        }

        return null;
    }

    private void deleteSelectedProducts() {
        showProgress();
        confirmDeleteDialog.dismiss();

        final List<String> skusToRemove = new ArrayList<>();
        for (Product p : adapter.getSelectedProducts())
            skusToRemove.add(p.getSku(0).getId());

        deleteSelectedProducts(skusToRemove, new Callback<BaseResponse>(getActivity()) {
            @Override
            public void onResponse(BaseResponse response) {
                shoppingList.deleteProducts(skusToRemove);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeSelectedProduct();
                        setTotalPrice();
                        setTotalCount();
                    }
                });
                hideProgress();
            }

            @Override
            protected void retry() {
                deleteSelectedProducts(skusToRemove, this);
            }
        });
    }

    private void deleteSelectedProducts(List<String> skusToRemove, Callback<BaseResponse> callback) {
        JanisService.getInstance(getActivity())
                .removeSkusFromList(shoppingList.getId(), skusToRemove, callback);
    }

    /********************
     * Private callback *
     ********************/

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        public void onError(String errorMessage) {
            showErrorToast(errorMessage);
        }

        @Override
        protected void requestSignIn() {
            mListener.signOut();
            mListener.requestSignIn(Main.FRAGMENT_TAG_LISTS);
        }

    }

}
