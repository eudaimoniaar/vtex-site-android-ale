package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.view.View;

public abstract class BackHandledFragment extends BaseFragment {

    protected BackHandlerInterface backHandlerInterface;
//    public abstract String getTagText();

    /**
     * @return true if the back pressed event was already consumed; false otherwise.
     * */
    public abstract boolean onBackPressed();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!(getActivity()  instanceof BackHandlerInterface))
            throw new ClassCastException("Hosting activity must implement BackHandlerInterface");
        else
            backHandlerInterface = (BackHandlerInterface) getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();

        // Mark this fragment as the selected Fragment.
        backHandlerInterface.setSelectedFragment(this);
    }

    public void backFromSignIn() {}

    /**
     * This method will be called when the fragment needs to be refreshed. By default it does nothing.
     * ie: sales channel change
     */
    public void refresh() {}

    /**
     * This method will be called when the cart has been updated
     */
    public void cartUpdated() {}

    /**
     * This method will be called when the user signs/is signed out.
     * */
    public boolean requiresLoggedUser() {
        return false;
    }

    public interface BackHandlerInterface {

        void setSelectedFragment(BackHandledFragment backHandledFragment);
        void addScrollListener(View view);
        void onViewScrollUp();

    }
}