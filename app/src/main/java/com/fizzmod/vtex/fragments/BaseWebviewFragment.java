package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.utils.MySSLErrorHandler;
import com.fizzmod.vtex.utils.Utils;

import java.lang.reflect.Field;

/**
 * Created by marcos on 30/04/17.
 */
public class BaseWebviewFragment extends BackHandledFragment {

    protected String URL = "";
    protected WebView webView;
    protected Object javascriptInterface = null;

    protected MySSLErrorHandler sslErrorHandler;

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.webview_progressbar_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (webView != null) {
            webView.setTag(null);
            webView.removeAllViews();
            webView.loadUrl("about:blank"); //To reliably reset the view state and release page resources
            webView.clearHistory();
            webView.destroy();
            webView = null;
        }

        if (sslErrorHandler != null) {
            sslErrorHandler.destroy();
            sslErrorHandler = null;
        }

        if (javascriptInterface != null) {
            javascriptInterface = null;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;

        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public Object getJavascriptInterface() {
        return javascriptInterface;
    }

    public void setJavascriptInterface(Object javascriptInterface) {
        this.javascriptInterface = javascriptInterface;
    }

    /**
     * Set Activity UI
     */

    public void setUI(final View view){

        sslErrorHandler = new MySSLErrorHandler(getActivity());
        webView = (WebView) view.findViewById(R.id.webView);
        backHandlerInterface.addScrollListener(webView);

        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.webViewWrapper);
        final RelativeLayout loading = (RelativeLayout) view.findViewById(R.id.loading);
        final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        final TextView progressText = (TextView) view.findViewById(R.id.progressText);

        loading.setVisibility(View.VISIBLE);
        progress.setProgress(0);
        progressText.setText("0%");

        swipeLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorSecondary);

        if (Build.VERSION.SDK_INT >= 19) {
            // chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                progress.setProgress(newProgress);
                progressText.setText(newProgress + "%");

            }
        });

        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                swipeLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);

                String script = Utils.loadAssetTextAsString(getActivity(), "base-webview.js");
                if (script != null)
                    view.loadUrl("javascript:" + script);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.matches("/^https?:///")) {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public void onReceivedSslError (WebView view, final SslErrorHandler handler, SslError error) {

                if(sslErrorHandler != null) {

                    sslErrorHandler.handle(view, handler, error, new Callback() {
                        @Override
                        public void run(Object data) {

                            if(data != null) {

                                if((Integer) data == MySSLErrorHandler.SSL_UPDATE_WEBVIEW) {
                                    Utils.openGooglePlay(getActivity(), "com.google.android.webview");
                                    if(mListener != null)
                                        mListener.closeFragment();
                                }

                                if((Integer) data == MySSLErrorHandler.SSL_HANDLER_CANCEL) {

                                    if(mListener != null)
                                        mListener.closeFragment();
                                }

                            }
                        }

                        @Override
                        public void run(Object data, Object data2) {

                        }
                    });
                }

            }

        });

        if(this.javascriptInterface != null)
            webView.addJavascriptInterface(this.javascriptInterface, "BHAndroid");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        webView.loadUrl(URL);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });
    }
}
