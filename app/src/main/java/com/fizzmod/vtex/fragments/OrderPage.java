/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Order;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by marcos on 05/07/16.
 */
public class OrderPage extends BackHandledFragment {

    public final static String ARG_ORDER_ID = "o";
    private Order order;
    private String orderId;
    private int apiRetries = 0;
    private AnimationAdapter itemsAdapter;

    public OrderPage() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Favourites.
     */

    public static OrderPage newInstance(String id) {

        OrderPage fragment =  new OrderPage();

        if(id != null) {
            Bundle args = new Bundle();
            args.putString(ARG_ORDER_ID, id);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderId = getArguments().getString(ARG_ORDER_ID, null);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_page, container, false);

        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);

        if (!User.isLogged(getActivity())) {
            mListener.closeFragment();
            return;
        }

        mListener.onFragmentStart(Main.FRAGMENT_TAG_ORDERS);
        order = DataHolder.getInstance().getOrder();

        if(order != null)
            setMainData(view);
        else
            order = new Order();

        order.userEmail = User.getInstance(getActivity()).getEmail();

        view.findViewById(R.id.reorder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reorder();
            }
        });

        getOrder(view);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    public void setStatus(View view) {

        try {

            Activity activity = getActivity();

            View greenBullet =  view.findViewById(R.id.order_status_billed);
            View redBullet =  view.findViewById(R.id.order_status_cancelled);
            View yellowBullet =  view.findViewById(R.id.order_status_in_progress);

            int bulletHeight = (int) getResources().getDimension(R.dimen.orderPageBulletHeight);
            int bulletWidth = (int) getResources().getDimension(R.dimen.orderPageBulletWidth);
            int bulletSizeOn = (int) getResources().getDimension(R.dimen.orderPageBulletOn);

            int statusColor = order.getStatusColor();
            greenBullet.setSelected(statusColor == Order.STATUS_BILLED);
            redBullet.setSelected(statusColor == Order.STATUS_CANCELLED);
            yellowBullet.setSelected(statusColor == Order.STATUS_IN_PROGRESS);

            if(statusColor == Order.STATUS_BILLED)
                ((TextView) view.findViewById(R.id.orderStatus)).setTextColor(ContextCompat.getColor(activity, R.color.billedBullet));
            else if(statusColor == Order.STATUS_IN_PROGRESS)
                ((TextView) view.findViewById(R.id.orderStatus)).setTextColor(ContextCompat.getColor(activity, R.color.inProgressBullet));
            else if(statusColor == Order.STATUS_CANCELLED)
                ((TextView) view.findViewById(R.id.orderStatus)).setTextColor(ContextCompat.getColor(activity, R.color.cancelledBullet));

            LinearLayout.LayoutParams paramsGreen = (LinearLayout.LayoutParams) greenBullet.getLayoutParams();
            paramsGreen.height =  statusColor == Order.STATUS_BILLED ? bulletSizeOn : bulletHeight;
            paramsGreen.width =  statusColor == Order.STATUS_BILLED ? bulletSizeOn : bulletWidth;
            greenBullet.setLayoutParams(paramsGreen);

            LinearLayout.LayoutParams paramsRed = (LinearLayout.LayoutParams) redBullet.getLayoutParams();
            paramsRed.height =  statusColor == Order.STATUS_CANCELLED ? bulletSizeOn : bulletHeight;
            paramsRed.width =  statusColor == Order.STATUS_CANCELLED ? bulletSizeOn : bulletWidth;
            redBullet.setLayoutParams(paramsRed);

            LinearLayout.LayoutParams paramsYellow = (LinearLayout.LayoutParams) yellowBullet.getLayoutParams();
            paramsYellow.height =  statusColor == Order.STATUS_IN_PROGRESS ? bulletSizeOn : bulletHeight;
            paramsYellow.width =  statusColor == Order.STATUS_IN_PROGRESS ? bulletSizeOn : bulletWidth;
            yellowBullet.setLayoutParams(paramsYellow);

        } catch (Exception e) {}

    }

    public boolean getOrder(final View view) {

        if (mListener == null)
            return false;

        mListener.onLoadingStart();

        User user = User.getInstance(getActivity());

        API.getOrder(user, orderId, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                retryGetOrder(view);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (!response.isSuccessful() && response.code() == 401) {
                    mListener.requestSignIn(Main.FRAGMENT_TAG_ORDERS);
                    return;
                }

                JSONObject JSONResponse;
                JSONObject orderData = null;

                Activity activity = getActivity();

                try {
                    JSONResponse = new JSONObject(response.body().string());

                    orderData = JSONResponse.getJSONObject("order");
                    JSONObject clientProfileData  = orderData.optJSONObject("clientProfileData");

                    if(clientProfileData != null){
                        order.userName = clientProfileData.optString("firstName") + " " + clientProfileData.optString("lastName");
                        order.userPhone = clientProfileData.optString("phone");
                    }

                    setTotals(orderData.getJSONArray("totals"));
                    setShipping(orderData.optJSONObject("shippingData"));
                    setPayment(orderData.optJSONObject("paymentData"));
                    setNote(orderData.optJSONObject("openTextField"));

                    order.id = orderData.getString("orderId");
                    order.sequence = orderData.optString("sequence");
                    order.date = orderData.optString("creationDate");
                    order.status = orderData.optString("status");
                    order.total = orderData.getDouble("value") / 100;

                } catch (JSONException e) {
                    e.printStackTrace();
                    // TODO handle error
                    if(mListener != null) {
                        mListener.closeFragment();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final ArrayList<Product> productList = setProductList(orderData);

                if (activity == null || activity.isFinishing())
                    return;


                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        itemsAdapter = Utils.setItemsSlider(
                                view,
                                productList,
                                R.id.orderItems,
                                R.id.orderItemProgress,
                                Main.FRAGMENT_TAG_ORDERS,
                                true,
                                mListener,
                                new ProductListCallback() {
                                    @Override
                                    public void goToProduct(Product product) {
                                        DataHolder.getInstance().setForceGetProduct(true);
                                        mListener.onProductSelected(product.getId());
                                    }

                                    @Override
                                    public int productAdded(int position) {
                                        return 0;
                                    }

                                    @Override
                                    public int productSubtracted(int position) {
                                        return 0;
                                    }

                                    @Override
                                    public int productAdded(Sku sku) {
                                        DataHolder.getInstance().setForceGetProduct(true);
                                        mListener.onProductSelected(sku.getProductId());

                                        return 0;
                                    }

                                    @Override
                                    public int productSubtracted(Sku sku) {
                                        return 0;
                                    }
                                }, false);

                        setMainData(view);
                        setRemainingOrderData(view);
                        mListener.onLoadingStop();
                    }
                });
            }
        });

        return true;
    }


    /**
     * Retry get order
     */

    public void retryGetOrder(final View view) {

        Activity activity = getActivity();

        if(activity == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int seconds = 1;

                if(apiRetries > 2)
                    seconds = 3;
                else if(apiRetries > 5)
                    seconds = 10;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        apiRetries++;

                        getOrder(view); //Retry!

                    }
                }, 1000 * seconds);
            }
        });
    }

    /**
     * Set order Items
     * @param orderData
     * @return
     */
    public ArrayList setProductList(JSONObject orderData) {
        ArrayList<Product> productList = new ArrayList<Product>();
        try{
            JSONArray items = orderData.getJSONArray("items");
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                JSONObject info = item.getJSONObject("additionalInfo");

                Product product = new Product();
                product.setId(item.optString("productId"));
                product.setName(item.optString("name"));
                product.setImage(item.optString("imageUrl"));
                product.setUrl(item.optString("detailUrl"));
                product.setBrand(info.optString("brandName"));

                Sku sku = new Sku();
                sku.setId(item.optString("id"));
                sku.setName(item.getString("name"));
                sku.setListPrice(item.optDouble("listPrice") / 100);
                sku.setBestPrice(item.optDouble("price") / 100);
                sku.setSellingPrice(item.optDouble("sellingPrice") / 100);
                sku.setSelectedQuantity(item.getInt("quantity"));
                sku.setRefId(item.optString("refId"));
                sku.setEAN(item.optString("ean"));
                sku.setMeasurementUnit(item.optString("measurementUnit"));
                sku.setUnitMultiplier(item.optDouble("unitMultiplier", 1f));
                sku.setStock(1);

                String[] images = {item.optString("imageUrl")};
                sku.setImages(images);

                // Product fields
                sku.setProductName(item.getString("name"));
                sku.setBrand(info.optString("brandName"));
                sku.setBrandId(Utils.extractBrandId(info.optString("_ProductData")));
                sku.setProductId(product.getId());

                product.addSku(sku);
                order.items.add(sku);

                productList.add(product);
            }

        }catch(JSONException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return productList;
    }

    /**
     * Set order totals
     * @param totals
     */
    public void setTotals(JSONArray totals) {

        if(totals != null){
            try {
                for(int i = 0; i < totals.length(); i++){
                    JSONObject item = null;

                        item = totals.getJSONObject(i);

                    String ID = item.optString("id", "");

                    if(ID.equals("Items"))
                        order.subtotal = item.getInt("value") / 100f;

                    else if(ID.equals("Shipping"))
                        order.shipping = item.getInt("value") / 100f;

                    else if(ID.equals("Discounts"))
                        order.discounts = item.getInt("value") / 100f;

                    else if(ID.equals("Tax"))
                        order.tax = item.getInt("value") / 100f;

                    else if(ID.equals("Change"))
                        order.change = item.getInt("value") / 100f;

                }
            } catch (JSONException e) {}

        }
    }

    /**
     * Set order totals
     * @param note
     */
    public void setNote(JSONObject note) {

        if(note != null){
            String value = note.optString("value");

            /**
             * If order note is a JSON string
             */

            try {
                JSONObject noteObject = new JSONObject(value);
                value = noteObject.optString("obs");

                if(!Utils.isEmpty(value) && !value.equals("undefined"))
                    order.note = value;

            } catch (JSONException e) {
                // Not a JSON String
                order.note = value;
            }

        }
    }


    /**
     * Set Shipping data
     * @param shippingData
     */
    public void setShipping(JSONObject shippingData) {

        if(shippingData == null)
            return;

        try {
            JSONObject address = shippingData.getJSONObject("address");
            JSONObject logisticsInfo = shippingData.getJSONArray("logisticsInfo").getJSONObject(0);
            JSONObject deliveryWindow = logisticsInfo.optJSONObject("deliveryWindow");

            order.street = address.optString("street", "");
            order.number = address.optString("number", null);
            order.complement = address.optString("complement", null);
            order.CP = address.optString("postalCode", null);
            order.city = address.optString("city", "");
            order.state = address.optString("state", "");
            order.receiverName = address.optString("receiverName", "");

            order.courier = logisticsInfo.optString("selectedSla", "-");
            order.deliveryWindow = deliveryWindow == null ? false : true;

            if(deliveryWindow != null){
                order.shippingDateStart = deliveryWindow.getString("startDateUtc");
                order.shippingDateEnd = deliveryWindow.getString("endDateUtc");
            }else {
                order.shippingDateStart = logisticsInfo.getString("shippingEstimateDate");
            }

        } catch (JSONException e) {}
    }

    /**
     * Set order payment data
     * @param paymentData
     */

    public void setPayment(JSONObject paymentData) {

        if(paymentData == null)
            return;

        try {
            JSONObject payment = paymentData
                                    .getJSONArray("transactions")
                                    .getJSONObject(0)
                                    .getJSONArray("payments")
                                    .getJSONObject(0);

            order.paymentName = payment.optString("paymentSystemName");

        } catch (JSONException e) {}
    }

    public void setMainData(View view) {

        if(view == null)
            return;

        if(!Utils.isEmpty(order.date)) {
            DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(order.date);
            DateTimeFormatter fmt = DateTimeFormat.forPattern(getString(R.string.orderDateTimeFormat));
            ((TextView) view.findViewById(R.id.orderDate)).setText(dateTime.toString(fmt));
        } else ((TextView) view.findViewById(R.id.orderDate)).setText("-");

        TextView orderIdText = view.findViewById(R.id.orderId);
        if (Config.getInstance().isGeantConfig()) {
            orderIdText.setText(order.id);
        } else {
            orderIdText.setText(Utils.isEmpty(order.sequence) ? "-" : order.sequence);
        }
        ((TextView) view.findViewById(R.id.orderStatus)).setText(order.getStatus());
        ((TextView) view.findViewById(R.id.userEmail)).setText(order.userEmail);
        ((TextView) view.findViewById(R.id.orderPayment)).setText(Utils.isEmpty(order.paymentName) ? "-" : order.paymentName);
        ((TextView) view.findViewById(R.id.totalPaymentMethod)).setText(Config.getInstance().formatPrice(order.total));
        ((TextView) view.findViewById(R.id.orderTotal)).setText(Config.getInstance().formatPrice(order.total));


        setStatus(view);
    }

    /**
     * Set Remaining order Data
     */

    public void setRemainingOrderData(View view) {

        String free = getResources().getString(R.string.free);

        // User Data
        ((TextView) view.findViewById(R.id.userName)).setText(order.userName);
        ((TextView) view.findViewById(R.id.userPhone)).setText(order.userPhone);


        // Shipping Data
        ((TextView) view.findViewById(R.id.receiverName)).setText(getString(R.string.receiverName) + order.receiverName);
        ((TextView) view.findViewById(R.id.deliveryWindow)).setText(order.getDeliveryWindow());
        ((TextView) view.findViewById(R.id.orderCourier)).setText(order.courier);
        ((TextView) view.findViewById(R.id.shippingFee)).setText(order.shipping == 0 ? free : Config.getInstance().formatPrice(order.shipping));
        ((TextView) view.findViewById(R.id.shippingStreet)).setText(order.street +
                (!Utils.isEmpty(order.number) ? " " + order.number : "") +
                (!Utils.isEmpty(order.complement) ? " " + order.complement : "") +
                ", " +
                order.getCity());



        // Set Totals
        ((TextView) view.findViewById(R.id.orderSubtotal)).setText(Config.getInstance().formatPrice(order.subtotal));
        ((TextView) view.findViewById(R.id.orderShippingValue)).setText(order.shipping == 0 ? free : Config.getInstance().formatPrice(order.shipping));

        // Taxes
        if(Config.getInstance().hasTax()|| order.tax > 0) {
            view.findViewById(R.id.orderTaxWrapper).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.orderTax)).setText(Config.getInstance().formatPrice(order.tax));
        }

        // Discount
        view.findViewById(R.id.orderDiscountsWrapper).setVisibility(order.discounts != 0 ? View.VISIBLE : View.GONE);
        ((TextView) view.findViewById(R.id.orderDiscounts)).setText(Config.getInstance().formatPrice(order.discounts));

        // Order change
        view.findViewById(R.id.orderChangeWrapper).setVisibility(order.change != 0 ? View.VISIBLE : View.GONE);
        ((TextView) view.findViewById(R.id.orderChange)).setText(Config.getInstance().formatPrice(order.change));


        if(Utils.isEmpty(order.note)) {
            view.findViewById(R.id.orderNoteWrapper).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.orderChangeWrapper).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.orderNote)).setText(order.note);
        }

    }

    public void reorder() {
        int size = order.items.size();
        for (int i = 0; i < size; i++)
            Cart.getInstance().addItem(order.items.get(i), getActivity(), i == (size - 1));
        mListener.onReorder();
    }

    @Override
    public void cartUpdated(){
        if (itemsAdapter != null)
            itemsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        itemsAdapter = null;
    }

}
