package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.activities.ShoppingListsActivity;
import com.fizzmod.vtex.adapters.ShoppingListAdapter;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.OptionsHeaderListener;
import com.fizzmod.vtex.interfaces.UserListAdapterListener;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.service.JanisService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.RetryableApiCallback;
import com.fizzmod.vtex.views.CartCartelMessage;
import com.fizzmod.vtex.views.CustomAlertDialog;
import com.fizzmod.vtex.views.OptionsHeaderLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseShoppingListsFragment extends BackHandledFragment implements UserListAdapterListener,
        OptionsHeaderListener {

    private ShoppingListAdapter adapter;
    private OptionsHeaderLayout optionsLayout;
    private RelativeLayout progressLayout;
    private CartCartelMessage cartelMessage;
    private FloatingActionButton fabButton;
    private List<ShoppingList> shoppingLists = new ArrayList<>();
    private List<Product> products = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        if (!User.isLogged(getActivity())) {
            mListener.closeFragment();
            mListener.requestSignIn(FRAGMENT_TAG_LISTS);
            return null;
        }

        mListener.onFragmentStart(FRAGMENT_TAG_LISTS);

        View view = inflater.inflate(R.layout.fragment_shopping_lists, container, false);

        progressLayout = (RelativeLayout) view.findViewById(R.id.progress);

        optionsLayout = (OptionsHeaderLayout) view.findViewById(R.id.shopping_lists_options_layout);
        optionsLayout.setTitle(R.string.list_title);
        optionsLayout.setListener(this);

        fabButton = (FloatingActionButton) view.findViewById(R.id.shopping_lists_create_new_list_fab);
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progressLayout.getVisibility() == GONE)
                    mListener.onCreateListPress();
            }
        });

        cartelMessage = (CartCartelMessage) view.findViewById(R.id.shopping_lists_cart_cartel);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.shopping_lists_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new ShoppingListAdapter(view.getContext(), this);
        recyclerView.setAdapter(adapter);

        fetchAllShoppingLists();

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mListener != null)
            mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            if (data != null && data.hasExtra(ShoppingListsActivity.REQUEST_SIGN_IN)) {
                mListener.signOut();
                mListener.requestSignIn(Main.FRAGMENT_TAG_LISTS);
            }
            return;
        }

        ShoppingList shoppingList = new Gson().fromJson(
                data.getStringExtra(ShoppingListsActivity.USER_LISTS_NEW_LIST),
                ShoppingList.class);

        shoppingLists.add(shoppingList);
        adapter.addShoppingList(shoppingList);
    }

    @Override
    public boolean onBackPressed() {

        boolean isEditing = isEditing();
        if (isEditing)
            exitEditMode();

        return isEditing || progressLayout.getVisibility() == VISIBLE;
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    /****************************
     * UserListsAdapterListener *
     ****************************/

    @Override
    public void onAddToCart(ShoppingList shoppingList) {
        if (shoppingList.isEmpty() || cartelMessage.isShown())
            return;

        Cart cart = Cart.getInstance();
        Activity activity = getActivity();

        for (String sku : shoppingList.getSkusList())
            cart.addItemQuantity(getSku(sku), activity, shoppingList.getProductQuantity(sku));

        onProductAdded(false);
    }

    @Override
    public void onListSelected(ShoppingList shoppingList) {
        mListener.onListSelected(shoppingList);
    }

    @Override
    public void onLongClick() {
        optionsLayout.toggleMenu();
        fabButton.setVisibility(GONE);
    }

    /*************************
     * OptionsHeaderListener *
     *************************/

    @Override
    public void onAddSelectedToCart() {

        if (adapter.getSelectedShoppingLists().isEmpty() || cartelMessage.isShown())
            return;

        HashMap<String, Integer> skusMap = new HashMap<>();
        Cart cart = Cart.getInstance();
        Activity activity = getActivity();

        for ( ShoppingList shoppingList : adapter.getSelectedShoppingLists() )
            for (String sku : shoppingList.getSkusList()) {
                Integer quantity = shoppingList.getProductQuantity(sku);
                if ( skusMap.containsKey( sku ) )
                    quantity += skusMap.get( sku );

                skusMap.put(sku, quantity);
            }

        for (Map.Entry<String, Integer> entry : skusMap.entrySet())
            cart.addItemQuantity( getSku( entry.getKey() ), activity, entry.getValue() );

        onProductAdded(true);
    }

    @Override
    public void onBack() {
        exitEditMode();
    }

    @Override
    public void onSelectAll() {
        if (cartelMessage.isShown())
            return;

        adapter.toggleSelectAll();
    }

    @Override
    public void onDeleteSelected() {

        int selectedCount = adapter.getSelectedShoppingLists().size();
        if (cartelMessage.isShown() || selectedCount == 0)
            return;

        final CustomAlertDialog alertDialog = new CustomAlertDialog(getActivity());
        alertDialog.setTxtMessage(selectedCount == 1 ?
                R.string.delete_single_shopping_list_alert :
                R.string.delete_many_shopping_lists_alert);
        alertDialog.setTxtAccept(R.string.delete, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                deleteSelectedLists();
            }
        });
        alertDialog.setTxtCancel(R.string.cancel);
        alertDialog.show();
    }

    /*******************
     * Private methods *
     *******************/

    private boolean isEditing() {
        return adapter.isEditing();
    }

    public void exitEditMode() {
        adapter.exitEditMode();
        optionsLayout.toggleMenu();
        fabButton.setVisibility(VISIBLE);
    }

    private void fetchAllShoppingLists() {
        final Activity activity = getActivity();
        fetchAllShoppingLists(activity, new Callback<List<ShoppingList>>(activity) {
            @Override
            public void onResponse(final List<ShoppingList> lists) {
                if (activity != null)
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            shoppingLists.clear();
                            shoppingLists.addAll(lists);
                            if (shoppingLists.isEmpty())
                                hideProgress();
                            else
                                fetchAllProducts();
                        }
                    });
            }

            @Override
            public void onError(final String errorMessage) {
                if (activity != null)
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showErrorToast(errorMessage);
                            mListener.closeFragment();
                        }
                    });
            }

            @Override
            protected void retry() {
                fetchAllShoppingLists(activity, this);
            }
        });
    }

    private void fetchAllShoppingLists(Activity activity, Callback<List<ShoppingList>> callback) {
        if (activity != null)
            JanisService.getInstance(activity).getShoppingLists(callback);
    }

    private void fetchAllProducts() {

        List<String> skus = new ArrayList<>();
        for (ShoppingList pl : shoppingLists)
            for (String sku : pl.getSkusList())
                if (!skus.contains(sku))
                    skus.add(sku);

        if (skus.isEmpty()) {
            hideProgress();
            adapter.setShoppingLists(shoppingLists);
            return;
        }

        final Activity activity = getActivity();
        API.getProductsBySku(getActivity(), skus, new ApiCallback<List<Product>>() {
            @Override
            public void onResponse(final List<Product> products) {
                if (activity != null)
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            BaseShoppingListsFragment.this.products = products;
                            adapter.setShoppingLists(shoppingLists);
                            hideProgress();
                        }
                    });
            }

            @Override
            public void onError(final String errorMessage) {
                if (activity != null)
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.setShoppingLists(shoppingLists);
                            showErrorToast( getString( R.string.errorOccurred ) );
                        }
                    });
            }

            @Override
            public void onUnauthorized() {
                onError(getString(R.string.errorOccurred));
            }
        });
    }

    private void hideProgress() {
        toggleProgress(GONE);
    }

    private void showProgress() {
        toggleProgress(VISIBLE);
    }

    private void toggleProgress(final int visibility) {
        Activity activity = getActivity();
        if (activity != null)
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressLayout.setVisibility(visibility);
                }
            });
    }

    private void showErrorToast(String errorMessage) {
        hideProgress();
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void deleteSelectedLists() {

        showProgress();
        final List<Integer> listsToDelete = new ArrayList<>();
        for (ShoppingList pl : adapter.getSelectedShoppingLists())
            listsToDelete.add(pl.getId());

        final Activity activity = getActivity();
        deleteSelectedLists(activity, listsToDelete, new Callback<Object>(activity) {
            @Override
            public void onResponse(Object object) {
                if (activity != null)
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.deleteSelected();
                            hideProgress();
                        }
                    });
            }

            @Override
            public void onError(final String errorMessage) {
                if (activity != null)
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showErrorToast(errorMessage);
                        }
                    });
            }

            @Override
            protected void retry() {
                deleteSelectedLists(activity, listsToDelete, this);
            }
        });
    }

    private void deleteSelectedLists(Activity activity, List<Integer> listsToDelete, Callback<Object> callback) {
        if (activity != null)
            JanisService.getInstance(activity).deleteShoppingLists(listsToDelete, callback);
    }

    private void onProductAdded(boolean addedVariousLists) {
        mListener.onProductAdded();

        int textResId = R.string.list_added;
        if (addedVariousLists)
            textResId = R.string.lists_added;

        cartelMessage.setText(textResId);
        cartelMessage.display();
    }

    private Sku getSku(String id) {

        for (Product p : products) {
            Sku sku = p.getSku(0);
            if (sku.getId().equals(id))
                return sku;
        }

        return null;
    }

    /********************
     * Private callback *
     ********************/

    private abstract class Callback<T> extends RetryableApiCallback<T> {

        Callback(Context context) {
            super(context);
        }

        @Override
        protected void requestSignIn() {
            mListener.signOut();
            mListener.requestSignIn(Main.FRAGMENT_TAG_LISTS);
        }
    }

}
