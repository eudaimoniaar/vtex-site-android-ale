package com.fizzmod.vtex.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.lang.reflect.Field;

/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
public class SignIn extends BackHandledFragment implements GoogleApiClient.OnConnectionFailedListener {

    public static final String ARG_SIGN_OUT = "signOutRequested";

    public static final int RC_SIGN_IN = 9001;
    public static final int VTEX_SIGN_IN_ACTIVITY = 326;

    private GoogleApiClient mGoogleApiClient;

    SignInButton signInButton;
    RelativeLayout googleSignInButtonWrapper;
    TextView continueWithoutSignIn;

    private boolean signOutRequested;

    public SignIn() {
        // Required empty public constructor
    }

    public static SignIn newInstance(boolean signOutRequested) {
        SignIn fragment = new SignIn();
        Bundle args = new Bundle();
        args.putBoolean(ARG_SIGN_OUT, signOutRequested);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            signOutRequested = getArguments().getBoolean(ARG_SIGN_OUT, false);
        buildGoogleApiClient();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mListener.onFragmentStart(Main.FRAGMENT_TAG_SIGN_IN);
        mListener.hideToolbar();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUI(view);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        mListener = null;
    }

    /**
     * Set Activity UI
     */
    private void setUI(final View view){
        // Views
        signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        googleSignInButtonWrapper = (RelativeLayout) view.findViewById(R.id.googleSignInButtonWrapper);
        continueWithoutSignIn = (TextView) view.findViewById(R.id.text_view_continue_without_sign_in);
        googleSignInUI(view);
        vtexSignInUI(view);
    }

    private void googleSignInUI(final View view){

        // Button listeners
        continueWithoutSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        // [START customize_button]
        // Set the dimensions of the sign-in button.
        signInButton.setSize(SignInButton.SIZE_ICON_ONLY);
        // [END customize_button]

        mListener.onLoadingStop();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);

        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.

            //Cached sign in disabled from issue AE-402
            /*
            Utils.log("Cached sign in!");
            GoogleSignInResult result = opr.get();
            if(!signOutRequested)
                handleSignInResult(result);
            */
        } else {
            Utils.log("Not signed in!");
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
        if (signOutRequested)
            googleSignOut();
    }

    private void buildGoogleApiClient() {
        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getResources().getString(R.string.server_client_id))
                //.requestServerAuthCode("762926673535-rbttrie370hga3h4lir4j650bnihsjh7.apps.googleusercontent.com")
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        if (mGoogleApiClient == null) {
            Utils.log("Creating google Api Client!");
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage((FragmentActivity) getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        // [END build_client]
    }

    private void vtexSignInUI(final View view){
        view.findViewById(R.id.vtexSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(getActivity(), VtexSignInActivity.class);
                mListener.startVtexSignIn();
                //startActivityForResult(intent, VTEX_SIGN_IN_ACTIVITY);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListener.showToolbar();
        disconnect();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        disconnect();
    }

    @Override
    public void onPause(){
        super.onPause();
        disconnect();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN)
            handleSignInResult( Auth.GoogleSignInApi.getSignInResultFromIntent( data ) );
    }

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        Utils.log("handleSignInResult:" + result.isSuccess() + " " + result.getStatus());
        if (!result.isSuccess()) {
            Utils.log("handleSignInResult:" + result.isSuccess() + " " + result.getStatus());
            return;
        }
        // Signed in successfully, show authenticated UI.
        GoogleSignInAccount acct = result.getSignInAccount();
        if (mListener != null && acct != null) {
            //Save to shared preferences
            Utils.log("Email:" + acct.getEmail());
            Utils.log("Given:" + acct.getGivenName());
            Utils.log("Family:" + acct.getFamilyName());
            Utils.log("token: " + acct.getIdToken());
            new User( acct.getEmail(), acct.getGivenName(), acct.getFamilyName() )
                    .setSignInToken(acct.getIdToken())
                    .setSignInLoginType(User.GOOGLE_LOGIN)
                    .save(getActivity(), new Callback() {
                        @Override
                        public void run(Object data) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mListener.onSignIn(false);
                                }
                            });
                        }

                        @Override
                        public void run(Object data, Object data2) {

                        }
                    });
        }
        // Once we saved the data sign out from google, we already have the account email.
        googleSignOut();
    }
    // [END handleSignInResult]

    private void disconnect(){
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            mGoogleApiClient.stopAutoManage((FragmentActivity) getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Sign in a user with Google Sign in API.
     *
     */
    private void signIn() {
        //mGoogleApiClient.clearDefaultAccountAndReconnect(); //To always pop the account selection
        startActivityForResult( Auth.GoogleSignInApi.getSignInIntent( mGoogleApiClient ), RC_SIGN_IN);
    }

    /**
     * Sign out a user from Google.
     *
     */
    private void googleSignOut() {
        if (mGoogleApiClient == null)
            return;
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
        else
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            // Nothing to do
                        }
                    });
    }

    /**
     * Revoke access from Google Sign In Api
     */
    private void revokeAccess() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        // Nothing to do
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        if (googleSignInButtonWrapper != null)
            googleSignInButtonWrapper.setVisibility(View.GONE);
        Utils.log("onConnectionFailed:" + connectionResult);
    }

}
