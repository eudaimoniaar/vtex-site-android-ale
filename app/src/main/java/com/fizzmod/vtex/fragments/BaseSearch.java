/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.AppliedFiltersAdapter;
import com.fizzmod.vtex.adapters.FiltersAdapter;
import com.fizzmod.vtex.adapters.GridAdapter;
import com.fizzmod.vtex.adapters.PagerAdapter;
import com.fizzmod.vtex.adapters.SelectorAdapter;
import com.fizzmod.vtex.adapters.viewholders.FilterValueAdapterViewHolder;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.models.Filter;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.SelectorItem;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.AnalyticsUtils;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.OnSwipeTouchListener;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ExpandableHeightGridView;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Search#newInstance} factory method to
 * create an instance of this fragment.
 */
public abstract class BaseSearch extends BackHandledFragment implements PagerAdapter.OnPageClickListener, FilterValueAdapterViewHolder.FilterSelectionListener {

    protected static final String ARG_TYPE = "ty";
    protected static final String ARG_QUERY = "qu";
    protected static final String ARG_CATEGORY_ID = "ci";
    protected static final String ARG_TITLE = "ti";

    final public static String FRAGMENT_STATE = "SFS"; // state fragment search.
    final public static String STATE_CURRENT_PAGE = "SFS_SCP"; // State current page.

    final public static int TYPE_CATEGORY = 1;
    final public static int TYPE_QUERY = 2;
    final public static int TYPE_ALL_CATEGORIES = 3;
    final public static int TYPE_TITLE = 4;
    final public static int TYPE_FULL_TEXT_QUERY = 5;

    final private static List<SelectorItem> sortFilters;

    public static final String PRODUCT_SORT_NAME_ASC = "OrderByNameASC";
    public static final String PRODUCT_SORT_NAME_DESC = "OrderByNameDESC";
    public static final String PRODUCT_SORT_PRICE_ASC = "OrderByPriceASC";
    public static final String PRODUCT_SORT_PRICE_DESC = "OrderByPriceDESC";
    public static final String PRODUCT_SORT_SALE_DESC = "OrderByTopSaleDESC";
    public static final String PRODUCT_SORT_RATE_DESC = "OrderByReviewRateDESC";
    public static final String PRODUCT_SORT_DISCOUNT_DESC = "OrderByBestDiscountDESC";
    public static final String PRODUCT_SORT_RELEASE_DATE_DESC = "OrderByReleaseDateDESC";

    /* Some servers would not allow requests above item 2500,
     * having 24 items per page, this would mean a total of 105 pages.
     * We limited it to 100 since it's rounder but this can change if a different
     * amount of items is showed per page, as long as the total of pages by items per page
     * is not higher than 2500 */
    final private int MAX_PAGES = 100;

    private Bundle savedState = null;

    private boolean sortOpen = false;
    private boolean filterOpen = false;
    private boolean lastSetupPagination;
    private boolean closingOverlay = false;
    private boolean isViewAvailable = false;
    protected boolean hasInitializedRootView = false;

    protected int type;
    private int totalPages;
    private int lastPage = 1;
    private int currentPage = 1;
    private int previousSortPosition = -1;
    private Integer categoryId;

    private String query;
    protected String pageTitle;
    private String queryOrder = PRODUCT_SORT_SALE_DESC;

    private Category category = null;

    private ArrayList<Product> productList;
    private final ArrayList<String> selectedFilters = new ArrayList<>();

    private View rootView;
    private View pagerView;
    private View filtersLayer;
    private View filterButton;
    private View bottomShadowView;
    private View closeFiltersDrawer;

    private TextView noResults;

    private LinearLayout retry;
    private LinearLayout filtersLayout;
    private LinearLayout sortOverlayView;
    private View pagerWrapper;
    private LinearLayout appliedFiltersView;

    private RelativeLayout drawerFilters;

    private RecyclerView pagerRecyclerView;
    private RecyclerView filtersRecyclerView;
    private RecyclerView appliedFiltersRecyclerView;
    private ListView sortListView;
    private ExpandableHeightGridView gridView;

    private GridAdapter adapter;
    private PagerAdapter pagerAdapter;
    private FiltersAdapter filtersAdapter;
    private SelectorAdapter selectorAdapter;
    private AppliedFiltersAdapter appliedFiltersAdapter;

    private ProgressBar progressBarPager;

    static
    {
        sortFilters = new ArrayList<>();
        sortFilters.add(new SelectorItem("A - Z", PRODUCT_SORT_NAME_ASC));
        sortFilters.add(new SelectorItem("Z - A", PRODUCT_SORT_NAME_DESC));
        sortFilters.add(new SelectorItem("Menor Precio", PRODUCT_SORT_PRICE_ASC));
        sortFilters.add(new SelectorItem("Mayor Precio", PRODUCT_SORT_PRICE_DESC));
        sortFilters.add(new SelectorItem("Más Vendidos", PRODUCT_SORT_SALE_DESC));
        sortFilters.add(new SelectorItem("Mejor Valorados", PRODUCT_SORT_RATE_DESC));
        //sortFilters.add(new SelectorItem("Mejor Descuento", PRODUCT_SORT_DISCOUNT_DESC));
        //sortFilters.add(new SelectorItem("Fecha Lanzamiento", PRODUCT_SORT_RELEASE_DATE_DESC));
    }

    public BaseSearch() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param type Search Type.
     * @param query Search Query.
     * @param title Search Page Title.
     * @param categoryId Category Id.
     * @return A new instance of fragment Search.
     */
    public static Search newInstance(int type, String query, String title, Integer categoryId) {
        Search fragment = new Search();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE, type);
        args.putString(ARG_QUERY, query);
        args.putString(ARG_TITLE, title);
        if (categoryId != null)
            args.putInt(ARG_CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public boolean onBackPressed() {
        if (filterOpen) {
            closeFilters();
            return true;
        } else if (sortOpen) {
            closeSort();
            return true;
        }
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            query = getArguments().getString(ARG_QUERY);
            pageTitle = getArguments().getString(ARG_TITLE, null);
            type = getArguments().getInt(ARG_TYPE);
            categoryId = getArguments().getInt(ARG_CATEGORY_ID);
        }
    }

    private Bundle saveState() { /* called either from onDestroyView() or onSaveInstanceState() */
        Bundle state = new Bundle();
        state.putInt(STATE_CURRENT_PAGE, currentPage);
        return state;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /* If onDestroyView() is called first, we can use the previously savedState but we can't call saveState() anymore */
        /* If onSaveInstanceState() is called first, we don't have savedState, so we need to call saveState() */
        /* => (?:) operator inevitable! */
        outState.putBundle(FRAGMENT_STATE, (savedState != null) ? savedState : saveState());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null)
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_search, container, false);
        else {
            // Do not inflate the layout again.
            // The returned View of onCreateView will be added into the fragment.
            // However it is not allowed to be added twice even if the parent is same.
            // So we must remove rootView from the existing parent view group
            // (it will be added back).
            ViewGroup parent = ((ViewGroup) rootView.getParent());
            if (parent != null)
                parent.removeView(rootView);
        }
        return rootView;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onFragmentStart(Main.FRAGMENT_TAG_SEARCH);
        final Activity activity = getActivity();

        /*
         * Get instance state
         */
        if(savedState != null)
            currentPage = savedState.getInt(STATE_CURRENT_PAGE);
        else if(savedInstanceState != null)
            currentPage = savedInstanceState.getInt(STATE_CURRENT_PAGE);
        savedState = null;
        /*
         * End restore instance state
         */

        if (!hasInitializedRootView) {
            hasInitializedRootView = true;
            TextView title = (TextView) view.findViewById(R.id.pageTitle);

            StringBuilder breadcrumbBuilder = new StringBuilder("<font color='#636363'>HOME / </font>");
            ArrayList<Category> categories;
            String breadCrumbOnColor = Utils.getHexFromResource(activity, R.color.breadCrumbOn);
            if (type == TYPE_TITLE && Utils.isEmpty(pageTitle))
                type = TYPE_QUERY;
            switch (type) {
                case Search.TYPE_CATEGORY:
                    categories = DataHolder.getInstance().getCategories(false);
                    if (categories == null)
                        categories = Category.restore(activity);
                    category = Category.getCategoryById(categories, categoryId);
                    if (category != null) { // TODO: Check what to do if category is null!
                        title.setText(category.getName());
                        ArrayList<String> breadcrumbItems = Category.buildBreadCrumb(categories, categoryId);
                        int breadcrumbLength = breadcrumbItems.size();
                        for (int i = 0; i < breadcrumbLength; i++)
                            if (i == (breadcrumbLength - 1))
                                breadcrumbBuilder.append("<font color='")
                                        .append(breadCrumbOnColor)
                                        .append("'>")
                                        .append(breadcrumbItems.get(i).toUpperCase())
                                        .append("</font>");
                            else
                                breadcrumbBuilder.append("<font color='#636363'>")
                                        .append(breadcrumbItems.get(i).toUpperCase())
                                        .append(" / </font>");
                        if (category.getCildrenSize() == 0)
                            title.setCompoundDrawables(null, null, null, null);
                        Categories.drawCategories(category.getChildren(), activity, getView(), mListener, true);
                    }
                    break;

                case Search.TYPE_ALL_CATEGORIES:
                    breadcrumbBuilder.append("<font color='")
                            .append(breadCrumbOnColor)
                            .append("'>")
                            .append(getResources().getString(R.string.breadcrumbSearch).toUpperCase())
                            .append("</font>");
                    title.setText(pageTitle);
                    categories = DataHolder.getInstance().getCategories(false);
                    if (categories == null)
                        categories = Category.restore(activity);
                    Categories.drawCategories(categories, activity, getView(), mListener, true);
                    break;

                case Search.TYPE_QUERY:
                case Search.TYPE_FULL_TEXT_QUERY:
                    title.setCompoundDrawables(null, null, null, null);
                    if (Utils.isEmpty(pageTitle))
                        title.setText(getResources().getString(R.string.searchResults));
                    else
                        title.setText(getResources().getString(R.string.resultsFor, pageTitle));
                    breadcrumbBuilder.append("<font color='")
                            .append(breadCrumbOnColor)
                            .append("'>")
                            .append(getResources().getString(R.string.breadcrumbSearch).toUpperCase())
                            .append("</font>");
                    break;

                case Search.TYPE_TITLE:
                    title.setCompoundDrawables(null, null, null, null);
                    title.setText(pageTitle);
                    breadcrumbBuilder.append("<font color='")
                            .append(breadCrumbOnColor)
                            .append("'>")
                            .append(pageTitle.toUpperCase())
                            .append("</font>");
                    break;

                default:
                    breadcrumbBuilder.delete(0, breadcrumbBuilder.toString().length());
                    break;
            }
            if (!breadcrumbBuilder.toString().isEmpty())
                ((TextView) view.findViewById(R.id.breadcrumb_text))
                        .setText(Html.fromHtml(breadcrumbBuilder.toString()), TextView.BufferType.SPANNABLE);

            noResults = (TextView) view.findViewById(R.id.noResults);
            retry = (LinearLayout) view.findViewById(R.id.retry);

            // Pagination
            pagerRecyclerView = (RecyclerView) view.findViewById(R.id.pager_recycler_view);
            pagerRecyclerView.setHasFixedSize(true);
            pagerRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            pagerAdapter = new PagerAdapter(this);
            pagerRecyclerView.setAdapter(pagerAdapter);
            pagerView = view.findViewById(R.id.pager);
            bottomShadowView = view.findViewById(R.id.bottom_shadow);
            progressBarPager = (ProgressBar) view.findViewById(R.id.progressBarPager);
            pagerWrapper = view.findViewById(R.id.pagerWrapper);

            // Filters
            filterButton = view.findViewById(R.id.filter);
            drawerFilters = (RelativeLayout) view.findViewById(R.id.drawerFilters);
            closeFiltersDrawer = drawerFilters.findViewById(R.id.closeFiltersDrawer);
            filtersLayout = (LinearLayout) drawerFilters.findViewById(R.id.filters_layout);
            filtersLayer = drawerFilters.findViewById(R.id.filtersLayer);
            filtersRecyclerView = (RecyclerView) view.findViewById(R.id.filters_recycler_view);
            filtersRecyclerView.setNestedScrollingEnabled(false);
            filtersRecyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ) );
            filtersAdapter = new FiltersAdapter(this);
            filtersRecyclerView.setAdapter(filtersAdapter);
            appliedFiltersView = (LinearLayout) drawerFilters.findViewById(R.id.appliedFilters);
            appliedFiltersRecyclerView = (RecyclerView) view.findViewById(R.id.applied_filters_recycler_view);
            appliedFiltersRecyclerView.setNestedScrollingEnabled(false);
            appliedFiltersRecyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ) );
            appliedFiltersAdapter = new AppliedFiltersAdapter(this);
            appliedFiltersRecyclerView.setAdapter(appliedFiltersAdapter);
            sortOverlayView = (LinearLayout) view.findViewById(R.id.itemSelectorOverlay);
            sortListView = (ListView) view.findViewById(R.id.itemSelectorList);

            /* Listeners */
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    getProducts(query, lastPage, lastSetupPagination);
                }
            });
            view.findViewById(R.id.sort).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSort();
                }
            });
            filterButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openFilters();
                }
            });
            view.findViewById(R.id.closeOverlay).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeSort();
                }
            });
            view.findViewById(R.id.nextPage).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (totalPages > currentPage)
                        goToPage(currentPage + 1);
                }
            });
            view.findViewById(R.id.previousPage).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentPage > 1)
                        goToPage(currentPage - 1);
                }
            });
            closeFiltersDrawer.setOnTouchListener(new OnSwipeTouchListener(activity) {
                public void onSwipeLeft() {
                    closeFilters();
                }

                public void onClick() {
                    closeFilters();
                }
            });
            filtersLayout.setOnTouchListener(new OnSwipeTouchListener(activity) {
                public void onSwipeLeft() {
                    closeFilters();
                }
            });
            activity.findViewById(R.id.filtersBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeFilters();
                }
            });
            activity.findViewById(R.id.applyFilters).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    applyFilters();
                }
            });
            activity.findViewById(R.id.removeFilters).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeAllFilters();
                }
            });
            setSortFilter();
            setProductList(view);
            getProducts(query, currentPage, true);
            getFilters();
        }
        pagerView.setVisibility(View.VISIBLE);
        bottomShadowView.setVisibility(View.VISIBLE);
        isViewAvailable = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isViewAvailable = false;
        savedState = saveState();
        if (adapter != null) {
            adapter.updateProducts(new ArrayList<Product>());
        }
        adapter = null;
        if (gridView != null)
            gridView.setAdapter(null);
        gridView = null;
        adapter = null;
        pagerView = null;
        bottomShadowView = null;
        progressBarPager = null;
        pagerWrapper = null;
        noResults = null;
        retry = null;
        hasInitializedRootView = false;
        rootView = null;
        filterButton = null;
        drawerFilters = null;
        filtersLayout = null;
        filtersRecyclerView = null;
        filtersAdapter = null;
        appliedFiltersView = null;
        appliedFiltersRecyclerView = null;
        closeFiltersDrawer = null;
        filtersLayer = null;
        if (productList != null)
            productList.clear();
        selectedFilters.clear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener.onLoadingStop();
        mListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Set Product list
     */
    private void setProductList(View view) {
        gridView = (ExpandableHeightGridView) view.findViewById(R.id.productGrid);
        adapter = new GridAdapter(
                getActivity(),
                productList,
                mListener,
                new ProductListCallback() {

                    @Override
                    public void goToProduct(Product product) {}

                    @Override
                    public int productAdded(int pos) {
                        Product product = productList.get(pos);
                        Sku sku = product.getMainSku();
                        int itemQuantity = Cart.getInstance().increaseItemQuantity(sku, getActivity());
                        mListener.onProductAdded();
                        return itemQuantity;
                    }

                    @Override
                    public int productSubtracted(int pos) {
                        Product product = productList.get(pos);
                        Sku sku = product.getMainSku();
                        int itemQuantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                        mListener.onProductAdded();
                        return itemQuantity;
                    }

                    @Override
                    public int productAdded(Sku sku) {
                        return 0;
                    }

                    @Override
                    public int productSubtracted(Sku sku) {
                        return 0;
                    }
                });
        adapter.setParentFragment(Main.FRAGMENT_TAG_SEARCH);
        gridView.setAdapter(adapter);
        gridView.setExpanded(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if (adapter.isMenuPressedInView(v))
                    return;
                Product product = productList.get((int) v.getTag(R.id.TAG_ADAPTER_ITEM));
                if (!Utils.isSearchApiEnabled() || type != TYPE_FULL_TEXT_QUERY)
                    DataHolder.getInstance().setProduct(product);
                else
                    DataHolder.getInstance().setForceGetProduct(true);
                mListener.onProductSelected(product.getId());
            }
        });
    }

    /**
     * Show retry!
     * @param error
     */
    private void showRetry(boolean error) {
        if (retry == null)
            return;
        ((TextView) retry.findViewById(R.id.retryText)).setText(error ? R.string.errorOccurred : R.string.noConnection);
        Utils.fadeIn(retry);
    }

    /**
     * Get filters
     */
    private void getFilters() {
        filterButton.setEnabled(false);
        filterButton.setAlpha(0.5f);
        final Activity activity = getActivity();
        if (query == null && category == null || type == TYPE_FULL_TEXT_QUERY && Utils.isSearchApiEnabled())
            return;
        String filterQuery = category != null ? category.getURI() : query;
        API.getSpecifications(filterQuery, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // TODO retry
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String json = response.body().string();
                if (activity == null || activity.isFinishing() || !isViewAvailable)
                    return;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setFilters(API.setSpecificationList(json));
                    }
                });
            }
        });
    }

    /**
     * Setup filters
     * @param filters
     */
    private void setFilters(ArrayList<Filter> filters) {
        final Activity activity = getActivity();
        if (activity == null || !isViewAvailable)
            return;
        appliedFiltersView.setVisibility(View.GONE);
        ArrayList<String> tmpSelectedFilters = new ArrayList<>();
        for (Filter filter : filters)
            for (String value : filter.values)
                if (selectedFilters.contains(value)) {
                    tmpSelectedFilters.add(value);
                    int position = filter.values.indexOf(value);
                    addToAppliedFilters(
                            filter.texts.get(position),
                            value,
                            position,
                            filters.indexOf(filter),
                            false);
                }
        appliedFiltersAdapter.notifyDataSetChanged();
        appliedFiltersView.setVisibility(appliedFiltersAdapter.getItemCount() == 0 ? View.GONE : View.VISIBLE);
        selectedFilters.clear();
        selectedFilters.addAll(tmpSelectedFilters);
        filtersAdapter.setFilters(filters, selectedFilters);
        filterButton.setEnabled(!filters.isEmpty());
        filterButton.setAlpha(filters.isEmpty() ? 0.5f : 1);
    }

    /***************************
     * FilterSelectionListener *
     ***************************/

    @Override
    public void onFilterSelected(String text, String value, Integer position, Integer filterPosition) {
        selectedFilters.add(value);
        addToAppliedFilters(text, value, position, filterPosition, true);
    }

    @Override
    public void onFilterDeselected(String text, String value, Integer position, Integer filterPosition) {
        selectedFilters.remove(value);
        appliedFiltersAdapter.removeItem(value);
        filtersAdapter.deselectFilter(position, filterPosition);
        if (selectedFilters.isEmpty())
            appliedFiltersView.setVisibility(View.GONE);
    }

    /**
     * Add filter to applied filters View
     * @param text
     * @param position
     * @param filterPosition
     */
    private void addToAppliedFilters(String text, String value, Integer position, Integer filterPosition, boolean notifyDataChanged) {
        appliedFiltersAdapter.addItem(text, value, position, filterPosition, notifyDataChanged);
        if (appliedFiltersView.getVisibility() == View.GONE)
            appliedFiltersView.setVisibility(View.VISIBLE);
    }

    /**
     * Remove all applied filters
     */
    private void removeAllFilters() {
        appliedFiltersAdapter.removeAllItems();
        appliedFiltersView.setVisibility(View.GONE);
        selectedFilters.clear();
        filtersAdapter.notifyDataSetChanged();
    }

    private void applyFilters() {
        closeFilters();
        getProducts(query,1, true);
    }

    private void setSortFilter() {
        for (int i = 0; i < sortFilters.size(); i++) {
            SelectorItem item = sortFilters.get(i);
            item.enabled = item.value.equals(queryOrder);
            if (item.enabled)
                previousSortPosition = i;
        }

        selectorAdapter = new SelectorAdapter(getSortFilters());
        sortListView.setAdapter(selectorAdapter);
        sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (type == TYPE_FULL_TEXT_QUERY && Utils.isSearchApiEnabled())
                    // A-Z & Z-A sorts not include when using Search API
                    position += 2;
                if (position != previousSortPosition) {
                    SelectorItem current = sortFilters.get(position);
                    if (previousSortPosition != -1)
                        sortFilters.get(previousSortPosition).enabled = false;
                    current.enabled = true;
                    queryOrder = current.value;
                    previousSortPosition = position;
                    selectorAdapter.setItems(getSortFilters());
                    currentPage = 1;
                    getProducts(query, 1, true);
                }
                closeSort();
            }
        });
        sortOverlayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeSort();
            }
        });
    }

    private List<SelectorItem> getSortFilters() {
        return type == TYPE_FULL_TEXT_QUERY && Utils.isSearchApiEnabled() ?
                sortFilters.subList(2, sortFilters.size()) :        // do not include A-Z & Z-A sorts
                sortFilters;
    }

    private void openSort() {
        sortOpen = true;
        Utils.fadeIn(sortOverlayView);
    }

    private void closeSort() {
        sortOpen = false;
        if (closingOverlay)
            return;
        closingOverlay = true;
        Utils.fadeOut(sortOverlayView, new com.fizzmod.vtex.interfaces.Callback() {
            @Override
            public void run(Object data) {
                closingOverlay = false;
            }

            @Override
            public void run(Object data, Object data2) {}
        });
    }

    /*********************************
     *
     *      PAGINATION FUNCTIONS
     *
     **********************************/

    /**
     * Set Pagination
     * @param pages Total Pages
     */
    private void setPagination(final int pages, final int current) {
        totalPages = pages;
        final Activity activity = getActivity();
        if (activity == null || activity.isFinishing() || !isViewAvailable)
            return;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                pagerAdapter.setPages(pages, current);
                progressBarPager.setVisibility(View.GONE);
                if (pages == 0)
                    return;
                pagerWrapper.setVisibility(View.VISIBLE);
                if (currentPage != 1)
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollToPage(currentPage);
                        }
                    }, 700);
            }
        });
    }

    private void goToPage(int pageNumber) {
        if (currentPage == pageNumber)
            return;
        currentPage = pageNumber;
        pagerAdapter.setCurrentPage(pageNumber);
        scrollToPage(pageNumber);
        getProducts(currentPage);
        scrollToTop();
    }

    private void scrollToPage(int pageNumber) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) pagerRecyclerView.getLayoutManager();
        View view = layoutManager.findViewByPosition(pageNumber - 1);
        int pageLeft;
        if (view != null)
            // Selected page is visible
            pageLeft = view.getLeft();
        else {
            // Selected page is not visible in the recyclerView, must calculate X distance
            view = layoutManager.findViewByPosition(layoutManager.findFirstVisibleItemPosition());
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            pageLeft = (view.getWidth() + layoutParams.leftMargin + layoutParams.rightMargin) * (pageNumber - 1);
        }
        int scrollX = (pageLeft - (pagerRecyclerView.getWidth() / 2)) + (view.getWidth() / 2);
        pagerRecyclerView.smoothScrollBy(scrollX, 0);
    }

    /************************************
     * PagerAdapter.OnPageClickListener *
     ************************************/

    @Override
    public void onPageClick(final int pageNumber) {
        goToPage(pageNumber);
    }

    /*********************************
     *
     * END PAGINATION
     *
     *********************************/

    private void getProducts(int page) {
        getProducts(query, page, false);
    }

    private void getProducts(String query, final int page, final boolean setupPagination) {
        if (mListener != null)
            mListener.onLoadingStart();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("O", queryOrder);
        if (setupPagination) {
            progressBarPager.setVisibility(View.VISIBLE);
            pagerWrapper.setVisibility(View.GONE);
        }
        lastPage = page;
        lastSetupPagination = setupPagination;
        for (int i = 0; i < selectedFilters.size(); i++)
            parameters.put("fq{{" + i + "}}", selectedFilters.get(i));
        if (category != null)
            parameters.put(API.PRODUCT_SEARCH_CATEGORY_KEY, "C:" + category.getIdsPath());
        AnalyticsUtils.logSearchEvent(getActivity(), query);
        API.search(getActivity(), query, parameters, page, API.SOFT_LIMIT, type == TYPE_FULL_TEXT_QUERY, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing() || !isViewAvailable)
                    return;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mListener != null)
                            mListener.onLoadingStop();
                        if (adapter == null)
                            return;
                        adapter.updateProducts(new ArrayList<Product>());
                        showRetry(false);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing() || !isViewAvailable)
                    return;
                final String JSON = response.body().string();
                productList = API.setProductList(JSON);
                final boolean error = productList.isEmpty() && response.code() >= 400;
                if (setupPagination && (!Utils.isSearchApiEnabled() || !error)) {
                    int pages = (int) Math.ceil((float) API.getTotalResources(response.headers().get("resources"), JSON) / (float) API.SOFT_LIMIT);
                    setPagination(pages < MAX_PAGES ? pages : MAX_PAGES, page);
                }
                final ArrayList<Filter> filters = new ArrayList<>();
                if (type == TYPE_FULL_TEXT_QUERY && Utils.isSearchApiEnabled() && filtersAdapter.getItemCount() == 0)
                    filters.addAll(API.setSpecificationList(JSON));
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (productList == null || noResults == null || adapter == null)
                            return;
                        if (productList.isEmpty()) {
                            if (error)
                                showRetry(true);
                            else
                                Utils.fadeIn(noResults);
                            Utils.fadeOut(pagerView);
                            Utils.fadeOut(bottomShadowView);
                        } else {
                            if (!filters.isEmpty())
                                setFilters(filters);
                            noResults.setVisibility(View.GONE);
                            Utils.fadeIn(pagerView);
                            Utils.fadeIn(bottomShadowView);
                        }
                        adapter.updateProducts(productList);
                        if (mListener != null)
                            mListener.onLoadingStop();
                        if (!error && setupPagination && progressBarPager != null && pagerWrapper != null) {
                            progressBarPager.setVisibility(View.GONE);
                            pagerWrapper.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
    }

    /**
     * Open Comments drawer
     */
    private void openFilters() {
        filterOpen = true;
        drawerFilters.setVisibility(View.VISIBLE);
        toggleFilters(Utils.ANIMATION_IN_LEFT_TO_RIGHT, Utils.ANIMATION_FADE_IN);
    }

    /**
     * Close Comments drawer
     */
    private void closeFilters() {
        filterOpen = false;
        toggleFilters(Utils.ANIMATION_OUT_RIGHT_TO_LEFT, Utils.ANIMATION_FADE_OUT);
    }

    private void toggleFilters(int filtersAnimationType, int fadeAnimationType) {
        filtersLayout.startAnimation( Utils.getAnimation( filtersAnimationType, filtersLayout, 250 ) );
        closeFiltersDrawer.startAnimation( Utils.getAnimation( fadeAnimationType, closeFiltersDrawer, 250 ) );
        filtersLayer.startAnimation( Utils.getAnimation( fadeAnimationType, filtersLayer, 250 ) );
    }

    @Override
    public void refresh() {
        getProducts(query, 1, true);
        getFilters();
        scrollToTop();
    }

    @Override
    public void cartUpdated() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    private void scrollToTop() {
        if (rootView != null)
            ((ScrollView) rootView.findViewById(R.id.searchWrapper)).smoothScrollTo(0, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.PROMOTIONS_API_ENABLED && PromotionsService.getInstance(getActivity()).promotionsAreOutdated())
            getProducts(query, currentPage, true);
    }
}
