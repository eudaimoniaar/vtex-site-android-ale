/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.models.Category;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Categories#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Categories extends BackHandledFragment {

    final public static String ARG_GROUP_ID = "g";

    private LinearLayout retry;
    private Integer groupId = null;

    public Categories() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Categories.
     */
    public static Categories newInstance(Integer group) {
        Categories fragment = new Categories();
        Bundle args = new Bundle();
        if(group != null)
            args.putInt(ARG_GROUP_ID, group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            groupId = getArguments().getInt(ARG_GROUP_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mListener.onFragmentStart(Main.FRAGMENT_TAG_CATEGORIES);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        backHandlerInterface.addScrollListener((ScrollView) view.findViewById(R.id.categoriesScrollView));
        ArrayList<Category> categoryList = DataHolder.getInstance().getCategories(false);
        view.findViewById(R.id.allCategories).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onCategorySelected(null, null);
            }
        });
        retry = (LinearLayout) view.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                getCategories(false);
            }
        });
        if (categoryList != null) {
            mListener.onLoadingStop();
            Categories.this.drawCategories(categoryList, groupId);
        } else
            getCategories(true);
        if (groupId != null)
            view.findViewById(R.id.allCategories).setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        retry = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get Category tree
     */
    private void getCategories(boolean fromFile){
        if (mListener != null)
            mListener.onLoadingStart();
        if (fromFile && Category.isSaved(getActivity())) {
            new LoadCategoriesFromPrefs(getActivity()).execute();
            return;
        }
        API.getCategories(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing())
                    return;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mListener != null)
                            mListener.onLoadingStop();
                        if (retry != null)
                            retry.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String JSON = response.body().string();
                boolean error = true;
                try {
                    JSONArray categories = new JSONArray(JSON);
                    error = false;
                } catch (JSONException e) {}
                final boolean success = !error;
                final ArrayList<Category> categoryList = Config.getInstance().getFilteredCategories(Category.setCategories(JSON, false));
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing())
                    return;
                if (success)
                    Category.save(activity, JSON);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mListener != null)
                            mListener.onLoadingStop();
                        if (success)
                            Categories.this.drawCategories(categoryList, groupId);
                        else if (retry != null)
                            retry.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }

    /**
     * Draw categories
     * @param categories
     */
    private void drawCategories(ArrayList<Category> categories, Integer groupId){
        View view = getView();
        if (groupId != null) {
            Category groupCategory = Category.getCategoryByGroup(categories, groupId);
            categories = groupCategory.getChildren();
            try {
                ((TextView) view.findViewById(R.id.categoriesTitle)).setText(groupCategory.getName());
            } catch (NullPointerException e) {}
        }
        Categories.drawCategories(categories, getActivity(), view, mListener, false);
    }

    public static void drawCategories(ArrayList<Category> categories,
                                      Activity activity,
                                      View view,
                                      final OnFragmentInteractionListener mListener,
                                      boolean list){
        if (view == null || categories == null)
            return;
        LinearLayout categoriesWrapper = (LinearLayout) view.findViewById(R.id.categoriesWrapper);
        for (int i = 0; i < categories.size(); i++) {
            Category category = categories.get(i);
            TextView categoryView = (TextView) activity.getLayoutInflater().inflate(R.layout.sublayout_category, categoriesWrapper, false);
            categoryView.setText(category.getName());
            categoryView.setTag(category.getId());
            final Integer currentGroupId = category.getGroup();
            categoryView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null)
                        mListener.onCategorySelected((Integer) v.getTag(), currentGroupId);
                }
            });
            if (i == 0 && !list)
                Utils.setBackground(categoryView, ContextCompat.getDrawable(activity, R.drawable.category_top));
            else if (i == (categories.size() - 1))
                Utils.setBackground(categoryView, ContextCompat.getDrawable(activity, R.drawable.category_bottom));
            if (list)
                Utils.setMargins(categoryView, 0, 0, 0, 0);
            categoriesWrapper.addView(categoryView);
        }
        if (mListener != null)
            mListener.onLoadingStop();
    }

    private class LoadCategoriesFromPrefs extends AsyncTask<Void, Void, ArrayList<Category>> {

        private Context context;

        LoadCategoriesFromPrefs(Context context){
            this.context = context;
        }

        @Override
        protected ArrayList<Category> doInBackground(Void... params) {
            return Category.restore(context);
        }

        @Override
        protected void onPostExecute(final ArrayList<Category> categoryList) {
            if (categoryList != null && categoryList.size() > 0) {
                // Wait UNTIL fragment animation stopped
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mListener != null)
                            mListener.onLoadingStop();
                        drawCategories(categoryList, groupId);
                    }
                }, 300);
            } else
                getCategories(false);
        }
    }

}
