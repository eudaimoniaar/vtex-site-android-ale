/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.GridAdapter;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.EndlessScrollListener;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Search#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Favourites extends BackHandledFragment {

    public static final String PREFS_NAME = "favourites";
    public static final int LIMIT = 45;

    private ExpandableHeightGridView gridView;
    private GridAdapter adapter;

    private View noResultsView;
    private LinearLayout addAllWrapper;

    private ArrayList<Product> productList;
    private ArrayList<String> productsIds;
    private boolean isViewAvailable = false;

    public Favourites() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Favourites.
     */

    public static Favourites newInstance() {
        return new Favourites();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favourites, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backHandlerInterface.addScrollListener(view.findViewById(R.id.searchWrapper));

        mListener.onFragmentStart(Main.FRAGMENT_TAG_FAVOURITES);

        String breadcrumb = "<font color='#636363'>HOME / </font><font color='" + Utils.getHexFromResource(getActivity(), R.color.breadCrumbOn) + "'>"
                + getResources().getString(R.string.breadcrumbFavourites).toUpperCase() + "</font>";

        ((TextView) view.findViewById(R.id.breadcrumText)).setText(Html.fromHtml(breadcrumb), TextView.BufferType.SPANNABLE);

        noResultsView = view.findViewById(R.id.noResults);
        addAllWrapper = (LinearLayout) view.findViewById(R.id.addAllWrapper);

        view.findViewById(R.id.addAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAll();
            }
        });

        setProductList(view);

        isViewAvailable = true;

        getProducts(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(gridView != null)
            gridView.setAdapter(null);
        gridView = null;
        adapter = null;
        noResultsView = null;
        addAllWrapper = null;
        productList = null;
        productsIds = null;
        isViewAvailable = false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Set Product list
     */
    public void setProductList(View view) {

        productsIds = restore(getActivity());
        productList = new ArrayList<>();
        gridView = (ExpandableHeightGridView) view.findViewById(R.id.productGrid);

        if (productsIds.isEmpty()) {
            noResultsView.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
            addAllWrapper.setVisibility(View.GONE);
        }

        adapter = new GridAdapter(getActivity(), productList, mListener, new ProductListCallback() {
            @Override
            public void goToProduct(Product product) {}

            @Override
            public int productAdded(int pos) {
                Product product = productList.get(pos);
                Sku sku = product.getMainSku();
                int currentQuantity = Cart.getInstance().increaseItemQuantity(sku, getActivity());
                mListener.onProductAdded();
                return currentQuantity;
            }

            @Override
            public int productSubtracted(int pos) {
                Product product = productList.get(pos);
                Sku sku = product.getMainSku();
                int currentQuantity = Cart.getInstance().decreaseItemQuantity(sku, getActivity());
                mListener.onProductAdded();
                return currentQuantity;
            }

            @Override
            public int productAdded(Sku sku) {
                return 0;
            }

            @Override
            public int productSubtracted(Sku sku) {
                return 0;
            }
        });
        adapter.setParentFragment(Main.FRAGMENT_TAG_FAVOURITES);

        gridView.setAdapter(adapter);
        gridView.setExpanded(true);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if (adapter.isMenuPressedInView(v))
                    return;

                Product product = productList.get((int) v.getTag(R.id.TAG_ADAPTER_ITEM));
                DataHolder.getInstance().setProduct(product);

                mListener.onProductSelected(product.getId());
            }
        });
        gridView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                return getProducts(page);
                // or customLoadMoreDataFromApi(totalItemsCount);
                //return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });
    }

    public boolean getProducts(int page) {
        HashMap<String, String> parameters = new HashMap<>();
        int offset = (page - 1) * LIMIT;
        int end = page * LIMIT > productsIds.size() ? productsIds.size() : page * LIMIT;
        int count = 0;
        for (int i = offset; i < end; i++, count++)
            parameters.put("fq{{" + i + "}}", "productId:" + productsIds.get(i));

        if (count == 0)
            return false;

        if (mListener != null)
            mListener.onLoadingStart();

        API.search(getActivity(), null, parameters, null, LIMIT, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //TODO Show error
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String JSON = response.body().string();

                ArrayList<Product> list = API.setProductList(JSON);

                Activity activity = getActivity();

                if (activity == null || activity.isFinishing() || !isViewAvailable)
                    return;

                productList.addAll(list);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (productList.isEmpty()) {
                            Utils.fadeIn(noResultsView);
                            addAllWrapper.setVisibility(View.GONE);
                            gridView.setVisibility(View.GONE);
                        } else {
                            noResultsView.setVisibility(View.GONE);
                            gridView.setVisibility(View.VISIBLE);
                            Utils.fadeIn(addAllWrapper);
                        }
                        if (mListener != null)
                            mListener.onLoadingStop();
                        adapter.updateProducts(productList);
                        gridView.invalidate();
                    }
                });
            }
        });

        return true;
    }

    /**
     * Restore cart data from SharedPreferences
     * @param context
     */
    public static ArrayList<String> restore(Context context) {

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        ArrayList<String> list = new ArrayList<>();
        try {
            JSONArray json = new JSONArray(settings.getString(PREFS_NAME, ""));

            if (json.length() == 0)
                return list;

            for (int i = 0; i < json.length(); i++)
                list.add(json.getString(i));

            Utils.log("Json Size: " + list.size());
        } catch (JSONException e) {}

        return list;
    }

    public void addAll() {
        int size = productList.size();

        for (int i = 0; i < size; i++) {
            Product product = productList.get(i);
            Sku sku = product.getMainSku(); //TODO check what to do with multiple SKU's
            sku.setSelectedQuantity(
                Cart.getInstance().addItem(sku, getActivity(), i == (size - 1))
            );
        }

        adapter.notifyDataSetChanged();

        mListener.onProductAdded();
    }

    /**
     * Save cart data to SharedPreferences
     * @param context
     */
    public static void save(Context context, String productId){
        JSONArray json = Utils.arrayToJSONArray( restore( context ) );
        json.put(productId);
        save(context, json);
    }

    public static void save(Context context, JSONArray json){
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_NAME, json.toString());
        // Commit the edits!
        editor.commit();
    }

    /**
     * Save cart data to SharedPreferences
     * @param context
     */
    public static void remove(Context context, String productId){
        ArrayList<String> list = restore(context);
        list.remove(productId);
        JSONArray json = Utils.arrayToJSONArray(list);
        save(context, json);
    }

    public static boolean exists(Context context, String productId){
        ArrayList<String> list = restore(context);
        return list.contains(productId);
    }

    @Override
    public void refresh(){
        if (adapter == null)
            return;
        productList.clear();
        adapter.updateProducts(productList);
        getProducts(1);
    }

    @Override
    public void cartUpdated(){
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.PROMOTIONS_API_ENABLED && PromotionsService.getInstance(getActivity()).promotionsAreOutdated())
            refresh();
    }
}

