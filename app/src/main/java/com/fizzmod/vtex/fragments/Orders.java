/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.OrderAdapter;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.models.Order;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Search#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Orders extends BackHandledFragment implements OrderAdapter.OrderClickListener {

    public static final int LIST = 0;
    public static final int GRID = 1;

    private RecyclerView list;
    private OrderAdapter adapter;

    private View noResults;
    private View noLogin;
    private View retry;
    private TextView userEmail;
    private ImageView listViewIcon;
    private ImageView gridViewIcon;

    private ArrayList<Order> orderList;

    private String email;

    private boolean isViewAvailable = false;

    public Orders() {
        // Required empty public constructor
    }

    /**
     * Create a new instance of this fragment
     * @return A new instance of fragment Orders.
     */

    public static Orders newInstance() {
        return new Orders();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListener.onFragmentStart(Main.FRAGMENT_TAG_ORDERS);

        noResults = view.findViewById(R.id.noResults);
        retry = view.findViewById(R.id.retry);
        noLogin = view.findViewById(R.id.noLogin);
        userEmail = (TextView) view.findViewById(R.id.userEmail);

        listViewIcon = (ImageView) view.findViewById(R.id.listViewIcon);
        gridViewIcon = (ImageView) view.findViewById(R.id.gridViewIcon);

        gridViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdapter(GRID);
            }
        });
        listViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdapter(LIST);
            }
        });
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO TODO use current page.
                view.setVisibility(View.GONE);
                getOrders(1);
            }
        });
        view.findViewById(R.id.ordersLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.requestSignIn(Main.FRAGMENT_TAG_ORDERS);
            }
        });
        isViewAvailable = true;
        setOrderList(view);
        updateUI();
    }

    private void updateUI() {
        if (noLogin == null || mListener == null)
            return;
        if (User.isLogged(getActivity())) {
            noLogin.setVisibility(View.GONE);
            if (!BuildConfig.TITLE.equals("Disco") && !BuildConfig.TITLE.equals("Devoto") && !BuildConfig.TITLE.equals("Geant")) {
                userEmail.setVisibility(View.VISIBLE);
                email = User.getInstance(getActivity()).getEmail();
                userEmail.setText(Utils.isEmpty(email) ? "" : email);
            }
            getOrders(1);
        } else
            //Log in
            requestSignIn(false);
    }

    private void requestSignIn(boolean signUserOut) {
        noLogin.setVisibility(View.VISIBLE);
        userEmail.setVisibility(View.GONE);
        if (signUserOut)
            mListener.signOut();
        mListener.requestSignIn(Main.FRAGMENT_TAG_ORDERS);
    }

    @Override
    public void backFromSignIn(){
        updateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (list != null)
            list.setAdapter(null);
        list = null;
        adapter = null;
        noResults = null;
        noLogin = null;
        retry = null;
        userEmail = null;
        orderList = null;
        listViewIcon = null;
        gridViewIcon = null;
        email = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean requiresLoggedUser() {
        return true;
    }

    private void setOrderList(View view) {
        orderList = new ArrayList<>();
        list = (RecyclerView) view.findViewById(R.id.list);
        setAdapter(BuildConfig.LIST_LAYOUT ? LIST : GRID);
    }

    private void setAdapter(int type) {
        //RESET
        adapter = null;
        list.setLayoutManager(null);
        list.setAdapter(null);

        RecyclerView.LayoutManager lm = type == LIST ?
                new LinearLayoutManager(getActivity()) :
                new GridLayoutManager(getActivity(), 2);
        lm.setAutoMeasureEnabled(true);
        list.setLayoutManager(lm);

        listViewIcon.setEnabled(type == GRID);
        gridViewIcon.setEnabled(type == LIST);

        adapter = new OrderAdapter(orderList, type == GRID ? R.layout.order_item :
                R.layout.order_item_horizontal, this, type == GRID);

        list.setHasFixedSize(true);
        list.setItemAnimator(new FadeInAnimator());
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setDuration(500);
        alphaAdapter.setFirstOnly(false);
        alphaAdapter.setInterpolator(new OvershootInterpolator(.5f));
        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        scaleAdapter.setFirstOnly(false);

        list.setNestedScrollingEnabled(true);
        list.setAdapter(scaleAdapter);
    }

    private void getOrders(int page) {
        if (!isViewAvailable)
            return;
        if (mListener != null && page == 1)
            mListener.onLoadingStart();
        API.getOrders(User.getInstance(getActivity()), page, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Activity activity = getActivity();
                if (activity == null)
                    return;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showRetry(false);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful() && response.code() == 401) {
                    onUnauthorized();
                    return;
                }
                try {
                    JSONObject JSONResponse = new JSONObject(response.body().string());
                    JSONArray list = JSONResponse.getJSONArray("orders");
                    for (int i = 0; i < list.length(); i++) {
                        JSONObject item = list.getJSONObject(i);
                        if (!Utils.isEmpty(BuildConfig.ORDER_HOSTNAME_FILTER)) {
                            String hostname = item.optString("hostname");
                            if (!BuildConfig.ORDER_HOSTNAME_FILTER.equals(hostname))
                                continue;
                        }
                        Order order = new Order();
                        order.id = item.getString("orderId");
                        order.sequence = item.getString("sequence");
                        order.date = item.getString("creationDate");
                        order.status = item.getString("status");
                        order.userEmail = email;
                        order.paymentName = item.optString("paymentNames");
                        order.total = item.getDouble("totalValue") / 100f;
                        if (orderList != null)
                            orderList.add(order);
                    }
                } catch(JSONException e) {
                    //Handle error
                }
                Activity activity = getActivity();
                if (activity == null || activity.isFinishing() || !isViewAvailable) {
                    orderList = null;
                    return;
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (orderList == null)
                            return;
                        if (orderList.isEmpty())
                            Utils.fadeIn(noResults);
                            //accountDetails.setVisibility(View.GONE);
                        else
                            noResults.setVisibility(View.GONE);
                        if (mListener != null)
                            mListener.onLoadingStop();
                        // Set personal data.
                        adapter.updateOrders(orderList);
                        //gridView.invalidate();
                    }
                });
            }
        });
    }

    private void onUnauthorized() {
        final Activity activity = getActivity();
        User user = User.getInstance(activity);
        if (Utils.isEmpty(user.getSignInToken())) {
            requestSignIn(true);
            return;
        }
        User.save(user, activity, new com.fizzmod.vtex.interfaces.Callback() {
            @Override
            public void run(Object isCallSuccessful) {
                if ((Boolean) isCallSuccessful)
                    getOrders(1);
                else
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            requestSignIn(true);
                        }
                    });
            }

            @Override
            public void run(Object data, Object data2) {
                // Nothing to do
            }
        });
    }

    public void showRetry(boolean error) {
        if (retry == null)
            return;
        if (mListener != null)
            mListener.onLoadingStop();
        ((TextView) retry.findViewById(R.id.retryText)).setText(error ? R.string.errorOccurred : R.string.noConnection);
        Utils.fadeIn(retry);
    }

    /**********************
     * OrderClickListener *
     **********************/

    @Override
    public void onOrderClicked(Order order) {
        DataHolder.getInstance().setOrder(order);
        mListener.onOrderSelected(order.id);
    }

}
