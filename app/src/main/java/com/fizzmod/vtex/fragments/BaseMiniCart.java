/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */
package com.fizzmod.vtex.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.adapters.MinicartAdapter;
import com.fizzmod.vtex.animations.ProgressBarAnimation;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.interfaces.ApiCallback;
import com.fizzmod.vtex.interfaces.MinicartCallback;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Promotion;
import com.fizzmod.vtex.models.Sku;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.Animations;
import com.fizzmod.vtex.utils.PromotionsHandler;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class BaseMiniCart extends BaseFragment {

    private static Call totalizersCall;
    private static Call minimumCartValueCall;
    private static int apiRetries = 0;

    public static MinicartAdapter minicartAdapter = null;

    private RelativeLayout minicart;
    private RelativeLayout totalizersLoading;
    private View totalizersBuyButton;
    private LinearLayout  minicartFooter;
    private LinearLayout minimumCartValueLayout;
    private TextView totalizersDiscounts;
    private TextView totalizersSubtotal;
    private TextView totalizersTotal;
    private TextView minimumCartValueTextView;
    private Button checkoutButton;
    private View blankSeparator;

    private boolean emptyPressed = false;
    private boolean expandingFooter = true;
    private boolean collapsingFooter = false;

    private double minimumCartValue = -1;
    private long lastAction = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_minicart, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onViewCreated(view, savedInstanceState);

        final FrameLayout cartTotalizers = (FrameLayout) view.findViewById(R.id.cartTotalizers);

        totalizersDiscounts = (TextView) view.findViewById(R.id.totalizersDiscounts);
        totalizersSubtotal = (TextView) view.findViewById(R.id.totalizersSubtotal);
        totalizersTotal = (TextView) view.findViewById(R.id.totalizersTotal);
        totalizersLoading = (RelativeLayout) view.findViewById(R.id.totalizersLoading);
        minicartFooter = (LinearLayout) view.findViewById(R.id.minicartFooter);
        minimumCartValueTextView = (TextView) view.findViewById(R.id.minimumMinicartValueText);
        minimumCartValueLayout = (LinearLayout) view.findViewById(R.id.minimumCartValue);
        blankSeparator = view.findViewById(R.id.blankSeparator);
        checkoutButton = (Button) view.findViewById(R.id.checkoutButton);
        totalizersBuyButton = view.findViewById(R.id.totalizersBuy);

        view.findViewById(R.id.checkoutButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mListener.startCheckout();
            }
        });

        view.findViewById(R.id.totalizersBuy).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mListener.startCheckout();
            }
        });

        view.findViewById(R.id.emptyCart).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                emptyCart();
            }
        });

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.minicartItemsWrapper);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

               if(newState != RecyclerView.SCROLL_STATE_IDLE){
                    actionPerformed();
                    expandingFooter = false;
                    collapseFooter(cartTotalizers);
               }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if(dy > 0) {
                    actionPerformed();
                    expandingFooter = false;
                    collapseFooter(cartTotalizers);
                }
            }
        });


        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            @Override
            public void run() {

                Activity activity = getActivity();

                if(lastAction != -1 && System.currentTimeMillis() - lastAction >= 3000 && activity != null){

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            collapsingFooter = false;
                            expandFooter(cartTotalizers);
                            lastAction = -1;
                        }
                    });

                }
            }
        };
        timer.scheduleAtFixedRate(t,1000,1000);
    }

    public void initialize(){
        minicart = (RelativeLayout) getActivity().findViewById(R.id.minicart);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;

        //http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Expand footer
     */
    private void expandFooter(View view){
        if (expandingFooter)
            return;

        expandingFooter = true;

        //We set it to true to prevent clicking a recycler view item since this view is in front
        view.findViewById(R.id.totalizersBackground).setClickable(true);

        Animations.slideToTop(view, 500);
    }

    /**
     * Collapse footer
     */
    private void collapseFooter(View view){
        if (collapsingFooter)
            return;

        collapsingFooter = true;

        //We set it to false when is hidden to prevent conflicting with recycler's scroll.
        view.findViewById(R.id.totalizersBackground).setClickable(false);

        Animations.slideToBottom(view, 500);
    }

    private void emptyCart(){

        emptyPressed = true;

        if (totalizersCall != null)
            totalizersCall.cancel();

        lastAction = -1; //To Avoid expanding totalizers

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Cart.getInstance().emptyCart(activity);
        clearMiniCart(activity);
    }

    public void getMinimumCartValue() {

        Cart cart = Cart.getInstance();

        if (minimumCartValueCall != null)
            minimumCartValueCall.cancel();

        if (cart.isEmpty() || emptyPressed)
            return;

        final Activity activity = getActivity();

        if (activity == null)
            return;

        checkoutButton.setEnabled(false);
        totalizersBuyButton.setEnabled(false);
        Utils.fadeIn(totalizersLoading);

        minimumCartValueCall = cart.getMinimumCartValue(activity, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                minimumCartValueCall = null;
                if (call != null && call.isCanceled())
                    return;
                retryGetMinimumCartValue();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //Reset
                minimumCartValueCall = null;

                if (call != null && call.isCanceled())
                    return;

                String body = response.body().string();

                try {
                    JSONObject JSON = new JSONObject(body);

                    if(JSON.getInt("code") != 1)
                        minimumCartValue = 0;
                    else
                        minimumCartValue = JSON.getInt("value")/100;

                } catch (Exception e) {
                    minimumCartValue = 0;
                }

                getTotalizers();
            }
        });
    }

    protected void getTotalizers(){

        Cart cart = Cart.getInstance();

        if (totalizersCall != null)
            totalizersCall.cancel();

        if (cart.isEmpty() || emptyPressed)
            return;

        Activity activity = getActivity();

        if (activity == null)
            return;

        totalizersCall = cart.getTotalizers(activity, new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                totalizersCall = null;
                if (call != null && call.isCanceled())
                    //Don't retry if the request was cancelled (by me...). Only if it failed!
                    return;

                retryGetMinimumCartValue();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                //Reset
                totalizersCall = null;

                if (call != null && call.isCanceled())
                    return;

                String body = response.body().string();

                final HashMap<String, Integer> quantityBySku = new HashMap<String, Integer>();

                try {
                    JSONObject JSON = new JSONObject(body);

                    /**
                     * Totalizers
                     */

                    JSONArray totals = JSON.getJSONArray("totalizers");

                    final HashMap<String, Float> totalizers = new HashMap<>();
                    totalizers.put("Total", JSON.getInt("value") / 100f);

                    if (totals != null) {
                        for (int i = 0; i < totals.length(); i++){
                            JSONObject item = totals.getJSONObject(i);;
                            String ID = item.optString("id", "");
                            totalizers.put(ID, item.getInt("value") / 100f);
                        }
                    }

                    /**
                     * Items
                     */

                    JSONArray items = JSON.optJSONArray("items");

                    if (items != null) {
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject item = items.optJSONObject(i);

                            if (item != null) {
                                String itemId = item.optString("id", "-");
                                int orderFormQuantity = item.optInt("quantity", 0);

                                Integer quantity = quantityBySku.containsKey(itemId) ? quantityBySku.get(itemId) + orderFormQuantity  : orderFormQuantity;
                                quantityBySku.put(itemId, quantity);
                            }
                        }
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Activity activity = getActivity();

                                Float total = totalizers.get("Total");

                                totalizersDiscounts.setText(totalizers.containsKey("Discounts") ? Config.getInstance().formatPrice(totalizers.get("Discounts")) : "-");
                                totalizersSubtotal.setText(Config.getInstance().formatPrice(totalizers.get("Items")));
                                totalizersTotal.setText(Config.getInstance().formatPrice(total));

                                ((TextView) activity.findViewById(R.id.minicartTotal)).setText(Config.getInstance().formatPrice(total));

                                updateTotalProducts(activity, quantityBySku.size());

                                Utils.fadeOut(totalizersLoading);

                                if (minimumCartValue > 0) {
                                    ProgressBar progress = (ProgressBar) activity.findViewById(R.id.minimumMinicartValueProgress);
                                    int percent = (int) ((total / minimumCartValue) * 100);

                                    if(percent >= 100){
                                        progress.setProgressDrawable(ContextCompat.getDrawable(activity, R.drawable.minicart_progress_full));
                                    }else if(percent < 75){
                                        progress.setProgressDrawable(ContextCompat.getDrawable(activity, R.drawable.minicart_progress_low));
                                    }else progress.setProgressDrawable(ContextCompat.getDrawable(activity, R.drawable.minicart_progress));

                                    ProgressBarAnimation anim = new ProgressBarAnimation(progress, progress.getProgress(), percent);
                                    anim.setDuration(300);
                                    progress.startAnimation(anim);
                                }

                                checkoutButton.setEnabled(total >= minimumCartValue);
                                totalizersBuyButton.setEnabled(total >= minimumCartValue);
                                minimumCartValueLayout.setVisibility(minimumCartValue > 0 ? View.VISIBLE : View.GONE);
                                blankSeparator.setVisibility(minimumCartValue > 0 ? View.VISIBLE : View.GONE);
                                minimumCartValueTextView.setText(getString(R.string.minimumCartValue, Config.getInstance().formatPrice(minimumCartValue)));

                                /* Check if quantity has changed!
                                 * User selected X quantity, but there isn't enough stock,
                                 * so we set it to the quantity VTEX Order form has returned for that item!
                                 *
                                 * If quantity has changed, we update adapter, otherwise we do nothing!
                                 */
                                if(Cart.getInstance().updateItemsQuantity(quantityBySku) && minicartAdapter != null) {
                                    minicartAdapter.notifyDataSetChanged();
                                    mListener.cartUpdated();
                                    //minicartAdapter.notifyItemChanged(0);
                                }
                            }
                            catch(NullPointerException e) {
                                e.printStackTrace();
                            }
                            catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    // Reset
                    apiRetries = 0;

                } catch (JSONException e) {
                    Utils.longInfo(body);
                    Utils.log("T failed: " + e.getMessage());
                    retryGetMinimumCartValue();
                } catch (Exception e) {}
            }
        });
    }

    /**
     * Retry totalizers
     */
    private void retryGetMinimumCartValue() {

        Activity activity = getActivity();

        if(activity == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int seconds = 1;

                if(apiRetries > 5)
                    seconds = 10;
                else if(apiRetries > 2)
                    seconds = 3;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        apiRetries++;

                        getMinimumCartValue(); //Retry!

                    }
                }, 1000 * seconds);
            }
        });
    }

    public void toggle(){
        toggle(false);
    }

    public void toggle(boolean forceClose){
        if(minicart == null)
            return;

        if (!forceClose && minicart.getVisibility() == View.GONE) {
            minicart.setVisibility(View.VISIBLE);
            minicart.setAnimation(
                    Utils.getAnimation(
                            Utils.ANIMATION_IN_RIGHT_TO_LEFT,
                            minicart,
                            300
                    )
            );

            emptyPressed = false;

            ArrayList<Sku> skus = new ArrayList<>(Cart.getInstance().getItems().values());
            if (BuildConfig.PROMOTIONS_API_ENABLED && skusHaveOutdatedPromotion(skus))
                getPromotionsForSkus(skus);
            else
                getMinimumCartValue();
        }
        else if (minicart.getVisibility() == View.VISIBLE){
            minicart.startAnimation(
                    Utils.getAnimation(
                            Utils.ANIMATION_OUT_LEFT_TO_RIGHT,
                            minicart,
                            300
                    )
            );
        }
    }

    /**
     * Tracks the last action so the totalizers view will know when to pop
     */
    private void actionPerformed() {
        lastAction = System.currentTimeMillis();
    }

    private void clearMiniCart(final AppCompatActivity context) {
        if (minicartAdapter == null)
            return;

        totalizersDiscounts.setText("-");
        totalizersSubtotal.setText(Config.getInstance().formatPrice(0f));
        totalizersTotal.setText(Config.getInstance().formatPrice(0f));

        minicartAdapter.updateProducts(new LinkedHashMap<String, Sku>());
        minicartAdapter.notifyDataSetChanged();

        mListener.cartUpdated();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateMiniCart();
            }
        }, 100);
    }


    public void updateMiniCart() {
        updateMiniCart(false, true, null);
    }

    public void updateMiniCart(boolean instantiate) {
        updateMiniCart(instantiate, true, null);
    }

    public void updateMiniCart(boolean instantiate, boolean refreshAdapter, final MinicartCallback callback) {
        updateMiniCart(instantiate, refreshAdapter, false, callback);
    }

    public void updateMiniCart(boolean instantiate, boolean refreshAdapter, boolean fromAdapter, final MinicartCallback callback) {

        final AppCompatActivity context = (AppCompatActivity) getActivity();
        final RecyclerView minicartWrapper = (RecyclerView) context.findViewById(R.id.minicartItemsWrapper);

        TextView minicartQuantity = (TextView) context.findViewById(R.id.minicartQuantity);
        ImageView minicartIconImageView = (ImageView) context.findViewById(R.id.minicartIconImage);
        LinkedHashMap<String, Sku> skus = Cart.getInstance().getItems();

        if(skus.isEmpty()) {
            minicartQuantity.setVisibility(View.GONE);
            minicartIconImageView.setImageResource(R.drawable.icn_cart_toolbar_empty);
        } else {
            minicartIconImageView.setImageResource(R.drawable.icn_cart_toolbar);
            minicartQuantity.setText(String.valueOf(skus.size()));
            minicartQuantity.setVisibility(View.VISIBLE);
        }

        if (refreshAdapter) {
            if (!instantiate && minicartAdapter != null) {
                minicartAdapter.updateProducts(skus);
                minicartAdapter.notifyDataSetChanged();
            } else {

                minicartWrapper.setAdapter(null);
                minicartAdapter = null;

                minicartAdapter = new MinicartAdapter(context, skus, new com.fizzmod.vtex.interfaces.Callback() {
                    @Override
                    public void run(Object action) {

                        if(action instanceof Boolean && (Boolean) action) {
                            actionPerformed();

                            if(totalizersCall != null)
                                totalizersCall.cancel();

                            if(callback != null)
                                callback.actionPerformed();
                        }
                    }

                    @Override
                    public void run(Object pos, Object isDelete) {
                        if (minicartAdapter != null && (boolean) isDelete) {
                            minicartAdapter.remove((int) pos);
                            updateMiniCart(false, false, true, null);

                            if(minicartAdapter.getItemCount() > 0)
                                getMinimumCartValue();

                            if(callback != null)
                                callback.actionPerformed();

                        } else {
                            //Go to product
                            Sku sku = Cart.getInstance().getItemByPosition((int) pos);

                            if(callback != null)
                                callback.itemClicked(sku.getProductId());
                        }

                    }
                });

                LinearLayoutManager llm = new LinearLayoutManager(context);

                minicartWrapper.setNestedScrollingEnabled(false); //Without this the scroll inside nestedScrollView isn't smooth.
                minicartWrapper.setLayoutManager(llm);
                minicartWrapper.setHasFixedSize(false);


                minicartWrapper.setItemAnimator(new ScaleInAnimator(new OvershootInterpolator(.5f)));
                minicartWrapper.getItemAnimator().setRemoveDuration(500);

                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(minicartAdapter);
                alphaAdapter.setDuration(500);
                alphaAdapter.setFirstOnly(false);
                alphaAdapter.setInterpolator(new OvershootInterpolator(.5f));

                ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
                scaleAdapter.setFirstOnly(false);

                minicartWrapper.setAdapter(scaleAdapter);
            }
        }

        if (fromAdapter && minimumCartValue > 0) {
            //Block buttons until request with order form value ends
            checkoutButton.setEnabled(false);
            totalizersBuyButton.setEnabled(false);
        }

        if (!fromAdapter || skus.size() == 0) {
            if (skus.size() > 0)
                updateNonEmptyMinicart(context, minicartWrapper);
            else
                updateEmptyMinicart(context, minicartWrapper);
        }
    }

    protected void updateNonEmptyMinicart(AppCompatActivity context, RecyclerView minicartWrapper) {
        context.findViewById(R.id.emptyCartText).setVisibility(View.GONE);
        minicartWrapper.setVisibility(View.VISIBLE);
        Utils.fadeIn(minicartFooter);
        Utils.fadeIn(context.findViewById(R.id.cartTotalizers));
        Utils.fadeIn(context.findViewById(R.id.emptyCart)); //Empty Cart button

        if(minimumCartValue > 0)
            Utils.fadeIn(context.findViewById(R.id.minimumCartValue));
    }

    protected void updateEmptyMinicart(final AppCompatActivity context, RecyclerView minicartWrapper) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.fadeIn(context.findViewById(R.id.emptyCartText), 500);
            }
        }, 350);
        minicartWrapper.setVisibility(View.GONE);
        Utils.fadeOut(minicartFooter);
        Utils.fadeOut(context.findViewById(R.id.cartTotalizers));
        Utils.fadeOut(context.findViewById(R.id.emptyCart)); //Empty Cart button

        if(minimumCartValue > 0)
            Utils.fadeOut(context.findViewById(R.id.minimumCartValue));
    }

    private boolean skusHaveOutdatedPromotion(ArrayList<Sku> skus) {
        for (Sku sku : skus)
            if (sku.hasOutdatedPromotionsInfo())
                return true;
        return false;
    }

    private void getPromotionsForSkus(final ArrayList<Sku> skus) {
        PromotionsService.getInstance(getActivity()).getPromotions(new ApiCallback<List<Promotion>>() {
            @Override
            public void onResponse(List<Promotion> object) {
                for (Sku sku : skus)
                    PromotionsHandler.getInstance().setPromotionToSku(sku);
                updateMiniCart();
                getMinimumCartValue();
            }

            @Override
            public void onError(String errorMessage) {
                getMinimumCartValue();
            }

            @Override
            public void onUnauthorized() {
                getMinimumCartValue();
            }
        });
    }

    public void refresh(){

        if(minicart == null)
            return;

        emptyPressed = false;

        ArrayList<Sku> skus = new ArrayList<>(Cart.getInstance().getItems().values());
        if (BuildConfig.PROMOTIONS_API_ENABLED && skusHaveOutdatedPromotion(skus))
            getPromotionsForSkus(skus);
        else
            getMinimumCartValue();
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    protected void updateTotalProducts(Activity activity, int size){

    }
}
