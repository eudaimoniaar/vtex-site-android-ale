package com.fizzmod.vtex.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;

public class OnlyChannelFragment extends BackHandledFragment {

    private TextView storeNameView;
    private TextView descriptionView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.only_channel_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        storeNameView = view.findViewById(R.id.only_channel_name);
        descriptionView = view.findViewById(R.id.only_channel_description);
        Store store = Store.restore(getActivity());
        if (store != null)
            setStore(store.name);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void setStore(String storeName) {
        storeNameView.setText(storeName);
        descriptionView.setText(getString(R.string.is_the_only_branch, storeName));
    }

}
