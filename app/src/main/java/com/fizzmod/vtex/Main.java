/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex;

import android.animation.LayoutTransition;
import android.app.ActivityManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.airbnb.deeplinkdispatch.DeepLink;
import com.facebook.FacebookSdk;
import com.fizzmod.vtex.activities.CartSyncSummaryActivity;
import com.fizzmod.vtex.activities.Checkout;
import com.fizzmod.vtex.activities.ShoppingListsActivity;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.controllers.DeepLinkRouter;
import com.fizzmod.vtex.firebase.CustomFirebaseInstanceIDService;
import com.fizzmod.vtex.fragments.BackHandledFragment;
import com.fizzmod.vtex.fragments.CartSyncFragment;
import com.fizzmod.vtex.fragments.Categories;
import com.fizzmod.vtex.fragments.Coupons;
import com.fizzmod.vtex.fragments.Home;
import com.fizzmod.vtex.fragments.MiniCart;
import com.fizzmod.vtex.fragments.OnlyChannelFragment;
import com.fizzmod.vtex.fragments.OrderPage;
import com.fizzmod.vtex.fragments.Orders;
import com.fizzmod.vtex.fragments.ProductOptionsFragment;
import com.fizzmod.vtex.fragments.ProductPage;
import com.fizzmod.vtex.fragments.SalesChannelsSelector;
import com.fizzmod.vtex.fragments.Search;
import com.fizzmod.vtex.fragments.ShoppingListDetailFragment;
import com.fizzmod.vtex.fragments.ShoppingListsFragment;
import com.fizzmod.vtex.fragments.SignIn;
import com.fizzmod.vtex.fragments.VtexSignIn;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.interfaces.MinicartCallback;
import com.fizzmod.vtex.interfaces.NavigationViewListener;
import com.fizzmod.vtex.interfaces.OnFragmentInteractionListener;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.interfaces.SearchBarListener;
import com.fizzmod.vtex.interfaces.ToolbarListener;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.models.User;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.AnalyticsUtils;
import com.fizzmod.vtex.utils.BaseScrollHandler;
import com.fizzmod.vtex.utils.DataHolder;
import com.fizzmod.vtex.utils.ScrollHandler;
import com.fizzmod.vtex.utils.UpdateChecker;
import com.fizzmod.vtex.utils.Utils;
import com.fizzmod.vtex.views.CustomNavigationView;
import com.fizzmod.vtex.views.CustomToolbar;
import com.fizzmod.vtex.views.CustomUpdateDialog;
import com.fizzmod.vtex.views.FavoriteCartelMessage;
import com.fizzmod.vtex.views.SearchOverlay;
import com.fizzmod.vtex.views.ShoppingCartelMessage;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.pushwoosh.Pushwoosh;
import com.pushwoosh.tags.Tags;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.StatsSnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

import static android.view.View.VISIBLE;
import static com.fizzmod.vtex.activities.ShoppingListsActivity.USER_LISTS_MODIFIED_MANY;
import static com.fizzmod.vtex.activities.ShoppingListsActivity.USER_LISTS_MODIFIED_NAMES;

public class Main extends AppCompatActivity
        implements
        OnFragmentInteractionListener,
        SalesChannelListener,
        BackHandledFragment.BackHandlerInterface,
        NavigationViewListener,
        ToolbarListener,
        SearchBarListener {


    public static final String LOG = "VT";

    public static final int CAMERA_PERMISSION = 1;
    public static final int CHECKOUT_ACTIVITY = 325;
    public static final int CREATE_SHOPPING_LISTS_ACTIVITY = 327;
    public static final int EDIT_SHOPPING_LISTS_ACTIVITY = 328;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int INTENT_TYPE_PRODUCT = 1;
    public static final int INTENT_TYPE_SEARCH = 2;
    public static final int COUPONS_INSTRUCTIONS_ACTIVITY = 666;

    public static final String FRAGMENT_TAG_CATEGORIES = "CATEGORIES";
    public static final String FRAGMENT_TAG_HOME = "HOME";
    public static final String FRAGMENT_TAG_STORES = "STORES";
    public static final String FRAGMENT_TAG_SEARCH = "SEARCH";
    public static final String FRAGMENT_TAG_PRODUCT = "PRODUCT";
    public static final String FRAGMENT_TAG_FAVOURITES = "FAVOURITES";
    public static final String FRAGMENT_TAG_LISTS = "LISTS";
    public static final String FRAGMENT_TAG_TERMS = "TERMS";
    public static final String FRAGMENT_TAG_CARD_REQUEST = "CARD_REQUEST";
    public static final String FRAGMENT_TAG_CONTACT = "CONTACT";
    public static final String FRAGMENT_TAG_SCANNER = "SCANNER";
    public static final String FRAGMENT_TAG_ORDERS = "ORDERS";
    public static final String FRAGMENT_TAG_ORDER = "ORDER";
    public static final String FRAGMENT_TAG_SIGN_IN = "SIGN_IN";
    public static final String FRAGMENT_TAG_VTEX_SIGN_IN = "VTEX_SIGN_IN";
    public static final String FRAGMENT_TAG_CART_SYNC = "CART_SYNC";
    public static final String FRAGMENT_TAG_PRODUCTS_LIST = "PRODUCTS_LIST";
    public static final String FRAGMENT_TAG_PRODUCT_OPTIONS = "PRODUCT_OPTIONS";
    public static final String FRAGMENT_TAG_COUPONS = "COUPONS";

    private String fragmentRequestingSignIn = null;
    private DrawerLayout drawer;
    private RelativeLayout progress;
    private RelativeLayout minicart;
    private CustomToolbar customToolbar;
    private RelativeLayout salesChannelOverlay;
    private RelativeLayout onlyChannelOverlay;
    private SearchOverlay searchOverlay;
    private ShoppingCartelMessage shoppingCartelMessage;
    private FavoriteCartelMessage favoriteCartelMessage;
    private CustomNavigationView customNavigationView;
    private AppBarLayout appBarLayout;
    private BaseScrollHandler scrollHandler;


    private boolean isBackButtonEnabled = true;

    private BackHandledFragment selectedFragment;

    /**
     * The {@link Tracker} used to record screen views.
     */
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // [START shared_tracker]
        // Obtain the shared Tracker instance.
        CustomApplication application = (CustomApplication) getApplication();
        mTracker = application.getDefaultTracker();
        // [END shared_tracker]

        User.destroyInstance();

        customToolbar = (CustomToolbar) findViewById(R.id.customToolbar);
        customToolbar.setToolbarListener(this);

        setUI(savedInstanceState);

        onNewIntent(getIntent());

        //Check if firebase token should be sent to the server
        CustomFirebaseInstanceIDService.checkRegister(this);

        printDebugData();

        FacebookSdk.sdkInitialize(getApplicationContext());

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);


        // This complements xml attribute animateLayoutChanges="true" and makes the animation for the search bar work properly
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ((ViewGroup) findViewById(R.id.app_bar_layout)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }

        scrollHandler = new ScrollHandler(customToolbar);

        registerForPushwooshNotifications();
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
        setCurrentStore(null);

        restoreMinicart(true);

        Log.d("UpdateChecker", "Update check is active: " + Config.getInstance().doesCheckForUpdates());
        if (Config.getInstance().doesCheckForUpdates()) {
            askForUpdates(true);
        }
    }

    /**
     * Print memory information
     */

    private void printMemoryData() {

        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.d(Main.LOG, "maxMemory: " + Utils.humanReadableByteCount(maxMemory, false));

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        Log.d(Main.LOG, "memoryClass: " + Integer.toString(memoryClass) + "MB");
    }

    /**
     * Print debug data
     */

    private void printDebugData() {

        if (!BuildConfig.DEBUG)
            return;

        printMemoryData();
        Utils.getDimensions(this);

    }

    private boolean checkPlayServices() {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Utils.log("This device is not supported.");
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (!isBackButtonEnabled)
            return;

        if (Config.getInstance().hasMultipleSalesChannels()) {
            BackHandledFragment backHandledFragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.salesChannelFragment);

            if (backHandledFragment != null && backHandledFragment.onBackPressed())
                return;

            if (salesChannelOverlay != null && salesChannelOverlay.getVisibility() == VISIBLE) {
                closeOverlay(salesChannelOverlay);
                return;
            }

            if (onlyChannelOverlay != null && onlyChannelOverlay.getVisibility() == VISIBLE) {
                closeOverlay(onlyChannelOverlay);
                return;
            }
        }

        if (searchOverlay != null && searchOverlay.getVisibility() == VISIBLE) {
            searchOverlay.closeOverlay();
            return;
        }

        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else if (minicart.getVisibility() == VISIBLE) {
            getMinicart().toggle();
        } else if (selectedFragment == null || !selectedFragment.onBackPressed()) {
            if (getFragmentManager().getBackStackEntryCount() > 1) {
                if (selectedFragment != null && selectedFragment.getTag().equals(FRAGMENT_TAG_CART_SYNC)) {
                    getFragmentManager().popBackStackImmediate();
                    getMinicart().toggle();
                } else
                    getFragmentManager().popBackStack();
                return;
            }

            if (getFragmentManager().getBackStackEntryCount() > 0)
                getFragmentManager().popBackStack();
            super.onBackPressed();
        }

    }

    @Override
    public void onNewIntent(Intent intent) {

        if (handleDeepLinkIntent(intent))
            return;


        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("NotificationMessage")) {
                String notificationMessage = extras.getString("NotificationMessage");

                if (notificationMessage == null)
                    return;

                //TODO notify server of notification received!

                try {
                    JSONObject data = new JSONObject(notificationMessage);

                    int intentType = data.getInt("type");

                    if (intentType == INTENT_TYPE_PRODUCT) {
                        DataHolder.getInstance().setForceGetProduct(true);
                        startProduct(ProductPage.ARG_PRODUCT, data.getString("id"));
                    } else if (intentType == INTENT_TYPE_SEARCH) {
                        String title = data.optString("title", null);
                        startSearchFragment(Utils.isEmpty(title) ? Search.TYPE_QUERY : Search.TYPE_TITLE, data.getString("query"), title, null);
                    }

                } catch (JSONException e) {
                } catch (Exception e) {
                }

            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startScanner();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.scannerCameraPermissionReject), Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        switch (requestCode) {

            case SignIn.RC_SIGN_IN:
            case SignIn.VTEX_SIGN_IN_ACTIVITY:
                /**
                 * Sign in result
                 * We need to call fragment onActivityResult to login the client correctly
                 */
                SignIn signInFragment = (SignIn) getFragmentManager().findFragmentById(R.id.fragmentContent);
                signInFragment.onActivityResult(requestCode, resultCode, data);
                break;

            case CHECKOUT_ACTIVITY:
                handleCheckoutResult(data);
                break;

            case CREATE_SHOPPING_LISTS_ACTIVITY:
                // If this happens it means ShoppingListsFragment is being shown
                ShoppingListsFragment userListFragment = (ShoppingListsFragment) getFragmentManager()
                        .findFragmentById(R.id.fragmentContent);
                if (userListFragment != null)
                    userListFragment.onActivityResult(requestCode, resultCode, data);
                break;

            case EDIT_SHOPPING_LISTS_ACTIVITY:
                if (resultCode != RESULT_OK || shoppingCartelMessage == null || data == null) {
                    if (resultCode == RESULT_CANCELED && data != null && data.hasExtra(ShoppingListsActivity.REQUEST_SIGN_IN)) {
                        signOut();
                        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragmentContent);
                        requestSignIn(fragment != null ? fragment.getTag() : null);
                    }
                    return;
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        shoppingCartelMessage.setText(data.getStringExtra(USER_LISTS_MODIFIED_NAMES));
                        shoppingCartelMessage.setPlural(data.getBooleanExtra(USER_LISTS_MODIFIED_MANY, false));
                        shoppingCartelMessage.display();
                    }
                }, Utils.TRASLATION_DURATION);
                break;

            case CartSyncSummaryActivity.REQUEST_CODE:
                if (resultCode != RESULT_OK)
                    return;
                getMinicart().toggle();
                openFragment(CartSyncFragment.newInstance(), FRAGMENT_TAG_CART_SYNC);
                break;

            default:
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                //After coming back from activity, reinstantiate actionbar_icon

                if (result != null) {

                    /** Scanner activity result **/

                    if (minicart != null && minicart.getVisibility() == VISIBLE)
                        getMinicart().toggle(true);

                    if (result.getContents() != null)
                        startProduct(ProductPage.ARG_EAN, result.getContents());

                } else
                    super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    public void handleCheckoutResult(Intent data) {

        //Back from checkout, update actionbar_icon
        getMinicart().updateMiniCart();
        getMinicart().getMinimumCartValue();

        if (data == null || data.getExtras() == null)
            return;

        Bundle result = data.getExtras();

        /**
         * Handle order finished
         */

        if (result.getBoolean(Checkout.RESULT_CLEAR_KEY, false)) {
            //After the sale is complete clear fragment stack
            getMinicart().toggle(true);
            clearFragmentStack();
        }

        String navigateTo = result.getString(Checkout.RESULT_FRAGMENT_KEY, "");

        switch (navigateTo) {
            case FRAGMENT_TAG_HOME:
                openHomeFragment();
                break;
            case FRAGMENT_TAG_PRODUCT:
                String productLink = result.getString(Checkout.RESULT_PRODUCT_LINK_KEY, null);
                if (productLink != null)
                    startProduct(ProductPage.ARG_PRODUCT_LINK, productLink);
                break;
            case FRAGMENT_TAG_ORDERS:
                openFragment(Orders.newInstance(), FRAGMENT_TAG_ORDERS);
                break;
        }
    }

    /**
     * Clear fragment stack
     */

    public void clearFragmentStack() {

        //Clear full stack if order was finished
        FragmentManager manager = getFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /**
     * Setup UI
     */

    public void setUI(Bundle savedInstanceState) {

        setNavigationDrawer();

        progress = (RelativeLayout) findViewById(R.id.progress);
        minicart = (RelativeLayout) findViewById(R.id.minicart);

        searchOverlay = (SearchOverlay) findViewById(R.id.search_overlay);
        searchOverlay.setSearchBarListener(this);

        shoppingCartelMessage = (ShoppingCartelMessage) findViewById(R.id.shopping_cartel);
        favoriteCartelMessage = (FavoriteCartelMessage) findViewById(R.id.favorite_cartel);

        if (Config.getInstance().hasMultipleSalesChannels())
            salesChannelOverlay = (RelativeLayout) findViewById(R.id.salesChannelOverlay);
            onlyChannelOverlay = (RelativeLayout) findViewById(R.id.onlyChannelOverlay);
        if (savedInstanceState == null) {

            openHomeFragment();

            if (User.isLogged(this)) {
                String email = User.getInstance(this).getEmail();

                customNavigationView.setUserEmail(email);
            } else {
                customNavigationView.onSignOut();
            }

        }

        /** Sales Channel Selector **/
        findViewById(R.id.salesChannelSelector).setVisibility(Config.getInstance().hasMultipleSalesChannels() ? VISIBLE : View.GONE);

        getMinicart().initialize();

    }

    public void setNavigationDrawer() {
        ImageView hamburgerIcon = (ImageView) findViewById(R.id.hamburgerIcon);

        hamburgerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        customNavigationView = (CustomNavigationView) findViewById(R.id.customNavigationView);
    }

    /**
     * Go to Categories fragment
     *
     * @param v
     */
    public void goToCategories(View v) {
        customNavigationView.checkDrawerItem(FRAGMENT_TAG_CATEGORIES);
        openFragment(new Categories(), FRAGMENT_TAG_CATEGORIES);
    }

    /**
     * Go to Coupons fragment
     *
     * @param v
     */

    public void goToCoupons(View v) {
        customNavigationView.checkDrawerItem(FRAGMENT_TAG_COUPONS);
        openFragment(new Coupons(), FRAGMENT_TAG_COUPONS, false);
    }

    /**
     * Show search progress
     */

    public void showProgress() {
        progress.setVisibility(VISIBLE);
    }

    /**
     * Hide Search progress
     */
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    /**
     * Toggle a view and show it's content
     *
     * @param v
     */

    public void toggleContent(final View v) {

        final View parent = (View) v.getParent();
        final View view = ((View) v.getParent()).findViewWithTag("toggle");
        final View change = ((View) v.getParent()).findViewWithTag("change");
        final boolean status;

        if (view instanceof View) {

            if (view.getVisibility() == VISIBLE) {
                Utils.collapse(view, 300);
                status = false;
            } else {
                Utils.expandView(view, 300);
                status = true;
            }

            if (change != null)
                change.setEnabled(status);

            parent.requestFocus();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    ScrollView scroll = (ScrollView) findViewById(R.id.productScroll);
                    if (scroll instanceof ScrollView)
                        Utils.smoothScroll(scroll, parent.getTop(), 300);

                }
            }, 300);

        }
    }

    /**
     * Toggle a view and show it's content
     *
     * @param v
     */

    public void toggleExpand(final View v) {

        final View parent = (View) v.getParent();
        final View view = parent.findViewWithTag("toggle");
        final View change = parent.findViewWithTag("change");


        final boolean status;
        if (view instanceof View) {
            if (view.getVisibility() == VISIBLE) {
                Utils.collapse(view, 300);
                status = false;
            } else {
                Utils.expandView(view, 300);
                status = true;
            }

            if (change != null)
                change.setEnabled(status);

        }
    }


    /**
     * Handle sign out click
     */
    @Override
    public void signOut() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                customNavigationView.onSignOut();
                User.logout(Main.this);
                // Detect if it's a "logged required" fragment and navigate to home
                BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
                if (fragment != null && fragment.requiresLoggedUser()) {
                    clearFragmentStack();
                    openHomeFragment();
                }
            }
        });
    }

    /**
     * Handle sign in click
     */

    public void signIn() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                openFragment(SignIn.newInstance(false), FRAGMENT_TAG_SIGN_IN);

            }
        }, 275);

    }

    /**
     * Fragment Listeners
     */

    @Override
    public void onCategorySelected(Integer category, Integer group) {
        String title = category == null ? getResources().getString(R.string.allCategories) : null;

        if (group != null)
            startCategoryFragment(group);
        else
            startSearchFragment(category != null ? Search.TYPE_CATEGORY : Search.TYPE_ALL_CATEGORIES, null, title, category);
    }

    @Override
    public void onLoadingStop() {
        hideProgress();
    }

    @Override
    public void onLoadingStart() {
        showProgress();
    }

    @Override
    public void onProductSelected(String id) {
        startProduct(ProductPage.ARG_PRODUCT, id);
    }

    @Override
    public void onProductLinkSelected(String productLink) {
        startProduct(ProductPage.ARG_PRODUCT_LINK, productLink);
    }

    @Override
    public void onProductAdded() {

        getMinicart().updateMiniCart();
        customToolbar.startMinicartIconAnimation();
    }

    @Override
    public void onReorder() {
        onProductAdded();
        refreshMinicart();
    }

    @Override
    public void onOrderSelected(String id) {
        openFragment(OrderPage.newInstance(id), FRAGMENT_TAG_ORDER);
    }

    @Override
    public void onFragmentStart(String tag) {
        Utils.log("Fragment Started: " + tag);
        hideProgress();

        getMinicart().toggle(true);

        if (tag == FRAGMENT_TAG_HOME) {
            customNavigationView.checkDrawerItem(tag);
        } else
            customToolbar.showSearchIcon();

        customNavigationView.checkDrawerItem(tag);

        if (BuildConfig.DEBUG) {
            StatsSnapshot picassoStats = Picasso.with(this).getSnapshot();
            Utils.log(picassoStats.toString());
        }

    }

    @Override
    public void startCheckout() {
        boolean needToUpdate = false;
        if (Config.getInstance().doesCheckForUpdates()) {
            needToUpdate = askForUpdates(true);
        }
        if (!needToUpdate) {
            AnalyticsUtils.logEcommercePurchaseEvent(this);
            Intent intent = new Intent(Main.this, Checkout.class);
            startActivityForResult(intent, CHECKOUT_ACTIVITY);
        }
    }

    @Override
    public void startVtexSignIn() {
        fragmentRequestingSignIn = FRAGMENT_TAG_SIGN_IN;
        openFragment(VtexSignIn.newInstance(), FRAGMENT_TAG_VTEX_SIGN_IN);
    }

    @Override
    public void performQuery(String query, String title) {
        startSearchFragment(Search.TYPE_QUERY, query, title, null);
    }

    @Override
    public void performQuery(String query, String title, int type) {
        startSearchFragment(type, query, title, null);
    }

    @Override
    public void requestSearch() {
        searchOverlay.openSearch();
    }

    @Override
    public void cartUpdated() {
        triggerCartUpdated();
    }

    /**
     * ShoppingListsFragment
     */

    @Override
    public void onCreateListPress() {
        ShoppingListsActivity.start(this);
    }

    @Override
    public void onAddProductToList(String sku, String parentFragment) {
        if (User.isLogged(this))
            ShoppingListsActivity.start(this, sku);
        else
            requestSignIn(parentFragment);
    }

    @Override
    public void onListSelected(ShoppingList shoppingList) {
        ShoppingListDetailFragment fragment = new ShoppingListDetailFragment();
        fragment.setProductList(shoppingList);
        openFragment(fragment, FRAGMENT_TAG_PRODUCTS_LIST, false);
    }

    @Override
    public void onBannerPress(String bannerStrackerId) {
        Utils.log(bannerStrackerId);
        mTracker.send(new HitBuilders.EventBuilder()
                .setAction("Click")
                .setCategory("BannerApp")
                .setLabel(bannerStrackerId)
                .build()
        );
    }

    @Override
    public void onSignIn(boolean fromVtex) {

        getFragmentManager().popBackStack();

        AnalyticsUtils.logLoginEvent(this, fromVtex);

        if (fromVtex) //VTEX sign in launches one aditional fragment
            getFragmentManager().popBackStack();

        if (fragmentRequestingSignIn == null)
            openHomeFragment();

        try {
            BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
            fragment.backFromSignIn();
        } catch (NullPointerException e) {
        }


        User user = User.getInstance(this);

        //Send
        API.registerUser(this, new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //

                Utils.log(response.body().string());
            }
        });

        customNavigationView.setUserEmail(user.getEmail());

        sendPushWooshTags(user.getEmail());
    }

    @Override
    public void onSignOut() {
        getFragmentManager().popBackStack();

        if (fragmentRequestingSignIn == null)
            openHomeFragment();

        findViewById(R.id.logout).setVisibility(View.GONE);
        Utils.fadeIn(findViewById(R.id.login));
        findViewById(R.id.navigationProfile).setVisibility(View.GONE);

        customNavigationView.onSignOut();
    }

    @Override
    public void requestSignIn(String fragment) {
        if (this.selectedFragment != null && this.selectedFragment.getTag().equals(FRAGMENT_TAG_SIGN_IN)) {
            return;
        }
        fragmentRequestingSignIn = fragment;
        openFragment(SignIn.newInstance(false), FRAGMENT_TAG_SIGN_IN);
    }

    @Override
    public void closeFragment() {
        try {
            getFragmentManager().popBackStack();
        } catch (Exception ignore) {
        }
    }

    @Override
    public void performFullTextQuery(String query) {
        searchOverlay.closeOverlay();
        startSearchFragment(Search.TYPE_FULL_TEXT_QUERY, query, null, null);
    }

    @Override
    public void showCartSyncSummary() {
        startActivityForResult(
                new Intent(this, CartSyncSummaryActivity.class),
                CartSyncSummaryActivity.REQUEST_CODE
        );
    }

    @Override
    public void onFavoriteProduct(boolean favoriteAdded) {
        favoriteCartelMessage.setText(favoriteAdded);
        favoriteCartelMessage.display();
    }

    /**
     * End Fragment Listeners
     */

    /**
     * Start the search Fragment
     *
     * @param type
     * @param query
     * @param categoryId
     */

    public void startSearchFragment(int type, String query, String title, Integer categoryId) {
        openFragment(Search.newInstance(type, query, title, categoryId), FRAGMENT_TAG_SEARCH);
    }

    /**
     * Start Product Fragment
     *
     * @param type
     * @param id
     */

    public void startProduct(String type, String id) {
        openFragment(ProductPage.newInstance(type, id), FRAGMENT_TAG_PRODUCT);
    }

    /**
     * Start Product Fragment
     */

    public void openHomeFragment() {

        /**
         * Get current fragment
         */

        try {
            BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);

            if (fragment instanceof Home) {
                if (minicart.getVisibility() == VISIBLE)
                    getMinicart().toggle(true);
                if (getFragmentManager().getBackStackEntryCount() > 0)
                    return;
            }
        } catch (NullPointerException e) {
        }

        openFragment(Home.newInstance(), FRAGMENT_TAG_HOME, true);
        customNavigationView.checkDrawerItem(FRAGMENT_TAG_HOME);
    }


    /**
     * Start the search Fragment
     */

    public void startCategoryFragment(Integer group) {
        openFragment(Categories.newInstance(group), FRAGMENT_TAG_CATEGORIES);
    }

    /**
     * Start the given fragment
     *
     * @param fragment
     * @param tag
     */

    public void openFragment(Fragment fragment, String tag) {
        openFragment(fragment, tag, true);
    }

    public void openFragment(final Fragment fragment, final String tag, final boolean uncheckDrawerItem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = getFragmentManager();

                try {
                    isBackButtonEnabled = false; //Disable back when fragment is performing it's animation

                    fragmentManager
                            .beginTransaction()
                            .setCustomAnimations(R.animator.anim_left, R.animator.anim_right, R.animator.anim_left, R.animator.anim_right)
                            .replace(R.id.fragmentContent, fragment, tag)
                            .addToBackStack(null)
                            .commit();

                    if (uncheckDrawerItem)
                        customNavigationView.uncheckDrawerItem();

                    if (mTracker != null) {
                        mTracker.setScreenName("" + tag);
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isBackButtonEnabled = true;
                        }
                    }, 510); //Enable back button after fragment animation ended to avoid crash.

                } catch (Exception e) {
                    isBackButtonEnabled = true;
                }
            }
        });
    }

    /**
     * Dismiss Keyboard
     *
     * @return
     */

    @Override
    public void dismissKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        findViewById(R.id.clearFocus).requestFocus();
    }


    /**
     * Set selected Fragment
     *
     * @param selectedFragment
     */

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        this.selectedFragment = selectedFragment;
    }

    /**
     * Start camera click listener
     *
     */

    public void startScanner(View v) {
        startScanner();
    }

    public void startScanner() {

        if (ContextCompat.checkSelfPermission(Main.this,
                android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(Main.this,
                    new String[]{android.Manifest.permission.CAMERA},
                    CAMERA_PERMISSION);

        } else {

            AnalyticsUtils.logScanOpenEvent(this);

            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);

            //Vertical Camera scan is only available on Android 6.0 or greater.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                integrator.setCaptureActivity(ScannerActivity.class);

            integrator.setOrientationLocked(false);
            integrator.setPrompt("");
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.initiateScan();

        }
    }


    /***********************************************************************************************************************
     *
     *                                          SALES CHANNEL CODE
     *
     ***********************************************************************************************************************/


    /**
     * Set current sales channel
     */

    public void setCurrentStore(Store store) {

        if (!Config.getInstance().hasMultipleSalesChannels())
            return;

        boolean shouldSave = store != null;

        if (store == null) {
            //Restore
            store = Store.restore(this);

            if (store == null)
                return;
        }

        Cart.getInstance().setSalesChannel(store.getSalesChannel());

        customNavigationView.setStoreName(store.getName());
        ((OnlyChannelFragment) getFragmentManager().findFragmentById(R.id.onlyChannelFragment))
                .setStore(store.name);

        closeOverlay(salesChannelOverlay);

        if (shouldSave) {
            Store.save(store, this);
            //restoreMinicart(true);
        }


    }


    /**
     * Close Store Selector overlay
     *
     * @param view
     */

    public void closeOverlay(View view) {
        if (view.getId() == searchOverlay.getId())
            searchOverlay.closeOverlay();
        else {
            SalesChannelsSelector salesChannelsSelector = (SalesChannelsSelector) getFragmentManager().findFragmentById(R.id.salesChannelFragment);
            if (salesChannelsSelector != null)
                salesChannelsSelector.onBackPressed();
            Utils.fadeOut(view);
            dismissKeyboard();
        }
    }

    /**
     * Open Store finder/selector
     *
     * @param view
     */
    public void changeStore(View view) {
        if (BuildConfig.CURRENTLY_ONLY_ONE_STORE)
            Utils.fadeIn(onlyChannelOverlay);
        else
            Utils.fadeIn(salesChannelOverlay);
        closeDrawers();
    }


    @Override
    public void salesChannelSelected(Store store) {
        setCurrentStore(store);

        /**
         * Refresh current fragment
         */

        try {
            BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
            fragment.refresh();
        } catch (NullPointerException e) {
        }


        /**
         * Refresh Minicart
         */
        refreshMinicart();

    }


    /**
     * Trigger cart updated in current fragment
     */

    public void triggerCartUpdated() {
        try {
            BackHandledFragment fragment = (BackHandledFragment) getFragmentManager().findFragmentById(R.id.fragmentContent);
            fragment.cartUpdated();
        } catch (NullPointerException e) {
        }
    }

    /**
     * Get Minicart
     *
     * @return
     */
    public MiniCart getMinicart() {
        return (MiniCart) getFragmentManager().findFragmentById(R.id.minicartFragment);
    }

    /**
     * Restore Minicart
     */

    public void restoreMinicart(boolean force) {
        findViewById(R.id.minicartProgress).setVisibility(VISIBLE);
        Cart.getInstance().restore(this, force, new Callback() {
            @Override
            public void run(final Object refresh) {

                Main.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /**
                         * Minicart Restore ended, get totalizers.
                         */
                        triggerCartUpdated();

                        try {

                            getMinicart().getMinimumCartValue();

                            if ((boolean) refresh) {
                                MiniCart.minicartAdapter = null;

                                getMinicart().updateMiniCart(true, true, new MinicartCallback() {

                                    /**
                                     * Minicart Product clicked!
                                     * @param productId
                                     */

                                    @Override
                                    public void itemClicked(String productId) {
                                        getMinicart().toggle();
                                        DataHolder.getInstance().setForceGetProduct(true);
                                        startProduct(ProductPage.ARG_PRODUCT, productId);
                                    }

                                    @Override
                                    public void actionPerformed() {
                                        triggerCartUpdated();
                                    }

                                });
                            }
                            findViewById(R.id.minicartProgress).setVisibility(View.GONE);
                        } catch (NullPointerException e) {
                        }
                    }
                });
            }

            @Override
            public void run(Object data, Object data2) {
            }
        });
    }

    /**
     * Refresh Minicart
     */

    public void refreshMinicart() {
        Cart.getInstance().refresh(this, new Callback() {
            @Override
            public void run(Object data) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getMinicart().updateMiniCart();
                        getMinicart().getMinimumCartValue();

                    }
                });
            }

            @Override
            public void run(Object data, Object data2) {

            }
        });
    }

    /**
     * Handle Deep Links navigations
     *
     * @param intent
     * @return
     */
    public boolean handleDeepLinkIntent(Intent intent) {

        if (!intent.getBooleanExtra(DeepLink.IS_DEEP_LINK, false))
            return false;

        Bundle parameters = intent.getExtras();

        String url = parameters != null ? parameters.getString("deep_link_uri") : null;
        Uri uri = url != null ? Uri.parse(url) : null;

        if (uri != null && !Utils.isEmpty(uri.getQueryParameter("utm_campaign")) && mTracker != null) {
            Utils.log("Tracking in Analytics: " + url);
            mTracker.send(new HitBuilders.ScreenViewBuilder()
                    .setCampaignParamsFromUrl(url)
                    .build()
            );
        }

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            referrer = getReferrer();
            Utils.log("Referer : " + referrer.toString());
        }*/

        if (DeepLinkRouter.ACTION_DEEP_LINK_PRODUCT.equals(intent.getAction())) {
            startProduct(
                    ProductPage.ARG_PRODUCT_LINK,
                    parameters != null ? parameters.getString("productLink") : null);
            return true;
        }

        if (DeepLinkRouter.ACTION_DEEP_LINK_SEARCH.equals(intent.getAction())) {
            startSearchFragment(
                    Search.TYPE_QUERY,
                    "?" + (uri != null ? uri.getQuery() : ""),
                    null,
                    null);
            return true;
        }

        // You can pass a query parameter with the URI, and it's also in parameters, like
        // dld://classDeepLink?qp=123
        //if (parameters.containsKey("qp"))
        //Utils.log(" with query parameter " + parameters.getString("qp"));

        return false;
    }

    @Override
    public void hideToolbar() {
        appBarLayout.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_OUT_BOTTOM_TO_TOP,
                        appBarLayout, 300
                )
        );
    }

    @Override
    public void showToolbar() {
        appBarLayout.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_IN_TOP_TO_BOTTOM,
                        appBarLayout, 300
                )
        );
    }

    public void synchronizeCart(View v) {
        if (User.isLogged(this))
            showCartSyncSummary();
        else
            requestSignIn(null);
    }

    @Override
    public void onProductOptionsClicked(Product product) {
        openFragment(ProductOptionsFragment.newInstance(product), FRAGMENT_TAG_PRODUCT_OPTIONS);
    }

    @Override
    public void onMenuItemSelected(String tag, Class fragmentClass) {

        hideProgress();

        Fragment fragment;

        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();

                if (minicart.getVisibility() == VISIBLE)

                    getMinicart().toggle();

                openFragment(fragment, tag, false);

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void closeMinicart() {
        if (minicart.getVisibility() == View.VISIBLE)
            getMinicart().toggle(true);
    }

    @Override
    public void onSignInClicked() {
        requestSignIn(selectedFragment.getTag());
    }

    @Override
    public void onSignOutClicked() {
        signOut();
        sendPushWooshTags(null);
    }

    @Override
    public void closeDrawers() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onScannerClicked() {
        startScanner();
    }

    @Override
    public void onMinicartClicked() {
        if (selectedFragment != null && selectedFragment.getTag().equals(FRAGMENT_TAG_CART_SYNC))
            getFragmentManager().popBackStackImmediate();
        getMinicart().toggle();
    }

    @Override
    public void onLogoClicked() {
        openHomeFragment();
    }

    @Override
    public void onSearchClicked() {
        requestSearch();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void addScrollListener(View view) {
        scrollHandler.addScrollListener(view);
    }

    @Override
    public void onViewScrollUp() {
        customToolbar.onScrollUp();
    }

    @Override
    public void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void reloadToolbarLayout() {
        customToolbar.loadHomeLayout();
    }

    @Override
    public void onSearchFromSearchOverlay(String query) {
        performFullTextQuery(query);
    }

    @Override
    public void onSearchFromHomeFragment(String query) {
        performFullTextQuery(query);
    }

    @Override
    public void dismissKeyboardFromToolbar() {
        dismissKeyboard();
    }

    public void keepShopping(View view) {
        Utils.fadeOut(onlyChannelOverlay);
    }

    public boolean askForUpdates(boolean isFromCheckout) {
        boolean needToUpdate = new UpdateChecker().needToUpdateApp();
        if (needToUpdate) {
            final CustomUpdateDialog updateDialog = new CustomUpdateDialog(Main.this);
            updateDialog.setCanceledOnTouchOutside(false);

            updateDialog.setTxtAccept(R.string.update_accept, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!BuildConfig.FLAVOR.equals(Config.getInstance().getDevFlavorName())) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                        }
                    }
                    updateDialog.dismiss();
                }
            });

            if (isFromCheckout) {
                updateDialog.setCancelListener(new View.OnClickListener (){
                    @Override
                    public void onClick(View view){
                        updateDialog.dismiss();
                        finish();
                    }
                });
            }
            updateDialog.show();
        }
        return needToUpdate;
    }

    public void registerForPushwooshNotifications() {
        if (hasPushWooshClient()) {
            Pushwoosh.getInstance().registerForPushNotifications();
        }
    }

    public void sendPushWooshTags(String tag) {
        if (hasPushWooshClient()) {
            Pushwoosh.getInstance().sendTags(Tags.stringTag(getResources().getString(R.string.pushwoosh_tag), tag));
        }
    }

    private boolean hasPushWooshClient() {
        return !getResources().getString(R.string.fcm_sender_id).isEmpty() &&
                !getResources().getString(R.string.pushwoosh_app_id).isEmpty();
    }
}

//https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer
