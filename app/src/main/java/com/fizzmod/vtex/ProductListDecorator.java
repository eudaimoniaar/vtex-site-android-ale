package com.fizzmod.vtex;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by marcos on 11/06/16.
 */
public class ProductListDecorator extends RecyclerView.ItemDecoration {

    private final int margin;

    public ProductListDecorator(int margin) {
        this.margin = margin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.right = margin;

        if (parent.getChildAdapterPosition(view) == 0)
            outRect.left = margin;
    }
}