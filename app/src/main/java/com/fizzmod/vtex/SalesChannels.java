/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.fragments.BackHandledFragment;
import com.fizzmod.vtex.fragments.SalesChannelsSelector;
import com.fizzmod.vtex.interfaces.SalesChannelListener;
import com.fizzmod.vtex.models.Cart;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.service.PromotionsService;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.Request;
import com.fizzmod.vtex.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class SalesChannels extends AppCompatActivity implements
        SalesChannelListener,
        BackHandledFragment.BackHandlerInterface  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.PROMOTIONS_API_ENABLED)
            PromotionsService.getInstance(this).initializePromotions(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!Utils.isEmpty(BuildConfig.VTEX_HOST_API))
                                getVtexHostApi();
                            else
                                initialize();
                        }
                    });
                }
            });
        else if (!Utils.isEmpty(BuildConfig.VTEX_HOST_API))
            getVtexHostApi();
        else
            initialize();
    }

    @Override
    public void onBackPressed(){
        SalesChannelsSelector salesChannelsSelector = (SalesChannelsSelector) getFragmentManager().findFragmentById(R.id.salesChannelFragment);
        if (salesChannelsSelector != null && salesChannelsSelector.onBackPressed())
            return;
        super.onBackPressed();
    }

    @Override
    public void salesChannelSelected(Store store) {
        Store.save(store, this);
        Cart.getInstance().setSalesChannel(store.getSalesChannel());
        launchMainActivity();
    }

    @Override
    public void setSelectedFragment(BackHandledFragment backHandledFragment) {
        // Nothing to do
    }

    public boolean dismissKeyboard(){
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch(NullPointerException e) {
            Utils.log("SanesChannel 'dismissKeyboard' failed: " + e.getMessage());
        }
        findViewById(R.id.clearFocus).requestFocus();
        return true;
    }

    /*******************
     * Private methods *
     *******************/

    private void initialize() {
        boolean launchActivity = true;
        if (Config.getInstance().hasMultipleSalesChannels() && !BuildConfig.CURRENTLY_ONLY_ONE_STORE) {
            Store store = Store.restore(this);
            if(store != null)
                Cart.getInstance().setSalesChannel(store.getSalesChannel());
            else
                launchActivity = false;
        }
        if (launchActivity) {
            launchMainActivity();
            return;
        }
        setContentView(R.layout.activity_sales_channels);
        setUI();
    }

    private void setUI() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    private void launchMainActivity() {
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
        finish();
    }

    private void getVtexHostApi() {
        Request.get(BuildConfig.VTEX_HOST_API, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Utils.log("SalesChannels 'launchMainActivity' failed: " + e.getMessage());
                onError();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                try {
                    JSONObject JSON = new JSONObject(body);
                    API.HOST = JSON.getString(BuildConfig.VTEX_HOST_API_ENV);
                    Config.getInstance().baseUrlChanged(API.HOST);
                    Log.d("VTEX API HOST", API.HOST);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initialize();
                        }
                    });
                    return;
                } catch (JSONException e) {
                    Utils.longInfo(body);
                    Utils.log("SalesChannels 'launchMainActivity' JSON error: " + e.getMessage());
                } catch (Exception e) {
                    Utils.longInfo(body);
                    Utils.log("SalesChannels 'launchMainActivity' error: " + e.getMessage());
                }
                onError();
            }

            private void onError() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SalesChannels.this, R.string.get_vtex_host_error, Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
            }
        });
    }

    @Override
    public void addScrollListener(View view) {
        // Nothing to do
    }

    @Override
    public void onViewScrollUp() {
        // Nothing to do
    }
}
