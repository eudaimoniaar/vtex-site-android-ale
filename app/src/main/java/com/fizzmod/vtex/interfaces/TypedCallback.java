package com.fizzmod.vtex.interfaces;

public interface TypedCallback<T> {

    void run(T data);

}
