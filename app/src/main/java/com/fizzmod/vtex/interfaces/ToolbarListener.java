package com.fizzmod.vtex.interfaces;

import android.view.View;

public interface ToolbarListener {

    void onScannerClicked();

    void onMinicartClicked();

    void onLogoClicked();

    void onSearchClicked();

    void performFullTextQuery(String query);

    void showKeyboard(View view);

    void dismissKeyboardFromToolbar();
}
