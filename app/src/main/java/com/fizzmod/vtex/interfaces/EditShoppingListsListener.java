package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.ShoppingListsModifications;

public interface EditShoppingListsListener {

    void onNewList();

    void onCloseEditShoppingLists();

    void onSaveLists(ShoppingListsModifications modifications);

}
