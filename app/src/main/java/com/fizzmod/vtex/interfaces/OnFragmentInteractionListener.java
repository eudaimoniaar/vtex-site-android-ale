/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.ShoppingList;

/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p/>
 * See the Android Training lesson <a href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</a> for more information.
 */
public interface OnFragmentInteractionListener {
    void onCategorySelected(Integer category, Integer isGroup);
    void onLoadingStop();
    void onLoadingStart();
    void onProductSelected(String id);
    void onProductLinkSelected(String productLink);
    void onOrderSelected(String id);
    void onProductAdded();
    void onReorder();
    void onFragmentStart(String fragment);
    void startCheckout();
    void startVtexSignIn();
    void performQuery(String query, String title);
    void performQuery(String query, String title, int type);
    void onSignIn(boolean fromVtex);
    void onSignOut();
    void closeFragment();
    void requestSignIn(String fragment);
    void requestSearch();
    void cartUpdated();
    void hideToolbar();
    void showToolbar();
    void showCartSyncSummary();
    void onCreateListPress();
    void onListSelected(ShoppingList shoppingList);
    void onAddProductToList(String sku, String parentFragment);
    void onFavoriteProduct(boolean favoriteAdded);
    void onBannerPress(String bannerStrackerId);
    void signOut ();
    void onProductOptionsClicked(Product product);
    void reloadToolbarLayout();
    void onSearchFromHomeFragment(String query);
}
