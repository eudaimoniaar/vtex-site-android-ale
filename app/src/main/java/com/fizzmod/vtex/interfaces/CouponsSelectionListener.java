package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.Coupon;

import java.util.List;

public interface CouponsSelectionListener extends BaseCouponsListener {

    void onTermsDialogClicked();

    void onCouponButtonClicked(String couponCode, final int position, final List<Coupon> coupons);

    void onInstructionsClicked();

    void onPhysicalPrintClicked();
}
