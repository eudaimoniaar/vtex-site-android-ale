package com.fizzmod.vtex.interfaces;

public interface NavigationViewListener {

    void onMenuItemSelected(String tag, Class fragmentClass);

    void startScanner();

    void onSignInClicked();

    void onSignOutClicked();

    void closeMinicart();

    void closeDrawers();
}


