package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.ShoppingList;

public interface UserListAdapterListener {

    void onAddToCart(ShoppingList shoppingList);

    void onListSelected(ShoppingList shoppingList);

    void onLongClick();

}
