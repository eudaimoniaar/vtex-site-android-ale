package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.Product;

public interface ShoppingListAdapterInterface {

    void onLongClick();

    void onClick(Product product);

}
