/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.interfaces;

/**
 * Created by marcos on 12/06/16.
 */
public interface MinicartCallback {
    void itemClicked(String productId);
    void actionPerformed();
}