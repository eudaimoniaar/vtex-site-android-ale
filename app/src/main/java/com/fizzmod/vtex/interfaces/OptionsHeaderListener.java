package com.fizzmod.vtex.interfaces;

public interface OptionsHeaderListener {

    void onBack();

    void onSelectAll();

    void onDeleteSelected();

    void onAddSelectedToCart();
}
