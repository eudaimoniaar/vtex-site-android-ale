/*
 * LICENSE
 * Copyright (C) Marcos Casagrande - All Rights Reserved | Todos los derechos reservados
 * Unauthorized use/copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Marcos Casagrande <marcoscvp90@gmail.com>, January 2016
 */

package com.fizzmod.vtex.interfaces;

import com.fizzmod.vtex.models.Store;

/**
 * Created by marcos on 03/08/16.
 */
public interface SalesChannelListener {
    void salesChannelSelected(Store store);
}
