package com.fizzmod.vtex.interfaces;

public interface SearchBarListener {

    void dismissKeyboard();
    void onSearchFromSearchOverlay(String query);
}
