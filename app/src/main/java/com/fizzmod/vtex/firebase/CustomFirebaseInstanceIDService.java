package com.fizzmod.vtex.firebase;


/**
 * Created by marcos on 23/06/16.
 */
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context;
import android.os.Build;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.config.Config;
import com.fizzmod.vtex.utils.API;
import com.fizzmod.vtex.utils.DeviceIdFactory;
import com.fizzmod.vtex.utils.MySharedPreferences;
import com.fizzmod.vtex.utils.Request;
import com.fizzmod.vtex.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.fizzmod.vtex.utils.API.X_APP_PACKAGE;


public class CustomFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    public final static String URL =  Utils.isEmpty(BuildConfig.CUSTOMER_API) ? API.JANIS_API +  "app" :  BuildConfig.CUSTOMER_API + "app/register";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.log("Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.


       sendRegistration(getApplicationContext(), token);
    }

    public static void sendRegistration(Context context, String token){

        final MySharedPreferences mySharedPreference = new MySharedPreferences(context);
        mySharedPreference.saveToken(token);

        HashMap<String, String> parameters = new HashMap<>();
        HashMap<String, String> headers = Config.getInstance().getExtraHeaders();

        parameters.put("firebaseId", token);
        parameters.put("deviceId", new DeviceIdFactory(context).getDeviceId());
        parameters.put("package", BuildConfig.APPLICATION_ID);
        parameters.put("versionName", BuildConfig.VERSION_NAME);
        parameters.put("versionCode", String.valueOf(BuildConfig.VERSION_CODE));
        parameters.put("osVersion", Build.VERSION.RELEASE);
        parameters.put("os", "Android");
        parameters.put("manufacturer", Build.MANUFACTURER);
        parameters.put("model", Build.MODEL);

        headers.put("Client-StoreName", BuildConfig.STORENAME);
        headers.put(X_APP_PACKAGE, BuildConfig.APPLICATION_ID);


        Request.post(URL, parameters, headers, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mySharedPreference.setRetry(true);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.code() != 200) {
                    Utils.log("Can't send a token to the server " + response.body().string());

                    mySharedPreference.setRetry(true);
                } else {
                    Utils.log("Token succesfully send to server");

                    mySharedPreference.saveNotificationSubscription(true);
                    mySharedPreference.setRetry(false);
                }
            }
        });
    }


    public static void checkRegister(Context context){
        MySharedPreferences mySharedPreference = new MySharedPreferences(context);

        if(mySharedPreference.shouldRetry()){

            String token = mySharedPreference.getToken();
            Utils.log("Token: " + token);
            if(!Utils.isEmpty(token)){
                sendRegistration(context, token);
            }
        }
    }
}