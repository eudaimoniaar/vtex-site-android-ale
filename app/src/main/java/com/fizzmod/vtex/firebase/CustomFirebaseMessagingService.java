package com.fizzmod.vtex.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.fizzmod.vtex.BuildConfig;
import com.fizzmod.vtex.Main;
import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.Store;
import com.fizzmod.vtex.utils.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pushwoosh.PushwooshFcmHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CustomFirebaseMessagingService extends FirebaseMessagingService{
    private static final String TAG = CustomFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Utils.log("From: " + remoteMessage.getFrom());
        //Utils.log("Notification Message Body: " + remoteMessage.getData().get("extra"));

        if (PushwooshFcmHelper.isPushwooshMessage(remoteMessage)) {
            PushwooshFcmHelper.onMessageReceived(this, remoteMessage);
        } else {
            sendNotification(
                remoteMessage.getData().get("body"),
                remoteMessage.getData().get("title"),
                remoteMessage.getData().get("extra")
            );
        }
    }

    private void sendNotification(String message, String title, String extra) {

        /**
         * Extra: {"query":"?fq=H:153","title":"Ofertas del DOT!","type":2,"stores":["1019","1002"]}
         * Extra: {"product":"523","type":1,"stores":["1019","1002"]}
         * Extra: {"stores":["1019","1002"]}
         */

        if(extra != null) {

            try {
                Store store = Store.restore(getApplicationContext());

                JSONObject data = new JSONObject(extra);

                if(data.has("stores") && !data.isNull("stores")) {
                    ArrayList<String> stores = Utils.JSONArrayToArrayList(data.getJSONArray("stores"));

                    // If user hasn't selected any store yet or current store does not match notifications stores
                    if(store == null || !stores.contains(store.getId())) {
                        // DO NOTHING, NOTIFICATION IS NOT MEANT FOR USERS FROM OTHER STORES
                        return;
                    }
                }

            }
            catch (JSONException e) {}
            catch (Exception e) {}
        }


        Intent intent = new Intent(this, Main.class);
        intent.putExtra("NotificationMessage", extra);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icn_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.icn_notification_big))
                .setColor(ContextCompat.getColor(this, R.color.notification))
                .setContentTitle(title != null ? title : BuildConfig.TITLE)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}