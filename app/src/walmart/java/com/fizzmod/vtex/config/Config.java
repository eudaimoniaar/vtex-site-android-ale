package com.fizzmod.vtex.config;

import com.fizzmod.vtex.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Config extends com.fizzmod.vtex.config.BaseConfig {

    private static Config config = null;

    public static Config getInstance() {
        if (config == null)
            config = new Config();

        return config;
    }

    private Config() {
        super();
        setContactURL("http://www.walmart.com.ar/institucional/contacto");
        setTermsUrl("http://www.walmart.com.ar/institucional/atencion-al-cliente#item-1");
        setDealsCollection("?fq=H:1347");
        setBestSellingCollection("?fq=H:1348");
        setOurBrandsCollection("?fq=H:2403");
        setOurBrandsQACollection("?fq=H:213");
        setHasMultipleSalesChannels(true);
        setCategoryGrouping(true);
        setExcludedCategories(Arrays.asList(62));
    }

    @Override
    public String getDevFlavorName() {
        return "walmartDev";
    }

    @Override
    public Map<String, Integer> getClientPromosMap() {
        Map<String, Integer> hashImage = new HashMap<>();
        hashImage.put("aun mas bajo", R.drawable.ic_aun_mas_bajo);
        hashImage.put("imbatible", R.drawable.ic_imbatible);
        hashImage.put("nuestras marcas", R.drawable.ic_nuestras_marcas);
        return hashImage;
    }

    @Override
    public boolean doesCheckForUpdates() {
        return true;
    }
}
