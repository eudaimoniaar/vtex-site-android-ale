package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class ProductDetailLayout extends BaseProductDetailLayout {

    public ProductDetailLayout(@NonNull Context context) {
        this(context, null);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
