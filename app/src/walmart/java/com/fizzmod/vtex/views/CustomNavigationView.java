package com.fizzmod.vtex.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.models.DrawerItem;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CATEGORIES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_CONTACT;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_FAVOURITES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_HOME;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_LISTS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_ORDERS;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_STORES;
import static com.fizzmod.vtex.Main.FRAGMENT_TAG_TERMS;

public class CustomNavigationView extends BaseCustomNavigationView {

    public CustomNavigationView(Context context) {
        this(context, null);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        signOut = (LinearLayout) findViewById(R.id.logout);
        signIn = (LinearLayout) findViewById(R.id.login);
        setClickListeners();
    }

    @Override
    public void selectItem(String fragmentTag) {
        switch (fragmentTag) {
            case FRAGMENT_TAG_LISTS :
                selectItem(fragmentTag, com.fizzmod.vtex.fragments.ShoppingListsFragment.class);
                break;
            default:
                super.selectItem(fragmentTag);
                return;
        }
    }


    @Override
    protected void addMenuItems() {
        super.addMenuItems();
        menuItems.add(new DrawerItem(R.drawable.icon_lists_on
                , R.drawable.icon_lists_off
                , R.string.drawerMenuItemLists
                , FRAGMENT_TAG_LISTS
                , false ));
    }


    @Override
    protected void rearrangeMenuItems() {
        List<DrawerItem> rearrangedMenuItems = new ArrayList<>();

        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_HOME));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CATEGORIES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_FAVOURITES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_LISTS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_STORES));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_TERMS));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_CONTACT));
        rearrangedMenuItems.add(findItem(FRAGMENT_TAG_ORDERS));

        menuItems = rearrangedMenuItems;
    }
}
