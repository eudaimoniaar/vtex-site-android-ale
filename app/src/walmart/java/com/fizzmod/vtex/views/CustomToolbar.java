package com.fizzmod.vtex.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class CustomToolbar extends BaseCustomToolbar {

    public CustomToolbar(Context context) {
        this(context, null);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        scannerIcon.setVisibility(VISIBLE);
    }

    @Override
    public void onScrollUp() {
        // Nothing to do
    }
}
