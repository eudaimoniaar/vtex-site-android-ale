package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.Callback;
import com.fizzmod.vtex.models.Sku;

public class MinicartHighlightAdapterViewholder extends BaseMinicartAdapterViewHolder {

    private TextView minicartPriceHighlight;

    MinicartHighlightAdapterViewholder(ViewGroup parent, Callback callback) {
        super(parent, callback);
        minicartPriceHighlight = (TextView) itemView.findViewById(R.id.minicartPriceHighlight);
    }

    @Override
    public void setView(Sku sku) {
        super.setView(sku);

        if (sku.hasStock()) {
            if (sku.showPriceDiffPercentage()) {
                minicartPriceHighlight.setText(sku.getPriceDiffPercentageFormatted(itemView.getContext()));
                minicartPriceHighlight.setVisibility(View.VISIBLE);
            } else
                minicartPriceHighlight.setVisibility(View.GONE);
        } else
            minicartPriceHighlight.setVisibility(View.GONE);
    }
}
