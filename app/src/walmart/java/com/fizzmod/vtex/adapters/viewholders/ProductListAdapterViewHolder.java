package com.fizzmod.vtex.adapters.viewholders;

import android.view.View;
import android.widget.TextView;

import com.fizzmod.vtex.R;
import com.fizzmod.vtex.interfaces.ProductListCallback;
import com.fizzmod.vtex.models.Product;
import com.fizzmod.vtex.models.Sku;

public class ProductListAdapterViewHolder extends FabProductListAdapterViewHolder {

    private TextView labelName;

    public ProductListAdapterViewHolder(View itemView, ProductListCallback clickListener) {
        super(itemView, clickListener);
        labelName = (TextView) itemView.findViewById(R.id.product_item_text_label);
    }

    @Override
    public void setView(int position, Product product, boolean isOrderPage, boolean isRelatedProductList) {
        super.setView(position, product, isOrderPage, isRelatedProductList);
        Sku sku = product.getMainSku();
        //sets the product label
        if (sku.hasCurrentPromotions() && sku.hasLabelPromotion()) {
            labelName.setText(sku.getLabelName().trim());
            labelName.setVisibility(View.VISIBLE);
        } else
            labelName.setVisibility(View.GONE);
    }

    @Override
    int getMenuButtonId() {
        return R.id.product_item_fab_menu_button;
    }

    @Override
    int getOptionsButtonId() {
        return R.id.product_options_button;
    }

}
