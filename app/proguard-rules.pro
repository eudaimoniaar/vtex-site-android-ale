# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/marcos/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes SourceFile,LineNumberTable
-keep class com.google.** { *; }
-keep class com.android.support.** { *; }
-keep class com.squareup.** { *; }
-keep class com.nineoldandroids.** { *; }
-keep class com.daimajia.** { *; }
-keep class com.github.** { *; }
-keep class com.journeyapps.** { *; }
-keep class org.apache.** { *; }
-keep class jp.wasabeef.** { *; }
-keep class com.fizzmod.vtex.models.** { *; }
-keep class com.fizzmod.vtex.service.params.** { *; }
-keep class com.fizzmod.vtex.service.response.** { *; }

## Joda Time 2.3
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *; }

-keep class com.airbnb.deeplinkdispatch.** { *; }
-keepclasseswithmembers class * {
     @com.airbnb.deeplinkdispatch.DeepLink <methods>;
}


# Suppress warnings if you are NOT using IAP:
-dontwarn com.google.**
-dontwarn org.apache.**
-dontwarn com.android.support.**
-dontwarn com.squareup.**
-dontwarn okio.*
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**
-dontwarn retrofit2.Platform$Java8


