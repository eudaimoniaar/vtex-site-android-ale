(function(window, undefined) {
	var self;
	var Checkout = {

		email: "{{email}}", //This will be replaced by the APP with the user's email.

		initialize: function() {

			self = this;

			self.loadCSS();

			if(~location.href.indexOf("email"))
			    self.fillEmail();

			self.addEvent(window, "hashchange", self.hashChange);

			self.addXMLRequestCallback(self.handleXMLRequestCallback);

			try {
			    document.body.classList.add("android-app");
			} catch(e) {}

		},

		addEvent: function(object, type, callback) {
			if (object === null || typeof(object) == 'undefined') return;
			if (object.addEventListener) {
				object.addEventListener(type, callback, false);
			} else if (object.attachEvent) {
				object.attachEvent("on" + type, callback);
			} else {
				object["on" + type] = callback;
			}
		},

		addXMLRequestCallback: function(callback) {
			var oldSend, i;
			if (XMLHttpRequest.callbacks) {
				// we've already overridden send() so just add the callback
				XMLHttpRequest.callbacks.push(callback);
			} else {
				// create a callback queue
				XMLHttpRequest.callbacks = [callback];
				// store the native send()
				oldSend = XMLHttpRequest.prototype.send;
				// override the native send()
				XMLHttpRequest.prototype.send = function() {
					// call the native send()
					oldSend.apply(this, arguments);

					var sendArguments = arguments;
					this.onload = function() {
						for (i = 0; i < XMLHttpRequest.callbacks.length; i++) {
							XMLHttpRequest.callbacks[i](this, sendArguments);
						}
					};
				}
			}
		},

		handleXMLRequestCallback: function(xhr, data) {
			if (xhr == undefined)
				return;

			if (typeof xhr.responseURL != "undefined" && xhr.responseURL.match("/items/update") && xhr.status == 200 && typeof xhr.responseText != "undefined") {
				try {
					var orderItems = JSON.parse(data[0]).orderItems;

					for (var i = 0; i < orderItems.length; i++) {

						//Call Android Method
						if (typeof BHAndroid == "object" && typeof BHAndroid.itemUpdated == "function")
							BHAndroid.itemUpdated(orderItems[i].id, orderItems[i].quantity);
					}
				} catch (e) {}

			}
		},

		hashChange: function(event) {
			var hash = window.location.hash.toLowerCase();
			if (~hash.indexOf("email")) {
				self.fillEmail();
			}
		},

		fillEmail: function() {
			if (self.email) {
				$("#client-pre-email").val(self.email).change().blur();
				$("#btn-client-pre-email").click();
			}
		},

		loadCSS: function() {
			var css = "";

            //Hide all email related elements as they will be autocompleted.
			if (self.email) {
				css += ".link-logout-container { display: none!important; }";
				css += "fieldset.pre-email { display: none!important; }";
				css += ".box-client-info-pf .client-email { display: none!important; }";
			}

            css += '#vtexIdContainer ul.vtexIdUI-providers-list li[ng-repeat="provider in auth.oAuthProviders"]  { display: none!important; }'; //It bugs in WebView

			var head = document.head || document.getElementsByTagName('head')[0];
			var style = document.createElement('style');

			style.type = 'text/css';
			if (style.styleSheet)
				style.styleSheet.cssText = css;
			else
				style.appendChild(document.createTextNode(css));

			head.appendChild(style);
		}
	};


	Checkout.initialize();



})(window);