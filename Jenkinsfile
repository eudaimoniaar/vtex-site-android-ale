pipeline {
  agent {
    docker {
      image 'vidueirof/jnlp-slave-jdk8'
      args '-e USER_HOME=/home/jenkins -e ANDROID_HOME=/usr/local/android/sdk -v /home/jenkins/.gradle:/home/jenkins/.gradle -v /home/jenkins/android_home:/usr/local/android/sdk -v /home/jenkins/.signing:/home/jenkins/.signing -v /home/jenkins/.android:/home/jenkins/.android'
    }
  }
  environment {
    GRADLE_OPTS='-Dorg.gradle.jvmargs=-Xmx3g -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8'
  }
  stages {
    stage('Clean') {
      steps {
        sh './gradlew clean'
      }
    }
    stage('Build PR') {
      when {
        not {
            anyOf {
                branch 'master';
                branch 'develop'

            }
        }
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex.dev/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }

        sh './gradlew assembleDebug -PpreDexEnable=false'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Build develop') {
      when {
        branch 'develop'
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex.dev/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }

        sh './gradlew assembleDebug -PpreDexEnable=false'
        sh './gradlew assembleRelease -PpreDexEnable=false'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Build Release') {
      when {
        branch 'master'
      }
      steps {
        script {
          def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.bighouseapps.vtex/next"
          env.BUILD_NUMBER = response.content
          currentBuild.displayName = "#" + env.BUILD_NUMBER
        }

        sh './gradlew assembleDebug'
        sh './gradlew assembleRelease -PpreDexEnable=false'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Clean Gradle') {
      steps {
        sh './gradlew clean'
      }
    }
  }
  post {
    success {
      emailext(subject: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'", to: 'santiago@eudaimonia.com.ar, masuello@eudaimonia.com.ar', body: """<p>SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                  <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""", recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']])
    }

    failure {
      emailext(subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'", body: """<p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                  <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""", recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']])
    }
  }
}