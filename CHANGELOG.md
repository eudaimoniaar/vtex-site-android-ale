# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] 2020-08-27 (Geant)
### Added
- Geant flavor

## [2.8.5] 2020-07-16 (Walmart)
## [1.5.4] 2020-07-16 (Changomas)
### Changed
- Adjust product item layout's image size and margins.

## [1.4.1] 2020-08-20 (Devoto)
## [1.4.1] 2020-08-20 (Disco)
### Changed
- Update list of categories displayed in Categories screen.

## [1.4.0] 2020-08-13 (Devoto)
## [1.4.0] 2020-08-13 (Disco)
### Added
- Search coupons by text.

## [2.8.4] 2020-08-05 (Walmart)
## [1.5.3] 2020-08-05 (Changomas)
## [1.6.7] 2020-08-05 (Tata)
## [1.3.4] 2020-08-05 (Devoto)
## [1.3.3] 2020-08-05 (Disco)
### Added
- Support for banners with product links.

## [1.3.3] 2020-08-05 (Devoto)
### Changed
- Show all categories (no filter applied).

## [2.8.3] 2020-07-08 (Walmart)
## [1.5.2] 2020-07-08 (Changomas)
## [1.6.6] 2020-07-08 (Tata)
## [1.3.2] 2020-07-08 (Devoto)
## [1.3.2] 2020-07-08 (Disco)
### Changed
- Adjust auto-cycle times for banner sliders for better UX.

## [2.8.2] 2020-07-08 (Walmart)
## [1.5.1] 2020-07-08 (Changomas)
## [1.6.5] 2020-07-08 (Tata)
## [1.3.1] 2020-07-08 (Devoto)
## [1.3.1] 2020-07-08 (Disco)
### Fixed
- Stores data in cache is valid for 24hs.

## [1.5.0] 2020-06-30 (Changomas)
### Changed
- Full text Search API provider.

## [2.8.1] 2020-06-08 (Walmart)
## [1.4.10] 2020-06-08 (Changomas)
## [1.6.4] 2020-06-08 (Tata)
### Fixed
- Stores screen crashing on Android 9 and higher

## [1.3.0] 2020-03-19 (Disco & Devoto)
### Added
- Push notifications support (Pushwoosh).

## [2.8.0] 2020-03-17 (Walmart)
### Added
- Instagram support.
### Changed
- Youtube link.
- Youtube & Facebook icons.

## [1.2.1] 2020-03-17 (Disco & Devoto)
### Changed
- Disco & Devoto Coupons terms and conditions texts.

## [1.2.0] 2019-12-20 (Disco & Devoto)
### Added
- Disco & Devoto Coupons feature

## [1.4.9] 2020-01-21 (Changomas)
### Fixed
- Changed my order style.

## [2.7.0] 2020-01-21 (Walmart)
### Added
- Pushwoosh test user deeplink feature.
### Fixed
- Changed my order style.

## [1.6.3] 2020-01-2 (TATA)
### Fixed
- Library versions updated.
- Login flow bug fixed.
- Order status now supports 2 lines.
- My orders screen layout fixed to fit status text.

## [1.4.8] 2020-01-2 (Changomas)
### Fixed
- Library versions updated.
- Login flow bug fixed.
- Order status now supports 2 lines.
- My orders screen layout fixed to fit status text.

## [2.6.4] 2020-01-2 (Walmart)
### Fixed
- Library versions updated.
- Login flow bug fixed.
- Order status now supports 2 lines.
- No longer supports cart synchronize
- My orders screen layout fixed to fit status text.

## [1.1.2] 2019-12-26 (Disco & Devoto)
### Fixed
- Google login caching removed to allow new account login.

## [1.1.1] 2019-12-23 (Disco & Devoto)
### Fixed
- Faulty automatically added permission removed.

## [1.1.0] 2019-12-20 (Disco & Devoto)
### Added
- Disco & Devoto store selector implemented
### Fixed
- Library versions updated.
- Login flow bug.
- My orders screen layout fixed to fit status text.
- Devoto & Disco home products additive margin bug fixed.

## [2.6.3] 2019-09-31 (Walmart)
### Fixed
- Apply filters crash.
### TODO
- More TODO of 2.6.2.

## [2.6.2] 2019-09-30 (Walmart)
### Fixed
- Back button crash.
### TODO
- Update all libraries to latest version, especially support libraries and recyclerview-animators.

## [2.6.1] 2019-09-30 (Walmart)
### Fixed
- Category screen pager crash due to old package.

## [2.6.0] 2019-09-28 (Walmart)
### Added
- Pushwoosh functionality.

## [2.5.7] 2019-08-30 (Walmart)
## [1.4.7] 2019-08-30 (Changomas)
### Added
- Check app version and notify user to update if necessary.

## [2.5.6] 2019-08-09 (Walmart)
### Changed
- Full text search API for products search feature.

## [1.0.0] 2019-08-09 (Disco)
### Added
- Disco flavor

## [1.4.6] 2019-07-15 (Changomas)
### Changed
- Automatically logs in to Salta store
- If user tries to change stores a pop up tells them that it's not possible at the time

## [1.0.1] 2019-07-15 (Devoto)
### Changed
- Devoto endpoint URL

## [1.0.0] 2019-06-25 (Devoto)
### Added
- Devoto Flavor

## [2.5.5] 2019-05-17 (Walmart)
## [1.4.5] 2019-05-17 (Changomas)
## [1.6.2] 2019-05-17 (Tata)
### Changed
- Changomás promotion endpoints
- Visual improvements
### Fixed
- Promotion visualization

## [2.4.4] 2019-04-10 (Walmart)
## [1.3.2] 2019-04-10 (Changomas)
## [1.5.1] 2019-04-10 (Tata)
- Vtex images should come in 500x500 size to assegurate they are processed from vtex.

## [2.4.3] 2019-02-19 (Walmart)
### Fixed
- Google map wasn't loading map with stores.

## [1.3.1] 2019-02-19 (Changomas)
### Fixed
- "Mis pedidos" section can be opened after checkout is done via web site.

## [1.5.0] 2019-02-19 (Tata)
### Added
- FB SDK support.

## [2.4.2] 2019-02-19 (Walmart)
### Changed
- Using Votic's Substance Search API for products search feature.

## [2.4.1] 2019-02-19 (Walmart)
### Fixed
- Ignore promotion priority if there's more than 1 hyphen.

## [1.3.0] 2018-01-24 (Changomas)
## [2.4.0] 2018-01-24 (Walmart)
### Added
- Home screen Our brands carousel

## [1.2.0] 2018-01-24 (Changomas)
## [2.3.0] 2018-01-24 (Walmart)
### Added
- Home screen footer banners slider

## [2.2.8] 2019-01-09 (Walmart)
## [1.1.7] 2019-01-09 (Changomas)
## [1.4.7] 2019-01-09 (Tata)
### Changed
- Linked the home carousels to environment gitID

## [2.2.7] 2018-12-17 (Walmart)
## [1.1.6] 2018-12-17 (Changomas)
## [1.4.6] 2018-12-17 (Tata)
### Fixed
- Hotfix for crash when building Google Api Client twice
- Get promotions when searching products only if promotions are enabled
### Changed
- Set UI for Sales channels screen after getting all needed data

## [2.2.6] 2018-12-14 (Walmart)
### Changed
- Refresh promotions when queried and outdated

## [2.2.5] 2018-12-14 (Walmart)
### Fixed
- Search overlay not shown when lens icon clicked

## [2.2.4] 2018-12-14 (Walmart)
### Fixed
- Shopping lists bugs with products quantity and products fetching

## [1.4.5] 2018-12-12 (Tata)
### Changed
- Change Contact URL

## [2.2.3] 2018-12-12 (Walmart)
## [1.1.5] 2018-12-12 (Changomas)
## [1.4.4] 2018-12-12 (Tata)
### Fixed
- Crash on apps with disabled promotions

## [2.2.2] 2018-12-10 (Walmart)
### Fixed
- HotFix for promotion refresh bug

## [2.2.1] 2018-12-10 (Walmart)
### Changed
- Change promotion refresh interval to 10 minutes

## [1.1.4] 2018-12-5 (Changomas)
### Fixed
- Crash when repeating order

## [2.1.6] 2018-12-5 (Walmart)
### Fixed
- Crash when parsing promotion label priority

## [2.1.5] 2018-12-4 (Walmart)
### Changed
- Text from first slide in Cart synchronization summary screen

## [2.1.4] 2018-11-26 (Walmart)
## [1.1.3] 2018-11-26 (Changomas)
## [1.4.3] 2018-11-26 (Tata)
### Fixed
- Grid list does not show price highlights

## [2.1.3] 2018-11-26 (Walmart)
## [1.1.2] 2018-11-26 (Changomas)
## [1.4.2] 2018-11-26 (Tata)
### Fixed
- Grid list shows incorrect number when product was added to cart

## [2.1.2] 2018-11-23 (Walmart)
## [1.1.1] 2018-11-23 (Changomas)
## [1.4.1] 2018-11-23 (Tata)
### Fixed
- Crash when clicking "Scan" button after scanned product code not found

## [2.1.1] 2018-11-22 (Walmart)
### Fixed
- Show dots icon after product image is loaded in grid item

## [1.4.0] 2018-11-22 (Tata)
### Changed
- Clicking on the product image on the More Options screen will take you to the Product Detail screen

## [2.1.0] 2018-11-22 (Walmart)
## [1.1.0] 2018-11-22 (Changomas)
## [1.3.0] 2018-11-22 (Tata)
### Changed
- Minimum cart value is now obtained trough endpoint (Error or zero means no minimum value)

## [1.2.0] 2018-11-22 (Tata)
### Changed
- Updated Product Detail design according to visual feedback

## [1.1.0] 2018-11-22 (Tata)
### Changed
- No favorites result background
- lowered not found message
- added a new line to not found message

## [2.0.6] 2018-11-22 (Walmart)
## [1.0.6] 2018-11-22 (Changomas)
## [1.0.2] 2018-11-22 (Tata)
### Changed
- Search results limited to 100 pages

## [2.0.5] 2018-11-22 (Walmart)
## [1.0.5] 2018-11-22 (Changomas)
## [1.0.1] 2018-11-22 (TATA)
### Fixed
- Restored minicart background to intended color

## [2.0.4] 2018-11-21 (Walmart)
### Fixed
- Expired JWT token for shopping lists endpoints

## [2.0.3] 2018-11-21 (Walmart)
### Fixed
- Search bar bug

## [1.0.3] 2018-11-06 (Changomas)
### Updated
- Contact Form link

## [1.9.0] - 2017-10-09
### Added
- Google Analytics tracking

### Fixed
- Null pointer in Product Page
- Null pointer in Search

## [1.8.0] - 2017-08-22
### Added
- "Added to cart" notification in product list.
- Push notification store filter
- Fast add now increases quantity by 1 each click.

### Changed
- WMT Deals & Best selling collections

## [1.7.1] - 2017-08-17
### Added
- Fix product page layout: Set it to visibility gone

## [1.7.0] - 2017-08-14
### Added
- Price per measurement unit to every product. AKA Legal price
- Proguard: Allow source file & line number


### Fixed
- Null pointer in Product Page.


## [1.6.0] - 2017-08-07
### Added
- WMT #2745: Navigate to home with toolbar logo.
- Null  check in Stores & Product onScrollChanged
- Changelog

### Fixed
- Null pointer exception in SalesChannelSelector
- Navigation drawer bug

### Changed
- WMT #2745: Launcher icons

## [1.5.2] - 2017-07-06

### Security
- All APIs now use HTTPS


## [1.5.1] - 2017-06-26

### Added
- Support for low resolution devices in Favourites Fragment

### Removed
- Vtex Id oAuth providers (Google, FB)


## [1.5.0] - 2017-06-26

### Added
- Support for lower resolutions in multiple fragments.
- Retry action in order page & list
- Order Page: Show order price changes

### Security
- SSL Error handling in baseWebViewFragment
- WebView SSL Alert

### Fixed
- Restore minicart
- Product notification


## [1.4.0] - 2017-06-21

### Added
- Rejected camera permission message

### Changed
- Order Page: Reorder improved

### Fixed
- Cart.getUrl bug
- Nullpointer exception in Favourites & Stores fragments
- Memory footprint greatly reduced


## [1.3.1] - 2017-06-19

### Changed
- WMT: Bestselling with highlighted products

### Fixed
- getVtexCategories
- Repeated category breadcrumb
- Home slides when changing SC